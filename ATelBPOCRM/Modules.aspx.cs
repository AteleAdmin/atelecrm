﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class Modules : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateModule();
            }
        }

        public void PopulateModule()
        {
            var ds = new Lucky.General.PopulateDataSource();
            var dt = ds.DataTableSqlString("SELECT * FROM dbo.Module");
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                var m = new Module();
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Module WHERE Name='" + txtName.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Module Already Exists');", true);
                }
                else
                {
                    m.Name = txtName.Text.Trim();
                    m.PageIcon = txtPageIcon.Text.Trim();
                    m.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text.Trim());
                    m.PageUrl = txtPageUrl.Text.Contains("java") ? txtPageUrl.Text.Trim() : txtPageUrl.Text.Trim();
                    m.CanView = false;
                    m.IsActive = true;
                    m.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); //DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    m.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); //DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    if (m.InsertModule(m) > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Module Saved Successfully.');", true);
                        PopulateModule();
                        Clear();
                        txtModuleId.Text = "";
                        btnSave.Text = "Save";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Module Cannot be Saved.');", true);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void Clear()
        {
            txtName.Text = string.Empty;
            txtPageIcon.Text = string.Empty;
            txtDisplayOrder.Text = "0";
            txtPageUrl.Text = string.Empty;
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                if (hfId != null)
                {
                    txtModuleId.Text = hfId.Value;
                    var lblName = (Literal)GridView1.Rows[rowIndex].FindControl("ltName");
                    var lblPageIcon = (Literal)GridView1.Rows[rowIndex].FindControl("ltPageIcon");
                    var lblDisplayOrder = (Literal)GridView1.Rows[rowIndex].FindControl("ltDisplayOrder");
                    var lblPageUrl = (Literal)GridView1.Rows[rowIndex].FindControl("ltPageUrl");

                    txtName.Text = lblName.Text;
                    txtPageIcon.Text = lblPageIcon.Text;
                    txtDisplayOrder.Text = lblDisplayOrder.Text;
                    txtPageUrl.Text = lblPageUrl.Text;
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var message = "";
                var m = new Module();
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var active = GridView1.Rows[rowIndex].FindControl("chkActive") as CheckBox;

                    if (active != null && active.Checked == true)
                    {
                        m.IsActive = false;
                        message = "Blocked";
                    }
                    else
                    {
                        m.IsActive = true;
                        message = "Active";
                    }

                    m.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    if (hfId != null) m.Id = Convert.ToInt32(hfId.Value);
                }

                if (m.BlockModule(m) > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>Module " + message + " Successfully');", true);
                    PopulateModule();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                var m = new Module();
                if (!string.IsNullOrEmpty(txtModuleId.Text.Trim()))
                {
                    m.Name = txtName.Text.Trim();
                    m.PageIcon = txtPageIcon.Text.Trim();
                    m.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text.Trim());
                    m.PageUrl = txtPageUrl.Text.Contains("java") ? txtPageUrl.Text.Trim() : txtPageUrl.Text.Trim();
                    m.CanView = false;
                    m.IsActive = true;
                    m.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); //DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    m.Id = Convert.ToInt32(txtModuleId.Text.Trim());
                    if (m.UpdateModule(m) > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Module Updated Successfully.');", true);
                        PopulateModule();
                        Clear();
                        txtModuleId.Text = "";

                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Module Cannot be Updated.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Update Error.');", true);
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}