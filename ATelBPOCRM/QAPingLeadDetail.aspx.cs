﻿using Lucky.General;
using System;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;

namespace ATelBPOCRM
{
    public partial class QAPingLeadDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["leadId"] != null)
                {
                    BindLeads(Convert.ToInt32(Request.QueryString["leadId"]));
                    txtLeadId.Text = Request.QueryString["leadId"].ToString();
                }
            }
            else
            {
                //
            }
        }

        private void BindLeads(int? leadId)
        {
            try
            {
                var sb = new StringBuilder();
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT i.QAComments,i.CallStatus,i.Id,i.FirstName+' '+i.LastName AS Name,i.Address,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.Make,i.Year,i.Model,i.DOB,i.Email,i.Gender,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip,i.Comments,i.FollowUpXDate,d.Name AS Disposition,ao.Name AS AgencyOwner,ap.Name AS AgencyProducer, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource = 1 THEN 'Outsource' ELSE 'ATel' END) AS Owner FROM dbo.PingNPost i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId= i.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.Id=" + leadId);
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("<table class='table table-bordered table-striped'>");

                    sb.Append("<tr>");
                    sb.Append("<th>Name</th>");
                    sb.Append("<td>" + dt.Rows[0]["Name"].ToString() + "</td>");
                    sb.Append("<th>City</th>");
                    sb.Append("<td>" + dt.Rows[0]["City"].ToString() + "</td>");
                    sb.Append("<th>State</th>");
                    sb.Append("<td>" + dt.Rows[0]["State"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Zip</th>");
                    sb.Append("<td>" + dt.Rows[0]["Zip"].ToString() + "</td>");
                    sb.Append("<th>Phone</th>");
                    sb.Append("<td>" + dt.Rows[0]["PhoneNo"].ToString() + "</td>");
                    sb.Append("<th>Company</th>");
                    sb.Append("<td>" + dt.Rows[0]["Company"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Make</th>");
                    sb.Append("<td>" + dt.Rows[0]["Make"].ToString() + "</td>");
                    sb.Append("<th>Year</th>");
                    sb.Append("<td>" + dt.Rows[0]["Year"].ToString() + "</td>");
                    sb.Append("<th>Model</th>");
                    sb.Append("<td>" + dt.Rows[0]["Model"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Date of Birth</th>");
                    sb.Append("<td>" + dt.Rows[0]["DOB"].ToString() + "</td>");
                    sb.Append("<th>Gender</th>");
                    sb.Append("<td>" + dt.Rows[0]["Gender"].ToString() + "</td>");
                    sb.Append("<th>Homeowner</th>");
                    sb.Append("<td>" + dt.Rows[0]["HOwnerShip"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Disposition</th>");
                    sb.Append("<td>" + dt.Rows[0]["Disposition"].ToString() + "</td>");
                    sb.Append("<th>Producer</th>");
                    sb.Append("<td>" + dt.Rows[0]["AgencyProducer"].ToString() + "</td>");
                    sb.Append("<th>Email</th>");
                    sb.Append("<td>" + dt.Rows[0]["Email"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Address</th>");
                    sb.Append("<td colspan='5'>" + dt.Rows[0]["Address"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Comments</th>");
                    sb.Append("<td colspan='5'>" + dt.Rows[0]["Comments"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Call Status</th>");
                    sb.Append("<td colspan='2'><span class='label label-info' style='padding: 3px 20px;border: 4px;'>" + dt.Rows[0]["CallStatus"].ToString() + "</span></td>");
                    sb.Append("<th>Owner</th>");
                    sb.Append("<td colspan='2'><span class='label label-default' style='padding: 3px 20px;border: 4px;'>" + dt.Rows[0]["Owner"].ToString() + "</span></td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>QA Comments</th>");
                    sb.Append("<td colspan='5'>" + dt.Rows[0]["QAComments"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("</table>");

                    ltLead.Text = sb.ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtLeadId.Text))
                {
                    if (!string.IsNullOrEmpty(txtQAComment.Text))
                    {
                        var cmd = new SqlCommand("UPDATE PingNPost SET QAComments=@QAComments, CallStatus=@CallStatus WHERE Id=@Id", Connection.Db());
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@CallStatus", ddlLeadStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@QAComments", txtQAComment.Text);
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Request.QueryString["leadId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Updated Successfully.');", true);
                            BindLeads(Convert.ToInt32(Request.QueryString["leadId"]));
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Updated.');", true);
                    }
                    else
                    {
                        var cmd = new SqlCommand("UPDATE PingNPost SET CallStatus=@CallStatus WHERE Id=@Id", Connection.Db());
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@CallStatus", ddlLeadStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Request.QueryString["leadId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Updated Successfully.');", true);
                            BindLeads(Convert.ToInt32(Request.QueryString["leadId"]));
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Updated.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}