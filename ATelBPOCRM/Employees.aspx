﻿<%@ Page Title="Employees" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="ATelBPOCRM.Employees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Employees</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>System Settings</a></li>
            <li class="active">Employee List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <div class="form-group">
                        <label>Select Role :</label>
                        <asp:DropDownList ID="ddlRole" AutoPostBack="True" OnSelectedIndexChanged="ddlRole_OnSelectedIndexChanged" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <asp:Panel ID="pnlAddUser" runat="server">
                    <div class="pull-right">
                        <a href="AddEmployee.aspx" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;Add New Employee</a>
                    </div>
                </asp:Panel>
            </div>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Employee List</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowDataBound="GridView1_OnRowDataBound" OnRowCreated="GridView1_OnRowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Serial #">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                            <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <a href="javascript:;" class="viewEmployee" data-request='<%# Eval("Id") %>'>
                                                <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Attendance Id" Visible="true">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAttendanceId" runat="server" Text='<%# Eval("AttendanceId") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ref Offer Letter" Visible="false">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltReferenceOfferLetter" runat="server" Text='<%# Eval("ReferenceOfferLetter") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Username" Visible="false">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltUName" runat="server" Text='<%# Eval("Username") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email" Visible="false">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Password" Visible="false">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPassword" runat="server" Text='<%# Eval("Password") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile No">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltMobileNo" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CNIC" Visible="false">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCNIC" runat="server" Text='<%# Eval("CNIC") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project" Visible="true">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltProject" runat="server" Text='<%# Eval("Project") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Role">
                                        <ItemTemplate>
                                            <asp:Label ID="ltRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reported To">
                                        <ItemTemplate>
                                            <asp:Label ID="ltReportedTo" runat="server" Text='<%# Eval("ReportedTo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active">
                                        <ItemTemplate>
                                            <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server" OnClick="btnEdit_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-pencil btn-sm label-success"></i></asp:LinkButton>
                                        |
                                        <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-toggle-on btn-sm label-info"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>

    <script type="text/javascript">
        $(function () {
            $('.dataTable').on('click', '.viewEmployee', function () {
                var empId = $(this).data('request');
                $.fancybox.open({
                    src: 'EmployeeDetail.aspx?empId=' + empId,
                    type: 'iframe'
                });
            });
        })
    </script>

    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 100%;
            margin: 0;
        }
    </style>
</asp:Content>

