﻿<%@ Page Title="Dispositions" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Dispositions.aspx.cs" Inherits="ATelBPOCRM.Dispositions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Dispositions</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Lead</a></li>
            <li class="active">Disposition List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Disposition</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label>Disposition</label>
                            <asp:TextBox ID="txtName" CssClass="form-control" placeholder="Disposition" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Scope Id</label>
                            <asp:TextBox ID="txtScopeId" CssClass="form-control" placeholder="Scope Id" TextMode="Number" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnSubmit" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat btn-sm" OnClick="btnSubmit_OnClick" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Disposition List</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowDataBound="GridView1_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Serial #">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <a href="javascript:;">
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scope Id">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltScopeId" runat="server" Text='<%# Eval("ScopeId") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkActive" Enabled="False" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("IsActive"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server" OnClick="btnEdit_OnClick" CssClass="simptip-position-top" data-tooltip="Edit Disposition" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-pencil btn-sm label-success"></i></asp:LinkButton>
                                        |
                                    <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_OnClick" CssClass="simptip-position-top" data-tooltip="Active / Block" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-refresh btn-sm label-info"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
