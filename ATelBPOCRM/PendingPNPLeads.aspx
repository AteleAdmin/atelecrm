﻿<%@ Page Title="Pending PNP Leads" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="PendingPNPLeads.aspx.cs" Inherits="ATelBPOCRM.PendingPNPLeads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Pending Leads</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Leads</a></li>
            <li class="active">Pending Leads</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnStats_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // col-md-12 -->
            <div class="col-md-12">
                <asp:Panel ID="pnlAgencyWise" runat="server" Visible="True">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agency Wise</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:GridView ID="gvAgencyStat" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvAgencyStat_OnRowDataBound" ShowFooter="True" FooterStyle="">
                                <Columns>
                                    <asp:TemplateField HeaderText="Lead Id">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblState" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rejected">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltRejected" runat="server" Text='<%# Eval("Rejected") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblRejected" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accepted">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAccepted" runat="server" Text='<%# Eval("Accepted") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblAccepted" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#00c0ef" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- / Total -->
                </asp:Panel>
                <!-- // col-md-12 -->
            </div>
            <!-- // col-md-12 -->
        </div>
        <!-- // row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Export Leads</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="gvAllLeads" CssClass="table table-bordered table-striped dataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowCreated="gvAllLeads_OnRowCreated" OnRowDataBound="gvAllLeads_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Lead Id">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <a href="UpdatePendingLead.aspx?LeadId=<%# Eval("Id") %>">
                                            <asp:Literal ID="ltN" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="State">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Zip">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                        <asp:HiddenField ID="hfPhoneNo" runat="server" Value='<%# Eval("PhoneNo") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Company">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Year">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Model">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agency">
                                    <ItemTemplate>
                                        <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                        <asp:HiddenField ID="hfAgEmail" runat="server" Value='<%# Eval("AgencyMail") %>' />
                                        <asp:HiddenField ID="hfAgToken" runat="server" Value='<%# Eval("Token") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Producer">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Homeowner">
                                    <ItemTemplate>
                                        <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Disposition">
                                    <ItemTemplate>
                                        <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent">
                                    <ItemTemplate>
                                        <asp:Label ID="ltAgent" runat="server" Text='<%# Eval("Agent") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product">
                                    <ItemTemplate>
                                        <asp:Label ID="ltProduct" runat="server" Text='<%# Eval("IsDemandPNP") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time">
                                    <ItemTemplate>
                                        <asp:Label ID="ltTime" runat="server" Text='<%# Eval("TimeStamp") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="ltAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Owner">
                                    <ItemTemplate>
                                        <asp:Label ID="ltOwner" runat="server" Text='<%# Eval("LeadOwner") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:Panel ID="pnlAction" runat="server">
                                            <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-trash btn-sm label-danger"></i></asp:LinkButton>
                                            |
                                        <asp:LinkButton ID="btnMove" runat="server" OnClick="btnMove_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-location-arrow btn-sm label-success"></i></asp:LinkButton>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <Lucky:DataTableControl ID="DataTable" runat="server" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.display').prepend($("<thead></thead>").append($('.display').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                "paging": false,
                "scrollX": true,
                buttons: [
                    'csv', 'excel'
                ]
            });
        });
    </script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>
</asp:Content>
