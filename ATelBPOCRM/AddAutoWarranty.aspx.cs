﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class AddAutoWarranty : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string InsertLead(string firstName, string lastName, string title, string phone, string address1, string address2, string address3, string postalCode, string state, string province, string city, string comments)
        {
            var message = "";
            using (var cmd = new SqlCommand("INSERT INTO AutoWarranty(FirstName, LastName, Title, Phone, Address1, Address2, Address3, State, Province, City, PostalCode, Comments, IsActive, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)VALUES(@FirstName, @LastName, @Title, @Phone, @Address1, @Address2, @Address3, @State, @Province, @City, @PostalCode, @Comments, @IsActive, @CreatedBy, @CreatedDate, @LastModifiedBy, @LastModifiedDate)"))
            {
                cmd.Parameters.AddWithValue("@FirstName", firstName);
                cmd.Parameters.AddWithValue("@LastName", lastName);
                cmd.Parameters.AddWithValue("@Title", title);
                cmd.Parameters.AddWithValue("@Phone", phone);
                cmd.Parameters.AddWithValue("@Address1", address1);
                cmd.Parameters.AddWithValue("@Address2", address2);
                cmd.Parameters.AddWithValue("@Address3", address3);
                cmd.Parameters.AddWithValue("@State", state);
                cmd.Parameters.AddWithValue("@Province", province);
                cmd.Parameters.AddWithValue("@City", city);
                cmd.Parameters.AddWithValue("@PostalCode", postalCode);
                cmd.Parameters.AddWithValue("@Comments", comments);
                cmd.Parameters.AddWithValue("@IsActive", 1);
                cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(HttpContext.Current.Session["UId"]));
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(HttpContext.Current.Session["UId"]));
                cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                cmd.Connection = Connection.Db();
                if (cmd.ExecuteNonQuery() > 0)
                    message = "Lead Added Successfully";
                else
                    message = "Lead Cannot be Added";
            }

            return message;
        }
    }
}