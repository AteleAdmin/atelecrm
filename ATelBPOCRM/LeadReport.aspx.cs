﻿using Lucky.General;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class LeadReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGenerate_OnClick(object sender, EventArgs e)
        {

        }

        #region Live Leads
        private void LoadLive()
        {
            if (Session["Role"] != null && (Session["Role"].ToString() == "Super Admin" || Session["Role"].ToString() == "Agency Owner"))
            {
                if (Session["UId"] != null)
                {
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                        PopulateLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        PopulateLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulateLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulateProducerCount(Convert.ToInt32(Session["UId"].ToString()), "", "");

                        //pnlAgencyProducer.Visible = true;
                        //pnlAcceptedRejected.Visible = true;
                        //pnlTotalCounter.Visible = true;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" &&
                        Session["Role"].ToString() != "Agency Owner")
                    {
                        PopulateLeadsSummary(0, 0, "", "");
                        PopulateLeadsCount(0, 0, "", "");

                        //pnlDateRange.Visible = true;
                        //pnlAcceptedRejected.Visible = true;
                        //pnlTotalCounter.Visible = true;
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa",
                        "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
                Response.Redirect("Dashboard.aspx");
        }

        public void PopulateProducerCount(int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvAgencyProducerTotal.DataSource = dt;
                    //gvAgencyProducerTotal.DataBind();
                }
                else
                {
                    //gvAgencyProducerTotal.EmptyDataText = "No Record Found!";
                    //gvAgencyProducerTotal.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvAgencyProducerRejected.DataSource = dt;
                    //gvAgencyProducerRejected.DataBind();
                }
                else
                {
                    //gvAgencyProducerRejected.EmptyDataText = "No Record Found!";
                    //gvAgencyProducerRejected.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvAgencyProducerAccepted.DataSource = dt;
                    //gvAgencyProducerAccepted.DataBind();
                }
                else
                {
                    //gvAgencyProducerAccepted.DataSource = dt;
                    //gvAgencyProducerAccepted.EmptyDataText = "No Record Found!";
                    //gvAgencyProducerAccepted.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

        }

        public void PopulateLeadsCount(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read()) { }
                    //ltTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read()) { }
                    //ltRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read()) { }
                    //ltAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateLeadsSummary(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvRejected.DataSource = dt;
                    //gvRejected.DataBind();
                }
                else
                {
                    //gvRejected.EmptyDataText = "No Record Found!";
                    //gvRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvAccepted.DataSource = dt;
                    //gvAccepted.DataBind();
                }
                else
                {
                    //gvAccepted.EmptyDataText = "No Record Found!";
                    //gvAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
        #endregion

        #region PNP Leads
        public void PopulatePNPLeadsCount(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read()) { }
                    //ltPNPTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read()) { }
                    //ltPNPRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read()) { }
                    //ltPNPAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulatePNPLeadsSummary(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvPNPRejected.DataSource = dt;
                    //gvPNPRejected.DataBind();
                }
                else
                {
                    //gvPNPRejected.EmptyDataText = "No Record Found!";
                    //gvPNPRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvPNPAccepted.DataSource = dt;
                    //gvPNPAccepted.DataBind();
                }
                else
                {
                    //gvPNPAccepted.EmptyDataText = "No Record Found!";
                    //gvPNPAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
        public void PopulatePNPProducerCount(int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvPNPAgencyProducerTotal.DataSource = dt;
                    //gvPNPAgencyProducerTotal.DataBind();
                }
                else
                {
                    //gvPNPAgencyProducerTotal.EmptyDataText = "No Record Found!";
                    //gvPNPAgencyProducerTotal.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvPNPAgencyProducerRejected.DataSource = dt;
                    //gvPNPAgencyProducerRejected.DataBind();
                }
                else
                {
                    //gvPNPAgencyProducerRejected.EmptyDataText = "No Record Found!";
                    //gvPNPAgencyProducerRejected.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13,23) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13,23) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    //gvPNPAgencyProducerAccepted.DataSource = dt;
                    //gvPNPAgencyProducerAccepted.DataBind();
                }
                else
                {
                    //gvPNPAgencyProducerAccepted.EmptyDataText = "No Record Found!";
                    //gvPNPAgencyProducerAccepted.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

        }
        #endregion
    }
}