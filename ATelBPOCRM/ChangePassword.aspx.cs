﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;

namespace ATelBPOCRM
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT Id FROM dbo.AgencyOwner WHERE Password='" + txtOldPassword.Text.Trim() + "' AND (Username='" + Session["UN"].ToString() + "' OR Email='" + Session["Email"].ToString() + "')");
                    if (dt.Rows.Count > 0)
                    {
                        if (Session["UN"] != null && Session["Email"] != null)
                        {
                            var cmd = new SqlCommand("UPDATE dbo.AgencyOwner SET Password=@Password WHERE (Username=@Username OR Email=@Email) AND Id=@Id");
                            cmd.Parameters.Clear();
                            cmd.Connection = Connection.Db();
                            cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                            cmd.Parameters.AddWithValue("@Username", Session["UN"].ToString());
                            cmd.Parameters.AddWithValue("@Email", Session["Email"].ToString());
                            cmd.Parameters.AddWithValue("@Id", Session["UId"].ToString());
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Password Changed Successfully');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Invalid Password. Try again');", true);
                    }
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT Id FROM dbo.AgencyProducer WHERE Password='" + txtOldPassword.Text.Trim() + "' AND (Username='" + Session["UN"].ToString() + "' OR Email='" + Session["Email"].ToString() + "')");
                    if (dt.Rows.Count > 0)
                    {
                        if (Session["UN"] != null && Session["Email"] != null)
                        {
                            var cmd = new SqlCommand("UPDATE dbo.AgencyProducer SET Password=@Password WHERE (Username=@Username OR Email=@Email) AND Id=@Id");
                            cmd.Parameters.Clear();
                            cmd.Connection = Connection.Db();
                            cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                            cmd.Parameters.AddWithValue("@Username", Session["UN"].ToString());
                            cmd.Parameters.AddWithValue("@Email", Session["Email"].ToString());
                            cmd.Parameters.AddWithValue("@Id", Session["UId"].ToString());
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Password Changed Successfully');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Invalid Password. Try again');", true);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}