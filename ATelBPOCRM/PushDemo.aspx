﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PushDemo.aspx.cs" Inherits="ATelBPOCRM.PushDemo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%;">
            Token:
            <asp:TextBox ID="txtDeviceToken" Text="CD41EE1E7B738C1DD99407610EEE7DAECCDB4ABA5CA0C06888C16B20FBD7F09F" runat="server"></asp:TextBox>
            <br />
            Message:
            <asp:TextBox ID="txtMessage" TextMode="MultiLine" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="btnSendNotification" runat="server" Text="Send Notification" OnClick="btnSendNotification_Click" />
            <br />
            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
        </div>
        <hr />
        <div style="width: 100%;">
            Token:
            <asp:TextBox ID="txtAToken" Text="dGkF0hgqNmE:APA91bF09PdYahXyh6wfLihNoZ9vCy-8JfGhsdafCiZ1rZCZiX7Atnqvq53Qs0507uAo-iUCGoueRaxPUeUCSxHcJz09eEg2uFs7wMO5HXFbAkWFK5GP5vjhYCE8X7hd_-XBNPbcmiFp" runat="server"></asp:TextBox>
            <br />
            Message:
            <asp:TextBox ID="txtAMessage" TextMode="MultiLine" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="btnSend" runat="server" Text="Send Notification" OnClick="btnSend_OnClick" />
            <br />
            <asp:Label ID="lblAStatus" runat="server" Text=""></asp:Label>
        </div>
        <hr />
        <div style="width: 100%;">
            Token:
            <asp:TextBox ID="txtHttp2DeviceToken" Text="dGkF0hgqNmE:APA91bF09PdYahXyh6wfLihNoZ9vCy-8JfGhsdafCiZ1rZCZiX7Atnqvq53Qs0507uAo-iUCGoueRaxPUeUCSxHcJz09eEg2uFs7wMO5HXFbAkWFK5GP5vjhYCE8X7hd_-XBNPbcmiFp" runat="server"></asp:TextBox>
            <br />
            Message:
            <asp:TextBox ID="txtHttp2Message" TextMode="MultiLine" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="btnHttp2Submit" runat="server" Text="Send Http2 Notification" OnClick="btnHttp2Submit_OnClick" />
            <br />
            <asp:Label ID="lblHttp2Msg" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
