﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Lucky.General;

namespace ATelBPOCRM
{
    /// <summary>
    /// Summary description for LeadAcceptance
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class LeadAcceptance : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod(EnableSession = true)]
        public bool CheckAcceptance(bool accept)
        {
            var isPaused = false;
            try
            {
                var cmd = new SqlCommand("UPDATE dbo.AgencyOwner SET IsAccept=@IsAccept WHERE IsActive =1 AND Id= @Id", Connection.Db());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@IsAccept", accept);
                if (Session["UId"] != null)
                {
                    cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Session["UId"]));
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        isPaused = true;
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

            return isPaused;
        }
    }
}
