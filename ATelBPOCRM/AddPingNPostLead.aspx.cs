﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;

namespace ATelBPOCRM
{
    public partial class AddPingNPostLead : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                {
                    PopulateDdlDisposition();
                    PopulateProducers();

                    if (Request.QueryString["msg"] != null && Request.QueryString["phone"] != null)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Saved Successfully');", true);
                        txtPhone.Text = Request.QueryString["phone"].ToString();
                        txtPhone.ReadOnly = true;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }

                    if (Request.QueryString["LeadId"] != null)
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT i.Id,i.FirstName,i.LastName,i.ViciLeadId,i.Gender,i.City,i.State,i.Zip,i.PhoneNo,i.Address,i.Comments,i.DOB,i.Company,i.HOwnerShip,i.Make,i.Year,i.Model,i.Email,ao.Name AS AgencyOwner,ap.Name AS AgencyProducer,d.Name AS Disposition FROM dbo.PingNPost i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.Id=" + Convert.ToInt32(Request.QueryString["LeadId"]));
                        if (dt.Rows.Count > 0)
                        {

                            txtPhone.Text = dt.Rows[0]["PhoneNo"].ToString();

                            txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                            txtLName.Text = dt.Rows[0]["LastName"].ToString();
                            txtCity.Text = dt.Rows[0]["City"].ToString();
                            txtState.Text = dt.Rows[0]["State"].ToString();
                            txtZip.Text = dt.Rows[0]["Zip"].ToString();
                            txtPhoneNo.Text = dt.Rows[0]["PhoneNo"].ToString();
                            txtCompany.Text = dt.Rows[0]["Company"].ToString();
                            txtMake.Text = dt.Rows[0]["Make"].ToString();
                            txtYear.Text = dt.Rows[0]["Year"].ToString();
                            txtModel.Text = dt.Rows[0]["Model"].ToString();
                            txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                            txtEmail.Text = dt.Rows[0]["Email"].ToString();
                            txtAddress.Text = dt.Rows[0]["Address"].ToString();
                            txtComments.Text = dt.Rows[0]["Comments"].ToString();

                            txtViciLeadId.Text = "";
                            txtViciLeadId.Text = dt.Rows[0]["ViciLeadId"].ToString();
                            txtViciLeadId.ReadOnly = true;

                            ddlHOwnerShip.ClearSelection();
                            ddlHOwnerShip.SelectedIndex = ddlHOwnerShip.Items.IndexOf(ddlHOwnerShip.Items.FindByText(dt.Rows[0]["HOwnerShip"].ToString()));
                            ddlHOwnerShip.DataBind();

                            ddlGender.ClearSelection();
                            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                            ddlGender.DataBind();

                            ddlDisposition.ClearSelection();
                            ddlDisposition.SelectedIndex = ddlDisposition.Items.IndexOf(ddlDisposition.Items.FindByText(dt.Rows[0]["Disposition"].ToString()));
                            ddlDisposition.DataBind();

                            ddlAgencyProducer.SelectedIndex = ddlAgencyProducer.Items.IndexOf(ddlAgencyProducer.Items.FindByText(dt.Rows[0]["AgencyProducer"].ToString()));
                            ddlAgencyProducer.DataBind();

                            txtLeadId.Text = "";
                            txtLeadId.Text = Request.QueryString["LeadId"];
                            txtLeadId.ReadOnly = true;

                            txtState.Enabled = false;

                            Panel1.Visible = true;
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["LeadId"] != null)
                {
                    var cmd = new SqlCommand("Update PingNPost SET FirstName=@FirstName, LastName=@LastName, City=@City, State=@State, Zip=@Zip, PhoneNo=@PhoneNo, Company=@Company, Make=@Make, Year=@Year, Model=@Model, DOB=@DOB, Email=@Email, Address=@Address, Comments=@Comments, HOwnerShip=@HOwnerShip, FollowUpXDate=@FollowUpXDate, DispositionId=@DispositionId, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate, AgencyProducerId=@AgencyProducerId WHERE Id=@Id AND ViciLeadId=@ViciLeadId");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                    cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                    cmd.Parameters.AddWithValue("@State", txtState.Text.Trim());
                    cmd.Parameters.AddWithValue("@Zip", txtZip.Text.Trim());
                    cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Company", txtCompany.Text.Trim());
                    cmd.Parameters.AddWithValue("@Make", txtMake.Text.Trim());
                    cmd.Parameters.AddWithValue("@Year", txtYear.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model", txtModel.Text.Trim());
                    cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                    cmd.Parameters.AddWithValue("@HOwnerShip", ddlHOwnerShip.SelectedValue);
                    cmd.Parameters.AddWithValue("@FollowUpXDate", !string.IsNullOrWhiteSpace(txtFollowDate.Text) ? txtFollowDate.Text : "");
                    cmd.Parameters.AddWithValue("@DispositionId", Convert.ToInt32(ddlDisposition.SelectedValue));
                    cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text));
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@AgencyProducerId", Convert.ToInt32(ddlAgencyProducer.SelectedValue));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Request.QueryString["LeadId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Updated Successfully.');", true);
                            Clear();
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Update.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void PopulateProducers()
        {
            try
            {
                var query = "";
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var d = new PopulateDataSource();
                    var t = d.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (t.Rows.Count > 0)
                    {
                        query = "SELECT Id,Name FROM dbo.AgencyProducer WHERE IsActive =1 AND AgencyOwnerId=" + Convert.ToInt32(t.Rows[0]["Id"].ToString());
                    }
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    query = "SELECT Id,Name FROM dbo.AgencyProducer WHERE IsActive =1 AND AgencyOwnerId=" + Convert.ToInt32(Session["UId"]);
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    ddlAgencyProducer.DataSource = dt;
                    ddlAgencyProducer.DataTextField = "Name";
                    ddlAgencyProducer.DataValueField = "Id";
                    ddlAgencyProducer.DataBind();
                }
                else
                {
                    ddlAgencyProducer.DataSource = dt;
                    ddlAgencyProducer.DataTextField = "Name";
                    ddlAgencyProducer.DataValueField = "Id";
                    ddlAgencyProducer.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void PopulateDdlDisposition()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id !=23");
                if (dt.Rows.Count > 0)
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();

                    //ddlAgencyOwner.Items.Insert(0, "Select Agency Owner");
                }
                else
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void Clear()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtPhoneNo.Text = "";
            txtCompany.Text = "";
            txtMake.Text = "";
            txtYear.Text = "";
            txtModel.Text = "";
            txtDOB.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtComments.Text = "";

            ddlDisposition.ClearSelection();

            txtLeadId.Text = "";
            txtViciLeadId.Text = "";
        }

        protected void ddlDisposition_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDisposition.SelectedItem.Text == "Follow-Up/X-Date")
                pnlFollow.Visible = true;
        }
    }
}