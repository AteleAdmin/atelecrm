﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Exception = System.Exception;

namespace ATelBPOCRM
{
    public partial class Dispositions : System.Web.UI.Page
    {
        public static int DId = 0;
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateDispositions();
            }
        }

        public void PopulateDispositions()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT * FROM dbo.Disposition");
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                if (hfId != null) DId = Convert.ToInt32(hfId.Value);
                var lblName = (Literal)GridView1.Rows[rowIndex].FindControl("ltName");
                var lblScope = (Literal)GridView1.Rows[rowIndex].FindControl("ltScopeId");

                txtName.Text = lblName.Text;
                txtScopeId.Text = lblScope.Text;
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var active = GridView1.Rows[rowIndex].FindControl("chkActive") as CheckBox;
                    if (hfId != null)
                    {
                        var query = "";
                        var message = "";
                        if (active != null && active.Checked == true)
                        {
                            query = "UPDATE dbo.Disposition SET LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate, IsActive=0 WHERE Id=@Id";
                            message = "Blocked";
                        }
                        else
                        {
                            query = "UPDATE dbo.Disposition SET LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate, IsActive=1 WHERE Id=@Id";
                            message = "Active";
                        }

                        var cmd = new SqlCommand(query, Connection.Db());
                        cmd.Parameters.Clear();
                        if (Session["UId"] != null)
                        {
                            cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(hfId.Value));
                            cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>Disposition " + message + " Successfully');", true);
                                PopulateDispositions();
                            }
                            else
                            {
                                //
                            }
                        }
                        else
                        {
                            //
                        }
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (DId > 0)
                {
                    if (Utility.ChkDataTableDuplicate("SELECT * FROM dbo.Disposition WHERE Name='" + txtName.Text.Trim() + "'") > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Disposition Already Exists');", true);
                    }
                    else
                    {
                        var cmd = new SqlCommand();
                        cmd.Parameters.Clear();
                        cmd.Connection = Connection.Db();
                        cmd.CommandText = "Update dbo.Disposition SET Name=@Name, IsActive=@IsActive, ScopeId=@ScopeId, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                        cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        cmd.Parameters.AddWithValue("@ScopeId", txtScopeId.Text.Trim());
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", DId);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Dispostion Updated Successfully.');", true);
                            PopulateDispositions();
                            txtName.Text = "";
                            txtScopeId.Text = "";
                            DId = 0;
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Dispostion Cannot be Updated.');", true);
                    }
                }
                else
                {
                    if (Utility.ChkDataTableDuplicate("SELECT * FROM dbo.Disposition WHERE Name='" + txtName.Text.Trim() + "'") > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Disposition Already Exists');", true);
                    }
                    else
                    {
                        var cmd = new SqlCommand();
                        cmd.Parameters.Clear();
                        cmd.Connection = Connection.Db();
                        cmd.CommandText = "INSERT INTO dbo.Disposition(Name, IsActive, ScopeId, CreatedBy, CreatedDate)VALUES(@Name, @IsActive, @ScopeId, @CreatedBy, @CreatedDate)";
                        cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        cmd.Parameters.AddWithValue("@ScopeId", txtScopeId.Text.Trim());
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Dispostion Saved Successfully.');", true);
                            PopulateDispositions();
                            txtName.Text = "";
                            DId = 0;
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Dispostion Cannot be Saved.');", true);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}