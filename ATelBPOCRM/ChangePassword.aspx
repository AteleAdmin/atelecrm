﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="ATelBPOCRM.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Account</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label>Old Password</label> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtOldPassword" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtOldPassword" TextMode="Password" placeholder="Old Password" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>New Password</label> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtPassword" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtPassword" TextMode="Password" placeholder="New Password" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <label>Confirm Password</label> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtCPassword" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtCPassword" TextMode="Password" placeholder="Confirm Password" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCPassword" ControlToCompare="txtPassword" ForeColor="Red" ErrorMessage="Password doesn't match."></asp:CompareValidator>
                        <hr/>
                        <div class="form-group">
                            <asp:Button ID="btnSubmit" CssClass="btn btn-success" runat="server" Text="Change Password" OnClick="btnSubmit_OnClick" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
