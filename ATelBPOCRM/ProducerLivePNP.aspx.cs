﻿using Lucky.General;
using System;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Exception = System.Exception;

namespace ATelBPOCRM
{
    public partial class ProducerLivePNP : System.Web.UI.Page
    {
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
        //        {

        //        }
        //        else
        //        {
        //            if (Session["RoleId"] != null)
        //            {
        //                var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
        //                if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
        //                {
        //                    //
        //                }
        //                else
        //                    Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PingLoad();
            }
        }

        #region Live Transfers

        private void LoadLive()
        {
            if (Session["Role"] != null && (Session["Role"].ToString() == "Super Admin" || Session["Role"].ToString() == "Agency Producer"))
            {
                if (Session["UId"] != null)
                {
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        PopulateLead(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        PopulateLead(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                    {
                        PopulateLead(0, 0, "", "");

                        pnlDateRange.Visible = true;
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa",
                        "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
                Response.Redirect("Dashboard.aspx");
        }

        protected void btnLive_OnClick(object sender, EventArgs e)
        {
            LoadLive();
            MultiView1.ActiveViewIndex = 2;
        }

        public void PopulateLead(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAllLeads.DataSource = dt;
                    gvAllLeads.DataBind();

                    gvAllLeads.UseAccessibleHeader = true;
                    gvAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnStats_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    //i.CreatedDate,101)
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLead(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLead(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLead(0, 0, from, to);
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void gvRejected_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvAccepted_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                //var flag = false;
                //e.Row.Cells[7].Visible = false;
                //e.Row.Cells[8].Visible = false;
                //e.Row.Cells[9].Visible = false;
                //e.Row.Cells[10].Visible = false;
                //e.Row.Cells[12].Visible = false;

                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                var flag = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                e.Row.Cells[12].Visible = false;

                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
            }
            else
            {
                //
            }
        }

        protected void gvAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
            }
            else
            {
                //
            }
        }

        protected void btnQuickSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var query = "";
                //if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                //{
                //    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                //    {
                //        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                //    }
                //    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                //    {
                //        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                //    }
                //    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                //    {
                //        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                //    }
                //    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                //    {
                //        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                //    }
                //    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                //    {
                //        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                //    }
                //    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                //    {
                //        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                //    }
                //    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                //    {
                //        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                //    }
                //}

                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) ORDER BY i.Id ASC";
                    }
                }

                var cmd = new SqlCommand(query, Connection.Db());
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    gvAllLeads.DataSource = dr;
                    gvAllLeads.DataBind();

                    gvAllLeads.UseAccessibleHeader = true;
                    gvAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

        }
        #endregion

        #region Pulse
        protected void btnPNP_OnClick(object sender, EventArgs e)
        {
            PNPLoad();
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnGetLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var fdate = DateTime.Parse(txtPNPFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtPNPTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        PopulatePNPLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), from, to);
                    }
                    else
                    {
                        gvPNPAllLeads.EmptyDataText = "No Record Found!";
                        gvPNPAllLeads.DataBind();
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void PNPLoad()
        {
            if (Session["UId"] != null)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        PopulatePNPLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), "", "");
                    }
                    else
                    {
                        gvPNPAllLeads.EmptyDataText = "No Record Found!";
                        gvPNPAllLeads.DataBind();
                    }
                }
                else
                {
                    //
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
            }
        }

        public void PopulatePNPLead(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAllLeads.DataSource = dt;
                    gvPNPAllLeads.DataBind();

                    gvPNPAllLeads.UseAccessibleHeader = true;
                    gvPNPAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvPNPAllLeads.EmptyDataText = "No Record Found!";
                    gvPNPAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void gvPNPAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;

                #region DisableEditPreviousLeads
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;
                #region DisableEditPreviousLead
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
            {
                //e.Row.Cells[20].Visible = true;
            }
        }

        protected void gvPNPAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
                if (disposition != null && (disposition.Text.Contains("Hot Lead")))
                {
                    disposition.CssClass = "label label-info";
                }
            }
            else
            {
                //
            }
        }

        protected void btnPNPQuickSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var query = "";
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) ORDER BY i.Id ASC";
                    }
                    if (ddlPNPFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) ORDER BY i.Id ASC";
                    }
                }

                var cmd = new SqlCommand(query, Connection.Db());
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    gvPNPAllLeads.DataSource = dr;
                    gvPNPAllLeads.DataBind();

                    gvPNPAllLeads.UseAccessibleHeader = true;
                    gvPNPAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvPNPAllLeads.EmptyDataText = "No Record Found!";
                    gvPNPAllLeads.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
        #endregion

        #region Ping Leads
        protected void btnPing_OnClick(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnPingQuickSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var query = "";
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    if (ddlPingFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPingFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPingFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPingFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPingFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPingFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlPingFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                }

                var cmd = new SqlCommand(query, Connection.Db());
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    gvPingAllLeads.DataSource = dr;
                    gvPingAllLeads.DataBind();

                    gvPingAllLeads.UseAccessibleHeader = true;
                    gvPingAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvPingAllLeads.EmptyDataText = "No Record Found!";
                    gvPingAllLeads.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnGetPingLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var fdate = DateTime.Parse(txtPingFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtPingTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        PopulatePingLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), from, to);
                    }
                    else
                    {
                        gvPingAllLeads.EmptyDataText = "No Record Found!";
                        gvPingAllLeads.DataBind();
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void PingLoad()
        {
            if (Session["UId"] != null)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        PopulatePingLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), "", "");
                    }
                    else
                    {
                        gvPingAllLeads.EmptyDataText = "No Record Found!";
                        gvPingAllLeads.DataBind();
                    }
                }
                else
                {
                    //
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
            }
        }

        public void PopulatePingLead(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPingAllLeads.DataSource = dt;
                    gvPingAllLeads.DataBind();

                    gvPingAllLeads.UseAccessibleHeader = true;
                    gvPingAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvPingAllLeads.EmptyDataText = "No Record Found!";
                    gvPingAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void gvPingAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;

                #region DisableEditPreviousLeads
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;
                #region DisableEditPreviousLead
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
            {
                //e.Row.Cells[20].Visible = true;
            }
        }

        protected void gvPingAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
                if (disposition != null && (disposition.Text.Contains("Hot Lead")))
                {
                    disposition.CssClass = "label label-info";
                }
            }
            else
            {
                //
            }
        }
        #endregion
    }
}