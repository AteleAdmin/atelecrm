﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class BonusSheet : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null)
                {
                    CalculateBonus("", "");
                }
            }
            else
            {
                //
            }
        }

        private void CalculateBonus(string from, string to)
        {
            try
            {
                var query = "";
                if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = @"SELECT Live.Name,ISNULL(Live,0) AS Live,ISNULL(PNP.PNP,0) AS PNP,(ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) AS Total,ISNULL((CASE WHEN (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) >=5 AND (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) <10 THEN (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5)*50 WHEN (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) >=10 THEN (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) *100 END),0) AS Bonus FROM (SELECT u.Name,COUNT(i.Id) AS Live FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id  WHERE i.DispositionId NOT IN (0,1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY u.Name) AS Live LEFT JOIN (SELECT u.Name,COUNT(i.Id) AS PNP FROM dbo.PingNPost i LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.DispositionId NOT IN (0,1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY u.Name) AS PNP ON PNP.Name = Live.Name";
                    //@"SELECT Live.Name,ISNULL(Live,0) AS Live,ISNULL(PNP.PNP,0) AS PNP,ISNULL(Live,0)+ISNULL(PNP.PNP,0)AS Total FROM (SELECT u.Name,(CASE WHEN COUNT(i.Id)>=5 AND COUNT(i.Id) < 10 THEN COUNT(i.Id) * 50 WHEN COUNT(i.Id) >=10 THEN COUNT(i.Id) * 100 END) AS Live FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id  WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '" + to + "' AND i.IsOutsource=0 GROUP BY u.Name ) AS Live LEFT JOIN (SELECT u.Name,(CASE WHEN COUNT(i.Id)>=5 THEN COUNT(i.Id) * 50 END) AS PNP FROM dbo.PingNPost i LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '" + to + "' GROUP BY u.Name) AS PNP ON PNP.Name = Live.Name";
                }
                if (string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = @"SELECT Live.Name,ISNULL(Live,0) AS Live,ISNULL(PNP.PNP,0) AS PNP,(ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) AS Total,ISNULL((CASE WHEN (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) >=5 AND (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) <10 THEN (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5)*50 WHEN (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) >=10 THEN (ISNULL(Live,0)+ISNULL(PNP.PNP,0) * 0.5) *100 END),0) AS Bonus FROM (SELECT u.Name,COUNT(i.Id) AS Live FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id  WHERE i.DispositionId NOT IN (0,1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY u.Name) AS Live LEFT JOIN (SELECT u.Name,COUNT(i.Id) AS PNP FROM dbo.PingNPost i LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.DispositionId NOT IN (0,1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY u.Name) AS PNP ON PNP.Name = Live.Name";
                    //@"SELECT Live.Name,ISNULL(Live,0) AS Live,ISNULL(PNP.PNP,0) AS PNP,ISNULL(Live,0)+ISNULL(PNP.PNP,0)AS Total FROM (SELECT u.Name,(CASE WHEN COUNT(i.Id)>=5 AND COUNT(i.Id) < 10 THEN COUNT(i.Id) * 50 WHEN COUNT(i.Id) >=10 THEN COUNT(i.Id) * 100 END) AS Live FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id  WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY u.Name) AS Live LEFT JOIN (SELECT u.Name,(CASE WHEN COUNT(i.Id)>=5 THEN COUNT(i.Id) * 50 END) AS PNP FROM dbo.PingNPost i LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY u.Name) AS PNP ON PNP.Name = Live.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvBonus.DataSource = dt;
                    gvBonus.DataBind();

                    gvBonus.UseAccessibleHeader = true;
                    gvBonus.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvBonus.EmptyDataText = "No Record Found!";
                    gvBonus.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnGetBonus_OnClick(object sender, EventArgs e)
        {
            if (Session["Role"] != null)
            {
                DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                var from = fdate.ToString("yyyyMMdd");

                DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                var to = tdate.ToString("yyyyMMdd");

                CalculateBonus(from, to);
            }
        }
    }
}