﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class ViewClients : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateAgencyOwner(0);
                PopulateAccountManager();
            }
        }

        private void PopulateAccountManager()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.AgencyOwner WHERE IsActive=1 AND RoleId=17 ORDER BY Name ASC");
                if (dt.Rows.Count > 0)
                {
                    ddlAccountManager.DataSource = dt;
                    ddlAccountManager.DataTextField = "Name";
                    ddlAccountManager.DataValueField = "Id";
                    ddlAccountManager.DataBind();

                    ddlAccountManager.Items.Insert(0,"Select Account Manager");
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateAgencyOwner(int? id)
        {
            try
            {
                var query = "";
                if (id == 0)
                    query = "SELECT am.Name AS Manager,ao.Id,ao.Name,ao.Username,ao.Email,ao.Password,ao.ContactNo,ao.Gender,ao.Address,ao.Company,ao.IsActive,ao.State FROM dbo.AgencyOwner ao LEFT JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id WHERE ao.RoleId=12 ORDER BY ao.Id ASC";
                if (id > 0)
                    query = "SELECT am.Name AS Manager,ao.Id,ao.Name,ao.Username,ao.Email,ao.Password,ao.ContactNo,ao.Gender,ao.Address,ao.Company,ao.IsActive,ao.State FROM dbo.AgencyOwner ao LEFT JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id WHERE ao.RoleId=12 AND ao.ReportedTo=" + id + " ORDER BY ao.Id ASC";
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    if (hfId != null)
                    {
                        var cmd = new SqlCommand();
                        cmd.Parameters.Clear();
                        cmd.Connection = Connection.Db();
                        cmd.CommandText = "UPDATE dbo.AgencyOwner SET IsActive=0 WHERE Id=" + Convert.ToInt32(hfId.Value);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Agency Block Successfully.');", true);
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Agency Cannot be Blocked.');", true);
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    if (hfId != null)
                    {
                        var cmd = new SqlCommand();
                        cmd.Parameters.Clear();
                        cmd.Connection = Connection.Db();
                        cmd.CommandText = "UPDATE dbo.AgencyOwner SET IsActive=1 WHERE Id=" + Convert.ToInt32(hfId.Value);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Agency Active Successfully.');", true);
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Agency Cannot be Active.');", true);
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void ddlAccountManager_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlAccountManager.SelectedIndex >0)
                PopulateAgencyOwner(Convert.ToInt32(ddlAccountManager.SelectedValue));
            if(ddlAccountManager.SelectedIndex == 0)
                PopulateAgencyOwner(0);
        }
    }
}