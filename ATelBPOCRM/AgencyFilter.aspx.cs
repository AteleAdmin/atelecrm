﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class AgencyFilter : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateState();
            }
            else
            {
                //
            }
        }

        private void PopulateState()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT DISTINCT(State) AS Name FROM dbo.AgencyOwner");
                if (dt.Rows.Count > 0)
                {
                    ddlState.DataSource = dt;
                    ddlState.DataTextField = "Name";
                    ddlState.DataValueField = "Name";
                    ddlState.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void ddlState_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.AgencyOwner WHERE State='" + ddlState.SelectedValue + "'");
                if (dt.Rows.Count > 0)
                {
                    ddlAgency.DataSource = dt;
                    ddlAgency.DataTextField = "Name";
                    ddlAgency.DataValueField = "Id";
                    ddlAgency.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name,Username,Email,Password,ContactNo,(CASE WHEN IsActive=1 THEN 'True' ELSE 'False' END) AS IsActive FROM dbo.AgencyProducer WHERE AgencyOwnerId=" + Convert.ToInt32(ddlAgency.SelectedValue) + "");
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }
    }
}