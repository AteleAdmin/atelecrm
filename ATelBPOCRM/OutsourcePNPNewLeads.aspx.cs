﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class OutsourcePNPNewLeads : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && (string)Session["Role"] == "Outsource")
                {
                    PopulateLeadCount(Convert.ToInt32(Session["UId"].ToString()), "", "");
                    PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), "", "");
                    PopulateLead(Convert.ToInt32(Session["UId"].ToString()), "", "");

                    PopulateAgencyCount(Convert.ToInt32(Session["UId"].ToString()), "", "");
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    PopulateLeadCount(0, "", "");
                    PopulateLeadsSummary(0, "", "");
                    PopulateLead(0, "", "");

                    PopulateAgencyCount(0, "", "");

                    pnlOwner.Visible = true;
                }

                PopulateOutsourceOwner();
            }
            else
            {
                //
            }
        }

        private void PopulateOutsourceOwner()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.OutsourceOwner WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlOutsourceOwner.DataSource = dt;
                    ddlOutsourceOwner.DataTextField = "Name";
                    ddlOutsourceOwner.DataValueField = "Id";
                    ddlOutsourceOwner.DataBind();

                    ddlOutsourceOwner.Items.Insert(0, "Select Owner");
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateLeadCount(int? outsourceOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (outsourceOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1";
                }
                if (outsourceOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1";
                }
                if (outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltTotalLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

            //Rejected
            try
            {
                var query = "";
                if (outsourceOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1";
                }
                if (outsourceOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1";
                }
                if (outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltRejectedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

            //Accepted
            try
            {
                var query = "";
                if (outsourceOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1";
                }
                if (outsourceOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1";
                }
                if (outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnStats_OnClick(object sender, EventArgs e)
        {
            if (Session["Role"] != null && (string)Session["Role"] == "Outsource")
            {
                DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                var from = fdate.ToString("yyyyMMdd");

                DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                var to = tdate.ToString("yyyyMMdd");


                PopulateLeadCount(Convert.ToInt32(Session["UId"].ToString()), from, to);
                PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), from, to);
                PopulateLead(Convert.ToInt32(Session["UId"].ToString()), from, to);
                PopulateAgencyCount(Convert.ToInt32(Session["UId"].ToString()), from, to);

                //PopulateLeadCount(Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
                //PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
                //PopulateLead(Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
                //PopulateAgencyCount(Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
            {
                if (ddlOutsourceOwner.SelectedIndex > 0)
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");


                    PopulateLeadCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), from, to);
                    PopulateLeadsSummary(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), from, to);
                    PopulateLead(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), from, to);
                    PopulateAgencyCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), from, to);


                    //PopulateLeadCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLeadsSummary(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLead(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateAgencyCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
                else
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");


                    PopulateLeadCount(0, from, to);
                    PopulateLeadsSummary(0, from, to);
                    PopulateLead(0, from, to);
                    PopulateAgencyCount(0, from, to);


                    //PopulateLeadCount(0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLeadsSummary(0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLead(0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateAgencyCount(0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
            }
        }

        public void PopulateLeadsSummary(int? outsourceOwnerId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (outsourceOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 GROUP BY d.Name";
                }
                if (outsourceOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 GROUP BY d.Name";
                }
                if (outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY d.Name";
                }

                if (outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvRejected.DataSource = dt;
                    gvRejected.DataBind();
                }
                else
                {
                    gvRejected.EmptyDataText = "No Record Found!";
                    gvRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (outsourceOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 GROUP BY d.Name";
                }
                if (outsourceOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 GROUP BY d.Name";
                }
                if (outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY d.Name";
                }

                if (outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAccepted.DataSource = dt;
                    gvAccepted.DataBind();
                }
                else
                {
                    gvAccepted.EmptyDataText = "No Record Found!";
                    gvAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulateLead(int? outsourceOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (outsourceOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 ORDER BY i.Id ASC"; ;
                }
                if (outsourceOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 ORDER BY i.Id ASC"; ;
                }

                if (outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 ORDER BY i.Id ASC";
                }
                if (outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAllLeads.DataSource = dt;
                    gvAllLeads.DataBind();

                    gvAllLeads.UseAccessibleHeader = true;
                    gvAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void gvRejected_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvAccepted_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                var flag = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                e.Row.Cells[12].Visible = false;

                if (flag == false)
                {
                    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                    {
                        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                        var d = DateTime.Parse(to) - DateTime.Parse(from);
                        var days = d.TotalDays;
                        if (days >= 5)
                        {
                            e.Row.Cells[1].Visible = false;
                            e.Row.Cells[2].Visible = true;
                        }
                        else
                        {
                            e.Row.Cells[1].Visible = true;
                            e.Row.Cells[2].Visible = false;
                        }

                        flag = true;
                    }
                    else
                    {
                        e.Row.Cells[2].Visible = false;
                    }
                }
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                var flag = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                e.Row.Cells[12].Visible = false;

                if (flag == false)
                {
                    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                    {
                        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                        var d = DateTime.Parse(to) - DateTime.Parse(from);
                        var days = d.TotalDays;
                        if (days >= 5)
                        {
                            e.Row.Cells[1].Visible = false;
                            e.Row.Cells[2].Visible = true;
                        }
                        else
                        {
                            e.Row.Cells[1].Visible = true;
                            e.Row.Cells[2].Visible = false;
                        }

                        flag = true;
                    }
                    else
                    {
                        e.Row.Cells[2].Visible = false;
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void gvAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
            }
            else
            {
                //
            }
        }

        protected void ddlOutsourceOwner_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlOutsourceOwner.SelectedIndex > 0)
                {
                    PopulateLeadCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), "", "");
                    PopulateLeadsSummary(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), "", "");
                    PopulateLead(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), "", "");
                    PopulateAgencyCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), "", "");

                    //PopulateLeadCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLeadsSummary(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLead(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateAgencyCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
                else
                {
                    PopulateLeadCount(0, "", "");
                    PopulateLeadsSummary(0, "", "");
                    PopulateLead(0, "", "");
                    PopulateAgencyCount(0, "", "");
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulateAgencyCount(int? outsourceOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (outsourceOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PingNPost i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 GROUP BY ao.Name, ao.State";
                }
                if (outsourceOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PingNPost i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 GROUP BY ao.Name, ao.State";
                }
                if (outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PingNPost i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY ao.Name, ao.State";
                }
                if (outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PingNPost i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY ao.Name, ao.State";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyStat.DataSource = dt;
                    gvAgencyStat.DataBind();
                }
                else
                {
                    gvAgencyStat.EmptyDataText = "No Record Found!";
                    gvAgencyStat.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        private int _rejected = 0;
        private int _accepted = 0;
        private int _total = 0;
        protected void gvAgencyStat_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    _rejected += Convert.ToInt32(((Literal)e.Row.FindControl("ltRejected")).Text);
                    _accepted += Convert.ToInt32(((Literal)e.Row.FindControl("ltAccepted")).Text);
                    _total += Convert.ToInt32(((Literal)e.Row.FindControl("ltTotal")).Text);
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    // (e.Row.FindControl("lblAmount") as Label).Text = amount.ToString();
                    //e.Row.Cells[7].Text = String.Format("{0:0}", "<b>" + Total + "</b>");
                    //gvAgencyStat.FooterRow.Cells[0].Text = "Total";

                    var head = (Label)e.Row.FindControl("lblHead");
                    var saleRejected = (Label)e.Row.FindControl("lblRejected");
                    var saleAccepted = (Label)e.Row.FindControl("lblAccepted");
                    var saleTotal = (Label)e.Row.FindControl("lblTotal");

                    head.Text = "Total";
                    saleRejected.Text = _rejected.ToString();
                    saleAccepted.Text = _accepted.ToString();
                    saleTotal.Text = _total.ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}