﻿<%@ Page Title="Maverick Leads" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="ProducerLivePNP.aspx.cs" Inherits="ATelBPOCRM.ProducerLivePNP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li <% if (MultiView1.ActiveViewIndex == 0) Response.Write("class=\"active\""); %> />
                        <asp:LinkButton ID="btnPing" runat="server" OnClick="btnPing_OnClick"><img src="backend/img/Ping-1.png" style="height: 30px;" />&nbsp;Ping</asp:LinkButton>
                        <li <% if (MultiView1.ActiveViewIndex == 1) Response.Write("class=\"active\""); %> />
                        <asp:LinkButton ID="btnPNP" runat="server" OnClick="btnPNP_OnClick"><img src="backend/img/Pulse-1.png" style="height: 30px;" />&nbsp;Pulse</asp:LinkButton>
                        <li <% if (MultiView1.ActiveViewIndex == 2) Response.Write("class=\"active\""); %> />
                        <asp:LinkButton ID="btnLive" runat="server" OnClick="btnLive_OnClick"><img src="backend/img/Live-1.png" style="height: 30px;" />&nbsp;Live</asp:LinkButton>
                    </ul>
                    <div class="tab-content">
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Date Range</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Select Option</label>
                                                        <asp:DropDownList ID="ddlPingFilter" CssClass="form-control" runat="server">
                                                            <asp:ListItem>Yesterday</asp:ListItem>
                                                            <asp:ListItem>Last 7 Days + Today</asp:ListItem>
                                                            <asp:ListItem>Last 30 Days + Today</asp:ListItem>
                                                            <asp:ListItem>Last 30 Days</asp:ListItem>
                                                            <asp:ListItem>This Month</asp:ListItem>
                                                            <asp:ListItem>Last Month</asp:ListItem>
                                                            <asp:ListItem>This Year</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" style="margin-top: 23px;">
                                                        <asp:Button ID="btnPingQuickSubmit" runat="server" Text="Search Leads" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnPingQuickSubmit_OnClick" CausesValidation="False" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>From :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ForeColor="Red" ControlToValidate="txtPNPFrom" ValidationGroup="pnpDateRange"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtPingFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>To :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ForeColor="Red" ControlToValidate="txtPNPTo" ValidationGroup="pnpDateRange"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtPingTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" style="margin-top: 23px;">
                                                        <asp:Button ID="btnGetPingLead" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnGetPingLead_OnClick" ValidationGroup="pnpDateRange" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- //col-md-12 -->
                                </div>
                                <!-- // row -->
                                <!-- row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Export Leads</h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="gvPingAllLeads" CssClass="table table-bordered table-striped pingDataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowCreated="gvPingAllLeads_OnRowCreated" OnRowDataBound="gvPingAllLeads_OnRowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Lead Id">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex+1 %>
                                                                    <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <a href="UpdatePNPLead.aspx?LeadId=<%# Eval("Id") %>">
                                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltN" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="City">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="State">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Zip">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Phone">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Email">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Company">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Year">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Model">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Agency">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Producer">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DOB">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Homeowner">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Disposition">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Active" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Agent">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltAgent" runat="server" Text='<%# Eval("Agent") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Product">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltProduct" runat="server" Text='<%# Eval("IsDemandPNP") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Time">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltTime" runat="server" Text='<%# Eval("TimeStamp") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Address">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                                <!-- // row -->
                            </asp:View>
                            <asp:View runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Date Range</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Select Option</label>
                                                        <asp:DropDownList ID="ddlPNPFilter" CssClass="form-control" runat="server">
                                                            <asp:ListItem>Yesterday</asp:ListItem>
                                                            <asp:ListItem>Last 7 Days + Today</asp:ListItem>
                                                            <asp:ListItem>Last 30 Days + Today</asp:ListItem>
                                                            <asp:ListItem>Last 30 Days</asp:ListItem>
                                                            <asp:ListItem>This Month</asp:ListItem>
                                                            <asp:ListItem>Last Month</asp:ListItem>
                                                            <asp:ListItem>This Year</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" style="margin-top: 23px;">
                                                        <asp:Button ID="btnPNPQuickSubmit" runat="server" Text="Search Leads" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnPNPQuickSubmit_OnClick" CausesValidation="False" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>From :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ForeColor="Red" ControlToValidate="txtPNPFrom" ValidationGroup="pnpDateRange"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtPNPFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>To :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ForeColor="Red" ControlToValidate="txtPNPTo" ValidationGroup="pnpDateRange"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtPNPTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" style="margin-top: 23px;">
                                                        <asp:Button ID="btnGetLead" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnGetLead_OnClick" ValidationGroup="pnpDateRange" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- //col-md-12 -->
                                </div>
                                <!-- // row -->
                                <!-- row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Export Leads</h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="gvPNPAllLeads" CssClass="table table-bordered table-striped pnpDataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowCreated="gvPNPAllLeads_OnRowCreated" OnRowDataBound="gvPNPAllLeads_OnRowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Lead Id">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex+1 %>
                                                                    <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <a href="UpdatePNPLead.aspx?LeadId=<%# Eval("Id") %>">
                                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltN" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="City">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="State">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Zip">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Phone">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Email">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Company">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Year">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Model">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Agency">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Producer">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DOB">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Homeowner">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Disposition">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Active" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Agent">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltAgent" runat="server" Text='<%# Eval("Agent") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Product">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltProduct" runat="server" Text='<%# Eval("IsDemandPNP") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Time">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltTime" runat="server" Text='<%# Eval("TimeStamp") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Address">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                                <!-- // row -->
                            </asp:View>
                            <asp:View runat="server">
                                <asp:Panel ID="pnlDateRange" runat="server" Visible="True">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Date Range</h3>
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Select Option</label>
                                                            <asp:DropDownList ID="ddlFilter" CssClass="form-control" runat="server">
                                                                <asp:ListItem>Yesterday</asp:ListItem>
                                                                <asp:ListItem>Last 7 Days + Today</asp:ListItem>
                                                                <asp:ListItem>Last 30 Days + Today</asp:ListItem>
                                                                <asp:ListItem>Last 30 Days</asp:ListItem>
                                                                <asp:ListItem>This Month</asp:ListItem>
                                                                <asp:ListItem>Last Month</asp:ListItem>
                                                                <asp:ListItem>This Year</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group" style="margin-top: 23px;">
                                                            <asp:Button ID="btnQuickSubmit" runat="server" Text="Search Leads" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnQuickSubmit_OnClick" CausesValidation="False" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>From :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ForeColor="Red" ControlToValidate="txtFrom" ValidationGroup="dateRange"></asp:RequiredFieldValidator>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>To :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ForeColor="Red" ControlToValidate="txtTo" ValidationGroup="dateRange"></asp:RequiredFieldValidator>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group" style="margin-top: 23px;">
                                                            <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnStats_OnClick" ValidationGroup="dateRange" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <!--// col-md-12 -->

                                <!-- col-md-12 -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Export Leads</h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="gvAllLeads" CssClass="table table-bordered table-striped liveDataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowCreated="gvAllLeads_OnRowCreated" OnRowDataBound="gvAllLeads_OnRowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Lead Id">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex+1 %>
                                                                    <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <a href="UpdateLead.aspx?LeadId=<%# Eval("Id") %>&ping=false">
                                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltN" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="City">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="State">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Zip">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Phone">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Email">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Company">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Year">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Model">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Agency">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Transfer To">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DOB">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Homeowner">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Disposition">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Active" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Address">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <!-- // col-md-12 All Leads -->
                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <Lucky:DataTableControl ID="DataTable" runat="server" />
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.liveDataTable').prepend($("<thead></thead>").append($('.liveDataTable').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                'paging': true,
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering": false,
                "scrollX": true,
                'info': true,
                buttons: [
                    'csv', 'excel', 'pageLength'
                ]
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.pnpDataTable').prepend($("<thead></thead>").append($('.pnpDataTable').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                'paging': true,
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering": false,
                "scrollX": true,
                'info': true,
                buttons: [
                    'csv', 'excel', 'pageLength'
                ]
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.pingDataTable').prepend($("<thead></thead>").append($('.pingDataTable').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                'paging': true,
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering": false,
                "scrollX": true,
                'info': true,
                buttons: [
                    'csv', 'excel', 'pageLength'
                ]
            });
        });
    </script>
</asp:Content>

