﻿<%@ Page Title="Producer Report" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ProducerReport.aspx.cs" Inherits="ATelBPOCRM.ProducerReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Producer Report</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Reports</a></li>
            <li class="active">Producer Report</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'dd/mm/yyyy'"></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'dd/mm/yyyy'"></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnStats_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // col-md-12 -->

            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Disposition Summary</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:ListView ID="ListViewCounts" RepeatDirection="Horizontal" runat="server">
                            <ItemTemplate>
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3><%# Eval("Total") %></h3>

                                            <p><%# Eval("Disposition") %></p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
            <!-- // col-md-12 -->

            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Disposition Wise Details</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-body table-responsive no-padding">
                            <asp:GridView ID="GridViewTotalCount" CssClass="table table-hover" BorderColor="#cccccc" runat="server" AutoGenerateColumns="False" CellPadding="3" AllowSorting="True" BackColor="White" BorderStyle="None" BorderWidth="1px" GridLines="Vertical">
                                <Columns>
                                    <asp:BoundField DataField="Producer" HeaderText="Producer" SortExpression="Producer"></asp:BoundField>
                                    <asp:BoundField DataField="Disposition" HeaderText="Disposition" SortExpression="Disposition"></asp:BoundField>
                                    <asp:BoundField DataField="Total" HeaderText="Total" ReadOnly="True" SortExpression="Total"></asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>
                                <HeaderStyle BackColor="#1593AD" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <PagerStyle HorizontalAlign="Center" BackColor="#BFEAF3" ForeColor="Black"></PagerStyle>
                                <RowStyle BackColor="#BFEAF3" ForeColor="Black"></RowStyle>
                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White"></SelectedRowStyle>
                                <SortedAscendingCellStyle BackColor="#c0c0c0"></SortedAscendingCellStyle>
                                <SortedAscendingHeaderStyle BackColor="#006600"></SortedAscendingHeaderStyle>
                                <SortedDescendingCellStyle BackColor="#dee3f3"></SortedDescendingCellStyle>
                                <SortedDescendingHeaderStyle BackColor="#006600"></SortedDescendingHeaderStyle>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- // col-md-12 -->

            <!-- col-md-12 -->
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Latest Transfers </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <asp:GridView CssClass="table table-hover" BorderColor="#cccccc" ID="GridViewSS" runat="server" AutoGenerateColumns="False" CellPadding="3" AllowSorting="True" BackColor="White" BorderStyle="None" BorderWidth="1px" GridLines="Vertical">
                            <Columns>
                                <asp:TemplateField HeaderText="Serial #">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("ID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ViciLead_ID" HeaderText="ID" SortExpression="ViciLead_ID"></asp:BoundField>
                                <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName"></asp:BoundField>
                                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName"></asp:BoundField>
                                <asp:BoundField DataField="State" HeaderText="ST" SortExpression="State"></asp:BoundField>
                                <asp:BoundField DataField="ZipCode" HeaderText="Zip" SortExpression="ZipCode"></asp:BoundField>
                                <asp:BoundField DataField="Phone" HeaderText="Phone" ReadOnly="True" SortExpression="Phone"></asp:BoundField>
                                <asp:BoundField DataField="Verifier" HeaderText="TransferedTo" SortExpression="Verifier"></asp:BoundField>
                                <asp:BoundField DataField="Agency" HeaderText="Agency" SortExpression="Agency"></asp:BoundField>
                                <asp:BoundField DataField="Disposition" HeaderText="Disposition" SortExpression="Disposition" ItemStyle-Font-Size="X-Small"></asp:BoundField>
                                <%--    <asp:BoundField DataField="DispositionDate" HeaderText="Date" SortExpression="DispositionDate" ItemStyle-Font-Size="XX-Small"></asp:BoundField>--%>
                                <%--  <asp:CommandField ShowSelectButton="True" HeaderText="Action" SelectText="View/Update">
                                            <ItemStyle Font-Bold="true" ForeColor="Black" />
                                        </asp:CommandField>--%>
                                <asp:TemplateField HeaderText="View/Update">
                                    <ItemTemplate>
                                        <asp:Button ID="Button1" runat="server" RowIndex='<%# Container.DisplayIndex %>' Text="View/Update" CssClass="btn btn-primary btn-xs" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>
                            <HeaderStyle BackColor="#1593AD" Font-Bold="True" ForeColor="White"></HeaderStyle>
                            <PagerStyle HorizontalAlign="Center" BackColor="#BFEAF3" ForeColor="Black"></PagerStyle>
                            <RowStyle BackColor="#BFEAF3" ForeColor="Black"></RowStyle>
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White"></SelectedRowStyle>
                            <SortedAscendingCellStyle BackColor="#c0c0c0"></SortedAscendingCellStyle>
                            <SortedAscendingHeaderStyle BackColor="#006600"></SortedAscendingHeaderStyle>
                            <SortedDescendingCellStyle BackColor="#dee3f3"></SortedDescendingCellStyle>
                            <SortedDescendingHeaderStyle BackColor="#006600"></SortedDescendingHeaderStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <!-- // col-md-12 -->

        </div>
        <!-- // row -->
    </section>
    <!-- // section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy'
        });
    </script>
</asp:Content>
