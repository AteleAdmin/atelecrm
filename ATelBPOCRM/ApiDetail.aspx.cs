﻿using Lucky.General;
using System;
using System.Text;

namespace ATelBPOCRM
{
    public partial class ApiDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["agencyId"] != null)
                {
                    PopulateList(Convert.ToInt32(Request.QueryString["agencyId"]));
                }
            }
        }
        public void PopulateList(int? agencyId)
        {
            try
            {
                var query = "SELECT ca.AgencyId,ca.Id,ca.Client,ca.AuthType,ca.Resource,ca.Url,ca.EndPoint,ca.Username,ca.Password,ca.BearerToken,ca.Description,ca.RequestType,ca.ResponseType,ca.Verbose,ao.Name AS AgencyOwner, ao.Username AS AgencyUsername,ca.IsImplemented,ca.IsActive,CONVERT(VARCHAR(20), ao.CreatedDate,101) AS Date FROM ClientApi ca JOIN AgencyOwner ao ON ca.AgencyId=ao.Id WHERE ca.AgencyId=" + agencyId;
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    var sb = new StringBuilder();
                    sb.Append("");
                    sb.Append("<div style=\"padding: 10px; background: #eff1f1; font-weight: bold;\">Api Information</div>");
                    sb.Append("<table style=\"margin-top: 30px;\" class='table'>");

                    sb.Append("<tr>");
                    sb.Append("<th>Client Name</th>");
                    sb.Append("<td>" + dt.Rows[0]["Client"].ToString() + "</td>");
                    sb.Append("<th>Agency</th>");
                    sb.Append("<td>" + dt.Rows[0]["AgencyOwner"].ToString() + "</td>");
                    sb.Append("<th>Agency Username</th>");
                    sb.Append("<td>" + dt.Rows[0]["AgencyUsername"].ToString() + "</td>");
                    sb.Append("<th>Auth Type</th>");
                    sb.Append("<td>" + dt.Rows[0]["AuthType"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Username</th>");
                    sb.Append("<td>" + dt.Rows[0]["Username"].ToString() + "</td>");
                    sb.Append("<th>Pssword</th>");
                    sb.Append("<td>" + dt.Rows[0]["Password"].ToString() + "</td>");
                    sb.Append("<th>Request</th>");
                    sb.Append("<td>" + dt.Rows[0]["RequestType"].ToString() + "</td>");
                    sb.Append("<th>Response</th>");
                    sb.Append("<td>" + dt.Rows[0]["ResponseType"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Url</th>");
                    sb.Append("<td colspan='6'>" + dt.Rows[0]["Url"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Bearer Token</th>");
                    sb.Append("<td colspan='6'>" + dt.Rows[0]["BearerToken"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Verbose</th>");
                    sb.Append("<td>" + dt.Rows[0]["Verbose"].ToString() + "</td>");
                    sb.Append("<th>EndPoint</th>");
                    sb.Append("<td>" + dt.Rows[0]["EndPoint"].ToString() + "</td>");
                    sb.Append("<th>Resouce</th>");
                    sb.Append("<td colspan='2'>" + dt.Rows[0]["Resource"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Description</th>");
                    sb.Append("<td colspan='6'>" + dt.Rows[0]["Description"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Is Implemented</th>");
                    sb.Append("<td><label class='label-info'>" + dt.Rows[0]["IsImplemented"].ToString() + "</label></td>");
                    sb.Append("<th>Active</th>");
                    sb.Append("<td><label class='label-info'>" + dt.Rows[0]["IsActive"].ToString() + "</label></td>");
                    sb.Append("<th>Date</th>");
                    sb.Append("<td>" + dt.Rows[0]["Date"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("</table>");


                    Literal1.Text = sb.ToString();
                }
                else
                {
                    Literal1.Text = "No Record Found!";
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

    }
}