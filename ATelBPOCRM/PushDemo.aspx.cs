﻿using DbManager;
using System;
using System.Configuration;
using System.Web;

namespace ATelBPOCRM
{
    public partial class PushDemo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSendNotification_Click(object sender, EventArgs e)
        {
            if (txtDeviceToken.Text != "" && txtMessage.Text != "")
            {
                var filePath = HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.p12");
                var notify = new PushNotify();
                lblStatus.Text = notify.SendPushNotification(txtDeviceToken.Text.Trim(), txtMessage.Text, filePath);
            }
        }

        protected void btnSend_OnClick(object sender, EventArgs e)
        {
            //Maverick Leads - New lead available
            //New Lead
            if (txtAToken.Text != "" && txtAMessage.Text != "")
            {
                lblAStatus.Text = AndroidNotify.PushNotification(txtAToken.Text, "New Lead", "Maverick Leads - New lead available");
            }
        }

        protected void btnHttp2Submit_OnClick(object sender, EventArgs e)
        {
            if (txtHttp2DeviceToken.Text != "" && txtHttp2Message.Text != "")
            {
                var filePath = HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.p12");
                var notify = new IosPushNotify();
                var certPassword = ConfigurationManager.AppSettings["certPassword"].ToString();
                lblHttp2Msg.Text = notify.SendIosPushNotification(txtHttp2DeviceToken.Text.Trim(), txtHttp2Message.Text, filePath, certPassword);
            }
        }
    }
}
