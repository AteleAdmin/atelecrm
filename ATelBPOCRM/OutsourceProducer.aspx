﻿<%@ Page Title="Outsource Producer" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="OutsourceProducer.aspx.cs" Inherits="ATelBPOCRM.OutsourceProducer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Outsource Owner</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Outsource Panel</a></li>
            <li class="active">Outsource Producer</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <asp:Panel ID="pnlOwner" runat="server" Visible="False">
                        <label>Select Role :</label>
                        <asp:DropDownList ID="ddlOutSourceOwner" AutoPostBack="True" OnSelectedIndexChanged="ddlOutSourceOwner_OnSelectedIndexChanged" CssClass="form-control" runat="server"></asp:DropDownList>
                    </asp:Panel>
                </div>
                <div class="pull-right">
                    <label> </label>
                    <a href="javascript:;" class="btn btn-primary addProducer"><i class="fa fa-plus-circle"></i>&nbsp;Create New Producer</a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Outsource Producer List</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" runat="server" OnRowDataBound="GridView1_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Serial #">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <a href="javascript:;">
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Username">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltUName" runat="server" Text='<%# Eval("Username") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Gender">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltGender" runat="server" Text='<%# Eval("Gender") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Password">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPassword" runat="server" Text='<%# Eval("Password") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contact No">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltContact" runat="server" Text='<%# Eval("ContactNo") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date-of-Join">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltDOJ" runat="server" Text='<%# Eval("DOJ") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" ToolTip="Edit" runat="server" OnClientClick='<%# string.Format("return editProducer({0});", Eval("Id")) %>' RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-pencil btn-sm label-info"></i></asp:LinkButton>
                                        |
                                        <asp:LinkButton ID="btnUpdate" ToolTip="Update" runat="server" OnClientClick="return updateRecord(this, event);" OnClick="btnUpdate_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-refresh btn-sm label-success"></i></asp:LinkButton>
                                        |
                                        <asp:LinkButton ID="btnDelete" ToolTip="Block" runat="server" OnClientClick="return blockRecord(this, event);" OnClick="btnDelete_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-trash btn-sm label-danger"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $('.addProducer').on('click',
                function () {
                    $.fancybox.open({
                        src: 'AddOutsourceProducer.aspx',
                        type: 'iframe'
                    });
                });
        });
    </script>
    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 90%;
            margin: 0;
        }
    </style>
    <script type="text/javascript">
        function editProducer(id) {
            $.fancybox.open({
                src: 'AddOutsourceProducer.aspx?producerId=' + id,
                type: 'iframe'
            });
            return false;
        }
    </script>
    <script type="text/javascript">
        function blockRecord(ctl, event) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, do it!'
            }).then((result) => {
                if (result.value == true) {
                    //Swal.fire('Deleted!','Your file has been deleted.','success')
                    window.location.href = defaultAction;
            return true;
        }
        else
                    return false;
        });
        }
    </script>

    <script type="text/javascript">
        function updateRecord(ctl, event) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, do it!'
            }).then((result) => {
                if (result.value == true) {
                    //Swal.fire('Deleted!','Your file has been deleted.','success')
                    window.location.href = defaultAction;
            return true;
        }
        else
                    return false;
        });
        }
    </script>
</asp:Content>
