﻿using Lucky.General;
using System;
using System.Data.SqlClient;
using System.Web.UI;

namespace ATelBPOCRM
{
    public partial class UpdateClientLead : System.Web.UI.Page
    {
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
        //        {

        //        }
        //        else
        //        {
        //            if (Session["RoleId"] != null)
        //            {
        //                var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
        //                if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
        //                {
        //                    //
        //                }
        //                else
        //                    Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                {
                    pnlDisposition.Visible = false;
                    Panel1.Visible = false;
                    PopulateDdlDisposition();
                    if (Request.QueryString["msg"] != null && Request.QueryString["phone"] != null)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Saved Successfully');", true);
                        txtPhone.Text = Request.QueryString["phone"].ToString();
                        txtPhone.ReadOnly = true;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }
                    if (Request.QueryString["LeadId"] != null)
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT i.Id,i.FirstName,i.LastName,i.ViciLeadId,i.Gender,i.City,UPPER(i.State) AS State,i.Zip,i.PhoneNo,i.Address,i.Comments,i.DOB,i.Company,(CASE WHEN i.HOwnerShip='True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip,i.Make,i.Year,i.Model,i.Make2,i.Year2,i.Model2,i.Email,i.ContinousCoverage,(CASE WHEN i.MaritalStatus='True' THEN 'Yes' ELSE 'No' END) AS MaritalStatus,ao.Name AS AgencyOwner,ap.Name AS AgencyProducer,d.Name AS Disposition FROM dbo.Insurance i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.Id=" + Convert.ToInt32(Request.QueryString["LeadId"]));
                        if (dt.Rows.Count > 0)
                        {

                            txtPhone.Text = dt.Rows[0]["PhoneNo"].ToString();

                            txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                            txtLName.Text = dt.Rows[0]["LastName"].ToString();
                            txtCity.Text = dt.Rows[0]["City"].ToString();
                            txtState.Text = dt.Rows[0]["State"].ToString();
                            txtZip.Text = dt.Rows[0]["Zip"].ToString();
                            txtPhoneNo.Text = dt.Rows[0]["PhoneNo"].ToString();
                            txtCompany.Text = dt.Rows[0]["Company"].ToString();
                            //txtMake.Text = dt.Rows[0]["Make"].ToString();
                            txtYear.Text = dt.Rows[0]["Year"].ToString();
                            txtModel.Text = dt.Rows[0]["Model"].ToString();
                            //txtMakeV2.Text = dt.Rows[0]["Make2"].ToString();
                            txtYearV2.Text = dt.Rows[0]["Year2"].ToString();
                            txtModelV2.Text = dt.Rows[0]["Model2"].ToString();
                            txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                            txtEmail.Text = dt.Rows[0]["Email"].ToString();
                            txtAddress.Text = dt.Rows[0]["Address"].ToString();
                            txtComments.Text = dt.Rows[0]["Comments"].ToString();
                            txtProducer.Text = dt.Rows[0]["AgencyProducer"].ToString();
                            txtProducer.ReadOnly = true;

                            txtPhoneNo.ReadOnly = true;

                            txtViciLeadId.Text = "";
                            txtViciLeadId.Text = dt.Rows[0]["ViciLeadId"].ToString();
                            txtViciLeadId.ReadOnly = true;

                            ddlMake.ClearSelection();
                            ddlMake.SelectedIndex = ddlMake.Items.IndexOf(ddlMake.Items.FindByText(dt.Rows[0]["Make"].ToString()));
                            ddlMake.DataBind();

                            ddlMake2.ClearSelection();
                            ddlMake2.SelectedIndex = ddlMake2.Items.IndexOf(ddlMake2.Items.FindByText(dt.Rows[0]["Make2"].ToString()));
                            ddlMake2.DataBind();

                            ddlHOwnerShip.ClearSelection();
                            ddlHOwnerShip.SelectedIndex = ddlHOwnerShip.Items.IndexOf(ddlHOwnerShip.Items.FindByText(dt.Rows[0]["HOwnerShip"].ToString()));
                            ddlHOwnerShip.DataBind();

                            ddlGender.ClearSelection();
                            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                            ddlGender.DataBind();

                            ddlDisposition.ClearSelection();
                            ddlDisposition.SelectedIndex = ddlDisposition.Items.IndexOf(ddlDisposition.Items.FindByText(dt.Rows[0]["Disposition"].ToString()));
                            ddlDisposition.DataBind();

                            //ddlAgencyOwner.ClearSelection();
                            //ddlAgencyOwner.SelectedIndex = ddlAgencyOwner.Items.IndexOf(ddlAgencyOwner.Items.FindByText(dt.Rows[0]["AgencyOwner"].ToString()));
                            //ddlAgencyOwner.DataBind();

                            ddlContinousCoverage.ClearSelection();
                            ddlContinousCoverage.SelectedIndex = ddlContinousCoverage.Items.IndexOf(ddlContinousCoverage.Items.FindByText(dt.Rows[0]["ContinousCoverage"].ToString()));
                            ddlContinousCoverage.DataBind();

                            ddlMaritalStatus.ClearSelection();
                            ddlMaritalStatus.SelectedIndex = ddlMaritalStatus.Items.IndexOf(ddlMaritalStatus.Items.FindByText(dt.Rows[0]["MaritalStatus"].ToString()));
                            ddlMaritalStatus.DataBind();

                            txtLeadId.Text = "";
                            txtLeadId.Text = Request.QueryString["LeadId"];
                            txtLeadId.ReadOnly = true;

                            txtState.Enabled = false;
                            Panel1.Visible = true;
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        private void PopulateDdlDisposition()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id IN(5,11,12,14,15,22,25) ORDER BY Name"); // SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id !=23
                if (dt.Rows.Count > 0)
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();

                    //ddlAgencyOwner.Items.Insert(0, "Select Agency Owner");
                }
                else
                {
                    ddlDisposition.DataSource = "";
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtLeadId.Text) && !string.IsNullOrEmpty(txtViciLeadId.Text))
            {
                var cmd = new SqlCommand("Update Insurance SET FirstName=@FirstName, LastName=@LastName, Gender=@Gender, City=@City, State=@State, Zip=@Zip, PhoneNo=@PhoneNo, Company=@Company, Make=@Make, Year=@Year, Model=@Model, Make2=@Make2, Year2=@Year2, Model2=@Model2, DOB=@DOB, Email=@Email, Address=@Address, Comments=@Comments, HOwnerShip=@HOwnerShip, ContinousCoverage=@ContinousCoverage, MaritalStatus=@MaritalStatus, FollowUpXDate=@FollowUpXDate, DispositionId=@DispositionId, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id AND ViciLeadId=@ViciLeadId");
                cmd.Parameters.Clear();
                cmd.Connection = Connection.Db();
                cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                cmd.Parameters.AddWithValue("@State", txtState.Text.ToUpper().Trim());
                cmd.Parameters.AddWithValue("@Zip", txtZip.Text.Trim());
                cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                cmd.Parameters.AddWithValue("@Company", txtCompany.Text.Trim());
                cmd.Parameters.AddWithValue("@Make", ddlMake.SelectedValue);
                cmd.Parameters.AddWithValue("@Year", txtYear.Text.Trim());
                cmd.Parameters.AddWithValue("@Model", txtModel.Text.Trim());
                cmd.Parameters.AddWithValue("@Make2", ddlMake2.SelectedValue);
                cmd.Parameters.AddWithValue("@Year2", txtYearV2.Text.Trim());
                cmd.Parameters.AddWithValue("@Model2", txtModelV2.Text.Trim());
                cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                cmd.Parameters.AddWithValue("@HOwnerShip", ddlHOwnerShip.SelectedValue);
                cmd.Parameters.AddWithValue("@ContinousCoverage", ddlContinousCoverage.SelectedValue);
                cmd.Parameters.AddWithValue("@MaritalStatus", ddlMaritalStatus.SelectedValue);
                cmd.Parameters.AddWithValue("@FollowUpXDate", !string.IsNullOrWhiteSpace(txtFollowDate.Text) ? txtFollowDate.Text : "");
                cmd.Parameters.AddWithValue("@DispositionId", Convert.ToInt32(ddlDisposition.SelectedValue));
                cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text));
                if (Session["UId"] != null)
                {
                    cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtLeadId.Text));
                    try
                    {
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Updated Successfully.');", true);
                            Clear();
                            txtLeadId.Text = "";
                            txtViciLeadId.Text = "";
                            txtPhone.Text = "";
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Update.');", true);
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogging.SendExcepToDb(ex);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                }
            }
        }

        public void Clear()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtPhoneNo.Text = "";
            txtCompany.Text = "";
            ddlMake.SelectedIndex = 0;
            txtYear.Text = "";
            txtModel.Text = "";
            ddlMake2.SelectedIndex = 0;
            txtYearV2.Text = "";
            txtModelV2.Text = "";
            txtDOB.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtComments.Text = "";

            ddlDisposition.ClearSelection();

            txtLeadId.Text = "";
            txtViciLeadId.Text = "";

            //ddlMaritalStatus.SelectedIndex = 0;
            //ddlContinousCoverage.SelectedIndex = 0;
        }

        protected void btnGetInsuranceLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDuplicate("SELECT * FROM dbo.Insurance WHERE IsActive = 1 AND PhoneNo='" + txtPhone.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "warningMsg('Lead Already Submitted.');", true);
                }
                else
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT * FROM dbo.ViciLead WHERE IsActive= 1 AND Phone='" + txtPhone.Text.Trim() + "'");
                    if (dt.Rows.Count > 0)
                    {
                        txtViciLeadId.Text = "";
                        txtViciLeadId.Text = dt.Rows[0]["Id"].ToString();
                        txtViciLeadId.ReadOnly = true;

                        txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                        txtLName.Text = dt.Rows[0]["LastName"].ToString();
                        txtCity.Text = dt.Rows[0]["City"].ToString();
                        txtState.Text = dt.Rows[0]["State"].ToString();
                        txtZip.Text = dt.Rows[0]["ZipCode"].ToString();
                        txtPhoneNo.Text = dt.Rows[0]["Phone"].ToString();
                        txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                        txtEmail.Text = dt.Rows[0]["Email"].ToString();
                        txtAddress.Text = dt.Rows[0]["Address"].ToString();

                        ddlGender.ClearSelection();
                        ddlGender.SelectedIndex = ddlHOwnerShip.Items.IndexOf(ddlHOwnerShip.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                        ddlGender.DataBind();

                        Panel1.Visible = true;

                        txtState.Enabled = true;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void ddlDisposition_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDisposition.SelectedItem.Text == "Follow-Up/X-Date")
                pnlFollow.Visible = true;
        }
    }
}