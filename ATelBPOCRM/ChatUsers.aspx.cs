﻿using Lucky.General;
using Lucky.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Data;
using System.IO;
using System.Web.UI;
namespace ATelBPOCRM
{
    public partial class ChatUsers : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUsers();
            }
        }

        private void BindUsers()
        {
            try
            {
                var client = new RestClient(AtelStrings.BaseUrl + "/users");
                client.Authenticator = new HttpBasicAuthenticator(AtelStrings.OpenUName, AtelStrings.OpenPassword);

                var request = new RestRequest(AtelStrings.BaseUrl + "/users", Method.GET);

                var response = client.Get(request);

                JObject obj = JObject.Parse(response.Content);
                var data = obj["users"];

                GridView1.DataSource = JsonConvert.DeserializeObject<DataTable>(JsonConvert.SerializeObject(data)); // myObject; // JsonConvert.DeserializeObject<DataTable>(response.Content);
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}