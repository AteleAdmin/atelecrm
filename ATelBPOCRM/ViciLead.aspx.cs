﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using Lucky.General;

namespace ATelBPOCRM
{
    public partial class ViciData : System.Web.UI.Page
    {
        public long user = 0;//: 1007
        public string title = "";//: 
        public string FirstName = "";//: Guy
        public string LastName = "";//: Parker
        public string Address = "";//: 6276-1 Sadowski Rd
        public string City = "";//: Ft. Hood 
        public string State = "";//: TX 
        public string ZipCode = "";//: 76544
        public long Phone = 0;//: 4783200651 
        public string Email = "";//: Marriedtoabrat@Aol.Com 
        public int VehicleYear = 0;//: 0 
        public string VehicleMake = "";//: Dodge 
        public string VehicleModel = "";//: Dakota 
        public DateTime Dob = Convert.ToDateTime("01 Jan 1900");//: 0000-00-00 
        public string Gender = "";//: U 
        public int AdditionalVehicleYear = 0;//: 
        public string AdditionalVehicleMake = "";//: 
        public string AdditionalVehicleModel = "";//: 
        public DateTime OptinDate = Convert.ToDateTime("01 Jan 1900");//: 
        public DateTime ExpireDate = Convert.ToDateTime("01 Jan 1900");//: 
        public string IPAddressFile = "";//: 
        public string CF_uses_custom_fields = "";//: Y
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] keys = Request.QueryString.AllKeys;
            if (keys.Length > 0)
            {
                for (int i = 0; i < keys.Length; i++)
                {
                    switch (keys[i].ToString())
                    {
                        case "user":
                            long.TryParse(Request.QueryString[keys[i]].ToString(), out user);
                            break;

                        case "first_name":
                            FirstName = Request.QueryString[keys[i]].ToString();
                            break;

                        case "last_name":
                            LastName = Request.QueryString[keys[i]].ToString();
                            break;

                        case "address1":
                            Address = Request.QueryString[keys[i]].ToString();
                            break;

                        case "city":
                            City = Request.QueryString[keys[i]].ToString();
                            break;

                        case "state":
                            State = Request.QueryString[keys[i]].ToString();
                            break;

                        case "postal_code":
                            ZipCode = Request.QueryString[keys[i]].ToString();
                            break;

                        case "phone_number":
                            long.TryParse(Request.QueryString[keys[i]].ToString(), out Phone);
                            break;

                        case "email":
                            Email = Request.QueryString[keys[i]].ToString();
                            break;
                    }
                }
            }

            if (user > 0)
            {
                var result = SavePostingInfo();

                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", result > 0 ? "successMsg('<h4>Success</h4>Lead Saved Successfully');" : "errorMsg('<h4>Error</h4>Lead Cannot be Saved');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>No User Id Exists.');", true);
            }
        }

        private int SavePostingInfo()
        {
            var id = 0;
            try
            {
                var command = new SqlCommand("InsertViciLead", Connection.Db()) { CommandType = CommandType.StoredProcedure };

                command.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.VarChar, 50, "FirstName"));
                command.Parameters["@FirstName"].Value = FirstName;

                command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.VarChar, 50, "LastName"));
                command.Parameters["@LastName"].Value = LastName;

                command.Parameters.Add(new SqlParameter("@Address", SqlDbType.VarChar, 200, "Address"));
                command.Parameters["@Address"].Value = Address;

                command.Parameters.Add(new SqlParameter("@City", SqlDbType.VarChar, 50, "City"));
                command.Parameters["@City"].Value = City;

                command.Parameters.Add(new SqlParameter("@State", SqlDbType.VarChar, 2, "State"));
                command.Parameters["@State"].Value = State;

                command.Parameters.Add(new SqlParameter("@ZipCode", SqlDbType.VarChar, 5, "ZipCode"));
                command.Parameters["@ZipCode"].Value = ZipCode;

                command.Parameters.Add(new SqlParameter("@Email", SqlDbType.VarChar, 100, "Email"));
                command.Parameters["@Email"].Value = Email;

                command.Parameters.Add(new SqlParameter("@Phone", SqlDbType.BigInt));
                command.Parameters["@Phone"].Value = Phone;

                command.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.BigInt));
                command.Parameters["@CreatedBy"].Value = Session["UId"];


                id = command.ExecuteNonQuery() > 0 ? 1 : 0;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
            finally
            {
                //
            }

            return id;
        }
    }
}