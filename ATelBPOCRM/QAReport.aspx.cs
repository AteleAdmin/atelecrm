﻿using Lucky.General;
using Lucky.Security;
using System;
using System.IO;
using System.Text;
using System.Web.UI;

namespace ATelBPOCRM
{
    public partial class QAReport : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLive();
                BindPulse();
                BindPing();
            }
        }

        private void BindLive()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT pn.CallStatus, COUNT(pn.CallStatus) AS Count, SUM(COUNT(pn.CallStatus)) OVER() AS Total, CAST(CAST(COUNT(pn.CallStatus) AS DECIMAL(18,2))/CAST(SUM(COUNT(pn.CallStatus)) OVER() AS DECIMAL(18,2)) AS DECIMAL(18,2))*100 AS Percentage FROM dbo.Insurance pn WHERE pn.CallStatus IS NOT NULL GROUP BY pn.CallStatus");
                if (dt.Rows.Count > 0)
                {
                    var sb = new StringBuilder();
                    sb.Append("");
                    for (var i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["CallStatus"].ToString() == "Good Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-green\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Good Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        // Good
                        if (dt.Rows[i]["CallStatus"].ToString() == "Bad Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-red\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Bad Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Bad
                        if (dt.Rows[i]["CallStatus"].ToString() == "Average Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-yellow\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Average Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Avg
                        if (dt.Rows[i]["CallStatus"].ToString() == "Fake Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-aqua\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Fake Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Fake
                    }
                    ltLive.Text = sb.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void BindPulse()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT pn.CallStatus, COUNT(pn.CallStatus) AS Count, SUM(COUNT(pn.CallStatus)) OVER() AS Total, CAST(CAST(COUNT(pn.CallStatus) AS DECIMAL(18,2))/CAST(SUM(COUNT(pn.CallStatus)) OVER() AS DECIMAL(18,2)) AS DECIMAL(18,2))*100 AS Percentage FROM dbo.PingNPost pn WHERE pn.CallStatus IS NOT NULL AND pn.IsDemandPNP=1 GROUP BY pn.CallStatus");
                if (dt.Rows.Count > 0)
                {
                    var sb = new StringBuilder();
                    sb.Append("");
                    for (var i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["CallStatus"].ToString() == "Good Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-green\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Good Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        // Good
                        if (dt.Rows[i]["CallStatus"].ToString() == "Bad Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-red\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Bad Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Bad
                        if (dt.Rows[i]["CallStatus"].ToString() == "Average Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-yellow\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Average Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Avg
                        if (dt.Rows[i]["CallStatus"].ToString() == "Fake Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-aqua\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Fake Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Fake
                    }
                    ltPulse.Text = sb.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void BindPing()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT pn.CallStatus, COUNT(pn.CallStatus) AS Count, SUM(COUNT(pn.CallStatus)) OVER() AS Total, CAST(CAST(COUNT(pn.CallStatus) AS DECIMAL(18,2))/CAST(SUM(COUNT(pn.CallStatus)) OVER() AS DECIMAL(18,2)) AS DECIMAL(18,2))*100 AS Percentage FROM dbo.PingNPost pn WHERE pn.CallStatus IS NOT NULL AND pn.IsDemandPNP=0 GROUP BY pn.CallStatus");
                if (dt.Rows.Count > 0)
                {
                    var sb = new StringBuilder();
                    sb.Append("");
                    for (var i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["CallStatus"].ToString() == "Good Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-green\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Good Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        // Good
                        if (dt.Rows[i]["CallStatus"].ToString() == "Bad Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-red\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Bad Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Bad
                        if (dt.Rows[i]["CallStatus"].ToString() == "Average Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-yellow\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Average Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Avg
                        if (dt.Rows[i]["CallStatus"].ToString() == "Fake Call")
                        {
                            sb.Append("<div class=\"col-md-3\">");
                            sb.Append("<div class=\"info-box bg-aqua\">");

                            sb.Append("<span class=\"info-box-icon\">");
                            sb.Append(dt.Rows[i]["Total"].ToString());
                            sb.Append("</span>");

                            sb.Append("<div class=\"info-box-content\">");
                            sb.Append("<span class=\"info-box-text\">Fake Call</span>");
                            sb.Append("<span class=\"info-box-number\">" + dt.Rows[i]["Count"].ToString() + "");
                            sb.Append("</span>");

                            sb.Append("<div class=\"progress\">");
                            sb.Append("<div class=\"progress-bar\" style=\"width: " + dt.Rows[i]["Percentage"].ToString() + "%\"></div>");
                            sb.Append("</div>");
                            sb.Append("<span class=\"progress-description\">" + dt.Rows[i]["Percentage"].ToString() + "%");
                            sb.Append("</span>");
                            sb.Append("</div>");

                            sb.Append("</div>");
                            sb.Append("</div>");
                        }
                        //Fake
                    }
                    ltPing.Text = sb.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}