﻿<%@ Page Title="QA Ping Leads" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="QAPingLeads.aspx.cs" Inherits="ATelBPOCRM.QAPingLeads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dataTable th, td {
            white-space: nowrap;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $(this).bind("contextmenu", function (e) {
                e.preventDefault();
            });
        });
    </script>
    <script type="text/javascript">
        function click(e) {
            if (document.all) {
                if (event.button == 2 || event.button == 3) {
                    return false;
                }
            }
            else {
                if (e.button == 2 || e.button == 3) {
                    e.preventDefault();
                    e.stopPropagation();
                    alert(message);
                    return false;
                }
            }
        }
        if (document.all) {
            document.onmousedown = click;
        }
        else {
            document.onclick = click;
        }


        document.oncontextmenu = function () {
            return false;

        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Leads</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Sales</a></li>
            <li class="active">Lead List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <asp:Panel ID="pnlAdd" runat="server">
                <%--   <div class="col-md-12">
                    <div class="pull-right">
                        <a href="AddLead.aspx" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;Add New Lead</a>
                    </div>
                </div>--%>
            </asp:Panel>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Select Option</label>
                                <asp:DropDownList ID="ddlFilter" CssClass="form-control" runat="server">
                                    <asp:ListItem>Yesterday</asp:ListItem>
                                    <asp:ListItem>Last 7 Days + Today</asp:ListItem>
                                    <asp:ListItem>Last 30 Days + Today</asp:ListItem>
                                    <asp:ListItem>Last 30 Days</asp:ListItem>
                                    <asp:ListItem>This Month</asp:ListItem>
                                    <asp:ListItem>Last Month</asp:ListItem>
                                    <asp:ListItem>This Year</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnSubmit" runat="server" Text="Search Leads" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnSubmit_OnClick" CausesValidation="False" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask=""></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask=""></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnLead" runat="server" Text="Get Leads" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnLead_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lead List</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped dataTable display" AutoGenerateColumns="False" runat="server" OnRowDataBound="GridView1_OnRowDataBound" OnRowCreated="GridView1_OnRowCreated" EmptyDataText="No Record Found!">
                                <Columns>
                                    <asp:TemplateField HeaderText="Lead Id">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                            <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <a href="javascript:;">
                                                <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Zip">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Company">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Year">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Model">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agency">
                                        <ItemTemplate>
                                            <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transfer To">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DOB">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Disposition">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltDisposition" runat="server" Text='<%# Eval("Disposition") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Homeowner">
                                        <ItemTemplate>
                                            <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Owner">
                                        <ItemTemplate>
                                            <asp:Label ID="ltOwner" CssClass="label label-info" runat="server" Text='<%# Eval("Owner") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <a href="javascript:;" class="viewLead" data-request='<%# Eval("Id") %>'><i class="fa fa-eye btn-sm label-info">&nbsp;View Detail</i></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <Lucky:DataTableControl ID="DataTable" runat="server" />
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        //$(document).ready(function () {
        //    $('.display').prepend($("<thead></thead>").append($('.display').find("tr:first"))).DataTable({
        //        //dom: 'Bfrtip',
        //        "paging": false,
        //        //buttons: [
        //        //    'csv', 'excel'
        //        //],
        //        'aoColumnDefs': [{
        //            orderable: false,
        //            'aTargets': [9, 10]

        //        }]
        //    });
        //});
    </script>

    <script type="text/javascript">
        $(function () {
            $('.dataTable').on('click', '.viewLead', function () {
                var leadId = $(this).data('request');
                $.fancybox.open({
                    src: 'QAPingLeadDetail.aspx?leadId=' + leadId,
                    type: 'iframe'
                });
            });
        })
    </script>

    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 100%;
            margin: 0;
        }
    </style>
</asp:Content>

