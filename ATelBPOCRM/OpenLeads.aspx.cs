﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class OpenLeads : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    PopulateLeads(0, 0, 0, "", "");
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Manager")
                {
                    PopulateLeads(0, Convert.ToInt32(Session["UId"]), 0, "", "");
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Team Lead")
                {
                    PopulateLeads(Convert.ToInt32(Session["UId"]), 0, 0, "", "");
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Outsource")
                {
                    PopulateLeads(0, 0, Convert.ToInt32(Session["UId"]), "", "");
                }
            }
        }

        private void PopulateLeads(int? teamLeadId, int? managerId, int? outsourceOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (teamLeadId > 0 && managerId == 0 && outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,i.Id,i.FirstName+' '+i.LastName AS Name,i.City,i.State,i.Zip,i.PhoneNo,CONVERT(VARCHAR(15),i.CreatedDate,101) AS Date,i.IsActive FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.DispositionId=0 AND CONVERT(VARCHAR(15),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(15),i.CreatedDate,101) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 ORDER BY i.CreatedDate ASC";
                }
                if (teamLeadId > 0 && managerId == 0 && outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,i.Id,i.FirstName+' '+i.LastName AS Name,i.City,i.State,i.Zip,i.PhoneNo,CONVERT(VARCHAR(15),i.CreatedDate,101) AS Date,i.IsActive FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.DispositionId=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 ORDER BY i.CreatedDate ASC";
                }
                if (teamLeadId == 0 && managerId > 0 && outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,i.Id,i.FirstName+' '+i.LastName AS Name,i.City,i.State,i.Zip,i.PhoneNo,CONVERT(VARCHAR(15),i.CreatedDate,101) AS Date,i.IsActive FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id LEFT JOIN dbo.Users um ON tl.ReportedTo=um.Id WHERE i.DispositionId=0 AND CONVERT(VARCHAR(15),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(15),i.CreatedDate,101) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 ORDER BY i.CreatedDate ASC";
                }
                if (teamLeadId == 0 && managerId > 0 && outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,i.Id,i.FirstName+' '+i.LastName AS Name,i.City,i.State,i.Zip,i.PhoneNo,CONVERT(VARCHAR(15),i.CreatedDate,101) AS Date,i.IsActive FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id LEFT JOIN dbo.Users um ON tl.ReportedTo=um.Id WHERE i.DispositionId=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 ORDER BY i.CreatedDate ASC";
                }
                if (teamLeadId == 0 && managerId == 0 && outsourceOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,i.Id,i.FirstName+' '+i.LastName AS Name,i.City,i.State,i.Zip,i.PhoneNo,CONVERT(VARCHAR(15),i.CreatedDate,101) AS Date,i.IsActive FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.DispositionId=0 AND CONVERT(VARCHAR(15),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(15),i.CreatedDate,101) <= '" + to + "' AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 ORDER BY i.CreatedDate ASC";
                }
                if (teamLeadId == 0 && managerId == 0 && outsourceOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,i.Id,i.FirstName+' '+i.LastName AS Name,i.City,i.State,i.Zip,i.PhoneNo,CONVERT(VARCHAR(15),i.CreatedDate,101) AS Date,i.IsActive FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.DispositionId=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId=" + outsourceOwnerId + " AND i.IsOutsource=1 ORDER BY i.CreatedDate ASC";
                }
                if (teamLeadId == 0 && managerId == 0 && outsourceOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,i.Id,i.FirstName+' '+i.LastName AS Name,i.City,i.State,i.Zip,i.PhoneNo,CONVERT(VARCHAR(15),i.CreatedDate,101) AS Date,i.IsActive FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.DispositionId=0 AND CONVERT(VARCHAR(15),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(15),i.CreatedDate,101) <= '" + to + "' ORDER BY i.CreatedDate ASC";
                }
                if (teamLeadId == 0 && managerId == 0 && outsourceOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,i.Id,i.FirstName+' '+i.LastName AS Name,i.City,i.State,i.Zip,i.PhoneNo,CONVERT(VARCHAR(15),i.CreatedDate,101) AS Date,i.IsActive FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.DispositionId=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.CreatedDate ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var active = e.Row.FindControl("ltActive") as Label;
                    if (active != null && active.Text == "True")
                        active.CssClass = "label label-success";
                    else if (active != null) active.CssClass = "label label-danger";
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnOpenLeads_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    PopulateLeads(0, 0, 0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Manager")
                {
                    PopulateLeads(0, Convert.ToInt32(Session["UId"]), 0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Team Lead")
                {
                    PopulateLeads(Convert.ToInt32(Session["UId"]), 0, 0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Outsource")
                {
                    PopulateLeads(0, 0, Convert.ToInt32(Session["UId"]), txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}