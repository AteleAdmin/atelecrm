﻿using Lucky.General;
using System;
using System.Text;


namespace ATelBPOCRM
{
    public partial class ClientMaster : BaseMaster
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    var sb = new StringBuilder();
                    sb.Append("");
                    sb.Append("<ul class=\"sidebar-menu\" data-widget=\"tree\">");
                    sb.Append("<li><a href=\"Dashboard.aspx\"><i class=\"fa fa-dashboard\"></i><span>Dashboard</span></a></li>");

                    // -- Leads
                    sb.Append("<li class=\"treeview\">");
                    sb.Append("<a href=\"javascript:;\">");
                    sb.Append("<i class=\"fa fa-edit\"></i>");
                    sb.Append("<span>Live Transfer</span>");
                    sb.Append("<span class=\"pull-right-container\">");
                    sb.Append("<i class=\"fa fa-angle-left pull-right\"></i>");
                    sb.Append("</span>");
                    sb.Append("</a>");
                    sb.Append("<ul class=\"treeview-menu\">");
                    sb.Append("<li><a href=\"NewLeads.aspx\"><i class=\"fa fa-angle-double-right\"></i>New Leads</a></li>");
                    sb.Append("<li><a href=\"AgentReport.aspx\"><i class=\"fa fa-angle-double-right\"></i>Agent Report</a></li>");
                    sb.Append("<li><a href=\"TLNewLeads.aspx\"><i class=\"fa fa-angle-double-right\"></i>TL New Leads</a></li>");
                    sb.Append("<li><a href=\"AgencyReport.aspx\"><i class=\"fa fa-angle-double-right\"></i>Agency Report</a></li>");
                    sb.Append("<li><a href=\"Leads.aspx\"><i class=\"fa fa-angle-double-right\"></i>Lead List</a></li>");
                    sb.Append("<li><a href=\"WarrantyLeads.aspx\"><i class=\"fa fa-angle-double-right\"></i>Warranty Leads</a></li>");
                    sb.Append("<li><a href=\"OpenLeads.aspx\"><i class=\"fa fa-angle-double-right\"></i>Open Leads</a></li>");
                    sb.Append("<li><a href=\"DeleteLead.aspx\"><i class=\"fa fa-angle-double-right\"></i>Delete Lead</a></li>");
                    sb.Append("<li><a href=\"BonusSheet.aspx\"><i class=\"fa fa-angle-double-right\"></i>Bonus Sheet</a></li>");
                    sb.Append("</ul>");
                    sb.Append("</li>");
                    // --// Leads

                    // -- PingNPost
                    sb.Append("<li class=\"treeview\">");
                    sb.Append("<a href=\"javascript:;\">");
                    sb.Append("<i class=\"fa fa-pie-chart\"></i>");
                    sb.Append("<span>Ping Leads</span>");
                    sb.Append("<span class=\"pull-right-container\">");
                    sb.Append("<i class=\"fa fa-angle-left pull-right\"></i>");
                    sb.Append("</span>");
                    sb.Append("</a>");
                    sb.Append("<ul class=\"treeview-menu\">");
                    sb.Append("<li><a href=\"PingNPost.aspx\"><i class=\"fa fa-angle-double-right\"></i>Ping Leads</a></li>");
                    sb.Append("<li><a href=\"PNPAgencyReport.aspx\"><i class=\"fa fa-angle-double-right\"></i>Agency Report</a></li>");
                    sb.Append("<li><a href=\"PNPTLNewLeads.aspx\"><i class=\"fa fa-angle-double-right\"></i>TL New Leads</a></li>");
                    sb.Append("<li><a href=\"PNPAgentReport.aspx\"><i class=\"fa fa-angle-double-right\"></i>Agent Report</a></li>");
                    sb.Append("<li><a href=\"MoveLeads.aspx\"><i class=\"fa fa-angle-double-right\"></i>Move Leads</a></li>");
                    sb.Append("</ul>");
                    sb.Append("</li>");
                    // --// PingNPost

                    // -- Clients
                    sb.Append("<li class=\"treeview\">");
                    sb.Append("<a href=\"javascript:;\">");
                    sb.Append("<i class=\"fa fa-adn\"></i>");
                    sb.Append("<span>Client Panel</span>");
                    sb.Append("<span class=\"pull-right-container\">");
                    sb.Append("<i class=\"fa fa-angle-left pull-right\"></i>");
                    sb.Append("</span>");
                    sb.Append("</a>");
                    sb.Append("<ul class=\"treeview-menu\">");
                    sb.Append("<li><a href=\"AgencyOwner.aspx\"><i class=\"fa fa-angle-double-right\"></i>Agency Owner</a></li>");
                    sb.Append("<li><a href=\"AgencyProducer.aspx\"><i class=\"fa fa-angle-double-right\"></i>Agency Producer</a></li>");
                    sb.Append("<li><a href=\"AgencyFilter.aspx\"><i class=\"fa fa-angle-double-right\"></i>Agency Filter</a></li>");
                    sb.Append("</ul>");
                    sb.Append("</li>");
                    // --// Clients

                    // -- Outsource
                    sb.Append("<li class=\"treeview\">");
                    sb.Append("<a href=\"javascript:;\">");
                    sb.Append("<i class=\"fa fa-database\"></i>");
                    sb.Append("<span>Outsource Panel</span>");
                    sb.Append("<span class=\"pull-right-container\">");
                    sb.Append("<i class=\"fa fa-angle-left pull-right\"></i>");
                    sb.Append("</span>");
                    sb.Append("</a>");
                    sb.Append("<ul class=\"treeview-menu\">");
                    sb.Append("<li><a href=\"OutSourceNewLeads.aspx\"><i class=\"fa fa-angle-double-right\"></i>New Leads</a></li>");
                    sb.Append("<li><a href=\"OutsouceOwner.aspx\"><i class=\"fa fa-angle-double-right\"></i>Outsource Owner</a></li>");
                    sb.Append("<li><a href=\"OutsourceProducer.aspx\"><i class=\"fa fa-angle-double-right\"></i>Outsource Producer</a></li>");
                    sb.Append("</ul>");
                    sb.Append("</li>");
                    // --// Outsource

                    sb.Append("<li><a href=\"Dispositions.aspx\"><i class=\"fa fa-cube\"></i><span>Dispositions</span></a></li>");

                    // -- System Settings
                    sb.Append("<li class=\"treeview\">");
                    sb.Append("<a href=\"javascript:;\">");
                    sb.Append("<i class=\"fa fa-gears\"></i>");
                    sb.Append("<span>System Settings</span>");
                    sb.Append("<span class=\"pull-right-container\">");
                    sb.Append("<i class=\"fa fa-angle-left pull-right\"></i>");
                    sb.Append("</span>");
                    sb.Append("</a>");
                    sb.Append("<ul class=\"treeview-menu\">");
                    sb.Append("<li><a href=\"Users.aspx\"><i class=\"fa fa-angle-double-right\"></i>Users</a></li>");
                    sb.Append("<li><a href=\"Roles.aspx\"><i class=\"fa fa-angle-double-right\"></i>Roles</a></li>");
                    sb.Append("<li><a href=\"Modules.aspx\"><i class=\"fa fa-angle-double-right\"></i>Modules</a></li>");
                    sb.Append("<li><a href=\"SubModule.aspx\"><i class=\"fa fa-angle-double-right\"></i>Sub Modules</a></li>");
                    sb.Append("<li><a href=\"ModulesPermission.aspx\"><i class=\"fa fa-angle-double-right\"></i>Modules Permission</a></li>");
                    //sb.Append("<li><a href=\"EntityTypes.aspx\"><i class=\"fa fa-angle-double-right\"></i>Entity Types</a></li>");
                    sb.Append("</ul>");
                    sb.Append("</li>");
                    // --// System Settings
                    sb.Append("</ul>");

                    //ltMenu.Text = sb.ToString();
                }

                else
                {
                    //if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    //{
                    //    pnlChangePassword.Visible = true;
                    //}
                    //if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    //{
                    //    pnlChangePassword.Visible = true;
                    //}

                    var ds = new Lucky.General.PopulateDataSource();
                    var q =
                        "SELECT m.Id,m.Name AS Module, m.PageUrl,m.PageIcon FROM dbo.ModulesPermission mp JOIN dbo.Module m ON mp.ModuleId = m.Id WHERE mp.IsActive=1 AND RoleId=" +
                        Convert.ToInt32(Session["RoleId"]) + " ORDER BY m.DisplayOrder ASC";
                    //var dt = ds.DataTableSqlString("SELECT m.Id,m.Name AS Module, m.PageUrl,m.PageIcon FROM dbo.ModulesPermission mp JOIN dbo.Module m ON mp.ModuleId = m.Id WHERE mp.IsActive =1 AND mp.RoleId=" + Convert.ToInt32(Session["RoleId"]));
                    var dt = ds.DataTableSqlString(q);
                    if (dt.Rows.Count > 0)
                    {
                        var sb = new StringBuilder();
                        sb.Append("");
                        sb.Append("<ul class=\"sidebar-menu\" data-widget=\"tree\">");
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            var query = "SELECT sm.Id,sm.Name AS SubModule,sm.PageUrl FROM dbo.RolesPermissions rp JOIN dbo.SubModule sm ON rp.SubModuleId = sm.Id WHERE rp.CanView=1 AND sm.ModuleId=" + Convert.ToInt32(dt.Rows[i]["Id"].ToString()) + " AND sm.RoleId=" + Convert.ToInt32(Session["RoleId"]);
                            var dtt = ds.DataTableSqlString("SELECT sm.Id,sm.Name AS SubModule,sm.PageUrl FROM dbo.SubModule sm JOIN dbo.RolesPermissions rp ON sm.Id=rp.SubModuleId WHERE rp.CanView=1 AND sm.ModuleId=" + Convert.ToInt32(dt.Rows[i]["Id"].ToString()) + " AND rp.RoleId=" + Convert.ToInt32(Session["RoleId"]));
                            if (dtt.Rows.Count > 0)
                            {
                                sb.Append("<li class=\"treeview\">");
                                sb.Append("<a href=" + dt.Rows[i]["PageUrl"].ToString() + ".aspx" + ">");
                                sb.Append("<i class=\"fa " + dt.Rows[i]["PageIcon"].ToString() + "\"></i>");
                                sb.Append("<span>" + dt.Rows[i]["Module"].ToString() + "</span>");
                                sb.Append("<span class=\"pull-right-container\">");
                                sb.Append("<i class=\"fa fa-angle-left pull-right\"></i>");
                                sb.Append("</span>");
                                sb.Append("</a>");
                                sb.Append("<ul class=\"treeview-menu\">");
                                for (int a = 0; a < dtt.Rows.Count; a++)
                                {
                                    sb.Append("<li><a class='hrefLink' href=" + dtt.Rows[a]["PageUrl"].ToString() + "><i class='fa fa-angle-double-right'></i>" + dtt.Rows[a]["SubModule"].ToString() + "</a></li>");
                                }
                                sb.Append("</ul>");
                            }
                            else
                            {
                                sb.Append("<li>");
                                sb.Append("<a href=" + dt.Rows[i]["PageUrl"].ToString() + ".aspx" + ">");
                                sb.Append("<i class=\"fa " + dt.Rows[i]["PageIcon"].ToString() + "\"></i>");
                                sb.Append("<span>" + dt.Rows[i]["Module"].ToString() + "</span>");
                                sb.Append("</a>");
                            }

                            sb.Append("</li>");
                        }
                        sb.Append("</ul>");
                        // Menu
                        //ltMenu.Text = sb.ToString();
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Name"] != null)
                {
                    if (Session["Position"] != null)
                        ltName.Text = Session["Name"].ToString() + " / " + Session["Position"];
                    else
                        ltName.Text = Session["Name"].ToString();

                    if (Session["Name"] != null)
                    {
                        //    if (Session["Name"].ToString().Contains(" "))
                        //    {
                        //        var sname = Session["Name"].ToString().Split(' ');
                        //        if (sname.Length > 0)
                        //        {
                        //            string fs = ""; string ss = "";
                        //            if (sname.Length == 1)
                        //            {
                        //                fs = sname[0].ToString().Substring(0, 1);
                        //            }

                        //            if (sname.Length == 2)
                        //            {
                        //                ss = sname[1].ToString().Substring(0, 1);
                        //            }

                        //            ltShortName.Text = fs + "." + ss;
                        //        }
                        //    }
                        //    if (Session["Name"].ToString().Contains("."))
                        //    {
                        //        var sname = Session["Name"].ToString().Split('.');
                        //        if (sname.Length > 0)
                        //        {
                        //            string fs = ""; string ss = "";
                        //            if (sname.Length == 1)
                        //            {
                        //                fs = sname[0].ToString().Substring(0, 1);
                        //            }

                        //            if (sname.Length == 2)
                        //            {
                        //                ss = sname[1].ToString().Substring(0, 1);
                        //            }

                        //            ltShortName.Text = fs + "." + ss;
                        //        }
                        //    }
                        //else

                        ltShortName.Text = Session["Name"].ToString().Substring(0, 1);
                        lblSPName.Text = Session["Name"].ToString();
                        ltRole.Text = Session["Role"].ToString();
                        ltPRole.Text = Session["Role"].ToString();
                    }

                    ltUName.Text = Session["Name"].ToString();
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        //ltPName.Text = "PRODUCER : " + Session["Name"].ToString();
                        if (Session["Role"] != null && Session["Agency"] != null)
                        {
                            //ltGName.Text = "AGENCY : " + Session["Agency"].ToString();
                        }

                        ltPRole.Text = "Sales Producer";
                        lblSPName.Text = Session["Name"].ToString();
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        //ltPName.Text = "OWNER : " + Session["Name"].ToString();
                        //ltGName.Text = "AGENCY : " + Session["Name"].ToString();

                        ltAgName.Text = Session["Name"].ToString();

                        ViewPause();

                        pnlPause.Visible = true;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin" || Session["Role"].ToString() == "CEO" || Session["Role"].ToString() == "CFO" || Session["Role"].ToString() == "GM Operations" || Session["Role"].ToString() == "Director" || Session["Role"].ToString() == "Manager" || Session["Role"].ToString() == "Team Lead")
                    {
                        AgencyStatus();

                        pnlAgencyStatus.Visible = true;
                    }

                    ltRole.Text = Session["Role"].ToString();

                    LeadNotify();
                }
                else
                {
                    Response.Redirect("Login.aspx", false);
                }
            }
            else
            {
                //
            }
        }

        private void LeadNotify()
        {
            try
            {
                var query = "";
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var d = new PopulateDataSource();
                    var t = d.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (t.Rows.Count > 0)
                    {
                        query = "SELECT COUNT(Id) AS Total FROM dbo.PingNPost WHERE CONVERT(VARCHAR(12),CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND AgencyOwnerId=" + Convert.ToInt32(t.Rows[0]["Id"].ToString());
                    }
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    query = "SELECT COUNT(Id) AS Total FROM dbo.PingNPost WHERE CONVERT(VARCHAR(12),CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND AgencyOwnerId=" + Convert.ToInt32(Session["UId"]);
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    ltNotification.Text = dt.Rows[0]["Total"].ToString();
                    //ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('New Lead Ping');", true);
                }
                else
                    ltNotification.Text = "";
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void ViewPause()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT IsAccept FROM dbo.AgencyOwner WHERE IsActive =1 AND Id=" + Convert.ToInt32(Session["UId"]));
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["IsAccept"].ToString() == "False")
                    {
                        chkAccept.Checked = false;
                    }
                    else
                    {
                        chkAccept.Checked = true;
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        private void AgencyStatus()
        {
            try
            {
                var sb = new StringBuilder();
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Name,IsAccept FROM dbo.AgencyOwner WHERE IsActive = 1 AND IsAccept IS NOT NULL ORDER BY IsAccept DESC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    for (var i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["IsAccept"].ToString() == "False")
                        {
                            sb.Append("<li>" + dt.Rows[i]["Name"].ToString() + "<i class='fa fa-circle text-default pull-right'></i></li>");
                        }
                        else
                        {
                            sb.Append("<li>" + dt.Rows[i]["Name"].ToString() + "<i class='fa fa-circle text-success pull-right'></i></li>");
                        }
                    }

                    ltAgencyStatus.Text = sb.ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void Timer1_OnTick(object sender, EventArgs e)
        {
            AgencyStatus();
        }
    }
}