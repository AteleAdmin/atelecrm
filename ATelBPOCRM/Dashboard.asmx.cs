﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using Lucky.General;

namespace ATelBPOCRM
{
    /// <summary>
    /// Summary description for Dashboard1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Dashboard1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public Object DayWiseWeekLive()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT CONVERT(VARCHAR,t.CreatedDate,101) AS CreatedDate,SUM(t.Total) AS Total,SUM(t.Rejected) AS Rejected,SUM(t.Accepted) AS Accepted FROM (
                                                            SELECT CONVERT(VARCHAR,i.CreatedDate,101) AS CreatedDate,
                                                            COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '01/01/2020' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '01/10/2020' AND i.IsOutsource=0 
                                                            GROUP BY i.DispositionId, CONVERT(VARCHAR,i.CreatedDate,101)) t GROUP BY CONVERT(VARCHAR,t.CreatedDate,101) ORDER BY CreatedDate ASC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"spline\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }

        [WebMethod]
        public Object DayWiseWeekPNP()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT CONVERT(VARCHAR,t.CreatedDate,101) AS CreatedDate,SUM(t.Total) AS Total,SUM(t.Rejected) AS Rejected,SUM(t.Accepted) AS Accepted FROM (
                                                            SELECT CONVERT(VARCHAR,i.CreatedDate,101) AS CreatedDate,
                                                            COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '01/01/2020' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '01/10/2020' AND i.IsOutsource=0 AND i.IsDemandPNP=0 
                                                            GROUP BY i.DispositionId, CONVERT(VARCHAR,i.CreatedDate,101)) t GROUP BY CONVERT(VARCHAR,t.CreatedDate,101) ORDER BY CreatedDate ASC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"spline\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }

        [WebMethod]
        public Object DayWiseWeekLivePNP()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT CONVERT(VARCHAR,t.CreatedDate,101) AS CreatedDate,SUM(t.Total) AS Total,SUM(t.Rejected) AS Rejected,SUM(t.Accepted) AS Accepted FROM (
                                                            SELECT CONVERT(VARCHAR,i.CreatedDate,101) AS CreatedDate,
                                                            COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '07/01/2020' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '07/10/2020' AND i.IsOutsource=0 AND i.IsDemandPNP=1 
                                                            GROUP BY i.DispositionId, CONVERT(VARCHAR,i.CreatedDate,101)) t GROUP BY CONVERT(VARCHAR,t.CreatedDate,101) ORDER BY CreatedDate ASC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"spline\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }

        [WebMethod]
        public Object MonthWiseLive()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT t.Name AS Agent,
                                                          --ISNULL(SUM(t.Total),0) AS Total,
                                                          ISNULL(SUM(t.Accepted),0) AS Accepted,
                                                          ISNULL(SUM(t.Rejected),0) AS Rejected FROM (
                                                            SELECT TOP 10 CONVERT(VARCHAR,i.CreatedDate,101) AS CreatedDate,
                                                            u.Name,COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.IsActive=1 
                                                              AND i.CreatedDate >= DATEADD(day,-30,GETDATE()) AND i.CreatedDate <= GETDATE()
                                                              AND i.IsOutsource=0 
                                                            GROUP BY u.Name,i.DispositionId, CONVERT(VARCHAR,i.CreatedDate,101) ORDER BY Total DESC) t GROUP BY t.Name --ORDER BY Total DESC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"bar\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }

        [WebMethod]
        public Object MonthWiseLivePNP()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT TOP 10 tt.Agent,
                                                          --ISNULL(SUM(tt.Total),0) AS Total,
                                                          ISNULL(SUM(tt.Accepted),0) AS Accepted,
                                                          ISNULL(SUM(tt.Rejected),0) AS Rejected FROM 
                                                            (SELECT t.Name AS Agent,ISNULL(SUM(t.Total),0) AS Total,ISNULL(SUM(t.Rejected),0) AS Rejected,ISNULL(SUM(t.Accepted),0) AS Accepted FROM (
                                                            SELECT 
                                                            u.Name,COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.IsActive=1 
                                                              AND i.CreatedDate >= DATEADD(day,-30,GETDATE()) AND i.CreatedDate <= GETDATE()
                                                              AND i.IsOutsource=0 
                                                              AND i.IsDemandPNP=1
                                                            GROUP BY u.Name,i.DispositionId) t GROUP BY t.Name) tt GROUP BY tt.Agent,tt.Rejected,tt.Accepted
                                                            --tt.Total,tt.Rejected,tt.Accepted --ORDER BY Total DESC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"bar\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }

        [WebMethod]
        public Object MonthWisePNP()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT TOP 10 tt.Agent,ISNULL(SUM(tt.Total),0) AS Total,ISNULL(SUM(tt.Rejected),0) AS Rejected,ISNULL(SUM(tt.Accepted),0) AS Accepted FROM 
                                                            (SELECT t.Name AS Agent,ISNULL(SUM(t.Total),0) AS Total,ISNULL(SUM(t.Rejected),0) AS Rejected,ISNULL(SUM(t.Accepted),0) AS Accepted FROM (
                                                            SELECT 
                                                            u.Name,COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.IsActive=1 
                                                              AND i.CreatedDate >= DATEADD(day,-30,GETDATE()) AND i.CreatedDate <= GETDATE()
                                                              AND i.IsOutsource=0 
                                                              AND i.IsDemandPNP=0
                                                            GROUP BY u.Name,i.DispositionId) t GROUP BY t.Name) tt GROUP BY tt.Agent,tt.Total,tt.Rejected,tt.Accepted ORDER BY Total DESC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"bar\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }

        [WebMethod]
        public Object DayWiseLiveLPD()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT t.Name AS Agent,
                                                          --ISNULL(SUM(t.Total),0) AS Total,
                                                          ISNULL(SUM(t.Accepted),0) AS Accepted,
                                                          ISNULL(SUM(t.Rejected),0) AS Rejected
                                                          --,CONVERT(VARCHAR,t.CreatedDate,101) AS CreatedDate 
                                                          FROM (
                                                            SELECT CONVERT(VARCHAR,i.CreatedDate,101) AS CreatedDate,
                                                            u.Name,COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '01/01/2020' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '01/10/2020' AND i.IsOutsource=0 
                                                            GROUP BY u.Name,i.DispositionId, CONVERT(VARCHAR,i.CreatedDate,101)) t GROUP BY t.Name HAVING ISNULL(SUM(t.Total),0) > 5
                                                        --CONVERT(VARCHAR,t.CreatedDate,101),t.Name HAVING ISNULL(SUM(t.Total),0) > 5 ORDER BY CreatedDate ASC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"spline\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }

        [WebMethod]
        public Object DayWisePNPLPD()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT t.Name AS Agent,
                                                          --ISNULL(SUM(t.Total),0) AS Total,
                                                          ISNULL(SUM(t.Rejected),0) AS Rejected,ISNULL(SUM(t.Accepted),0) AS Accepted
                                                          --,CONVERT(VARCHAR,t.CreatedDate,101) AS CreatedDate 
                                                          FROM (
                                                            SELECT CONVERT(VARCHAR,i.CreatedDate,101) AS CreatedDate,
                                                            u.Name,COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '01/01/2020' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '01/10/2020' AND i.IsOutsource=0 AND i.IsDemandPNP=0 
                                                            GROUP BY u.Name,i.DispositionId, CONVERT(VARCHAR,i.CreatedDate,101)) t GROUP BY t.Name
                                                            --CONVERT(VARCHAR,t.CreatedDate,101) ORDER BY CreatedDate ASC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"spline\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }

        [WebMethod]
        public Object DayWiseLivePNPLPD()
        {
            var sb = new StringBuilder();
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(@"SELECT t.Name AS Agent,
                                                          --ISNULL(SUM(t.Total),0) AS Total,
                                                          ISNULL(SUM(t.Accepted),0) AS Accepted,
                                                          ISNULL(SUM(t.Rejected),0) AS Rejected
                                                          --,CONVERT(VARCHAR,t.CreatedDate,101) AS CreatedDate 
                                                          FROM (
                                                            SELECT CONVERT(VARCHAR,i.CreatedDate,101) AS CreatedDate,
                                                            u.Name,COUNT(d.Name) AS Total, (CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN COUNT(d.Name) END) AS Rejected,(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN COUNT(d.Name) END) AS Accepted FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.Users u ON i.CreatedBy= u.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '07/01/2020' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '07/10/2020' AND i.IsOutsource=0 AND i.IsDemandPNP=1 
                                                            GROUP BY u.Name,i.DispositionId, CONVERT(VARCHAR,i.CreatedDate,101)) t GROUP BY t.Name
                                                         --CONVERT(VARCHAR,t.CreatedDate,101),t.Name --ORDER BY CreatedDate ASC");
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("[");
                    for (var x = 1; x < dt.Columns.Count; x++)
                    {
                        //var ss = dt.Columns[x].ColumnName;
                        sb.Append("{");
                        sb.Append("type: \"spline\",");
                        sb.Append("visible: true,");
                        sb.Append("showInLegend: true,");
                        sb.Append("yValueFormatString: \"##.00\",");
                        sb.Append("name: '" + dt.Columns[x].ColumnName.ToString() + "',");
                        sb.Append("dataPoints: [");

                        for (var z = 0; z < dt.Rows.Count; z++)
                        {
                            sb.Append("{");
                            if (!string.IsNullOrEmpty(dt.Rows[z][x].ToString()))
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + dt.Rows[z][x].ToString());
                            }
                            else
                            {
                                sb.Append("label:'" + dt.Rows[z][0].ToString() + "',y:" + 0);
                            }

                            sb.Append("},");
                        }
                        sb.Append("]");
                        if (x == dt.Columns.Count - 1)
                        {
                            sb.Append("}");
                        }
                        else
                        {
                            sb.Append("},");
                        }
                    }
                    sb.Append("]");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
            return sb.ToString();
        }
    }
}
