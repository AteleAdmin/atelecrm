﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class AgencyReport : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    PopulateLeadsSummary(0, "", "");
                    PopulateLeadsCount(0, "", "");
                    PopulateLead(0, "", "");

                    PopulateAgencyCount(0, "", "");
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Team Lead")
                {
                    PopulateLeadsSummary(Convert.ToInt32(Session["UId"]), "", "");
                    PopulateLeadsCount(Convert.ToInt32(Session["UId"]), "", "");
                    PopulateLead(Convert.ToInt32(Session["UId"]), "", "");

                    PopulateAgencyCount(Convert.ToInt32(Session["UId"]), "", "");
                }
            }
            else
            {
                //
            }
        }

        public void PopulateAgencyCount(int? teamLeadId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (teamLeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.Insurance i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 GROUP BY ao.Name, ao.State";
                }
                if (teamLeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.Insurance i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 GROUP BY ao.Name, ao.State";
                }
                if (teamLeadId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.Insurance i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY ao.Name, ao.State";
                }
                if (teamLeadId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.Insurance i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY ao.Name, ao.State";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyStat.DataSource = dt;
                    gvAgencyStat.DataBind();
                }
                else
                {
                    gvAgencyStat.EmptyDataText = "No Record Found!";
                    gvAgencyStat.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateLead(int? teamLeadId, string from, string to)
        {
            try
            {
                var query = "";
                if (teamLeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 ORDER BY i.Id ASC";
                }
                if (teamLeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 ORDER BY i.Id ASC";
                }
                if (teamLeadId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip,(CASE WHEN i.IsOutsource = 'True' THEN 'O.S' ELSE 'ATel' END) AS Owner FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                }
                if (teamLeadId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip,(CASE WHEN i.IsOutsource = 'True' THEN 'O.S' ELSE 'ATel' END) AS Owner FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAllLeads.DataSource = dt;
                    gvAllLeads.DataBind();

                    gvAllLeads.UseAccessibleHeader = true;
                    gvAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnStats_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(0, from, to);
                    PopulateLeadsCount(0, from, to);
                    PopulateLead(0, from, to);
                    PopulateAgencyCount(0, from, to);

                    //PopulateLeadsSummary(0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLeadsCount(0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLead(0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateAgencyCount(0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Team Lead")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(Convert.ToInt32(Session["UId"]), from, to);
                    PopulateLeadsCount(Convert.ToInt32(Session["UId"]), from, to);
                    PopulateLead(Convert.ToInt32(Session["UId"]), from, to);
                    PopulateAgencyCount(Convert.ToInt32(Session["UId"]), from, to);

                    //PopulateLeadsSummary(Convert.ToInt32(Session["UId"]), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLeadsCount(Convert.ToInt32(Session["UId"]), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLead(Convert.ToInt32(Session["UId"]), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateAgencyCount(Convert.ToInt32(Session["UId"]), txtFrom.Text.Trim(), txtTo.Text.Trim());
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulateLeadsCount(int? teamLeadId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (teamLeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.DispositionId NOT IN(1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0";
                }
                if (teamLeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.DispositionId NOT IN(1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0";
                }
                if (teamLeadId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=0 AND i.DispositionId NOT IN(1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (teamLeadId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=0 AND i.DispositionId NOT IN(1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }

                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (teamLeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0";
                }
                if (teamLeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0";
                }
                if (teamLeadId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (teamLeadId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }

                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (teamLeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0";
                }
                if (teamLeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0";
                }
                if (teamLeadId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (teamLeadId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }

                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateLeadsSummary(int? teamLeadId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (teamLeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 GROUP BY d.Name";
                }
                if (teamLeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 GROUP BY d.Name";
                }
                if (teamLeadId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id WHERE i.IsHomeLead=0 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY d.Name";
                }
                if (teamLeadId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id WHERE i.IsHomeLead=0 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY d.Name";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvRejected.DataSource = dt;
                    gvRejected.DataBind();
                }
                else
                {
                    gvRejected.EmptyDataText = "No Record Found!";
                    gvRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (teamLeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 GROUP BY d.Name";
                }
                if (teamLeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 GROUP BY d.Name";
                }
                if (teamLeadId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id WHERE i.IsHomeLead=0 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY d.Name";
                }
                if (teamLeadId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id WHERE i.IsHomeLead=0 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY d.Name";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAccepted.DataSource = dt;
                    gvAccepted.DataBind();
                }
                else
                {
                    gvAccepted.EmptyDataText = "No Record Found!";
                    gvAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void gvRejected_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
        }

        protected void gvAccepted_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
        }

        protected void gvAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                e.Row.Cells[12].Visible = false;
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                e.Row.Cells[12].Visible = false;
            }
            else
            {
                //
            }
        }

        protected void gvAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected")))
                {
                    disposition.CssClass = "label label-danger";
                }
            }
            else
            {
                //
            }
        }

        private int _rejected = 0;
        private int _accepted = 0;
        private int _total = 0;
        protected void gvAgencyStat_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    _rejected += Convert.ToInt32(((Literal)e.Row.FindControl("ltRejected")).Text);
                    _accepted += Convert.ToInt32(((Literal)e.Row.FindControl("ltAccepted")).Text);
                    _total += Convert.ToInt32(((Literal)e.Row.FindControl("ltTotal")).Text);
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    // (e.Row.FindControl("lblAmount") as Label).Text = amount.ToString();
                    //e.Row.Cells[7].Text = String.Format("{0:0}", "<b>" + Total + "</b>");
                    //gvAgencyStat.FooterRow.Cells[0].Text = "Total";

                    var head = (Label)e.Row.FindControl("lblHead");
                    var saleRejected = (Label)e.Row.FindControl("lblRejected");
                    var saleAccepted = (Label)e.Row.FindControl("lblAccepted");
                    var saleTotal = (Label)e.Row.FindControl("lblTotal");

                    head.Text = "Total";
                    saleRejected.Text = _rejected.ToString();
                    saleAccepted.Text = _accepted.ToString();
                    saleTotal.Text = _total.ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}