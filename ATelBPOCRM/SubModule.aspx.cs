﻿using Lucky.General;
using Lucky.Security;
using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class SubModule : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateModule();
                FillModuleDdl();
            }
            else
            {
                //
            }
        }

        public void FillModuleDdl()
        {
            try
            {
                var ds = new Lucky.General.PopulateDataSource();
                var dt = ds.DataTableSqlString(
                    "SELECT Id,Name FROM dbo.Module WHERE IsActive=1");
                ddlModule.DataSource = dt;
                ddlModule.DataTextField = "Name";
                ddlModule.DataValueField = "Id";
                ddlModule.DataBind();
            }
            catch (Exception ex)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void PopulateModule()
        {
            try
            {
                var ds = new Lucky.General.PopulateDataSource();
                var dt = ds.DataTableSqlString(
                    "SELECT sm.Id,sm.Name,sm.PageUrl,m.Name AS Module,sm.IsActive FROM dbo.SubModule sm JOIN dbo.Module m ON sm.ModuleId = m.Id");
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                var m = new Lucky.Security.SubModule();

                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.SubModule WHERE Name='" + txtName.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Sub Module Already Exists');", true);
                }
                else
                {
                    m.Name = txtName.Text.Trim();
                    m.PageUrl = txtPageUrl.Text.Trim();
                    m.IsActive = true;
                    m.ModuleId = Convert.ToInt32(ddlModule.SelectedValue);
                    m.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); //DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    m.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); //DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    if (m.InsertSubModule(m) > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Sub Module Saved Successfully.');", true);
                        PopulateModule();
                        Clear();
                        txtSubModuleId.Text = "";
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    if (hfId != null) txtSubModuleId.Text = hfId.Value;
                    var lblModule = (Literal)GridView1.Rows[rowIndex].FindControl("ltModule");
                    var lblName = (Literal)GridView1.Rows[rowIndex].FindControl("ltName");
                    var lblPageUrl = (Literal)GridView1.Rows[rowIndex].FindControl("ltPageUrl");

                    txtName.Text = lblName.Text;
                    txtPageUrl.Text = lblPageUrl.Text;
                    ddlModule.SelectedIndex = ddlModule.Items.IndexOf(ddlModule.Items.FindByText(lblModule.Text));
                }

                ddlModule.DataBind();
                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var message = "";
                var m = new Lucky.Security.SubModule();
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var active = GridView1.Rows[rowIndex].FindControl("chkActive") as CheckBox;

                    if (active != null && active.Checked == true)
                    {
                        m.IsActive = false;
                        message = "Blocked";
                    }
                    else
                    {
                        m.IsActive = true;
                        message = "Active";
                    }

                    m.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    if (hfId != null) m.Id = Convert.ToInt32(hfId.Value);
                }

                if (m.BlockSubModule(m) > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>Module " + message + " Successfully');", true);
                    PopulateModule();
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Module Cannot be Saved.');", true);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void Clear()
        {
            txtName.Text = string.Empty;
            txtPageUrl.Text = string.Empty;
            ddlModule.SelectedIndex = 0;
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                var m = new Lucky.Security.SubModule();
                if (!string.IsNullOrEmpty(txtSubModuleId.Text.Trim()))
                {
                    m.Name = txtName.Text.Trim();
                    m.PageUrl = txtPageUrl.Text.Contains(".aspx") ? txtPageUrl.Text.Trim() : txtPageUrl.Text.Trim();
                    m.IsActive = true;
                    m.ModuleId = Convert.ToInt32(ddlModule.SelectedValue);
                    m.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); //DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    m.Id = Convert.ToInt32(txtSubModuleId.Text.Trim());
                    if (m.UpdateSubModule(m) > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Module Updated Successfully.');", true);
                        PopulateModule();
                        Clear();
                        txtSubModuleId.Text = "";

                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Sub Module Cannot be Updated.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Update Error.');", true);
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}