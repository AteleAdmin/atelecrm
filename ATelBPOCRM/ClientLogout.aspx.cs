﻿using System;

namespace ATelBPOCRM
{
    public partial class ClientLogout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();

            Response.Redirect("ClientLogin.aspx");
        }
    }
}