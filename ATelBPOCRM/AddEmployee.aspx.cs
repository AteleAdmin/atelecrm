﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace ATelBPOCRM
{
    public partial class AddEmployee : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateRoles();

                if (Request.QueryString["UserId"] != null && Request.QueryString["accountType"] != null)
                {
                    if (Request.QueryString["accountType"].ToString() == "User")
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT r.Name AS Role,u.Id,u.Name,u.Guardian,u.MeritalStatus,u.RelationWithGuardian,u.ReferenceOfferLetter,u.AltPhoneNo,u.ContactPerson,u.CNIC,u.Gender,u.MobileNo,u.CurrentAddress,u.PermanentAddress,u.Username,u.Email,u.Password,CONVERT(NVARCHAR(12),u.DOB,101) AS DOB,CONVERT(NVARCHAR(12),u.DOJ,101) AS DOJ,u.Position,u.SalaryOffered,u.CNIC,u.ContactPerson,u.RelationWithContactPerson,u.AttendanceId,u.Project,u.Profile,u.IsActive,u.CreatedBy FROM dbo.Employees u JOIN dbo.Roles r ON u.RoleId= r.Id WHERE u.Id=" + Convert.ToInt32(Request.QueryString["UserId"]));
                        if (dt.Rows.Count > 0)
                        {
                            txtName.Text = dt.Rows[0]["Name"].ToString();
                            txtGuardianName.Text = dt.Rows[0]["Guardian"].ToString();
                            txtGRelation.Text = dt.Rows[0]["RelationWithGuardian"].ToString();

                            txtUName.Text = dt.Rows[0]["Username"].ToString();
                            txtUName.ReadOnly = true;

                            txtEmail.Text = dt.Rows[0]["Email"].ToString();
                            txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                            txtDOJ.Text = dt.Rows[0]["DOJ"].ToString();

                            txtRefOfferLetter.Text = dt.Rows[0]["ReferenceOfferLetter"].ToString();
                            txtMobileNo.Text = dt.Rows[0]["MobileNo"].ToString();
                            txtAltPhoneNo.Text = dt.Rows[0]["AltPhoneNo"].ToString();
                            txtCNIC.Text = dt.Rows[0]["CNIC"].ToString();
                            txtCurrentAddress.Text = dt.Rows[0]["CurrentAddress"].ToString();
                            txtPermanentAddress.Text = dt.Rows[0]["PermanentAddress"].ToString();
                            txtPosition.Text = dt.Rows[0]["Position"].ToString();
                            txtSalaryOffered.Text = dt.Rows[0]["SalaryOffered"].ToString();
                            txtContactPNo.Text = dt.Rows[0]["ContactPerson"].ToString();
                            txtRelationWContactPerson.Text = dt.Rows[0]["RelationWithContactPerson"].ToString();
                            txtAttendanceId.Text = dt.Rows[0]["AttendanceId"].ToString();

                            if (!string.IsNullOrEmpty(dt.Rows[0]["Profile"].ToString()))
                            {
                                Image1.ImageUrl = "Uploads/" + dt.Rows[0]["Profile"].ToString();
                                pnlThumbnail.Visible = true;
                            }
                            else
                                pnlThumbnail.Visible = false;

                            txtPassword.Text = dt.Rows[0]["Password"].ToString();
                            chkAuto.Enabled = false;
                            txtPassword.ReadOnly = true;

                            ddlGender.ClearSelection();
                            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                            ddlGender.DataBind();

                            ddlMaritalSatus.ClearSelection();
                            ddlMaritalSatus.SelectedIndex = ddlMaritalSatus.Items.IndexOf(ddlMaritalSatus.Items.FindByText(dt.Rows[0]["MeritalStatus"].ToString()));
                            ddlMaritalSatus.DataBind();

                            ddlProject.ClearSelection();
                            ddlProject.SelectedIndex = ddlProject.Items.IndexOf(ddlProject.Items.FindByText(dt.Rows[0]["Project"].ToString()));
                            ddlProject.DataBind();

                            ddlRole.ClearSelection();
                            ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByText(dt.Rows[0]["Role"].ToString()));
                            ddlRole.DataBind();

                            txtEmployeeId.Text = Request.QueryString["UserId"];
                            btnSave.Visible = false;
                            btnUpdate.Visible = true;
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }
        }

        public void PopulateRoles()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Roles WHERE IsActive=1 AND IsSuperAdmin=0 AND Id NOT IN (2,3,12,13,15)");
                if (dt.Rows.Count > 0)
                {
                    ddlRole.DataSource = dt;
                    ddlRole.DataTextField = "Name";
                    ddlRole.DataValueField = "Id";
                    ddlRole.DataBind();

                    ddlRole.SelectedValue = "11";
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Users WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyProducer WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Admin WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.OutsourceOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Employees WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Employees WHERE AttendanceId='" + txtAttendanceId.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                else
                {
                    var cmd = new SqlCommand("INSERT INTO Employees(Name, Username, Email, Password, Guardian, RelationWithGuardian, ReferenceOfferLetter, CNIC, DOB, DOJ, DOE, Reason, MobileNo, AltPhoneNo, CurrentAddress, PermanentAddress, Position, SalaryOffered, Gender, MeritalStatus, ContactPerson, RelationWithContactPerson, AttendanceId, JoinCount, Status, RoleId, IsActive, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate,Profile,Project)VALUES(@Name, @Username, @Email, @Password, @Guardian, @RelationWithGuardian, @ReferenceOfferLetter, @CNIC, @DOB, @DOJ, @DOE, @Reason, @MobileNo, @AltPhoneNo, @CurrentAddress, @PermanentAddress, @Position, @SalaryOffered, @Gender, @MeritalStatus, @ContactPerson, @RelationWithContactPerson, @AttendanceId, @JoinCount, @Status, @RoleId, @IsActive, @CreatedBy, @CreatedDate, @LastModifiedBy, @LastModifiedDate,@Profile,@Project)", Connection.Db());
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Guardian", txtGuardianName.Text.Trim());
                    cmd.Parameters.AddWithValue("@RelationWithGuardian", txtGRelation.Text.Trim());
                    cmd.Parameters.AddWithValue("@ReferenceOfferLetter", txtRefOfferLetter.Text.Trim());
                    cmd.Parameters.AddWithValue("@Username", txtUName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    if (!chkAuto.Checked && string.IsNullOrEmpty(txtPassword.Text.Trim()))
                        cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                    if (chkAuto.Checked)
                        cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                    if (!chkAuto.Checked && !string.IsNullOrEmpty(txtPassword.Text.Trim()))
                        cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                    cmd.Parameters.AddWithValue("@CNIC", txtCNIC.Text.Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@MobileNo", txtMobileNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@AltPhoneNo", txtAltPhoneNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@CurrentAddress", txtCurrentAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@PermanentAddress", txtPermanentAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());//DateTime.ParseExact(txtDOB.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim());//DateTime.ParseExact(txtDOJ.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@DOE", "");//DateTime.ParseExact(txtDOJ.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Reason", "");
                    cmd.Parameters.AddWithValue("@Position", txtPosition.Text.Trim());
                    cmd.Parameters.AddWithValue("@SalaryOffered", txtSalaryOffered.Text.Trim());
                    cmd.Parameters.AddWithValue("@MeritalStatus", ddlMaritalSatus.Text.Trim());
                    cmd.Parameters.AddWithValue("@ContactPerson", txtContactPNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@RelationWithContactPerson", txtRelationWContactPerson.Text.Trim());
                    cmd.Parameters.AddWithValue("@AttendanceId", Convert.ToInt32(txtAttendanceId.Text.Trim()));
                    cmd.Parameters.AddWithValue("@JoinCount", 1);
                    cmd.Parameters.AddWithValue("@Status", "Trainee");

                    var thumbnail = "";
                    var timeStamp = DateTime.Now.ToString("hh.mm.ss.ffffff");
                    if (FileUpload1.HasFile)
                    {
                        var path = "~/Uploads";
                        var fileName = Path.GetFileName(FileUpload1.FileName);

                        var folderExists = Directory.Exists(Server.MapPath(path));
                        if (!folderExists)
                        {
                            Directory.CreateDirectory(Server.MapPath(path));
                        }
                        FileUpload1.SaveAs(Server.MapPath("~/Uploads/") + timeStamp + "_" + fileName);
                        thumbnail = timeStamp + "_" + fileName;
                    }
                    cmd.Parameters.AddWithValue("@Profile", thumbnail);
                    cmd.Parameters.AddWithValue("@RoleId", Convert.ToInt32(ddlRole.SelectedValue));
                    cmd.Parameters.AddWithValue("@Project", ddlProject.SelectedValue);
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Employee Save Successfully.');", true);
                            Clear();

                            txtEmployeeId.Text = "";
                        }
                        else
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Employee Cannot be Save.');", true);
                    }
                    else
                    {
                        //
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void Clear()
        {
            txtName.Text = "";
            txtGuardianName.Text = "";
            txtGRelation.Text = "";
            txtUName.Text = "";
            txtEmail.Text = "";
            ddlGender.SelectedIndex = 0;
            txtCNIC.Text = "";
            txtMobileNo.Text = "";
            txtAltPhoneNo.Text = "";
            txtContactPNo.Text = "";
            txtRelationWContactPerson.Text = "";
            txtSalaryOffered.Text = "";
            txtPosition.Text = "";
            txtPassword.ReadOnly = false;
            txtPassword.Text = "";
            txtUName.ReadOnly = false;
            txtDOB.Text = "";
            txtDOJ.Text = "";
            chkAuto.Enabled = true;

            ddlRole.SelectedIndex = 0;
        }

        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var res = new StringBuilder();
            var rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtEmployeeId.Text.Trim()))
                {
                    var query = "";
                    var cmd = new SqlCommand();

                    var thumbnail = "";
                    var timeStamp = DateTime.Now.ToString("hh.mm.ss.ffffff");

                    if (FileUpload1.HasFile)
                    {
                        query = "Update Employees SET Profile=@Profile,ContactPerson=@ContactPerson,RelationWithContactPerson=@RelationWithContactPerson,MeritalStatus=@MeritalStatus,Name=@Name, Guardian=@Guardian, RelationWithGuardian=@RelationWithGuardian, Email=@Email, CNIC=@CNIC, Gender=@Gender, MobileNo=@MobileNo, AltPhoneNo=@AltPhoneNo CurrentAddress=@CurrentAddress, PermanentAddress=@PermanentAddress, DOB=@DOB, DOJ=@DOJ, Position=@Position, RoleId=@RoleId, SalaryOffered=@SalaryOffered, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                        var path = "~/Uploads";
                        var fileName = Path.GetFileName(FileUpload1.FileName);

                        var folderExists = Directory.Exists(Server.MapPath(path));
                        if (!folderExists)
                        {
                            Directory.CreateDirectory(Server.MapPath(path));
                        }
                        FileUpload1.SaveAs(Server.MapPath("~/Uploads/") + timeStamp + "_" + fileName);
                        thumbnail = timeStamp + "_" + fileName;

                        cmd.Parameters.AddWithValue("@Profile", thumbnail);
                    }
                    else
                    {
                        query = "Update Employees SET ContactPerson=@ContactPerson,RelationWithContactPerson=@RelationWithContactPerson,MeritalStatus=@MeritalStatus,Name=@Name, Guardian=@Guardian, RelationWithGuardian=@RelationWithGuardian, Email=@Email, CNIC=@CNIC, Gender=@Gender, MobileNo=@MobileNo, AltPhoneNo=@AltPhoneNo CurrentAddress=@CurrentAddress, PermanentAddress=@PermanentAddress, DOB=@DOB, DOJ=@DOJ, Position=@Position, RoleId=@RoleId, SalaryOffered=@SalaryOffered, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                    }
                    cmd.CommandText = query;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Guardian", txtGuardianName.Text.Trim());
                    cmd.Parameters.AddWithValue("@RelationWithGuardian", txtGRelation.Text.Trim());
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    cmd.Parameters.AddWithValue("@CNIC", txtCNIC.Text.Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@MobileNo", txtMobileNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@AltPhoneNo", txtAltPhoneNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@CurrentAddress", txtCurrentAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@PermanentAddress", txtPermanentAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim()); //DateTime.ParseExact(txtDOB.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim()); //DateTime.ParseExact(txtDOJ.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@RoleId", Convert.ToInt32(ddlRole.SelectedValue));
                    cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtEmployeeId.Text.Trim()));
                    cmd.Parameters.AddWithValue("@Position", txtPosition.Text.Trim());
                    cmd.Parameters.AddWithValue("@SalaryOffered", txtSalaryOffered.Text.Trim());
                    cmd.Parameters.AddWithValue("@MeritalStatus", ddlMaritalSatus.Text.Trim());
                    cmd.Parameters.AddWithValue("@ContactPerson", txtContactPNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@RelationWithContactPerson", txtRelationWContactPerson.Text.Trim());
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Employee Save Successfully.');", true);
                            Clear();

                            txtEmployeeId.Text = "";

                            btnSave.Visible = true;
                            btnUpdate.Visible = false;
                        }
                        else
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Employee Cannot be Save.');", true);
                    }
                    else
                    {

                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}