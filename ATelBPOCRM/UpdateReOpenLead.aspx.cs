﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class UpdateReOpenLead : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                {
                    pnlDisposition.Visible = false;
                    Panel1.Visible = false;
                    PopulateDdlAgencyOwner();
                    PopulateDdlDisposition();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        private void PopulateDdlAgencyOwner()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.AgencyOwner WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlAgencyOwner.DataSource = dt;
                    ddlAgencyOwner.DataTextField = "Name";
                    ddlAgencyOwner.DataValueField = "Id";
                    ddlAgencyOwner.DataBind();

                    //ddlAgencyOwner.Items.Insert(0, "Select Agency Owner");
                }
                else
                {
                    ddlAgencyOwner.DataSource = dt;
                    ddlAgencyOwner.DataTextField = "Name";
                    ddlAgencyOwner.DataValueField = "Id";
                    ddlAgencyOwner.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void PopulateDdlDisposition()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();

                    //ddlAgencyOwner.Items.Insert(0, "Select Agency Owner");
                }
                else
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void PopulateDdlAgencyProducer(int? agencyOwner)
        {
            try
            {

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.AgencyProducer WHERE IsActive =1 AND AgencyOwnerId=" + agencyOwner);
                if (dt.Rows.Count > 0)
                {
                    ddlAgencyProducer.DataSource = dt;
                    ddlAgencyProducer.DataTextField = "Name";
                    ddlAgencyProducer.DataValueField = "Id";
                    ddlAgencyProducer.DataBind();

                    //ddlAgencyProducer.Items.Insert(0, "Select Agency Producer");
                }
                else
                {
                    ddlAgencyProducer.DataSource = dt;
                    ddlAgencyProducer.DataTextField = "Name";
                    ddlAgencyProducer.DataValueField = "Id";
                    ddlAgencyProducer.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtLeadId.Text) && !string.IsNullOrEmpty(txtViciLeadId.Text))
            {
                var cmd = new SqlCommand("Update Insurance SET FirstName=@FirstName, LastName=@LastName, City=@City, State=@State, Zip=@Zip, PhoneNo=@PhoneNo, Company=@Company, Make=@Make, Year=@Year, Model=@Model, DOB=@DOB, Email=@Email, Address=@Address, Comments=@Comments, HOwnerShip=@HOwnerShip, FollowUpXDate=@FollowUpXDate, DispositionId=@DispositionId, IsActive=@IsActive, CreatedBy=@CreatedBy, CreatedDate=@CreatedDate, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id AND ViciLeadId=@ViciLeadId");
                cmd.Parameters.Clear();
                cmd.Connection = Connection.Db();
                cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                cmd.Parameters.AddWithValue("@State", txtState.Text.Trim());
                cmd.Parameters.AddWithValue("@Zip", txtZip.Text.Trim());
                cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                cmd.Parameters.AddWithValue("@Company", txtCompany.Text.Trim());
                cmd.Parameters.AddWithValue("@Make", txtMake.Text.Trim());
                cmd.Parameters.AddWithValue("@Year", txtYear.Text.Trim());
                cmd.Parameters.AddWithValue("@Model", txtModel.Text.Trim());
                cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                cmd.Parameters.AddWithValue("@HOwnerShip", ddlHOwnerShip.SelectedValue);
                cmd.Parameters.AddWithValue("@FollowUpXDate", !string.IsNullOrWhiteSpace(txtFollowDate.Text) ? txtFollowDate.Text : "");
                cmd.Parameters.AddWithValue("@DispositionId", 2);
                cmd.Parameters.AddWithValue("@IsActive", 1);
                cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text));
                if (Session["UId"] != null)
                {
                    cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtLeadId.Text));
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Updated Successfully.');", true);
                        Clear();
                        txtLeadId.Text = "";
                        txtViciLeadId.Text = "";
                        txtPhone.Text = "";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Update.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Something Went Wrong.');", true);
            }
        }

        public void Clear()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtPhoneNo.Text = "";
            txtCompany.Text = "";
            txtMake.Text = "";
            txtYear.Text = "";
            txtModel.Text = "";
            txtDOB.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtComments.Text = "";

            ddlAgencyOwner.SelectedIndex = 0;
            ddlAgencyProducer.SelectedIndex = 0;

            ddlDisposition.ClearSelection();
            ddlAgencyOwner.ClearSelection();
            ddlAgencyProducer.ClearSelection();

            txtLeadId.Text = "";
            txtViciLeadId.Text = "";
        }

        protected void ddlAgencyOwner_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlAgencyOwner.SelectedIndex > 0)
            PopulateDdlAgencyProducer(Convert.ToInt32(ddlAgencyOwner.SelectedValue));
        }

        protected void btnGetInsuranceLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDuplicate("SELECT * FROM dbo.Insurance WHERE IsActive = 1 AND PhoneNo='" + txtPhone.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "warningMsg('Lead Already Submitted.');", true);
                }
                else
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT i.Id,i.FirstName,i.LastName,i.ViciLeadId,i.Gender,i.City,i.State,i.Zip,i.PhoneNo,i.Address,i.Comments,i.DOB,i.Company,i.HOwnerShip,i.Make,i.Year,i.Model,i.Email,ao.Name AS AgencyOwner,ap.Name AS AgencyProducer FROM dbo.Insurance i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.PhoneNo='" + txtPhone.Text.Trim() + "' AND i.DispositionId = 0");
                    if (dt.Rows.Count > 0)
                    {
                        txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                        txtLName.Text = dt.Rows[0]["LastName"].ToString();
                        txtCity.Text = dt.Rows[0]["City"].ToString();
                        txtState.Text = dt.Rows[0]["State"].ToString();
                        txtZip.Text = dt.Rows[0]["Zip"].ToString();
                        txtPhoneNo.Text = dt.Rows[0]["PhoneNo"].ToString();
                        txtCompany.Text = dt.Rows[0]["Company"].ToString();
                        txtMake.Text = dt.Rows[0]["Make"].ToString();
                        txtYear.Text = dt.Rows[0]["Year"].ToString();
                        txtModel.Text = dt.Rows[0]["Model"].ToString();
                        txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                        txtEmail.Text = dt.Rows[0]["Email"].ToString();
                        txtAddress.Text = dt.Rows[0]["Address"].ToString();
                        txtComments.Text = dt.Rows[0]["Comments"].ToString();

                        txtViciLeadId.Text = "";
                        txtViciLeadId.Text = dt.Rows[0]["ViciLeadId"].ToString();
                        txtViciLeadId.ReadOnly = true;

                        ddlHOwnerShip.ClearSelection();
                        ddlHOwnerShip.SelectedIndex = ddlHOwnerShip.Items.IndexOf(ddlHOwnerShip.Items.FindByText(dt.Rows[0]["HOwnerShip"].ToString()));
                        ddlHOwnerShip.DataBind();

                        ddlGender.ClearSelection();
                        ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                        ddlGender.DataBind();

                        //ddlDisposition.ClearSelection();
                        //ddlDisposition.SelectedIndex = ddlDisposition.Items.IndexOf(ddlDisposition.Items.FindByText(dt.Rows[0]["Disposition"].ToString()));
                        //ddlDisposition.DataBind();

                        ddlAgencyOwner.ClearSelection();
                        ddlAgencyOwner.SelectedIndex = ddlAgencyOwner.Items.IndexOf(ddlAgencyOwner.Items.FindByText(dt.Rows[0]["AgencyOwner"].ToString()));
                        ddlAgencyOwner.DataBind();

                        PopulateDdlAgencyProducer(Convert.ToInt32(ddlAgencyOwner.SelectedValue));

                        //ddlAgencyProducer.ClearSelection();
                        ddlAgencyProducer.SelectedIndex = ddlAgencyProducer.Items.IndexOf(ddlAgencyProducer.Items.FindByText(dt.Rows[0]["AgencyProducer"].ToString()));
                        ddlAgencyProducer.DataBind();

                        txtLeadId.Text = "";
                        txtLeadId.Text = dt.Rows[0]["Id"].ToString();
                        txtLeadId.ReadOnly = true;
                        //Id = Convert.ToInt32(Request.QueryString["LeadId"]);


                        Panel1.Visible = true;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void ddlDisposition_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDisposition.SelectedItem.Text == "Follow-Up/X-Date")
                pnlFollow.Visible = true;
        }
    }
}