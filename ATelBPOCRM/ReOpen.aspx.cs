﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class ReOpen : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Insurance WHERE PhoneNo='" + txtPhone.Text.Trim() + "' AND DispositionId=0 AND IsActive=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Re-Opened');", true);
                }
                else
                {
                    var query = "";
                    if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                    {
                        query = "UPDATE dbo.Insurance SET DispositionId = 0, IsActive =0, Make='', Year='', Model='', DOB='' WHERE PhoneNo='" + txtPhone.Text.Trim() + "'";
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() == "Manager")
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT i.Id FROM dbo.Insurance i LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id LEFT JOIN dbo.Users um ON tl.ReportedTo=um.Id WHERE um.Id=" + Convert.ToInt32(Session["UId"]) + " AND i.PhoneNo='" + txtPhone.Text.Trim() + "' AND i.IsOutsource=0");
                        if (dt.Rows.Count > 0)
                        {
                            query = "UPDATE dbo.Insurance SET DispositionId = 0, IsActive =0, Make='', Year='', Model='', DOB='' WHERE PhoneNo='" + txtPhone.Text.Trim() + "' AND IsOutsource=0";
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>You don't have access to Re-Open');", true);
                        }
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() == "Team Lead")
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT i.Id FROM dbo.Insurance i JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE tl.Id=" + Convert.ToInt32(Session["UId"]) + " AND i.PhoneNo='" + txtPhone.Text.Trim() + "' AND i.IsOutsource=0");
                        if (dt.Rows.Count > 0)
                        {
                            query = "UPDATE dbo.Insurance SET DispositionId = 0, IsActive =0, Make='', Year='', Model='', DOB='' WHERE PhoneNo='" + txtPhone.Text.Trim() + "' AND IsOutsource=0";
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>You don't have access to Re-Open');", true);
                        }
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() == "Outsource")
                    {
                        query = "UPDATE dbo.Insurance SET DispositionId = 0, IsActive =0, Make='', Year='', Model='', DOB='' WHERE PhoneNo='" + txtPhone.Text.Trim() + "' AND IsOutsource=1";
                    }

                    var cmd = new SqlCommand(query, Connection.Db());
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Re-Open Successfully');", true);

                        txtPhone.Text = "";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Re-Open');", true);
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}