﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class ClientLeadApiList : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null)
                {
                    PopulateList();
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }
        }

        public void PopulateList()
        {
            try
            {
                var query = "SELECT ca.AgencyId,ca.Id,ca.Client,ca.AuthType,ca.Resource,ca.Url,ca.EndPoint,ca.Username,ca.Password,ca.BearerToken,ca.Description,ca.RequestType,ca.ResponseType,ca.Verbose,ao.Name AS AgencyOwner, ao.Username AS AgencyUsername,ca.IsImplemented,ca.IsActive,CONVERT(VARCHAR(20), ao.CreatedDate,101) AS Date FROM ClientApi ca JOIN AgencyOwner ao ON ca.AgencyId=ao.Id";
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAllLeads.DataSource = dt;
                    gvAllLeads.DataBind();
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            var btn = sender as LinkButton;
            if (btn != null)
            {
                var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                var hfId = gvAllLeads.Rows[rowIndex].FindControl("hfId") as HiddenField;
                if (hfId != null)

                    Response.Redirect("AddApi.aspx?ApiId=" + Convert.ToInt32(hfId.Value));
            }
            else
            {
                //
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = gvAllLeads.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var active = gvAllLeads.Rows[rowIndex].FindControl("ltActive") as Label;
                    if (hfId != null)
                    {
                        var query = "";
                        var message = "";
                        if (active != null && active.Text == "True")
                        {
                            query = "UPDATE dbo.ClientApi SET IsActive=0, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                            message = "Blocked";
                        }
                        else
                        {
                            query = "UPDATE dbo.ClientApi SET IsActive=1, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                            message = "Active";
                        }
                        var cmd = new SqlCommand(query, Connection.Db());
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(hfId.Value));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            PopulateList();
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>Api " + message + " Successfully');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Api Cannot be Blocked');", true);
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvAllLeads_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var isImplemented = e.Row.FindControl("ltIsImplemented") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (isImplemented != null && isImplemented.Text == "True")
                    isImplemented.CssClass = "label label-success";
                else if (isImplemented != null) isImplemented.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void btnImplement_Click(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = gvAllLeads.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var isImplemented = gvAllLeads.Rows[rowIndex].FindControl("ltIsImplemented") as Label;
                    if (hfId != null)
                    {
                        var query = "";
                        var message = "";
                        if (isImplemented != null && isImplemented.Text == "True")
                        {
                            query = "UPDATE dbo.ClientApi SET IsImplemented=0, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                            message = "Blocked";
                        }
                        else
                        {
                            query = "UPDATE dbo.ClientApi SET IsImplemented=1, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                            message = "Active";
                        }
                        var cmd = new SqlCommand(query, Connection.Db());
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(hfId.Value));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            PopulateList();
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>Api " + message + " Successfully');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Api Cannot be Blocked');", true);
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}