﻿using Lucky.General;
using Lucky.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class ChatGroups : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGroups();
            }
        }

        private void BindGroups()
        {
            try
            {
                var client = new RestClient(AtelStrings.BaseUrl + "/users");
                client.Authenticator = new HttpBasicAuthenticator(AtelStrings.OpenUName, AtelStrings.OpenPassword);

                var request = new RestRequest(AtelStrings.BaseUrl + "/groups", Method.GET);

                var response = client.Get(request);

                JObject obj = JObject.Parse(response.Content);
                var data = obj["groups"];

                GridView1.DataSource = JsonConvert.DeserializeObject<DataTable>(JsonConvert.SerializeObject(data)); // myObject; // JsonConvert.DeserializeObject<DataTable>(response.Content);
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {

            try
            {
                var client = new RestClient(AtelStrings.BaseUrl + "/users");
                client.Authenticator = new HttpBasicAuthenticator(AtelStrings.OpenUName, AtelStrings.OpenPassword);

                var request = new RestRequest(AtelStrings.BaseUrl + "/groups/" + txtName.Text, Method.GET);

                var response = client.Get(request);

                if (response.StatusCode.ToString() == "OK")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Group Already Exists');", true);
                }
                else
                {
                    var req = new RestRequest(AtelStrings.BaseUrl + "/groups/", Method.POST);
                    var param = new Groups() { name = txtName.Text, description = txtName.Text };
                    req.RequestFormat = DataFormat.Json;
                    req.AddHeader("Accept", "application/json;odata=verbose");
                    req.AddHeader("Content-Type", "application/json;odata=verbose");
                    req.AddJsonBody(param);
                    var resp = client.Execute(req);

                    if (resp.StatusCode.ToString() == "Created")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Group Saved Successfully.');", true);
                        BindGroups();
                        txtName.Text = "";
                        txtGName.Text = "";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Group Cannot be Saved.');", true);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var lblName = (Literal)GridView1.Rows[rowIndex].FindControl("ltName");
                    txtName.Text = lblName.Text;
                    txtGName.Text = lblName.Text;
                }

                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtGName.Text.Trim()) && !string.IsNullOrEmpty(txtName.Text.Trim()))
                {
                    var client = new RestClient(AtelStrings.BaseUrl + "/users");
                    client.Authenticator = new HttpBasicAuthenticator(AtelStrings.OpenUName, AtelStrings.OpenPassword);

                    var req = new RestRequest(AtelStrings.BaseUrl + "/groups/{groupName}", Method.PUT).AddUrlSegment("groupName", txtGName.Text);
                    var param = new Groups() { name = txtName.Text, description = txtName.Text };
                    //req.RequestFormat = DataFormat.Json;
                    req.AddHeader("Accept", "application/json;odata=verbose");
                    req.AddHeader("Content-Type", "application/json;odata=verbose");
                    req.AddJsonBody(param);
                    var resp = client.Execute(req);

                    if (resp.StatusCode.ToString() == "OK")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Group Updated Successfully.');", true);
                        BindGroups();
                        txtName.Text = "";
                        txtGName.Text = "";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Group Cannot be Updated.');", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Update Error.');", true);
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtName.Text.Trim().Trim()))
                {
                    var client = new RestClient(AtelStrings.BaseUrl + "/users");
                    client.Authenticator = new HttpBasicAuthenticator(AtelStrings.OpenUName, AtelStrings.OpenPassword);

                    var req = new RestRequest(AtelStrings.BaseUrl + "/groups/" + txtName.Text, Method.DELETE);
                    var resp = client.Delete(req);
                    if (resp.StatusCode.ToString() == "OK")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Group Deleted Successfully.');", true);
                        BindGroups();
                        txtName.Text = "";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Group Cannot be Deleted.');", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Deletion Error.');", true);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

    }

    public class Groups
    {
        public string name { get; set; }
        public string description { get; set; }
    }
}