﻿using DbManager;
using Lucky.General;
using Newtonsoft.Json;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;

namespace ATelBPOCRM
{
    public partial class AddInsurance : System.Web.UI.Page
    {
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
        //        {

        //        }
        //        else
        //        {
        //            if (Session["RoleId"] != null)
        //            {
        //                var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
        //                if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
        //                {
        //                    //
        //                }
        //                else
        //                    Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (Session["OutsourceOwnerId"] != null)
                //{
                //    btnPingNPost.Visible = false;
                //}
                //else
                //{
                //    btnPingNPost.Visible = true;
                //}
                if (Session["UId"] != null)
                {
                    if (Request.QueryString["ping"] != null)
                    {
                        if (Request.QueryString["ping"].ToString() == "false")
                            btnPingNPost.Enabled = false;
                    }
                    else
                    {
                        btnPingNPost.Enabled = true;
                    }

                    pnlDisposition.Visible = false;
                    Panel1.Visible = false;
                    PopulateDdlAgencyOwner();
                    PopulateDdlDisposition();
                    if (Request.QueryString["msg"] != null && Request.QueryString["phone"] != null)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Saved Successfully');", true);
                        txtPhone.Text = Request.QueryString["phone"].ToString();
                        txtPhone.ReadOnly = true;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }
                    if (Request.QueryString["LeadId"] != null)
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT i.Id,i.FirstName,i.LastName,i.ViciLeadId,i.Gender,i.City,UPPER(i.State) AS State,i.Zip,i.PhoneNo,i.Address,i.Comments,i.DOB,i.Company,(CASE WHEN i.HOwnerShip='True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip,i.Make,i.Year,i.Model,i.Make2,i.Year2,i.Model2,i.Email,i.ContinousCoverage,(CASE WHEN i.MaritalStatus='True' THEN 'Yes' ELSE 'No' END) AS MaritalStatus,ao.Name AS AgencyOwner,ap.Name AS AgencyProducer,d.Name AS Disposition FROM dbo.Insurance i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.Id=" + Convert.ToInt32(Request.QueryString["LeadId"]));
                        if (dt.Rows.Count > 0)
                        {

                            txtPhone.Text = dt.Rows[0]["PhoneNo"].ToString();

                            txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                            txtLName.Text = dt.Rows[0]["LastName"].ToString();
                            txtCity.Text = dt.Rows[0]["City"].ToString();
                            txtState.Text = dt.Rows[0]["State"].ToString();
                            txtZip.Text = dt.Rows[0]["Zip"].ToString();
                            txtPhoneNo.Text = dt.Rows[0]["PhoneNo"].ToString();
                            //txtCompany.Text = dt.Rows[0]["Company"].ToString();
                            //txtMake.Text = dt.Rows[0]["Make"].ToString();
                            txtYear.Text = dt.Rows[0]["Year"].ToString();
                            txtModel.Text = dt.Rows[0]["Model"].ToString();
                            //txtMakeV2.Text = dt.Rows[0]["Make2"].ToString();
                            txtYearV2.Text = dt.Rows[0]["Year2"].ToString();
                            txtModelV2.Text = dt.Rows[0]["Model2"].ToString();
                            txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                            txtEmail.Text = dt.Rows[0]["Email"].ToString();
                            txtAddress.Text = dt.Rows[0]["Address"].ToString();
                            txtComments.Text = dt.Rows[0]["Comments"].ToString();

                            txtViciLeadId.Text = "";
                            txtViciLeadId.Text = dt.Rows[0]["ViciLeadId"].ToString();
                            txtViciLeadId.ReadOnly = true;

                            ddlInsuranceCompany.ClearSelection();
                            ddlInsuranceCompany.SelectedIndex = ddlInsuranceCompany.Items.IndexOf(ddlInsuranceCompany.Items.FindByText(dt.Rows[0]["Company"].ToString()));
                            ddlInsuranceCompany.DataBind();

                            ddlMake.ClearSelection();
                            ddlMake.SelectedIndex = ddlMake.Items.IndexOf(ddlMake.Items.FindByText(dt.Rows[0]["Make"].ToString()));
                            ddlMake.DataBind();

                            ddlMake2.ClearSelection();
                            ddlMake2.SelectedIndex = ddlMake2.Items.IndexOf(ddlMake2.Items.FindByText(dt.Rows[0]["Make2"].ToString()));
                            ddlMake2.DataBind();

                            ddlHOwnerShip.ClearSelection();
                            ddlHOwnerShip.SelectedIndex = ddlHOwnerShip.Items.IndexOf(ddlHOwnerShip.Items.FindByText(dt.Rows[0]["HOwnerShip"].ToString()));
                            ddlHOwnerShip.DataBind();

                            ddlGender.ClearSelection();
                            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                            ddlGender.DataBind();

                            ddlDisposition.ClearSelection();
                            ddlDisposition.SelectedIndex = ddlDisposition.Items.IndexOf(ddlDisposition.Items.FindByText(dt.Rows[0]["Disposition"].ToString()));
                            ddlDisposition.DataBind();

                            ddlAgencyOwner.ClearSelection();
                            ddlAgencyOwner.SelectedIndex = ddlAgencyOwner.Items.IndexOf(ddlAgencyOwner.Items.FindByText(dt.Rows[0]["AgencyOwner"].ToString()));
                            ddlAgencyOwner.DataBind();

                            ddlContinousCoverage.ClearSelection();
                            ddlContinousCoverage.SelectedIndex = ddlContinousCoverage.Items.IndexOf(ddlContinousCoverage.Items.FindByText(dt.Rows[0]["ContinousCoverage"].ToString()));
                            ddlContinousCoverage.DataBind();

                            ddlMaritalStatus.ClearSelection();
                            ddlMaritalStatus.SelectedIndex = ddlMaritalStatus.Items.IndexOf(ddlMaritalStatus.Items.FindByText(dt.Rows[0]["MaritalStatus"].ToString()));
                            ddlMaritalStatus.DataBind();

                            if (!string.IsNullOrWhiteSpace(ddlAgencyOwner.SelectedValue))
                            {
                                PopulateDdlAgencyProducer(Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                            }

                            ddlAgencyProducer.SelectedIndex = ddlAgencyProducer.Items.IndexOf(ddlAgencyProducer.Items.FindByText(dt.Rows[0]["AgencyProducer"].ToString()));
                            ddlAgencyProducer.DataBind();

                            txtLeadId.Text = "";
                            txtLeadId.Text = Request.QueryString["LeadId"];
                            txtLeadId.ReadOnly = true;

                            ddlAgencyOwner.Enabled = false;
                            ddlAgencyProducer.Enabled = false;
                            txtState.Enabled = false;

                            Panel1.Visible = true;

                            //if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                            //{
                            //    pnlAgency.Visible = false;
                            //}
                            //else
                            //{
                            //    pnlAgency.Visible = true;
                            //}
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        private void PopulateDdlAgencyOwner(string state = "")
        {
            try
            {
                var query = "";
                if (state == "")
                {
                    query = "SELECT Id,Name FROM dbo.AgencyOwner WHERE IsActive=1";
                }
                else
                {
                    query = "SELECT ao.Id,ao.Name FROM dbo.AgencyOwner ao JOIN dbo.AgencyStateMapping am ON ao.Id = am.AgencyOwnerId WHERE ao.RoleId=12 AND ao.IsActive=1 AND am.ST='" + state + "'";
                    //query = "SELECT Id,Name FROM dbo.AgencyOwner WHERE IsActive=1 AND State='" + state + "'";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    ddlAgencyOwner.DataSource = dt;
                    ddlAgencyOwner.DataTextField = "Name";
                    ddlAgencyOwner.DataValueField = "Id";
                    ddlAgencyOwner.DataBind();

                    //ddlAgencyOwner.Items.Insert(0, "Select Agency Owner");
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void PopulateDdlDisposition()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id !=23");
                if (dt.Rows.Count > 0)
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();

                    //ddlAgencyOwner.Items.Insert(0, "Select Agency Owner");
                }
                else
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void PopulateDdlAgencyProducer(int? agencyOwner)
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.AgencyProducer WHERE IsActive =1 AND AgencyOwnerId=" + agencyOwner);
                if (dt.Rows.Count > 0)
                {
                    ddlAgencyProducer.DataSource = dt;
                    ddlAgencyProducer.DataTextField = "Name";
                    ddlAgencyProducer.DataValueField = "Id";
                    ddlAgencyProducer.DataBind();
                }
                else
                {
                    ddlAgencyProducer.DataSource = dt;
                    ddlAgencyProducer.DataTextField = "Name";
                    ddlAgencyProducer.DataValueField = "Id";
                    ddlAgencyProducer.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtViciLeadId.Text))
            {
                try
                {
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Insurance WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "' AND DispositionId !=0") > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PendingPNP WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PingNPost WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                        return;
                    }
                    var isValid = false;
                    var company = "";
                    var source = new PopulateDataSource();
                    var table = source.DataTableSqlString("SELECT Company FROM dbo.AgencyOwner WHERE Name='" + ddlAgencyOwner.SelectedItem.Text.Trim() + "'");
                    if (table.Rows.Count > 0)
                    {
                        company = table.Rows[0]["Company"].ToString();
                    }
                    if (!string.IsNullOrEmpty(ddlInsuranceCompany.SelectedItem.Text))
                    {
                        if (company == ddlInsuranceCompany.SelectedItem.Text)
                        {
                            isValid = true;
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Cannot Submit Lead to this Agency.');", true);
                            return;
                        }
                        else
                        {
                            isValid = false;
                        }
                    }
                    if (isValid == false)
                    {
                        #region RicochetLive

                        var lead = new RicochetLive();
                        lead.FirstName = txtFName.Text.Trim();
                        lead.LastName = txtLName.Text.Trim();
                        lead.Gender = ddlGender.SelectedValue;
                        lead.City = txtCity.Text.Trim();
                        lead.State = txtState.Text.Trim();
                        lead.Zip = txtZip.Text.Trim();
                        lead.PhoneNo = txtPhoneNo.Text.Trim();
                        lead.Phone_Number = txtPhoneNo.Text.Trim();
                        lead.Company = ddlInsuranceCompany.SelectedValue;
                        lead.Make = ddlMake.SelectedValue;
                        lead.Year = txtYear.Text.Trim();
                        lead.Model = txtModel.Text.Trim();
                        lead.Make2 = ddlMake2.SelectedValue;
                        lead.Model2 = txtYearV2.Text.Trim();
                        lead.Year2 = txtModelV2.Text.Trim();

                        lead.Email = txtEmail.Text.Trim();
                        lead.Address = txtAddress.Text.Trim();
                        lead.Comments = txtComments.Text.Trim();
                        lead.HOwnerShip = Convert.ToBoolean(ddlHOwnerShip.SelectedValue);
                        lead.ContinousCoverage = ddlContinousCoverage.SelectedValue;
                        lead.MaritalStatus = Convert.ToBoolean(ddlMaritalStatus.SelectedValue);
                        lead.DispositionId = 23;
                        lead.IsActive = true;

                        #endregion

                        var cmd = new SqlCommand("INSERT INTO Insurance(FirstName, LastName, Gender, City, State, Zip, PhoneNo, Company, Make, Year, Model, Make2, Year2, Model2, DOB, Email, Address, Comments, HOwnerShip, ContinousCoverage, MaritalStatus, AgencyOwnerId, AgencyProducerId,OutsourceOwnerId,IsOutsource,DispositionId,ViciLeadId, IsActive, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate,IsHomeLead,IsFlooded,IsOpenClaim)VALUES(@FirstName, @LastName, @Gender, @City, @State, @Zip, @PhoneNo, @Company, @Make, @Year, @Model, @Make2, @Year2, @Model2, @DOB, @Email, @Address, @Comments, @HOwnerShip, @ContinousCoverage, @MaritalStatus, @AgencyOwnerId, @AgencyProducerId,@OutsourceOwnerId,@IsOutsource,@DispositionId,@ViciLeadId, @IsActive, @CreatedBy, @CreatedDate, @LastModifiedBy, @LastModifiedDate,@IsHomeLead,@IsFlooded,@IsOpenClaim)");
                        cmd.Parameters.Clear();
                        cmd.Connection = Connection.Db();
                        cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                        cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                        cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                        cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                        cmd.Parameters.AddWithValue("@State", txtState.Text.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@Zip", txtZip.Text.Trim());
                        cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                        cmd.Parameters.AddWithValue("@Company", ddlInsuranceCompany.SelectedValue);
                        cmd.Parameters.AddWithValue("@Make", ddlMake.SelectedValue);
                        cmd.Parameters.AddWithValue("@Year", txtYear.Text.Trim());
                        cmd.Parameters.AddWithValue("@Model", txtModel.Text.Trim());
                        cmd.Parameters.AddWithValue("@Make2", ddlMake2.SelectedValue);
                        cmd.Parameters.AddWithValue("@Year2", txtYearV2.Text.Trim());
                        cmd.Parameters.AddWithValue("@Model2", txtModelV2.Text.Trim());
                        if (chkHomeLead.Checked)
                        {
                            cmd.Parameters.AddWithValue("@IsHomeLead", 1);
                            cmd.Parameters.AddWithValue("@IsFlooded", ddlIsFlooded.SelectedValue);
                            cmd.Parameters.AddWithValue("@IsOpenClaim", ddlIsOpenClaim.SelectedValue);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@IsHomeLead", 0);
                            cmd.Parameters.AddWithValue("@IsFlooded", ddlIsFlooded.SelectedValue);
                            cmd.Parameters.AddWithValue("@IsOpenClaim", ddlIsOpenClaim.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
                        {
                            var birthdate = DateTime.Parse(txtDOB.Text.Trim());
                            var today = DateTime.Today;
                            var age = today.Year - birthdate.Year;
                            if (age >= 65)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4> Age is greater than 65.');", true);
                                return;
                            }
                            cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                            lead.DOB = txtDOB.Text.Trim();
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                            lead.DOB = txtDOB.Text.Trim();
                        }
                        cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                        cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                        cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                        cmd.Parameters.AddWithValue("@HOwnerShip", ddlHOwnerShip.SelectedValue);
                        cmd.Parameters.AddWithValue("@ContinousCoverage", ddlContinousCoverage.SelectedValue);
                        cmd.Parameters.AddWithValue("@MaritalStatus", ddlMaritalStatus.SelectedValue);
                        if (Session["OutsourceOwnerId"] != null)
                        {
                            cmd.Parameters.AddWithValue("@OutsourceOwnerId", Convert.ToInt32(Session["OutsourceOwnerId"]));
                            cmd.Parameters.AddWithValue("@IsOutsource", 1);

                            if (string.IsNullOrEmpty(ddlInsuranceCompany.SelectedValue.ToString()) || string.IsNullOrEmpty(ddlMake.SelectedValue.ToString()) ||
                                string.IsNullOrEmpty(txtModel.Text) || string.IsNullOrEmpty(txtYear.Text) || string.IsNullOrEmpty(txtPhone.Text))
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please fill the required fields first.');", true);
                                return;
                            }
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@OutsourceOwnerId", 0);
                            cmd.Parameters.AddWithValue("@IsOutsource", 0);
                        }
                        cmd.Parameters.AddWithValue("@AgencyOwnerId", Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                        cmd.Parameters.AddWithValue("@AgencyProducerId", Convert.ToInt32(ddlAgencyProducer.SelectedValue));
                        cmd.Parameters.AddWithValue("@DispositionId", 2);
                        cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text.Trim()));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        if (Session["UId"] != null)
                        {
                            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                            cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                if (ddlAgencyOwner.SelectedItem.Text == "Clint Hibbard")
                                {
                                    var live = PostLiveLead("https://leads.ricochet.me/api/v1/lead/create/Calls-Mavs-Leads?token=4f30d2b3e8e3632d76f7affe8d9b046e&imported=1", JsonConvert.SerializeObject(lead));
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + live + "');", true);
                                }
                                if (ddlAgencyOwner.SelectedItem.Text == "Michael Nocella")
                                {
                                    var live = PostLiveLead("https://leads.ricochet.me/api/v1/lead/create/mavrik/?token=545bd52ecf08db95b5a5243ded334f9b&cid=Auto", JsonConvert.SerializeObject(lead));
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + live + "');", true);
                                }
                                if (ddlAgencyOwner.SelectedItem.Text == "Paul PNP")
                                {
                                    var live = PostLiveLead("https://leads.ricochet.me/api/v1/lead/create/Maverick-Leads?token=b7c89f603750a3bc066fa88358ab2126&imported=1", JsonConvert.SerializeObject(lead));
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + live + "');", true);
                                }
                                if (ddlAgencyOwner.SelectedItem.Text == "John Wood")
                                {
                                    var live = PostLiveLead("https://leads.ricochet.me/api/v1/lead/create/MavLeads-Transfers?token=85611d54a202472c6a388ad66021ab8b&imported=1", JsonConvert.SerializeObject(lead));
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + live + "');", true);
                                }
                                else
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.');", true);

                                if (chkHomeLead.Checked)
                                {
                                    var dsAgToken = new PopulateDataSource();
                                    var dtAg = dsAgToken.DataTableSqlString("SELECT ao.Token FROM dbo.AgencyOwner ao WHERE ao.Id=" + Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                                    if (dtAg.Rows.Count > 0)
                                    {
                                        var filePath = HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.p12");

                                        var notify = new PushNotify();
                                        notify.SendPushNotification(dtAg.Rows[0]["Token"].ToString(), "Maverick Leads - New Home Ins. Lead in Portal", filePath);
                                    }

                                    var dsProToken = new PopulateDataSource();
                                    var dtPro = dsProToken.DataTableSqlString("SELECT ap.Token FROM dbo.AgencyProducer ap WHERE ap.Id=" + Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                                    if (dtPro.Rows.Count > 0)
                                    {
                                        var filePath = HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.p12");

                                        var notify = new PushNotify();
                                        notify.SendPushNotification(dtPro.Rows[0]["Token"].ToString(), "Maverick Leads - New Home Ins. Lead in Portal", filePath);
                                    }
                                }
                                else
                                {
                                    var dsAgToken = new PopulateDataSource();
                                    var dtAg = dsAgToken.DataTableSqlString("SELECT ao.Token FROM dbo.AgencyOwner ao WHERE ao.Id=" + Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                                    if (dtAg.Rows.Count > 0)
                                    {
                                        var filePath = HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.p12");

                                        var notify = new PushNotify();
                                        notify.SendPushNotification(dtAg.Rows[0]["Token"].ToString(), "Maverick Leads - New lead available", filePath);
                                    }

                                    var dsProToken = new PopulateDataSource();
                                    var dtPro = dsProToken.DataTableSqlString("SELECT ap.Token FROM dbo.AgencyProducer ap WHERE ap.Id=" + Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                                    if (dtPro.Rows.Count > 0)
                                    {
                                        var filePath = HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.p12");

                                        var notify = new PushNotify();
                                        notify.SendPushNotification(dtPro.Rows[0]["Token"].ToString(), "Maverick Leads - New lead available", filePath);
                                    }
                                }

                                Clear();
                                txtLeadId.Text = "";
                                txtViciLeadId.Text = "";
                                txtPhone.Text = "";
                                company = "";
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Save.');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogging.SendExcepToDb(ex);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Something Went Wrong.');", true);
            }
        }

        public void Clear()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtPhoneNo.Text = "";
            ddlInsuranceCompany.SelectedIndex = 0;
            ddlMake.SelectedIndex = 0;
            txtYearV2.Text = "";
            txtModelV2.Text = "";
            ddlMake2.SelectedIndex = 0;
            txtYear.Text = "";
            txtModel.Text = "";
            txtDOB.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtComments.Text = "";

            ddlAgencyOwner.SelectedIndex = 0;
            ddlAgencyProducer.SelectedIndex = 0;

            ddlDisposition.ClearSelection();
            ddlAgencyOwner.ClearSelection();
            ddlAgencyProducer.ClearSelection();

            txtLeadId.Text = "";
            txtViciLeadId.Text = "";
        }

        protected void ddlAgencyOwner_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(ddlAgencyOwner.SelectedItem.Text))
            {
                PopulateDdlAgencyProducer(Convert.ToInt32(ddlAgencyOwner.SelectedValue));

                if (ddlAgencyOwner.SelectedItem.Text.Trim() == "Dale Schueller" && ddlHOwnerShip.SelectedValue.ToString().Trim() == "False")
                {
                    btnSave.Enabled = false;
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>This Client only deal with Homeowner.');", true);
                }
                else
                {
                    btnSave.Enabled = true;
                }

                try
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ContactNo FROM dbo.AgencyOwner WHERE Id=" +
                                                   Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                    if (dt.Rows.Count > 0)
                    {
                        lblAgencyPhone.Text = dt.Rows[0]["ContactNo"].ToString().Trim();
                        pnlCopy.Visible = true;
                    }
                    else
                    {
                        lblAgencyPhone.Text = "";
                    }
                }
                catch (Exception exception)
                {
                    ExceptionLogging.SendExcepToDb(exception);
                }
            }
        }

        protected void btnGetInsuranceLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDuplicate("SELECT * FROM dbo.Insurance WHERE IsActive = 1 AND PhoneNo='" + txtPhone.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "warningMsg('Lead Already Submitted.');", true);
                    return;
                }
                if (Utility.ChkDuplicate("SELECT * FROM dbo.PingNPost WHERE IsActive = 1 AND PhoneNo='" + txtPhone.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "warningMsg('Lead Already Submitted.');", true);
                    return;
                }
                if (Utility.ChkDuplicate("SELECT * FROM dbo.PendingPNP WHERE IsActive = 1 AND PhoneNo='" + txtPhone.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                else
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT * FROM dbo.ViciLead WHERE IsActive= 1 AND Phone='" + txtPhone.Text.Trim() + "'");
                    if (dt.Rows.Count > 0)
                    {
                        txtViciLeadId.Text = "";
                        txtViciLeadId.Text = dt.Rows[0]["Id"].ToString();
                        txtViciLeadId.ReadOnly = true;

                        txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                        txtLName.Text = dt.Rows[0]["LastName"].ToString();
                        txtCity.Text = dt.Rows[0]["City"].ToString();
                        txtState.Text = dt.Rows[0]["State"].ToString();
                        txtZip.Text = dt.Rows[0]["ZipCode"].ToString();
                        txtPhoneNo.Text = dt.Rows[0]["Phone"].ToString();
                        txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                        txtEmail.Text = dt.Rows[0]["Email"].ToString();
                        txtAddress.Text = dt.Rows[0]["Address"].ToString();

                        ddlGender.ClearSelection();
                        ddlGender.SelectedIndex = ddlHOwnerShip.Items.IndexOf(ddlHOwnerShip.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                        ddlGender.DataBind();

                        PopulateDdlAgencyOwner(dt.Rows[0]["State"].ToString());
                        if (!string.IsNullOrWhiteSpace(ddlAgencyOwner.SelectedValue))
                        {
                            PopulateDdlAgencyProducer(Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                        }

                        Panel1.Visible = true;

                        txtState.Enabled = true;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void ddlDisposition_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDisposition.SelectedItem.Text == "Follow-Up/X-Date")
                pnlFollow.Visible = true;
        }

        protected void txtState_OnTextChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateDdlAgencyOwner(txtState.Text.Trim());
                if (!string.IsNullOrWhiteSpace(ddlAgencyOwner.SelectedItem.Text))
                {
                    PopulateDdlAgencyProducer(Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnPingNPost_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PingNPost WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PendingPNP WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Insurance WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                var isValid = false;
                var company = "";
                var source = new PopulateDataSource();
                var table = source.DataTableSqlString("SELECT Company FROM dbo.AgencyOwner WHERE Name='" + ddlAgencyOwner.SelectedItem.Text.Trim() + "'");
                if (table.Rows.Count > 0)
                {
                    company = table.Rows[0]["Company"].ToString();
                }
                if (!string.IsNullOrEmpty(ddlInsuranceCompany.SelectedItem.Text))
                {
                    if (company == ddlInsuranceCompany.SelectedItem.Text)
                    {
                        isValid = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Cannot Submit Lead to this Agency.');", true);
                        return;
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                if (isValid == false)
                {
                    #region RicochetPingLead

                    var lead = new RicochetLead();
                    lead.FirstName = txtFName.Text.Trim();
                    lead.LastName = txtLName.Text.Trim();
                    lead.Gender = ddlGender.SelectedValue;
                    lead.City = txtCity.Text.Trim();
                    lead.State = txtState.Text.Trim();
                    lead.Zip = txtZip.Text.Trim();
                    lead.PhoneNo = txtPhoneNo.Text.Trim();
                    lead.Phone_Number = txtPhoneNo.Text.Trim();
                    lead.Company = ddlInsuranceCompany.SelectedValue;
                    lead.Make = ddlMake.SelectedValue;
                    lead.Year = txtYear.Text.Trim();
                    lead.Model = txtModel.Text.Trim();
                    lead.Make2 = ddlMake2.SelectedValue;
                    lead.Model2 = txtYearV2.Text.Trim();
                    lead.Year2 = txtModelV2.Text.Trim();

                    lead.Email = txtEmail.Text.Trim();
                    lead.Address = txtAddress.Text.Trim();
                    lead.Comments = txtComments.Text.Trim();
                    lead.HOwnerShip = Convert.ToBoolean(ddlHOwnerShip.SelectedValue);
                    lead.ContinousCoverage = ddlContinousCoverage.SelectedValue;
                    lead.MaritalStatus = Convert.ToBoolean(ddlMaritalStatus.SelectedValue);
                    lead.DispositionId = 23;
                    lead.IsActive = true;

                    #endregion

                    var cmd = new SqlCommand("INSERT INTO PingNPost(FirstName, LastName, Gender, City, State, Zip, PhoneNo, Company, Make, Year, Model, Make2, Year2, Model2, DOB, Email, Address, Comments, HOwnerShip, ContinousCoverage, MaritalStatus, AgencyOwnerId, AgencyProducerId,DispositionId,ViciLeadId, IsActive, OutsourceOwnerId, IsOutsource, IsDemandPNP, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate,IsHomeLead,IsFlooded,IsOpenClaim)VALUES(@FirstName, @LastName, @Gender, @City, @State, @Zip, @PhoneNo, @Company, @Make, @Year, @Model, @Make2, @Year2, @Model2, @DOB, @Email, @Address, @Comments, @HOwnerShip, @ContinousCoverage, @MaritalStatus, @AgencyOwnerId, @AgencyProducerId, @DispositionId, @ViciLeadId, @IsActive, @OutsourceOwnerId, @IsOutsource, @IsDemandPNP, @CreatedBy, @CreatedDate, @LastModifiedBy, @LastModifiedDate,@IsHomeLead,@IsFlooded,@IsOpenClaim)");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                    cmd.Parameters.AddWithValue("@State", txtState.Text.Trim());
                    cmd.Parameters.AddWithValue("@Zip", txtZip.Text.Trim());
                    cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Company", ddlInsuranceCompany.SelectedValue);
                    cmd.Parameters.AddWithValue("@Make", ddlMake.SelectedValue);
                    cmd.Parameters.AddWithValue("@Year", txtYear.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model", txtModel.Text.Trim());
                    cmd.Parameters.AddWithValue("@Make2", ddlMake2.SelectedValue);
                    cmd.Parameters.AddWithValue("@Year2", txtYearV2.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model2", txtModelV2.Text.Trim());
                    if (chkHomeLead.Checked)
                    {
                        cmd.Parameters.AddWithValue("@IsHomeLead", 1);
                        cmd.Parameters.AddWithValue("@IsFlooded", ddlIsFlooded.SelectedValue);
                        cmd.Parameters.AddWithValue("@IsOpenClaim", ddlIsOpenClaim.SelectedValue);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@IsHomeLead", 0);
                        cmd.Parameters.AddWithValue("@IsFlooded", ddlIsFlooded.SelectedValue);
                        cmd.Parameters.AddWithValue("@IsOpenClaim", ddlIsOpenClaim.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
                    {
                        var birthdate = DateTime.Parse(txtDOB.Text.Trim());
                        var today = DateTime.Today;
                        var age = today.Year - birthdate.Year;
                        if (age >= 65)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4> Age is greater than 65.');", true);
                            return;
                        }
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                        lead.DOB = txtDOB.Text.Trim();
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                        lead.DOB = txtDOB.Text.Trim();
                    }
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                    cmd.Parameters.AddWithValue("@HOwnerShip", ddlHOwnerShip.SelectedValue);
                    cmd.Parameters.AddWithValue("@ContinousCoverage", ddlContinousCoverage.SelectedValue);
                    cmd.Parameters.AddWithValue("@MaritalStatus", ddlMaritalStatus.SelectedValue);
                    cmd.Parameters.AddWithValue("@AgencyOwnerId", Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                    cmd.Parameters.AddWithValue("@AgencyProducerId", 0);
                    cmd.Parameters.AddWithValue("@DispositionId", 23);
                    cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text.Trim()));
                    cmd.Parameters.AddWithValue("@IsActive", 1);

                    if (Session["OutsourceOwnerId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@OutsourceOwnerId", Convert.ToInt32(Session["OutsourceOwnerId"]));
                        cmd.Parameters.AddWithValue("@IsOutsource", 1);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@OutsourceOwnerId", 0);
                        cmd.Parameters.AddWithValue("@IsOutsource", 0);
                    }

                    #region IsDemandCheck
                    //if (chlLivePNP.Checked)
                    //{
                    //    lead.IsDemandPNP = true;

                    //    cmd.Parameters.AddWithValue("@IsDemandPNP", 1);
                    //    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    //    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

                    //    var ds = new PopulateDataSource();
                    //    var dt = ds.DataTableSqlString("SELECT Email FROM AgencyOwner WHERE Id=" + Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                    //    if (dt.Rows.Count > 0)
                    //    {
                    //        var email = dt.Rows[0]["Email"].ToString();

                    //        var thread = new Thread(delegate ()
                    //        {
                    //            var sb = new StringBuilder();
                    //            sb.Append("");
                    //            sb.Append("<div style=\"padding: 10px; background: #eff1f1; font-weight: bold;\">New Lead</div>");
                    //            sb.Append("<table style=\"margin-top: 30px;\">");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th>First Name</th>");
                    //            sb.Append("<td>" + txtFName.Text + "</td>");
                    //            sb.Append("<th>Last Name</th>");
                    //            sb.Append("<td>" + txtLName.Text + "</td>");
                    //            sb.Append("<th>Gender</th>");
                    //            sb.Append("<td>" + ddlGender.SelectedValue + "</td>");
                    //            sb.Append("<th>Date-of-Birth</th>");
                    //            sb.Append("<td>" + txtDOB.Text + "</td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th>City</th>");
                    //            sb.Append("<td>" + txtCity.Text + "</td>");
                    //            sb.Append("<th>State</th>");
                    //            sb.Append("<td>" + txtState.Text + "</td>");
                    //            sb.Append("<th>Zip</th>");
                    //            sb.Append("<td>" + txtZip.Text + "</td>");
                    //            sb.Append("<th>Address</th>");
                    //            sb.Append("<td>" + txtAddress.Text + "</td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th>Phone</th>");
                    //            sb.Append("<td>" + txtPhone.Text + "</td>");
                    //            sb.Append("<th>Company</th>");
                    //            sb.Append("<td>" + ddlInsuranceCompany.SelectedValue + "</td>");
                    //            sb.Append("<th>Disposition</th>");
                    //            sb.Append("<td>Lead</td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th>Email</th>");
                    //            sb.Append("<td>" + txtEmail.Text + "</td>");
                    //            sb.Append("<th>Marital Status</th>");
                    //            sb.Append("<td>" + ddlMaritalStatus.SelectedItem.Text + "</td>");
                    //            sb.Append("<th>Home Owner</th>");
                    //            sb.Append("<td>" + ddlHOwnerShip.SelectedItem.Text + "</td>");
                    //            sb.Append("<th>Continuous Coverage</th>");
                    //            sb.Append("<td>" + ddlContinousCoverage.SelectedValue + "</td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th><a href='http://mavleads.online/LivePNP.aspx'>View Lead</a></td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("</table>");

                    //            MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads", email, "", "", "Maverick Leads - New Lead in Portal", sb.ToString(), true);
                    //            MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads", "jason.smith@rapidratesonline.com", "", "", "Maverick Leads - New Lead in Portal", sb.ToString(), true);
                    //        });

                    //        thread.IsBackground = true;
                    //        thread.Start();
                    //    }
                    //}
                    //else
                    //{
                    //    lead.IsDemandPNP = false;
                    //    cmd.Parameters.AddWithValue("@IsDemandPNP", 0);
                    //    //cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")));
                    //    //cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")));

                    //    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    //    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

                    //    var ds = new PopulateDataSource();
                    //    var dt = ds.DataTableSqlString("SELECT Email FROM AgencyOwner WHERE Id=" + Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                    //    if (dt.Rows.Count > 0)
                    //    {
                    //        var email = dt.Rows[0]["Email"].ToString();

                    //        var thread = new Thread(delegate ()
                    //        {
                    //            var sb = new StringBuilder();
                    //            sb.Append("");
                    //            sb.Append("<div style=\"padding: 10px; background: #eff1f1; font-weight: bold;\">New Lead</div>");
                    //            sb.Append("<table style=\"margin-top: 30px;\">");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th>First Name</th>");
                    //            sb.Append("<td>" + txtFName.Text + "</td>");
                    //            sb.Append("<th>Last Name</th>");
                    //            sb.Append("<td>" + txtLName.Text + "</td>");
                    //            sb.Append("<th>Gender</th>");
                    //            sb.Append("<td>" + ddlGender.SelectedValue + "</td>");
                    //            sb.Append("<th>Date-of-Birth</th>");
                    //            sb.Append("<td>" + txtDOB.Text + "</td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th>City</th>");
                    //            sb.Append("<td>" + txtCity.Text + "</td>");
                    //            sb.Append("<th>State</th>");
                    //            sb.Append("<td>" + txtState.Text + "</td>");
                    //            sb.Append("<th>Zip</th>");
                    //            sb.Append("<td>" + txtZip.Text + "</td>");
                    //            sb.Append("<th>Address</th>");
                    //            sb.Append("<td>" + txtAddress.Text + "</td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th>Phone</th>");
                    //            sb.Append("<td>" + txtPhone.Text + "</td>");
                    //            sb.Append("<th>Company</th>");
                    //            sb.Append("<td>" + ddlInsuranceCompany.SelectedValue + "</td>");
                    //            sb.Append("<th>Disposition</th>");
                    //            sb.Append("<td>Lead</td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th>Email</th>");
                    //            sb.Append("<td>" + txtEmail.Text + "</td>");
                    //            sb.Append("<th>Marital Status</th>");
                    //            sb.Append("<td>" + ddlMaritalStatus.SelectedItem.Text + "</td>");
                    //            sb.Append("<th>Home Owner</th>");
                    //            sb.Append("<td>" + ddlHOwnerShip.SelectedItem.Text + "</td>");
                    //            sb.Append("<th>Continuous Coverage</th>");
                    //            sb.Append("<td>" + ddlContinousCoverage.SelectedValue + "</td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("<tr>");
                    //            sb.Append("<th><a href='http://mavleads.online/LivePNP.aspx'>View Lead</a></td>");
                    //            sb.Append("</tr>");

                    //            sb.Append("</table>");

                    //            MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads", email, "", "", "Maverick Leads - New Lead in Portal", sb.ToString(), true);
                    //            MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads", "jason.smith@rapidratesonline.com", "", "", "Maverick Leads - New Lead in Portal", sb.ToString(), true);
                    //        });

                    //        thread.IsBackground = true;
                    //        thread.Start();
                    //    }
                    //}
                    #endregion

                    if (Session["UId"] != null)
                    {
                        lead.IsDemandPNP = false;
                        cmd.Parameters.AddWithValue("@IsDemandPNP", 0);
                        //cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")));
                        //cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")));

                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT Email FROM AgencyOwner WHERE Id=" + Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                        if (dt.Rows.Count > 0)
                        {
                            var email = dt.Rows[0]["Email"].ToString();

                            var thread = new Thread(delegate ()
                            {
                                if (chkHomeLead.Checked)
                                {
                                    var sb = new StringBuilder();
                                    sb.Append("");
                                    sb.Append("<div style=\"padding: 10px; background: #eff1f1; font-weight: bold;\">Maverick Leads - New Home Ins. Lead in Portal</div>");
                                    sb.Append("<table style=\"margin-top: 30px;\">");

                                    sb.Append("<tr>");
                                    sb.Append("<th>First Name</th>");
                                    sb.Append("<td>" + txtFName.Text + "</td>");
                                    sb.Append("<th>Last Name</th>");
                                    sb.Append("<td>" + txtLName.Text + "</td>");
                                    sb.Append("<th>Gender</th>");
                                    sb.Append("<td>" + ddlGender.SelectedValue + "</td>");
                                    sb.Append("<th>Date-of-Birth</th>");
                                    sb.Append("<td>" + txtDOB.Text + "</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<th>City</th>");
                                    sb.Append("<td>" + txtCity.Text + "</td>");
                                    sb.Append("<th>State</th>");
                                    sb.Append("<td>" + txtState.Text + "</td>");
                                    sb.Append("<th>Zip</th>");
                                    sb.Append("<td>" + txtZip.Text + "</td>");
                                    sb.Append("<th>Address</th>");
                                    sb.Append("<td>" + txtAddress.Text + "</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<th>Phone</th>");
                                    sb.Append("<td>" + txtPhone.Text + "</td>");
                                    sb.Append("<th>Company</th>");
                                    sb.Append("<td>" + ddlInsuranceCompany.SelectedValue + "</td>");
                                    sb.Append("<th>Disposition</th>");
                                    sb.Append("<td>Lead</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<th>Email</th>");
                                    sb.Append("<td>" + txtEmail.Text + "</td>");
                                    sb.Append("<th>Marital Status</th>");
                                    sb.Append("<td>" + ddlMaritalStatus.SelectedItem.Text + "</td>");
                                    sb.Append("<th>Home Owner</th>");
                                    sb.Append("<td>" + ddlHOwnerShip.SelectedItem.Text + "</td>");
                                    sb.Append("<th>Continuous Coverage</th>");
                                    sb.Append("<td>" + ddlContinousCoverage.SelectedValue + "</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<th><a href='http://mavleads.online/LivePNP.aspx'>View Lead</a></td>");
                                    sb.Append("</tr>");

                                    sb.Append("</table>");

                                    MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads - New Home Ins. Lead in Portal", email, "", "", "Maverick Leads - New Home Ins. Lead in Portal", sb.ToString(), true);
                                    MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads - New Home Ins. Lead in Portal", "jason.smith@rapidratesonline.com", "", "", "Maverick Leads - New Home Ins. Lead in Portal", sb.ToString(), true);
                                }
                                else
                                {
                                    var sb = new StringBuilder();
                                    sb.Append("");
                                    sb.Append("<div style=\"padding: 10px; background: #eff1f1; font-weight: bold;\">Maverick Leads - New Lead in Portal</div>");
                                    sb.Append("<table style=\"margin-top: 30px;\">");

                                    sb.Append("<tr>");
                                    sb.Append("<th>First Name</th>");
                                    sb.Append("<td>" + txtFName.Text + "</td>");
                                    sb.Append("<th>Last Name</th>");
                                    sb.Append("<td>" + txtLName.Text + "</td>");
                                    sb.Append("<th>Gender</th>");
                                    sb.Append("<td>" + ddlGender.SelectedValue + "</td>");
                                    sb.Append("<th>Date-of-Birth</th>");
                                    sb.Append("<td>" + txtDOB.Text + "</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<th>City</th>");
                                    sb.Append("<td>" + txtCity.Text + "</td>");
                                    sb.Append("<th>State</th>");
                                    sb.Append("<td>" + txtState.Text + "</td>");
                                    sb.Append("<th>Zip</th>");
                                    sb.Append("<td>" + txtZip.Text + "</td>");
                                    sb.Append("<th>Address</th>");
                                    sb.Append("<td>" + txtAddress.Text + "</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<th>Phone</th>");
                                    sb.Append("<td>" + txtPhone.Text + "</td>");
                                    sb.Append("<th>Company</th>");
                                    sb.Append("<td>" + ddlInsuranceCompany.SelectedValue + "</td>");
                                    sb.Append("<th>Disposition</th>");
                                    sb.Append("<td>Lead</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<th>Email</th>");
                                    sb.Append("<td>" + txtEmail.Text + "</td>");
                                    sb.Append("<th>Marital Status</th>");
                                    sb.Append("<td>" + ddlMaritalStatus.SelectedItem.Text + "</td>");
                                    sb.Append("<th>Home Owner</th>");
                                    sb.Append("<td>" + ddlHOwnerShip.SelectedItem.Text + "</td>");
                                    sb.Append("<th>Continuous Coverage</th>");
                                    sb.Append("<td>" + ddlContinousCoverage.SelectedValue + "</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<th><a href='http://mavleads.online/LivePNP.aspx'>View Lead</a></td>");
                                    sb.Append("</tr>");

                                    sb.Append("</table>");

                                    MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads", email, "", "", "Maverick Leads - New Lead in Portal", sb.ToString(), true);
                                    MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads", "jason.smith@rapidratesonline.com", "", "", "Maverick Leads - New Lead in Portal", sb.ToString(), true);
                                }
                            });

                            thread.IsBackground = true;
                            thread.Start();
                        }

                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));

                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            if (ddlAgencyOwner.SelectedItem.Text == "Clint Hibbard Ping Only")
                            {
                                var url = "https://leads.ricochet.me/api/v1/lead/create/Internet-Maverick-Data-Leads/?token=4f30d2b3e8e3632d76f7affe8d9b046e&cid=Internet-Maverick-Data-Leads-auto";
                                var data = PostPingLead(url, JsonConvert.SerializeObject(lead));
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + data + "');", true);
                            }
                            if (ddlAgencyOwner.SelectedItem.Text == "Don Kaminski")
                            {
                                var url = "https://leads.ricochet.me/api/v1/lead/create/other?token=9219d521767181b33c69c981725ab18e";
                                var data = PostPingLead(url, JsonConvert.SerializeObject(lead));
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + data + "');", true);
                            }
                            if (ddlAgencyOwner.SelectedItem.Text == "Eric Spring")
                            {
                                var url = "https://leads.ricochet.me/api/v1/lead/create/Maverick-Ping-leads?token=3d4abdb90bd13725ef7a895789a1b099";
                                var data = PostPingLead(url, JsonConvert.SerializeObject(lead));
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + data + "');", true);
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.');", true);

                            var dsAgToken = new PopulateDataSource();
                            var dtAg = dsAgToken.DataTableSqlString("SELECT ao.Token FROM dbo.AgencyOwner ao WHERE ao.Id=" + Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                            if (dtAg.Rows.Count > 0)
                            {
                                var filePath = HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.p12");

                                var notify = new PushNotify();
                                notify.SendPushNotification(dtAg.Rows[0]["Token"].ToString(), "Maverick Leads - New lead available", filePath);
                            }

                            Clear();
                            txtLeadId.Text = "";
                            txtViciLeadId.Text = "";
                            txtPhone.Text = "";
                            company = "";
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Saved.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnPendingSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PendingPNP WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PingNPost WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Insurance WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                var isValid = false;
                var company = "";
                var source = new PopulateDataSource();
                var table = source.DataTableSqlString("SELECT Company FROM dbo.AgencyOwner WHERE Name='" + ddlAgencyOwner.SelectedItem.Text.Trim() + "'");
                if (table.Rows.Count > 0)
                {
                    company = table.Rows[0]["Company"].ToString();
                }
                if (!string.IsNullOrEmpty(ddlInsuranceCompany.SelectedItem.Text))
                {
                    if (company == ddlInsuranceCompany.SelectedItem.Text)
                    {
                        isValid = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Cannot Submit Lead to this Agency.');", true);
                        return;
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                if (isValid == false)
                {
                    var cmd = new SqlCommand("INSERT INTO PendingPNP(FirstName, LastName, Gender, City, State, Zip, PhoneNo, Company, Make, Year, Model, Make2, Year2, Model2, DOB, Email, Address, Comments, HOwnerShip, ContinousCoverage, MaritalStatus, AgencyOwnerId, AgencyProducerId,DispositionId,ViciLeadId, IsActive, OutsourceOwnerId, IsOutsource, IsDemandPNP, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate,IsHomeLead,IsFlooded,IsOpenClaim)VALUES(@FirstName, @LastName, @Gender, @City, @State, @Zip, @PhoneNo, @Company, @Make, @Year, @Model, @Make2, @Year2, @Model2, @DOB, @Email, @Address, @Comments, @HOwnerShip, @ContinousCoverage, @MaritalStatus, @AgencyOwnerId, @AgencyProducerId, @DispositionId, @ViciLeadId, @IsActive, @OutsourceOwnerId, @IsOutsource, @IsDemandPNP, @CreatedBy, @CreatedDate, @LastModifiedBy, @LastModifiedDate,@IsHomeLead,@IsFlooded,@IsOpenClaim)");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                    cmd.Parameters.AddWithValue("@State", txtState.Text.Trim());
                    cmd.Parameters.AddWithValue("@Zip", txtZip.Text.Trim());
                    cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Company", ddlInsuranceCompany.SelectedValue);
                    cmd.Parameters.AddWithValue("@Make", ddlMake.SelectedValue);
                    cmd.Parameters.AddWithValue("@Year", txtYear.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model", txtModel.Text.Trim());
                    cmd.Parameters.AddWithValue("@Make2", ddlMake2.SelectedValue);
                    cmd.Parameters.AddWithValue("@Year2", txtYearV2.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model2", txtModelV2.Text.Trim());
                    if (chkHomeLead.Checked)
                    {
                        cmd.Parameters.AddWithValue("@IsHomeLead", 1);
                        cmd.Parameters.AddWithValue("@IsFlooded", ddlIsFlooded.SelectedValue);
                        cmd.Parameters.AddWithValue("@IsOpenClaim", ddlIsOpenClaim.SelectedValue);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@IsHomeLead", 0);
                        cmd.Parameters.AddWithValue("@IsFlooded", ddlIsFlooded.SelectedValue);
                        cmd.Parameters.AddWithValue("@IsOpenClaim", ddlIsOpenClaim.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
                    {
                        var birthdate = DateTime.Parse(txtDOB.Text.Trim());
                        var today = DateTime.Today;
                        var age = today.Year - birthdate.Year;
                        if (age >= 65)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4> Age is greater than 65.');", true);
                            return;
                        }
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                    }
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                    cmd.Parameters.AddWithValue("@HOwnerShip", ddlHOwnerShip.SelectedValue);
                    cmd.Parameters.AddWithValue("@ContinousCoverage", ddlContinousCoverage.SelectedValue);
                    cmd.Parameters.AddWithValue("@MaritalStatus", ddlMaritalStatus.SelectedValue);
                    cmd.Parameters.AddWithValue("@AgencyOwnerId", Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                    cmd.Parameters.AddWithValue("@AgencyProducerId", 0);
                    cmd.Parameters.AddWithValue("@DispositionId", 23);
                    cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text.Trim()));
                    cmd.Parameters.AddWithValue("@IsActive", 1);

                    if (Session["OutsourceOwnerId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@OutsourceOwnerId", Convert.ToInt32(Session["OutsourceOwnerId"]));
                        cmd.Parameters.AddWithValue("@IsOutsource", 1);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@OutsourceOwnerId", 0);
                        cmd.Parameters.AddWithValue("@IsOutsource", 0);
                    }
                    //if (chlLivePNP.Checked)
                    //{
                    //    cmd.Parameters.AddWithValue("@IsDemandPNP", 1);
                    //    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    //    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    //}
                    //else
                    //{
                    //    cmd.Parameters.AddWithValue("@IsDemandPNP", 0);
                    //    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")));
                    //    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")));
                    //}
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@IsDemandPNP", 1);
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));

                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.');", true);

                            Clear();
                            txtLeadId.Text = "";
                            txtViciLeadId.Text = "";
                            txtPhone.Text = "";
                            company = "";
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Saved.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnRicochetSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.RicochetLive WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.RicochetLeads WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PingNPost WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PendingPNP WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Insurance WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                var isValid = false;
                var company = "";
                var source = new PopulateDataSource();
                var table = source.DataTableSqlString("SELECT Company FROM dbo.AgencyOwner WHERE Name='" + ddlAgencyOwner.SelectedItem.Text.Trim() + "'");
                if (table.Rows.Count > 0)
                {
                    company = table.Rows[0]["Company"].ToString();
                }
                if (!string.IsNullOrEmpty(ddlInsuranceCompany.SelectedItem.Text))
                {
                    if (company == ddlInsuranceCompany.SelectedItem.Text)
                    {
                        isValid = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Cannot Submit Lead to this Agency.');", true);
                        return;
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                if (isValid == false)
                {
                    var lead = new RicochetLead();

                    var cmd = new SqlCommand("INSERT INTO RicochetLeads(FirstName, LastName, Gender, City, State, Zip, PhoneNo, Company, Make, Year, Model, Make2, Year2, Model2, DOB, Email, Address, Comments, HOwnerShip, ContinousCoverage, MaritalStatus, AgencyOwnerId, AgencyProducerId,DispositionId,ViciLeadId, IsActive, OutsourceOwnerId, IsOutsource, IsDemandPNP, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)VALUES(@FirstName, @LastName, @Gender, @City, @State, @Zip, @PhoneNo, @Company, @Make, @Year, @Model, @Make2, @Year2, @Model2, @DOB, @Email, @Address, @Comments, @HOwnerShip, @ContinousCoverage, @MaritalStatus, @AgencyOwnerId, @AgencyProducerId, @DispositionId, @ViciLeadId, @IsActive, @OutsourceOwnerId, @IsOutsource, @IsDemandPNP, @CreatedBy, @CreatedDate, @LastModifiedBy, @LastModifiedDate)");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                    cmd.Parameters.AddWithValue("@State", txtState.Text.Trim());
                    cmd.Parameters.AddWithValue("@Zip", txtZip.Text.Trim());
                    cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Company", ddlInsuranceCompany.SelectedValue);
                    cmd.Parameters.AddWithValue("@Make", ddlMake.SelectedValue);
                    cmd.Parameters.AddWithValue("@Year", txtYear.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model", txtModel.Text.Trim());
                    cmd.Parameters.AddWithValue("@Make2", ddlMake2.SelectedValue);
                    cmd.Parameters.AddWithValue("@Year2", txtYearV2.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model2", txtModelV2.Text.Trim());

                    lead.FirstName = txtFName.Text.Trim();
                    lead.LastName = txtLName.Text.Trim();
                    lead.Gender = ddlGender.SelectedValue;
                    lead.City = txtCity.Text.Trim();
                    lead.State = txtState.Text.Trim();
                    lead.Zip = txtZip.Text.Trim();
                    lead.PhoneNo = txtPhoneNo.Text.Trim();
                    lead.Phone_Number = txtPhoneNo.Text.Trim();
                    lead.Company = ddlInsuranceCompany.SelectedValue;
                    lead.Make = ddlMake.SelectedValue;
                    lead.Year = txtYear.Text.Trim();
                    lead.Model = txtModel.Text.Trim();
                    lead.Make2 = ddlMake2.SelectedValue;
                    lead.Model2 = txtYearV2.Text.Trim();
                    lead.Year2 = txtModelV2.Text.Trim();

                    if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
                    {
                        var birthdate = DateTime.Parse(txtDOB.Text.Trim());
                        var today = DateTime.Today;
                        var age = today.Year - birthdate.Year;
                        if (age >= 65)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4> Age is greater than 65.');", true);
                            return;
                        }
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                        lead.DOB = txtDOB.Text.Trim();
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                        lead.DOB = txtDOB.Text.Trim();
                    }
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                    cmd.Parameters.AddWithValue("@HOwnerShip", ddlHOwnerShip.SelectedValue);
                    cmd.Parameters.AddWithValue("@ContinousCoverage", ddlContinousCoverage.SelectedValue);
                    cmd.Parameters.AddWithValue("@MaritalStatus", ddlMaritalStatus.SelectedValue);
                    cmd.Parameters.AddWithValue("@AgencyOwnerId", 0);
                    cmd.Parameters.AddWithValue("@AgencyProducerId", 0);
                    cmd.Parameters.AddWithValue("@DispositionId", 23);
                    cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text.Trim()));
                    cmd.Parameters.AddWithValue("@IsActive", 1);

                    lead.Email = txtEmail.Text.Trim();
                    lead.Address = txtAddress.Text.Trim();
                    lead.Comments = txtComments.Text.Trim();
                    lead.HOwnerShip = Convert.ToBoolean(ddlHOwnerShip.SelectedValue);
                    lead.ContinousCoverage = ddlContinousCoverage.SelectedValue;
                    lead.MaritalStatus = Convert.ToBoolean(ddlMaritalStatus.SelectedValue);
                    lead.DispositionId = 23;

                    if (Session["OutsourceOwnerId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@OutsourceOwnerId", Convert.ToInt32(Session["OutsourceOwnerId"]));
                        cmd.Parameters.AddWithValue("@IsOutsource", 1);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@OutsourceOwnerId", 0);
                        cmd.Parameters.AddWithValue("@IsOutsource", 0);
                    }
                    //if (chlLivePNP.Checked)
                    //{
                    //    cmd.Parameters.AddWithValue("@IsDemandPNP", 1);
                    //    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    //    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

                    //    lead.IsDemandPNP = true;
                    //}
                    //else
                    //{
                    //    cmd.Parameters.AddWithValue("@IsDemandPNP", 0);
                    //    lead.IsDemandPNP = false;

                    //    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    //    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    //}

                    lead.IsActive = true;

                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@IsDemandPNP", 1);
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

                        lead.IsDemandPNP = true;

                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            var data = PostDataLead(JsonConvert.SerializeObject(lead));
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + data + "');", true);

                            Clear();
                            txtLeadId.Text = "";
                            txtViciLeadId.Text = "";
                            txtPhone.Text = "";
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Saved.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected string PostDataLead(string json)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://leads.ricochet.me/api/v1/lead/create/Internet-Maverick-Data-Leads/?token=4f30d2b3e8e3632d76f7affe8d9b046e&cid=Internet-Maverick-Data-Leads-auto");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var result = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            return result;
        }

        protected void btnRicochetLive_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.RicochetLive WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.RicochetLeads WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PingNPost WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.PendingPNP WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Insurance WHERE PhoneNo='" + txtPhoneNo.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Already Submitted');", true);
                    return;
                }
                var isValid = false;
                var company = "";
                var source = new PopulateDataSource();
                var table = source.DataTableSqlString("SELECT Company FROM dbo.AgencyOwner WHERE Name='" + ddlAgencyOwner.SelectedItem.Text.Trim() + "'");
                if (table.Rows.Count > 0)
                {
                    company = table.Rows[0]["Company"].ToString();
                }
                if (!string.IsNullOrEmpty(ddlInsuranceCompany.SelectedItem.Text))
                {
                    if (company == ddlInsuranceCompany.SelectedItem.Text)
                    {
                        isValid = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Cannot Submit Lead to this Agency.');", true);
                        return;
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                if (isValid == false)
                {
                    var lead = new RicochetLive();

                    var cmd = new SqlCommand("INSERT INTO RicochetLive(FirstName, LastName, Gender, City, State, Zip, PhoneNo, Company, Make, Year, Model, Make2, Year2, Model2, DOB, Email, Address, Comments, HOwnerShip, ContinousCoverage, MaritalStatus, AgencyOwnerId, AgencyProducerId,DispositionId,ViciLeadId, IsActive, OutsourceOwnerId, IsOutsource, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)VALUES(@FirstName, @LastName, @Gender, @City, @State, @Zip, @PhoneNo, @Company, @Make, @Year, @Model, @Make2, @Year2, @Model2, @DOB, @Email, @Address, @Comments, @HOwnerShip, @ContinousCoverage, @MaritalStatus, @AgencyOwnerId, @AgencyProducerId, @DispositionId, @ViciLeadId, @IsActive, @OutsourceOwnerId, @IsOutsource, @CreatedBy, @CreatedDate, @LastModifiedBy, @LastModifiedDate)");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                    cmd.Parameters.AddWithValue("@State", txtState.Text.Trim());
                    cmd.Parameters.AddWithValue("@Zip", txtZip.Text.Trim());
                    cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Company", ddlInsuranceCompany.SelectedValue);
                    cmd.Parameters.AddWithValue("@Make", ddlMake.SelectedValue);
                    cmd.Parameters.AddWithValue("@Year", txtYear.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model", txtModel.Text.Trim());
                    cmd.Parameters.AddWithValue("@Make2", ddlMake2.SelectedValue);
                    cmd.Parameters.AddWithValue("@Year2", txtYearV2.Text.Trim());
                    cmd.Parameters.AddWithValue("@Model2", txtModelV2.Text.Trim());

                    lead.FirstName = txtFName.Text.Trim();
                    lead.LastName = txtLName.Text.Trim();
                    lead.Gender = ddlGender.SelectedValue;
                    lead.City = txtCity.Text.Trim();
                    lead.State = txtState.Text.Trim();
                    lead.Zip = txtZip.Text.Trim();
                    lead.PhoneNo = txtPhoneNo.Text.Trim();
                    lead.Phone_Number = txtPhoneNo.Text.Trim();
                    lead.Company = ddlInsuranceCompany.SelectedValue;
                    lead.Make = ddlMake.SelectedValue;
                    lead.Year = txtYear.Text.Trim();
                    lead.Model = txtModel.Text.Trim();
                    lead.Make2 = ddlMake2.SelectedValue;
                    lead.Model2 = txtYearV2.Text.Trim();
                    lead.Year2 = txtModelV2.Text.Trim();

                    if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
                    {
                        var birthdate = DateTime.Parse(txtDOB.Text.Trim());
                        var today = DateTime.Today;
                        var age = today.Year - birthdate.Year;
                        if (age >= 65)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4> Age is greater than 65.');", true);
                            return;
                        }
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                        lead.DOB = txtDOB.Text.Trim();
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                        lead.DOB = txtDOB.Text.Trim();
                    }
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                    cmd.Parameters.AddWithValue("@HOwnerShip", ddlHOwnerShip.SelectedValue);
                    cmd.Parameters.AddWithValue("@ContinousCoverage", ddlContinousCoverage.SelectedValue);
                    cmd.Parameters.AddWithValue("@MaritalStatus", ddlMaritalStatus.SelectedValue);
                    cmd.Parameters.AddWithValue("@AgencyOwnerId", 0);
                    cmd.Parameters.AddWithValue("@AgencyProducerId", 0);
                    cmd.Parameters.AddWithValue("@DispositionId", 23);
                    cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text.Trim()));
                    cmd.Parameters.AddWithValue("@IsActive", 1);

                    lead.Email = txtEmail.Text.Trim();
                    lead.Address = txtAddress.Text.Trim();
                    lead.Comments = txtComments.Text.Trim();
                    lead.HOwnerShip = Convert.ToBoolean(ddlHOwnerShip.SelectedValue);
                    lead.ContinousCoverage = ddlContinousCoverage.SelectedValue;
                    lead.MaritalStatus = Convert.ToBoolean(ddlMaritalStatus.SelectedValue);
                    lead.DispositionId = 23;

                    if (Session["OutsourceOwnerId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@OutsourceOwnerId", Convert.ToInt32(Session["OutsourceOwnerId"]));
                        cmd.Parameters.AddWithValue("@IsOutsource", 1);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@OutsourceOwnerId", 0);
                        cmd.Parameters.AddWithValue("@IsOutsource", 0);
                    }

                    lead.IsActive = true;

                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            var live = PostLiveLead("https://leads.ricochet.me/api/v1/lead/create/Maverick-Leads?token=b7c89f603750a3bc066fa88358ab2126&imported=1", JsonConvert.SerializeObject(lead));
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Save Successfully.<br/>" + live + "');", true);

                            Clear();
                            txtLeadId.Text = "";
                            txtViciLeadId.Text = "";
                            txtPhone.Text = "";
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Saved.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        #region PingLead
        protected string PostPingLead(string url, string json)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var result = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            return result;
        }
        #endregion

        #region LiveTransfer
        protected string PostLiveLead(string url, string json)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var result = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            return result;
        }
        #endregion
    }
}