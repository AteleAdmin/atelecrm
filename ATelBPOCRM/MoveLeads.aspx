﻿<%@ Page Title="Move Leads" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="MoveLeads.aspx.cs" Inherits="ATelBPOCRM.MoveLeads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="backend/multi-select/multi-select.css" rel="stylesheet" />
    <style type="text/css">
        .ms-container {
            width: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Move Leads</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Leads</a></li>
            <li class="active">Move Leads</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <asp:Panel ID="pnlDateRange" runat="server" Visible="True">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Date Range</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" style="margin-top: 25px !important;">
                                        <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control" AutoPostBack="True" OnTextChanged="ddlFilter_OnTextChanged">
                                            <asp:ListItem Value="0">Filter</asp:ListItem>
                                            <asp:ListItem Value="Agency Owner">Agency Owner</asp:ListItem>
                                            <asp:ListItem Value="Phone No">Phone No</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>From :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ControlToValidate="txtFrom" ForeColor="red" ValidationGroup="G2"></asp:RequiredFieldValidator>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>To :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ControlToValidate="txtTo" ForeColor="red" ValidationGroup="G2"></asp:RequiredFieldValidator>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnlAgency" runat="server" Visible="False">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>From Agency Owner</label>
                                            <asp:DropDownList ID="ddlFromAgency" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>To Agency Owner</label>
                                            <asp:DropDownList ID="ddlToAgency" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlPhone" runat="server" Visible="False">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Phone No (Comma (,) Separated)</label>
                                            <asp:TextBox ID="txtPhoneNo" TextMode="MultiLine" Rows="1" CssClass="form-control" placeholder="Phone No with Comma (,) separated" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group" style="margin-top: 23px;">
                                        <asp:Button ID="btnGetLeads" runat="server" Text="Get Leads" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnGetLeads_OnClick" ValidationGroup="G2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!--// col-md-12 -->
            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leads</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:ListBox ID="ddlField" runat="server" CssClass="form-control select2" SelectionMode="Multiple"></asp:ListBox>
                        <br />
                        <div class="col-md-4">
                            <label>Move Date</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ControlToValidate="txtMoveDate" ForeColor="red" ValidationGroup="G1"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtMoveDate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Button ID="btnSubmit" runat="server" Text="Move" CssClass="btn btn-primary btn-block" OnClick="btnSubmit_OnClick" Style="margin-top: 22px;" ValidationGroup="G1" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div style="padding: 10px; width: 100%; background: #111; margin-bottom: 5px; color: #fff; font-weight: bold; text-align: center;">Move Disconnected & Ineligible</div>
            </div>
            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Move Leads</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ControlToValidate="txtMFrom" ForeColor="red" ValidationGroup="G3"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtMFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To :</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ControlToValidate="txtMTo" ForeColor="red" ValidationGroup="G3"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtMTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'"></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <asp:Button ID="btnPNPMove" runat="server" Text="Move" CssClass="btn btn-primary btn-block" OnClick="btnPNPMove_OnClick" Style="margin-top: 22px;" ValidationGroup="G3" />
                        </div>
                    </div>
                </div>
            </div>
            <!--// col-md-12 -->
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="backend/multi-select/jquery.multi-select.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.select2').multiSelect();
        })
    </script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>
</asp:Content>
