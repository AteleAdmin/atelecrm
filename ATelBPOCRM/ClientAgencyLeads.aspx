﻿<%@ Page Title="More Leads" Language="C#" MasterPageFile="~/ClientPanel.Master" AutoEventWireup="true" CodeBehind="ClientAgencyLeads.aspx.cs" Inherits="ATelBPOCRM.ClientAgencyLeads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dataTable th, td {
            white-space: nowrap;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>From :</label>
                        <div class="input-group">
                            <%--<div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>--%>
                            <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>To :</label>
                        <div class="input-group">
                            <%-- <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>--%>
                            <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>
                <div class="col-md-2">
                    <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-primary mb-2" OnClick="btnStats_Click" style="margin-top:31px;" />
                </div>
        </div>
        <div class="row layout-top-spacing" id="cancel-row">

            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <div class="table-responsive mb-4 mt-4">
                        <asp:GridView ID="gvAllLeads" CssClass="table table-bordered table-striped dataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowCreated="gvAllLeads_OnRowCreated" OnRowDataBound="gvAllLeads_OnRowDataBound" Style="width: 100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Lead Id">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <a href="UpdateClientLead.aspx?LeadId=<%# Eval("Id") %>" style="color:blue;">
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltN" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="State">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Zip">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Company">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Year">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Model">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agency">
                                    <ItemTemplate>
                                        <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Producer">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Homeowner">
                                    <ItemTemplate>
                                        <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Disposition">
                                    <ItemTemplate>
                                        <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="ltAddress" CssClass="label label-default" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>
</asp:Content>
