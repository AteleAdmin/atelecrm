﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;
using System.Web.UI;
namespace ATelBPOCRM
{
    public partial class ModulesPermission : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateRoles();
                PopulateModules();
                PopulateModulePermission(0);
            }
            else
            {
                //
            }
        }

        public void PopulateModulePermission(int? roleId = 0)
        {
            try
            {
                var query = "";
                if (roleId > 0)
                    query = "SELECT mp.Id,m.Name AS Module,r.Name AS Role,mp.IsActive AS Active FROM dbo.ModulesPermission mp JOIN dbo.Module m ON mp.ModuleId= m.Id JOIN dbo.Roles r  ON mp.RoleId =r.Id WHERE mp.RoleId=" + roleId;
                else
                    query = "SELECT mp.Id,m.Name AS Module,r.Name AS Role,mp.IsActive AS Active FROM dbo.ModulesPermission mp JOIN dbo.Module m ON mp.ModuleId= m.Id JOIN dbo.Roles r  ON mp.RoleId =r.Id";
                var ds = new Lucky.General.PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = dt;
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception e)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateRoles()
        {
            try
            {
                var ds = new Lucky.General.PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Roles WHERE IsActive=1 AND IsSuperAdmin=0");
                if (dt.Rows.Count > 0)
                {
                    ddlRole.DataSource = dt;
                    ddlRole.DataTextField = "Name";
                    ddlRole.DataValueField = "Id";
                    ddlRole.DataBind();

                    ddlRoles.DataSource = dt;
                    ddlRoles.DataTextField = "Name";
                    ddlRoles.DataValueField = "Id";
                    ddlRoles.DataBind();
                    ddlRoles.Items.Insert(0, " Select Role ");
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void PopulateModules()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Module WHERE IsActive =1");
                if (dt.Rows.Count > 0)
                {
                    ddlModule.DataSource = dt;
                    ddlModule.DataTextField = "Name";
                    ddlModule.DataValueField = "Id";
                    ddlModule.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            var flag = 0;
            foreach (ListItem item in ddlModule.Items)
            {
                if (item.Selected == true)
                {
                    var cmd = new SqlCommand(
                        "INSERT INTO dbo.ModulesPermission(RoleId, ModuleId, IsActive, CreatedDate, UpdatedDate)VALUES(@RoleId, @ModuleId, @IsActive, @CreatedDate, @UpdatedDate)",
                        Connection.Db());
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.ModulesPermission WHERE RoleId=" +
                                                      Convert.ToInt32(ddlRole.SelectedValue) + "AND ModuleId=" +
                                                      Convert.ToInt32(item.Value)) > 0)
                    {

                    }
                    else
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@RoleId", Convert.ToInt32(ddlRole.SelectedValue));
                        cmd.Parameters.AddWithValue("@ModuleId", Convert.ToInt32(item.Value));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        cmd.Parameters.AddWithValue("@CreatedDate",
                            DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@UpdatedDate",
                            DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        flag = cmd.ExecuteNonQuery() > 0 ? 1 : 0;
                    }
                }


                var cmd1 = new SqlCommand(
                    "INSERT INTO dbo.RolesPermissions(RoleId, SubModuleId, IsActive, CreatedDate, UpdatedDate)VALUES(@RoleId, @SubModuleId, @IsActive, @CreatedDate, @UpdatedDate)",
                    Connection.Db());
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT sm.Id FROM dbo.SubModule sm WHERE ModuleId=" +
                                               Convert.ToInt32(item.Value));
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (item.Selected == true)
                        {
                            if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.RolesPermissions WHERE SubModuleId=" +Convert.ToInt32(dt.Rows[i]["Id"].ToString()) +" AND RoleId=" + Convert.ToInt32(ddlRole.SelectedValue)) >0)
                            {
                                //System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Record Already Exists');", true);
                            }
                            else
                            {
                                cmd1.Parameters.Clear();
                                cmd1.Parameters.AddWithValue("@RoleId", Convert.ToInt32(ddlRole.SelectedValue));
                                cmd1.Parameters.AddWithValue("@SubModuleId",
                                    Convert.ToInt32(dt.Rows[i]["Id"].ToString()));
                                cmd1.Parameters.AddWithValue("@IsActive", 1);
                                cmd1.Parameters.AddWithValue("@CreatedDate",
                                    DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                                cmd1.Parameters.AddWithValue("@UpdatedDate",
                                    DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

                                if (cmd1.ExecuteNonQuery() > 0)
                                    flag = 2;
                                else
                                    flag = 0;
                            }
                        }
                        else
                        {
                            //
                        }

                    }
                }
                else
                {
                    //
                }
            }

            if (flag > 0)
            {
                PopulateModulePermission(0);
                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa","successMsg('<h4>Success</h4>Module Assigned Successfully.');", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa","errorMsg('<h4>Error</h4>Modules are Already Assigned');", true);
        }

        protected void ddlRoles_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRoles.SelectedIndex > 0)
                PopulateModulePermission(Convert.ToInt32(ddlRoles.SelectedValue));
        }
    }
}