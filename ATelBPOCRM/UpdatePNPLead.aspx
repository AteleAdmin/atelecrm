﻿<%@ Page Title="Update PNP Lead" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="UpdatePNPLead.aspx.cs" Inherits="ATelBPOCRM.UpdatePNPLead" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <asp:Panel ID="pnlGetLead" runat="server">
                <div class="col-md-12">
                    <div class="pull-left">
                        <div class="col-md-1">
                            Id:<asp:TextBox ID="txtLeadId" ReadOnly="True" TextMode="Number" CssClass="form-control" Width="70" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            VId:<asp:TextBox ID="txtViciLeadId" ReadOnly="True" TextMode="Number" CssClass="form-control" Width="70" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            Phone :<asp:TextBox ID="txtPhone" TextMode="Number" CssClass="form-control" MaxLength="10" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="Panel1" runat="server">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lead Information</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Producer</label>
                                        <asp:DropDownList ID="ddlAgencyProducer" CssClass="form-control select2" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <asp:Panel ID="pnlDisposition" runat="server">
                                        <div class="form-group">
                                            <label>Disposition</label>
                                            <asp:DropDownList ID="ddlDisposition" CssClass="form-control select2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDisposition_OnSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <asp:TextBox ID="txtFName" CssClass="form-control" placeholder="First Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <asp:TextBox ID="txtLName" CssClass="form-control" placeholder="Last Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                                            <asp:ListItem>Male</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Date-of-Birth</label>
                                        <asp:TextBox ID="txtDOB" CssClass="form-control datepicker" placeholder="Date-of-Birth" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <asp:TextBox ID="txtPhoneNo" CssClass="form-control" placeholder="Phone Number" ReadOnly="true" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <asp:TextBox ID="txtEmail" CssClass="form-control" placeholder="Email" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <asp:TextBox ID="txtAddress" CssClass="form-control" placeholder="Address" TextMode="MultiLine" Rows="1" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>City</label>
                                        <asp:TextBox ID="txtCity" CssClass="form-control" placeholder="City" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>State</label>
                                        <asp:TextBox ID="txtState" CssClass="form-control" placeholder="State" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Zip Code</label>&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator2" Text="*" ControlToValidate="txtZip" ForeColor="Red" ValidationExpression="\d{5}(-\d{4})?" runat="server" ErrorMessage="RegularExpressionValidator"></asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txtZip" CssClass="form-control" MaxLength="5" placeholder="Zip Code" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Homeowner</label>
                                        <asp:DropDownList ID="ddlHOwnerShip" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="False">No</asp:ListItem>
                                            <asp:ListItem Value="True">Yes</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Marital Status</label>
                                        <asp:DropDownList ID="ddlMaritalStatus" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="False">Single</asp:ListItem>
                                            <asp:ListItem Value="True">Married</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <%-- <div class="box box-solid card">
                                        <div class="card-header" style="font-size: 20px; font-weight: bold;">Vehicle 1:</div>
                                    </div>--%>
                                    <div class="card-body">
                                        <div class="col-md-3">
                                            <div class="card-header" style="font-size: 20px; font-weight: bold;">Vehicle 1:</div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Year</label>
                                                <asp:TextBox ID="txtYear" TextMode="Number" CssClass="form-control" placeholder="Year" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Make</label>
                                                <%--<asp:TextBox ID="txtMake" CssClass="form-control" placeholder="Make" runat="server" autocomplete="off"></asp:TextBox>--%>
                                                <asp:DropDownList ID="ddlMake" runat="server" CssClass="form-control select2">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="Acura">Acura</asp:ListItem>
                                                    <asp:ListItem Value="Alfa Romeo">Alfa Romeo</asp:ListItem>
                                                    <asp:ListItem Value="Audi">Audi</asp:ListItem>
                                                    <asp:ListItem Value="BMW">BMW</asp:ListItem>
                                                    <asp:ListItem Value="Buick">Buick</asp:ListItem>
                                                    <asp:ListItem Value="Cadillac">Cadillac</asp:ListItem>
                                                    <asp:ListItem Value="Chevrolet">Chevrolet</asp:ListItem>
                                                    <asp:ListItem Value="Chrysler">Chrysler</asp:ListItem>
                                                    <asp:ListItem Value="Dodge">Dodge</asp:ListItem>
                                                    <asp:ListItem Value="Fiat">Fiat</asp:ListItem>
                                                    <asp:ListItem Value="Ford">Ford</asp:ListItem>
                                                    <asp:ListItem Value="GMC">GMC</asp:ListItem>
                                                    <asp:ListItem Value="Honda">Honda</asp:ListItem>
                                                    <asp:ListItem Value="Hyundai">Hyundai</asp:ListItem>
                                                    <asp:ListItem Value="Infiniti">Infiniti</asp:ListItem>
                                                    <asp:ListItem Value="Jaguar">Jaguar</asp:ListItem>
                                                    <asp:ListItem Value="Jeep">Jeep</asp:ListItem>
                                                    <asp:ListItem Value="Kia">Kia</asp:ListItem>
                                                    <asp:ListItem Value="Land Rover">Land Rover</asp:ListItem>
                                                    <asp:ListItem Value="Lexus">Lexus</asp:ListItem>
                                                    <asp:ListItem Value="Lincoln">Lincoln</asp:ListItem>
                                                    <asp:ListItem Value="Mazda">Mazda</asp:ListItem>
                                                    <asp:ListItem Value="Mercedes-Benz">Mercedes-Benz</asp:ListItem>
                                                    <asp:ListItem Value="Mini">Mini</asp:ListItem>
                                                    <asp:ListItem Value="Mitsubishi">Mitsubishi</asp:ListItem>
                                                    <asp:ListItem Value="Nissan">Nissan</asp:ListItem>
                                                    <asp:ListItem Value="Porsche">Porsche</asp:ListItem>
                                                    <asp:ListItem Value="Ram">Ram</asp:ListItem>
                                                    <asp:ListItem Value="Smart">Smart</asp:ListItem>
                                                    <asp:ListItem Value="Subaru">Subaru</asp:ListItem>
                                                    <asp:ListItem Value="Toyota">Toyota</asp:ListItem>
                                                    <asp:ListItem Value="Volkswagen">Volkswagen</asp:ListItem>
                                                    <asp:ListItem Value="Volvo">Volvo</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Model</label>
                                                <asp:TextBox ID="txtModel" CssClass="form-control" placeholder="Model" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card-body">
                                        <div class="col-md-3">
                                            <div class="card-header" style="font-size: 20px; font-weight: bold;">Vehicle 2:</div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Year</label>
                                                <asp:TextBox ID="txtYearV2" TextMode="Number" CssClass="form-control" placeholder="Year" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Make</label>
                                                <%--<asp:TextBox ID="txtMakeV2" CssClass="form-control" placeholder="Make" runat="server" autocomplete="off"></asp:TextBox>--%>
                                                <asp:DropDownList ID="ddlMake2" runat="server" CssClass="form-control select2">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="Acura">Acura</asp:ListItem>
                                                    <asp:ListItem Value="Alfa Romeo">Alfa Romeo</asp:ListItem>
                                                    <asp:ListItem Value="Audi">Audi</asp:ListItem>
                                                    <asp:ListItem Value="BMW">BMW</asp:ListItem>
                                                    <asp:ListItem Value="Buick">Buick</asp:ListItem>
                                                    <asp:ListItem Value="Cadillac">Cadillac</asp:ListItem>
                                                    <asp:ListItem Value="Chevrolet">Chevrolet</asp:ListItem>
                                                    <asp:ListItem Value="Chrysler">Chrysler</asp:ListItem>
                                                    <asp:ListItem Value="Dodge">Dodge</asp:ListItem>
                                                    <asp:ListItem Value="Fiat">Fiat</asp:ListItem>
                                                    <asp:ListItem Value="Ford">Ford</asp:ListItem>
                                                    <asp:ListItem Value="GMC">GMC</asp:ListItem>
                                                    <asp:ListItem Value="Honda">Honda</asp:ListItem>
                                                    <asp:ListItem Value="Hyundai">Hyundai</asp:ListItem>
                                                    <asp:ListItem Value="Infiniti">Infiniti</asp:ListItem>
                                                    <asp:ListItem Value="Jaguar">Jaguar</asp:ListItem>
                                                    <asp:ListItem Value="Jeep">Jeep</asp:ListItem>
                                                    <asp:ListItem Value="Kia">Kia</asp:ListItem>
                                                    <asp:ListItem Value="Land Rover">Land Rover</asp:ListItem>
                                                    <asp:ListItem Value="Lexus">Lexus</asp:ListItem>
                                                    <asp:ListItem Value="Lincoln">Lincoln</asp:ListItem>
                                                    <asp:ListItem Value="Mazda">Mazda</asp:ListItem>
                                                    <asp:ListItem Value="Mercedes-Benz">Mercedes-Benz</asp:ListItem>
                                                    <asp:ListItem Value="Mini">Mini</asp:ListItem>
                                                    <asp:ListItem Value="Mitsubishi">Mitsubishi</asp:ListItem>
                                                    <asp:ListItem Value="Nissan">Nissan</asp:ListItem>
                                                    <asp:ListItem Value="Porsche">Porsche</asp:ListItem>
                                                    <asp:ListItem Value="Ram">Ram</asp:ListItem>
                                                    <asp:ListItem Value="Smart">Smart</asp:ListItem>
                                                    <asp:ListItem Value="Subaru">Subaru</asp:ListItem>
                                                    <asp:ListItem Value="Toyota">Toyota</asp:ListItem>
                                                    <asp:ListItem Value="Volkswagen">Volkswagen</asp:ListItem>
                                                    <asp:ListItem Value="Volvo">Volvo</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Model</label>
                                                <asp:TextBox ID="txtModelV2" CssClass="form-control" placeholder="Model" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Current Insurer</label>
                                        <asp:TextBox ID="txtCompany" CssClass="form-control" placeholder="Insurance Company" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Length of Continous Coverage</label>
                                        <asp:DropDownList ID="ddlContinousCoverage" CssClass="form-control" runat="server">
                                            <%--<asp:ListItem Value="Less than 1 Year">Less than 1 Year</asp:ListItem>--%>
                                            <asp:ListItem Value="1-2 Years">1-2 Years</asp:ListItem>
                                            <asp:ListItem Value="3-5 Years">3-5 Years</asp:ListItem>
                                            <asp:ListItem Value="5+ Years">5+ Years</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlFollow" runat="server" Visible="False">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Follow-up Date</label>
                                            <asp:TextBox ID="txtFollowDate" CssClass="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Note</label>
                                        <asp:TextBox ID="txtComments" Rows="2" CssClass="form-control" TextMode="MultiLine" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat btn-sm" OnClick="btnSave_OnClick" />
                                <% if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                                   { %>
                                 | <a href="LivePNP.aspx" class="btn btn-sm btn-info btn-flat"><< Back</a>
                                <% } %>
                                <% if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                                   { %>
                                    | <a href="ProducerLivePNP.aspx" class="btn btn-sm btn-info btn-flat"><< Back</a>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </section>
    <!-- // section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>
</asp:Content>
