﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;

namespace ATelBPOCRM
{
    public partial class AccountHeadLivePNP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Account Head")
                {
                    LoadLive(Convert.ToInt32(Session["UId"].ToString()), "", "");
                    LoadPNP(Convert.ToInt32(Session["UId"].ToString()), "", "");
                    PopulateAccountManager(Convert.ToInt32(Session["UId"].ToString()));
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }
        }

        private void PopulateAccountManager(int? accountHeadId)
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT ao.Id,ao.Name FROM dbo.AgencyOwner ao WHERE ao.IsActive=1 AND ao.RoleId=17 AND ao.ReportedTo=" + accountHeadId);
                if (dt.Rows.Count > 0)
                {
                    ddlAccountManager.DataSource = dt;
                    ddlAccountManager.DataTextField = "Name";
                    ddlAccountManager.DataValueField = "Id";
                    ddlAccountManager.DataBind();
                    ddlAccountManager.Items.Insert(0, "Select Account Manager");
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        private void LoadLive(int? accountHeadId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (accountHeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT am.Name AS Manager,ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id JOIN dbo.AgencyOwner ah ON am.ReportedTo=ah.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND ah.Id=" + accountHeadId + " GROUP BY ao.Name, ao.State, am.Name";
                }
                if (accountHeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT am.Name AS Manager,ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id JOIN dbo.AgencyOwner ah ON am.ReportedTo=ah.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND ah.Id=" + accountHeadId + " GROUP BY ao.Name, ao.State, am.Name";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvLiveAgencyStat.DataSource = dt;
                    gvLiveAgencyStat.DataBind();
                }
                else
                {
                    gvLiveAgencyStat.EmptyDataText = "No Record Found!";
                    gvLiveAgencyStat.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        private void LoadPNP(int? accountHeadId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (accountHeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT am.Name AS Manager,ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id JOIN dbo.AgencyOwner ah ON am.ReportedTo=ah.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND ah.Id=" + accountHeadId + " GROUP BY ao.Name, ao.State, am.Name";
                }
                if (accountHeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT am.Name AS Manager,ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id JOIN dbo.AgencyOwner ah ON am.ReportedTo=ah.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND ah.Id=" + accountHeadId + " GROUP BY ao.Name, ao.State, am.Name";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAgencyStat.DataSource = dt;
                    gvPNPAgencyStat.DataBind();
                }
                else
                {
                    gvPNPAgencyStat.EmptyDataText = "No Record Found!";
                    gvPNPAgencyStat.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnStats_OnClick(object sender, EventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Account Head")
            {
                if (ddlAccountManager.SelectedIndex == 0)
                {
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    LoadLive(Convert.ToInt32(Session["UId"].ToString()), from, to);
                    LoadPNP(Convert.ToInt32(Session["UId"].ToString()), from, to);
                }
                else
                {
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    LoadLiveManagerWise(Convert.ToInt32(Session["UId"].ToString()), Convert.ToInt32(ddlAccountManager.SelectedValue), from, to);
                    LoadPNPManagerWise(Convert.ToInt32(Session["UId"].ToString()), Convert.ToInt32(ddlAccountManager.SelectedValue), from, to);
                }
            }
            else
            {
                //
            }
        }

        private int rejected = 0;
        private int accepted = 0;
        private int total = 0;
        protected void gvLiveAgencyStat_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    rejected += Convert.ToInt32(((Literal)e.Row.FindControl("ltRejected")).Text);
                    accepted += Convert.ToInt32(((Literal)e.Row.FindControl("ltAccepted")).Text);
                    total += Convert.ToInt32(((Literal)e.Row.FindControl("ltTotal")).Text);
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    var head = (Label)e.Row.FindControl("lblHead");
                    var saleRejected = (Label)e.Row.FindControl("lblRejected");
                    var saleAccepted = (Label)e.Row.FindControl("lblAccepted");
                    var saleTotal = (Label)e.Row.FindControl("lblTotal");

                    head.Text = "Total";
                    saleRejected.Text = rejected.ToString();
                    saleAccepted.Text = accepted.ToString();
                    saleTotal.Text = total.ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private int _rejected = 0;
        private int _accepted = 0;
        private int _total = 0;
        protected void gvPNPAgencyStat_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    _rejected += Convert.ToInt32(((Literal)e.Row.FindControl("ltRejected")).Text);
                    _accepted += Convert.ToInt32(((Literal)e.Row.FindControl("ltAccepted")).Text);
                    _total += Convert.ToInt32(((Literal)e.Row.FindControl("ltTotal")).Text);
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    var head = (Label)e.Row.FindControl("lblHead");
                    var saleRejected = (Label)e.Row.FindControl("lblRejected");
                    var saleAccepted = (Label)e.Row.FindControl("lblAccepted");
                    var saleTotal = (Label)e.Row.FindControl("lblTotal");

                    head.Text = "Total";
                    saleRejected.Text = _rejected.ToString();
                    saleAccepted.Text = _accepted.ToString();
                    saleTotal.Text = _total.ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void LoadLiveManagerWise(int? accountHeadId, int? accountManagerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (accountHeadId > 0 && accountManagerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT am.Name AS Manager,ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id JOIN dbo.AgencyOwner ah ON am.ReportedTo=ah.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND ah.Id=" + accountHeadId + " AND am.Id=" + accountManagerId + " GROUP BY ao.Name, ao.State, am.Name";
                }
                if (accountHeadId > 0 && accountManagerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT am.Name AS Manager,ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id JOIN dbo.AgencyOwner ah ON am.ReportedTo=ah.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND ah.Id=" + accountHeadId + " AND am.Id=" + accountManagerId + " GROUP BY ao.Name, ao.State, am.Name";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvLiveAgencyStat.DataSource = dt;
                    gvLiveAgencyStat.DataBind();
                }
                else
                {
                    gvLiveAgencyStat.EmptyDataText = "No Record Found!";
                    gvLiveAgencyStat.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        private void LoadPNPManagerWise(int? accountHeadId, int? accountManagerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (accountHeadId > 0 && accountManagerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT am.Name AS Manager,ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id JOIN dbo.AgencyOwner ah ON am.ReportedTo=ah.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND ah.Id=" + accountHeadId + " AND am.Id=" + accountManagerId + " GROUP BY ao.Name, ao.State, am.Name";
                }
                if (accountHeadId > 0 && accountManagerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT am.Name AS Manager,ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id JOIN dbo.AgencyOwner ah ON am.ReportedTo=ah.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND ah.Id=" + accountHeadId + " AND am.Id=" + accountManagerId + " GROUP BY ao.Name, ao.State, am.Name";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAgencyStat.DataSource = dt;
                    gvPNPAgencyStat.DataBind();
                }
                else
                {
                    gvPNPAgencyStat.EmptyDataText = "No Record Found!";
                    gvPNPAgencyStat.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void ddlAccountManager_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAccountManager.SelectedIndex > 0)
            {
                LoadLiveManagerWise(Convert.ToInt32(Session["UId"].ToString()), Convert.ToInt32(ddlAccountManager.SelectedValue), "", "");
                LoadPNPManagerWise(Convert.ToInt32(Session["UId"].ToString()), Convert.ToInt32(ddlAccountManager.SelectedValue), "", "");
            }
            else
            {
                //
            }
        }
    }
}