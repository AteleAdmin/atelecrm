﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;

namespace ATelBPOCRM
{
    public partial class AddOutsourceProducer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    pnlReportingTo.Visible = true;
                    PopulateReportedTo();
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Outsource" && (Session["Role"] != null))
                {
                    pnlReportingTo.Visible = false;
                }
                if (Request.QueryString["producerId"] != null)
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT Id, Name, SudoName, FatherName, Username, Email, Password, Gender, ContactNo, Address, CNIC, Description, CONVERT(NVARCHAR(12),DOJ, 101) AS DOJ FROM dbo.OutsourceProducer WHERE Id=" + Convert.ToInt32(Request.QueryString["producerId"]));
                    if (dt.Rows.Count > 0)
                    {
                        txtName.Text = dt.Rows[0]["Name"].ToString();
                        txtSudoName.Text = dt.Rows[0]["SudoName"].ToString();
                        txtFName.Text = dt.Rows[0]["FatherName"].ToString();
                        txtUName.Text = dt.Rows[0]["Username"].ToString();
                        txtUName.ReadOnly = true;
                        txtEmail.Text = dt.Rows[0]["Email"].ToString();
                        txtPassword.Text = dt.Rows[0]["Password"].ToString();
                        chkAuto.Enabled = false;
                        txtPassword.ReadOnly = true;
                        txtContactNo.Text = dt.Rows[0]["ContactNo"].ToString();
                        txtAddress.Text = dt.Rows[0]["Address"].ToString();
                        txtDescription.Text = dt.Rows[0]["Description"].ToString();
                        txtDOJ.Text = dt.Rows[0]["DOJ"].ToString();
                        txtCNIC.Text = dt.Rows[0]["CNIC"].ToString();

                        ddlGender.ClearSelection();
                        ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                        ddlGender.DataBind();
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //Response.Redirect("Login.aspx");
                }
            }
            else
            {
                //
            }
        }

        public void PopulateReportedTo()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.OutsourceOwner WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlOwner.DataSource = dt;
                    ddlOwner.DataTextField = "Name";
                    ddlOwner.DataValueField = "Id";
                    ddlOwner.DataBind();

                    ddlOwner.Items.Insert(0, "Select Owner");
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["producerId"] != null)
                {
                    var cmd = new SqlCommand("Update dbo.OutsourceProducer SET Name=@Name, SudoName=@SudoName, FatherName=@FatherName, Email=@Email, Gender=@Gender, ContactNo=@ContactNo, Address=@Address, CNIC=@CNIC, Description=@Description, DOJ=@DOJ, LastModifiedBy=@LastModifiedBy, UpdatedDate=@UpdatedDate WHERE Id=@Id");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@FatherName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@SudoName", txtSudoName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.ToLower().Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@CNIC", txtCNIC.Text.Trim());
                    cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim());
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Request.QueryString["producerId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Producer Updated Successfully.');", true);
                            chkAuto.Enabled = true;
                            txtUName.ReadOnly = false;
                            txtPassword.ReadOnly = false;
                            Clear();
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Producer Cannot be Update.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Please login and try again.');", true);
                    }
                }
                else
                {
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Users WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyProducer WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Admin WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.OutsourceOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    else
                    {
                        if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.OutsourceProducer WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Producer Already Exists');", true);
                        }
                        else
                        {
                            var cmd = new SqlCommand("INSERT INTO OutsourceProducer(Name, SudoName, FatherName, Username, Email, Password, CNIC, Gender, ContactNo, Address, DOJ, RoleId, IsActive, OutsourceOwnerId, CreatedBy, CreatedDate, LastModifiedBy, UpdatedDate)VALUES(@Name, @SudoName, @FatherName, @Username, @Email, @Password, @CNIC, @Gender, @ContactNo, @Address, @DOJ, @RoleId, @IsActive, @OutsourceOwnerId, @CreatedBy, @CreatedDate, @LastModifiedBy, @UpdatedDate)", Connection.Db());
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                            cmd.Parameters.AddWithValue("@SudoName", txtSudoName.Text.Trim());
                            cmd.Parameters.AddWithValue("@FatherName", txtFName.Text.Trim());
                            cmd.Parameters.AddWithValue("@Username", txtUName.Text.Trim());
                            cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                            if (!chkAuto.Checked && string.IsNullOrEmpty(txtPassword.Text.Trim()))
                                cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                            if (chkAuto.Checked)
                                cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                            if (!chkAuto.Checked && !string.IsNullOrEmpty(txtPassword.Text.Trim()))
                                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                            cmd.Parameters.AddWithValue("@CNIC", txtCNIC.Text.Trim());
                            cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                            cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.Trim());
                            cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                            cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim());//DateTime.ParseExact(txtDOJ.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                            cmd.Parameters.AddWithValue("@RoleId", 11);
                            if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                            {
                                if (!string.IsNullOrEmpty(ddlOwner.SelectedValue))
                                    cmd.Parameters.AddWithValue("@OutsourceOwnerId", Convert.ToInt32(ddlOwner.SelectedValue));
                                else
                                    cmd.Parameters.AddWithValue("@OutsourceOwnerId", 0);
                            }
                            if (Session["Role"] != null && Session["Role"].ToString() == "Outsource" && (Session["Role"] != null))
                            {
                                if (Session["UId"] != null)
                                {
                                    cmd.Parameters.AddWithValue("@OutsourceOwnerId", Convert.ToInt32(Session["UId"]));
                                }
                            }
                            if (Session["UId"] != null)
                            {
                                cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"].ToString()));
                                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                                cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                                cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                                cmd.Parameters.AddWithValue("@IsActive", 1);
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Producer Save Successfully.');", true);
                                    Clear();
                                    ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                                }
                                else
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Producer Cannot be Save.');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Please login and try again.');", true);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void Clear()
        {
            txtName.Text = "";
            txtUName.Text = "";
            txtEmail.Text = "";
            txtPassword.Text = "";
            txtContactNo.Text = "";
            txtAddress.Text = "";
            txtDescription.Text = "";
            txtDOJ.Text = "";

            ddlGender.SelectedIndex = 0;
        }

        private static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var res = new StringBuilder();
            var rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }
}