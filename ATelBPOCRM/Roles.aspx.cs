﻿using System;
using System.IO;
using System.Web.UI.WebControls;
using Lucky.Security;
using System.Web.UI;
using Lucky.General;

namespace ATelBPOCRM
{
    public partial class Roles : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateRoles();
            }
        }

        public void PopulateRoles()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT * FROM dbo.Roles WHERE IsSuperAdmin=0");
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {

            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Roles WHERE Name='" + txtName.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Role Already Exists');", true);
                }
                else
                {
                    var r = new Lucky.Security.Roles
                    {
                        Name = txtName.Text.Trim(),
                        IsActive = true,
                        IsSuperAdmin = false,
                        CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")),
                        UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                    };
                    if (r.InsertRoles(r) > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Role Saved Successfully.');", true);
                        PopulateRoles();
                        txtName.Text = "";
                        txtRoleId.Text = "";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Role Cannot be Saved.');", true);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnAssign_OnClick(object sender, EventArgs e)
        {
            var btn = sender as LinkButton;
            if (btn != null)
            {
                var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                var role = GridView1.Rows[rowIndex].FindControl("ltName") as Literal;
                if (hfId != null)
                    if (role != null)
                        Response.Redirect("RolesPermission.aspx?RId=" + Convert.ToInt32(hfId.Value) + "&Role=" + role.Text);
            }
            else
            {
                //
            }
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    if (hfId != null) txtRoleId.Text = hfId.Value;
                    var lblName = (Literal)GridView1.Rows[rowIndex].FindControl("ltName");
                    txtName.Text = lblName.Text;
                }

                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var message = "";
                var m = new Lucky.Security.Roles();
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var active = GridView1.Rows[rowIndex].FindControl("chkActive") as CheckBox;

                    if (active != null && active.Checked == true)
                    {
                        m.IsActive = false;
                        message = "Blocked";
                    }
                    else
                    {
                        m.IsActive = true;
                        message = "Active";
                    }

                    m.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    if (hfId != null) m.Id = Convert.ToInt32(hfId.Value);
                }

                if (m.BlockRole(m) > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>Role " + message + " Successfully');", true);
                    PopulateRoles();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtRoleId.Text.Trim()))
                {
                    var r = new Lucky.Security.Roles
                    {
                        Name = txtName.Text.Trim(),
                        IsActive = true,
                        IsSuperAdmin = false,
                        UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")),
                        Id = Convert.ToInt32(txtRoleId.Text.Trim())
                    };
                    if (r.UpdateRoles(r) > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Role Updated Successfully.');", true);
                        PopulateRoles();
                        txtName.Text = "";
                        txtRoleId.Text = "";

                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Role Cannot be Updated.');", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Update Error.');", true);
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}