﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class ProducerLeadCountDetail : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                {
                    if (Request.QueryString["find"] != null && Request.QueryString["producer"] != null)
                    {
                        if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                        {
                            if (Request.QueryString["from"] != null && Request.QueryString["to"] != null)
                                PopulateLead(Request.QueryString["find"], Convert.ToInt32(Session["UId"].ToString()), Request.QueryString["from"].ToString(), Request.QueryString["to"].ToString(), Request.QueryString["producer"].ToString());
                            else
                                PopulateLead(Request.QueryString["find"], Convert.ToInt32(Session["UId"]), "", "", Request.QueryString["producer"].ToString());
                        }
                        else
                        {
                            PopulateLead(Request.QueryString["find"], Convert.ToInt32(Session["UId"]), "", "", Request.QueryString["producer"].ToString());
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        public void PopulateLead(string type, int? agencyOwnerId, string from, string to, string producer)
        {
            try
            {
                var query = "";
                if (type == "producerTotal" && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(producer))
                {
                    query = "SELECT d.Name AS Disposition,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '" + to + "' AND ap.Name='" + producer + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                    ltHeader.Text = Request.QueryString["producer"].ToString() + " - Total Leads";
                }
                if (type == "producerTotal" && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(producer))
                {
                    query = "SELECT d.Name AS Disposition,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(10), GETDATE(), 111) AND ap.Name='" + producer + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                    ltHeader.Text = Request.QueryString["producer"].ToString() + " - Total Leads";
                }
                if (type == "producerAccepted" && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(producer))
                {
                    query = "SELECT d.Name AS Disposition,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '" + to + "' AND ap.Name='" + producer + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                    ltHeader.Text = Request.QueryString["producer"].ToString() + " - Accepted Leads";
                }
                if (type == "producerAccepted" && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(producer))
                {
                    query = "SELECT d.Name AS Disposition,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(10), GETDATE(), 111) AND ap.Name='" + producer + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                    ltHeader.Text = Request.QueryString["producer"].ToString() + " - Accepted Leads";
                }
                if (type == "producerRejected" && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(producer))
                {
                    query = "SELECT d.Name AS Disposition,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,101) <= '" + to + "' AND ap.Name='" + producer + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                    ltHeader.Text = Request.QueryString["producer"].ToString() + " - Rejected Leads";
                }
                if (type == "producerRejected" && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(10), GETDATE(), 111) AND ap.Name='" + producer + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                    ltHeader.Text = Request.QueryString["producer"].ToString() + " - Rejected Leads";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = dt;
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }

            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        //protected void GridView1_OnRowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
        //    {
        //        e.Row.Cells[7].Visible = false;
        //        e.Row.Cells[8].Visible = false;
        //        e.Row.Cells[9].Visible = false;
        //        e.Row.Cells[10].Visible = false;
        //        e.Row.Cells[11].Visible = false;
        //        e.Row.Cells[12].Visible = false;
        //    }
        //    else
        //    {
        //        //
        //    }
        //}

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected")))
                {
                    disposition.CssClass = "label label-danger";
                }
            }
            else
            {
                //
            }
        }
    }
}