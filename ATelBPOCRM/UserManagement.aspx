﻿<%@ Page Title="User Management" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="ATelBPOCRM.UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border" style="padding-bottom: 18px !important;">
                        <h3 class="box-title">Producers List</h3>
                        <div class="box-tools pull-right">
                            <a href="javascript:;" class="btn btn-primary addProducer"><i class="fa fa-plus-circle"></i>&nbsp;Add Producer</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" runat="server" OnRowDataBound="GridView1_OnRowDataBound">
                            <Columns>
                                <%--<asp:TemplateField HeaderText="Serial #">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <a href="javascript:;">
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                            <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Username">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltUName" runat="server" Text='<%# Eval("Username") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Gender">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltGender" runat="server" Text='<%# Eval("Gender") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Password">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPassword" runat="server" Text='<%# Eval("Password") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Contact No">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltContact" runat="server" Text='<%# Eval("ContactNo") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                               <%-- <asp:TemplateField HeaderText="Agency">
                                    <ItemTemplate>
                                        <asp:Label ID="ltAgency" CssClass="label label-info" runat="server" Text='<%# Eval("AgencyOwner") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server" OnClientClick='<%# string.Format("return editProducer({0});", Eval("Id")) %>' RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-pencil btn-sm label-success"></i></asp:LinkButton>
                                        |
                                        <asp:LinkButton ID="btnDelete" runat="server"  OnClientClick="return blockRecord(this, event);" OnClick="btnDelete_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-trash btn-sm label-danger"></i></asp:LinkButton>
                                        |
                                        <asp:LinkButton ID="btnActive" runat="server"  OnClientClick="return blockRecord(this, event);" OnClick="btnActive_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-check btn-sm label-info"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>
    <script type="text/javascript">
        $(function () {
            $('.addProducer').on('click',
                function () {
                    $.fancybox.open({
                        src: 'AddSalesProducer.aspx',
                        type: 'iframe',
                        iframe : {
                            css : {
                                width : '600px'
                            }
                        }
                    });
                });
        });
    </script>
    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 55%;
            margin: 0;
        }
    </style>
    <script type="text/javascript">
        function editProducer(id) {
            $.fancybox.open({
                src: 'AddSalesProducer.aspx?producerId=' + id,
                type: 'iframe',
                iframe : {
                    css : {
                        width : '600px'
                    }
                }
            });
            return false;
        }
    </script>
    <script type="text/javascript">
        function blockRecord(ctl, event) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, do it!'
            }).then((result) => {
                if (result.value == true) {
                    //Swal.fire('Deleted!','Your file has been deleted.','success')
                    window.location.href = defaultAction;
            return true;
        }
        else
                    return false;
        });
        }
    </script>
</asp:Content>

