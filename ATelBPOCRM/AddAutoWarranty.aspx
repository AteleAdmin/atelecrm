﻿<%@ Page Title="Auto Warranty" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AddAutoWarranty.aspx.cs" Inherits="ATelBPOCRM.AddAutoWarranty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Auto Warranty Form</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Leads</a></li>
            <li class="active">Add New Lead</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <asp:Panel ID="Panel1" runat="server">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lead Information</h3>
                            <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Text="*" ForeColor="Red" ControlToValidate="txtPhone" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtPhone" CssClass="form-control" TextMode="Number" placeholder="Phone" MaxLength="10" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>First Name</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" Text="*" ForeColor="Red" ControlToValidate="txtFName" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtFName" CssClass="form-control" placeholder="First Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Last Name</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator9" Text="*" ForeColor="Red" ControlToValidate="txtLName" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtLName" CssClass="form-control" placeholder="Last Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address 1</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator10" Text="*" ForeColor="Red" ControlToValidate="txtAddress1" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtAddress1" CssClass="form-control" placeholder="Address 1" TextMode="MultiLine" Rows="1" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address 2</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator11" Text="*" ForeColor="Red" ControlToValidate="txtAddress2" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtAddress2" CssClass="form-control" placeholder="Address 2" TextMode="MultiLine" Rows="1" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address 3</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator12" Text="*" ForeColor="Red" ControlToValidate="txtAddress3" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtAddress3" CssClass="form-control" placeholder="Address 3" TextMode="MultiLine" Rows="1" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>State</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" Text="*" ForeColor="Red" ControlToValidate="txtState" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtState" CssClass="form-control" placeholder="State" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Province</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" Text="*" ForeColor="Red" ControlToValidate="txtProvince" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtProvince" CssClass="form-control" placeholder="Province" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" Text="*" ForeColor="Red" ControlToValidate="txtCity" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtCity" CssClass="form-control" placeholder="City" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Postal Code</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" Text="*" ForeColor="Red" ControlToValidate="txtPostalCode" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtPostalCode" TextMode="Number" CssClass="form-control" placeholder="Postal Code" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Title</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator8" Text="*" ForeColor="Red" ControlToValidate="txtTitle" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtTitle" TextMode="Number" CssClass="form-control" placeholder="Title" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Comments</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" Text="*" ForeColor="Red" ControlToValidate="txtComments" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtComments" CssClass="form-control" placeholder="Comments" TextMode="MultiLine" Rows="1" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat btn-sm" OnClick="btnSave_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </section>
    <!-- // section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('#<%= btnSave.ClientID %>').click(function (e) {
            e.preventDefault();
            var fName = $('#<%= txtFName.ClientID %>').val();
            var lName = $('#<%= txtLName.ClientID %>').val();
            var phone = $('#<%= txtPhone.ClientID %>').val();
            var address1 = $('#<%= txtAddress1.ClientID %>').val();
            var address2 = $('#<%= txtAddress2.ClientID %>').val();
            var address3 = $('#<%= txtAddress3.ClientID %>').val();
            var postalCode = $('#<%= txtPostalCode.ClientID %>').val();
            var state = $('#<%= txtState.ClientID %>').val();
            var province = $('#<%= txtProvince.ClientID %>').val();
            var city = $('#<%= txtCity.ClientID %>').val();
            var title = $('#<%= txtTitle.ClientID %>').val();
            var comment = $('#<%= txtComments.ClientID %>').val();

            if (phone === '' || title === '' || fName === '' || lName === '' || address1 === '' || address2 === '' || address3 === '' || postalCode === '' || state === '' || province === '' || city === '' || comment === '') {
                errorMsg('Please fill the required fields.');
            }
            else {
                $.post('https://retreaverdata.com/data_writing?key=009d0347-4db1-4ddc-a809-7750d9937c7f&caller_number=' + phone + '&title=' + title + '&first_name=' + fName + '&last_name=' + lName + '&address1=' + address1 + '&address2=' + address2 + '&address3=' + address3 + '&state=' + state + '&province=' + province + '&city=' + city + '&postal_code=' + postalCode + '&comments=' + comment, $("#form1").serialize(), function (result) {
                    console.log(result.status);
                });
                $.ajax({
                    type: "POST",
                    url: "AddAutoWarranty.aspx/InsertLead",
                    data: '{firstName: "' + fName + '", lastName:"' + lName + '", title:"' + title + '", phone: "' + phone + '", address1: "' + address1 + '", address2:"' + address2 + '" ,address3:"' + address3 + '", postalCode: "' + postalCode + '", state: "' + state + '", province: "' + province + '", city: "' + city + '", comments: "' + comment + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        successMsg(response.d);
                    }
                });
            }
            return false;
        });
    </script>
</asp:Content>
