﻿<%@ Page Title="Agency Report" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AgencyReport.aspx.cs" Inherits="ATelBPOCRM.AgencyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Agency New Leads</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Reports</a></li>
            <li class="active">Agency New Leads</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnStats_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-md-12 -->
            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leads Counts</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-info card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Total</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltTotalLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="totalLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Total -->
                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-success card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Accepted</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltAcceptedLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="acceptedLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Accepted -->

                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-danger card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Rejected</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltRejectedLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="rejectedLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Rejected -->
                    </div>
                </div>
            </div>
            <!-- // col-md-12 -->
            <div class="row">
                <div class="col-md-12">
                    <asp:Panel ID="pnlAgencyWise" runat="server" Visible="True">
                        <div class="col-md-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Agency Wise</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <asp:GridView ID="gvAgencyStat" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvAgencyStat_OnRowDataBound" ShowFooter="True" FooterStyle="">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Lead Id">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <a href="javascript:;" data-find="producerRejected" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                    </a>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="State">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblState" runat="server" Text=""></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Rejected">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltRejected" runat="server" Text='<%# Eval("Rejected") %>'></asp:Literal>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblRejected" runat="server" Text=""></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Accepted">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltAccepted" runat="server" Text='<%# Eval("Accepted") %>'></asp:Literal>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblAccepted" runat="server" Text=""></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#00c0ef" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <!-- / Total -->
                    </asp:Panel>
                    <!-- // col-md-12 -->
                </div>
                <!-- // col-md-12 -->
            </div>
            <!-- col-md-12 -->
            <div class="row">
                <div class="col-md-12">
                    <!-- col-md-6 -->
                    <div class="col-md-6">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">Rejected Leads</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <asp:GridView ID="gvRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowCreated="gvRejected_OnRowCreated">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lead Id">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Disposition">
                                            <ItemTemplate>
                                                <a href="javascript:;">
                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-6 -->
                    <!-- col-md-6 -->
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Accepted Leads</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <asp:GridView ID="gvAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowCreated="gvAccepted_OnRowCreated">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lead Id">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Disposition">
                                            <ItemTemplate>
                                                <a href="javascript:;">
                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-6 -->
                </div>
            </div>

            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Export Leads</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="gvAllLeads" CssClass="table table-bordered table-striped dataTable display" AutoGenerateColumns="False" runat="server" OnRowCreated="gvAllLeads_OnRowCreated" OnRowDataBound="gvAllLeads_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Lead Id">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="State">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Zip">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Company">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Year">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Model">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agency">
                                    <ItemTemplate>
                                        <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Transfer To">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Homeowner">
                                    <ItemTemplate>
                                        <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Disposition">
                                    <ItemTemplate>
                                        <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Owner">
                                    <ItemTemplate>
                                        <asp:Label ID="ltOwner" runat="server" Text='<%# Eval("Owner") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- // col-md-12 -->
    </section>

    <div class="modal fade" id="modal-total">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Default Modal</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <Lucky:DataTableControl ID="DataTable" runat="server" />
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.display').prepend($("<thead></thead>").append($('.display').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                "paging": false,
                "scrollX": true,
                buttons: [
                    'csv', 'excel'
                ]
            });
        });
    </script>

    <script type="text/javascript">
        <%-- $(function () {
            $('.leadCount').on('click', function () {
                var type = $(this).data('request');
                var from = $('#<%= txtFrom.ClientID %>').val();
                var to = $('#<%= txtTo.ClientID %>').val();
                if (from === '' && to === '') {
                    $.fancybox.open({
                        src: 'LeadCount.aspx?find=' + type,
                        type: 'iframe'
                    });
                } else {
                    $.fancybox.open({
                        src: 'LeadCount.aspx?find=' + type + '&from=' + from + '&to=' + to,
                        type: 'iframe'
                    });
                }
            });
        })--%>
    </script>

    <script type="text/javascript">
        <%-- $(function () {
            $('.producerCountDetail').on('click', function () {
                var type = $(this).data('find');
                var from = $('#<%= txtFrom.ClientID %>').val();
                var to = $('#<%= txtTo.ClientID %>').val();
                var producer = $(this).data('producer');
                if (from === '' && to === '') {
                    $.fancybox.open({
                        src: 'ProducerLeadCountDetail.aspx?find=' +
                            type +
                            '&producer=' +
                            producer,
                        type: 'iframe'
                    });
                } else {
                    $.fancybox.open({
                        src: 'ProducerLeadCountDetail.aspx?find=' + type + '&from=' + from + '&to=' + to + '&producer=' + producer,
                        type: 'iframe'
                    });
                }
            });
        })--%>
    </script>

    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 100%;
            margin: 0;
        }
    </style>
</asp:Content>

