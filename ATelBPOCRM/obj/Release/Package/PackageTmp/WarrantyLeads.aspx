﻿<%@ Page Title="Warranty Leads" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="WarrantyLeads.aspx.cs" Inherits="ATelBPOCRM.WarrantyLeads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Warranty Leads</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Leads</a></li>
            <li class="active">Warranty Lead List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnLead" runat="server" Text="Get Leads" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnLead_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lead List</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped dataTable display" AutoGenerateColumns="False" runat="server" EmptyDataText="No Record Found!" OnRowDataBound="GridView1_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Lead Id">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <a href="javascript:;">
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("Phone") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Postal Code">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPostalCode" runat="server" Text='<%# Eval("PostalCode") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address1">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltAddress1" runat="server" Text='<%# Eval("Address1") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address2">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltAddress2" runat="server" Text='<%# Eval("Address2") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address3">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltAddress3" runat="server" Text='<%# Eval("Address3") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server" OnClick="btnEdit_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-pencil btn-sm label-success"></i></asp:LinkButton>

                                        <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-trash btn-sm label-danger"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <Lucky:DataTableControl ID="DataTable" runat="server" />
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.display').prepend($("<thead></thead>").append($('.display').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                "paging": false,
                buttons: [
                    'csv', 'excel'
                ],
                'aoColumnDefs': [{
                    orderable: false,
                    'aTargets': [7, 8]

                }]
            });
        });
    </script>
</asp:Content>
