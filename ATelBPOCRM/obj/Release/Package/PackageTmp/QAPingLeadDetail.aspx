﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QAPingLeadDetail.aspx.cs" Inherits="ATelBPOCRM.QAPingLeadDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lead Detail</title>
    <link rel="icon" href="backend/favicon.ico" type="image/ico" sizes="16x16" />
    <link href="backend/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="backend/icomoon/styles.css" rel="stylesheet" />
    <link href="backend/dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="backend/BS-4/css/bootstrap.min.css" rel="stylesheet" />
    <link href="backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="backend/lucky-form-val/css/formValidation.css" rel="stylesheet" />
    <link href="backend/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="backend/toaster/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />
    <link href="backend/toaster/toastr.min.css" rel="stylesheet" />
    <script src="backend/plugins/jquery/dist/jquery.min.js"></script>
    <script src="backend/toaster/toastr.min.js"></script>
    <script src="backend/toaster/toasterCustom.js"></script>
    <style type="text/css">
        .label {
            -ms-border-radius: 0px !important;
            border-radius: 0px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h2 class="box-title">Lead Detail</h2>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:Literal ID="ltLead" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <asp:TextBox ID="txtLeadId" TextMode="Number" Visible="false" runat="server"></asp:TextBox>
                    <div class="form-group">
                        <label>Call Status&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Text="*" ForeColor="Red" ControlToValidate="ddlLeadStatus" InitialValue="0" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator></label>
                        <asp:DropDownList ID="ddlLeadStatus" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                            <asp:ListItem Value="Good Call" Text="Good Call"></asp:ListItem>
                            <asp:ListItem Value="Average Call" Text="Average Call"></asp:ListItem>
                            <asp:ListItem Value="Bad Call" Text="Bad Call"></asp:ListItem>
                            <asp:ListItem Value="Fake Call" Text="Fake Call"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <asp:TextBox ID="txtQAComment" CssClass="form-control" TextMode="MultiLine" Rows="5" runat="server"></asp:TextBox>
                </div>
                <div class="col-md-4" style="margin-top: 10px;">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success btn-flat" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
