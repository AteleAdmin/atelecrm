﻿<%@ Page Title="Bonus Sheet" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="BonusSheet.aspx.cs" Inherits="ATelBPOCRM.BonusSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Bonus</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Lead</a></li>
            <li class="active">Bonus Sheet</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnGetBonus" runat="server" Text="Get Bonus" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnGetBonus_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bonus List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="gvBonus" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                            <Columns>
                                <asp:TemplateField HeaderText="Serial #">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <%--<asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Live">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltLive" runat="server" Text='<%# Eval("Live") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PNP">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPNP" runat="server" Text='<%# Eval("PNP") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Leads">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bonus">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltBonus" runat="server" Text='<%# Eval("Bonus") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dataTable').DataTable({
                dom: 'Bfrtip',
                "paging": false,
                "ordering": false,
                buttons: [
                    'csv', 'excel'
                ]
            });
        });
    </script>
</asp:Content>
