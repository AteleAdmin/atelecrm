﻿<%@ Page Title="Delete Lead" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="DeleteLead.aspx.cs" Inherits="ATelBPOCRM.DeleteLead" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Delete Lead</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Delete (Live Transfer)</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label>Phone No</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtPhone" ErrorMessage="RequiredFieldValidator" ValidationGroup="g1"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtPhone" TextMode="Number" placeholder="Phone No" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <asp:Button ID="btnSubmit" CssClass="btn btn-success" runat="server" Text="Search Lead" OnClick="btnSubmit_Click" ValidationGroup="g1" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lead Info</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped dataTable" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Label CssClass="label label-info" ID="ltType" runat="server" Text='<%# Eval("Type") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Zip">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone No">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPhoneNo" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Disposition">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltDisposition" runat="server" Text='<%# Eval("Disposition") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agent">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAgent" runat="server" Text='<%# Eval("Agent") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agency">
                                        <ItemTemplate>
                                            <asp:Literal ID="Agency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCreatedDate" runat="server" Text='<%# Eval("CreatedDate") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDelete" Style="padding: 4px 10px;" OnClientClick="deleteRecord(this, event);" CssClass="btn btn-danger btn-sm" RowIndex='<%# Container.DisplayIndex %>' OnClick="btnDelete_Click" runat="server"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // row -->
    </section>
    <!-- // section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        function deleteRecord(ctl, event) {
            var defaultAction = $(ctl).prop("href");
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, do it!'
            }).then((result) => {
                if (result.value == true) {
                    window.location.href = defaultAction;
                    return true;
                }
                else
                    return false;
            });

        }
    </script>
</asp:Content>

