﻿<%@ Page Title="Server | PC | Laptop | Desktop | Inventory" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Computers.aspx.cs" Inherits="ATelBPOCRM.inventory.Computers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <%--<section class="content-header">
        <h1>Server, PC, Laptops</h1>
    </section>--%>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Server List</h3>
                        <div class="box-tools pull-right">
                            <a href="javascript:;" class="btn btn-primary add-laptop-pc"><i class="fa fa-plus-circle"></i>&nbsp;Add Accessories</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="nav nav-tabs">
                            <li <% if (MultiView1.ActiveViewIndex == 0) Response.Write("class=\"active\""); %> />
                            <asp:LinkButton ID="btnVLaptops" runat="server" Text="Direct Bonus" OnClick="btnVLaptops_Click" />
                             <li <% if (MultiView1.ActiveViewIndex == 0) Response.Write("class=\"active\""); %> />
                            <asp:LinkButton ID="btnVPcs" runat="server" Text="Direct Bonus" OnClick="btnVPcs_Click" />
                             <li <% if (MultiView1.ActiveViewIndex == 0) Response.Write("class=\"active\""); %> />
                            <asp:LinkButton ID="btnVServer" runat="server" Text="Direct Bonus" OnClick="btnVServer_Click" />
                        </ul>
                        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
                      <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>--%>
                                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                                    <asp:View runat="server">
                                        <asp:GridView ID="GvLaptops" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Serial #">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex+1 %>
                                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <a class="update-item" data-request="<%# Eval("Id") %>" href="javascript:;">
                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Brand">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltBrand" runat="server" Text='<%# Eval("Brand") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Model">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Serial No">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltSerialNo" runat="server" Text='<%# Eval("SerialNo") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Processor">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltProcessor" runat="server" Text='<%# Eval("Processor") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="RAM">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltRAMMemory" runat="server" Text='<%# Eval("RAMMemory") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Supplier">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltSupplier" runat="server" Text='<%# Eval("Supplier") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Warranty">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltWarranty" runat="server" Text='<%# Eval("Warranty") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Purchase Date">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltPurchaseDate" runat="server" Text='<%# Eval("PurchaseDate") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Active">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkActive" Enabled="False" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("IsActive"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:View>
                                    <!--// Laptops -->
                                    <asp:View runat="server">
                                        <asp:GridView ID="GvPCs" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Serial #">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex+1 %>
                                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <a class="update-item" data-request="<%# Eval("Id") %>" href="javascript:;">
                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Brand">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltBrand" runat="server" Text='<%# Eval("Brand") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Model">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Serial No">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltSerialNo" runat="server" Text='<%# Eval("SerialNo") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Processor">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltProcessor" runat="server" Text='<%# Eval("Processor") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="RAM">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltRAMMemory" runat="server" Text='<%# Eval("RAMMemory") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Supplier">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltSupplier" runat="server" Text='<%# Eval("Supplier") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Warranty">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltWarranty" runat="server" Text='<%# Eval("Warranty") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Purchase Date">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltPurchaseDate" runat="server" Text='<%# Eval("PurchaseDate") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Active">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkActive" Enabled="False" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("IsActive"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:View>
                                    <!--// PCS -->
                                    <asp:View runat="server">
                                        <asp:GridView ID="GvServers" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Serial #">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex+1 %>
                                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <a class="update-item" data-request="<%# Eval("Id") %>" href="javascript:;">
                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Brand">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltBrand" runat="server" Text='<%# Eval("Brand") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Model">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Serial No">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltSerialNo" runat="server" Text='<%# Eval("SerialNo") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Processor">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltProcessor" runat="server" Text='<%# Eval("Processor") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="RAM">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltRAMMemory" runat="server" Text='<%# Eval("RAMMemory") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Supplier">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltSupplier" runat="server" Text='<%# Eval("Supplier") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Warranty">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltWarranty" runat="server" Text='<%# Eval("Warranty") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Purchase Date">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltPurchaseDate" runat="server" Text='<%# Eval("PurchaseDate") %>'></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Active">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkActive" Enabled="False" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("IsActive"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:View>
                                    <!--// Servers -->
                                </asp:MultiView>
                            <%--</ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $('.add-laptop-pc').on('click',
                function () {
                    if (id !== '') {
                        $.fancybox.open({
                            src: 'AddAccessories.aspx',
                            type: 'iframe'
                        });
                    }
                });
        });
    </script>

    <%--<script type="text/javascript">
        $(function () {
            $('.update-item').on('click',
                function () {
                    var id = $(this).data('request');
                    if (id !== '') {
                        $.fancybox.open({
                            src: 'AddAccessories.aspx?itemId=' + id,
                            type: 'iframe'
                        });
                    }
                });
        });
    </script>--%>

    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 100%;
            margin: 0;
        }
    </style>
</asp:Content>

