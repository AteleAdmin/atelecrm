﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAccessories.aspx.cs" Inherits="ATelBPOCRM.inventory.AddAccessories" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Agency Producer</title>
    <link rel="icon" href="backend/favicon.ico" type="image/ico" sizes="16x16" />
    <link href="backend/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="backend/icomoon/styles.css" rel="stylesheet" />
    <link href="backend/dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="backend/BS-4/css/bootstrap.min.css" rel="stylesheet" />
    <link href="backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="backend/lucky-form-val/css/formValidation.css" rel="stylesheet" />
    <link href="backend/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="backend/toaster/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />
    <style type="text/css">
        .label {
            -ms-border-radius: 0px !important;
            border-radius: 0px !important;
        }
    </style>
    <script src="backend/lucky-form-val/js/jquery/jquery.min.js"></script>
    <script src="backend/toaster/toastr.min.js"></script>
    <script src="backend/toaster/toasterCustom.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <form id="form1" runat="server">
        <div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New Device</h3>
                            <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="defaultForm" class="form-horizontal">
                                <div class="row">
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <asp:TextBox ID="txtDeviceId" TextMode="Number" placeholder="txtDeviceId" Visible="false" runat="server"></asp:TextBox>
                                            <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" Text="*" ControlToValidate="txtName" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtName" CssClass="form-control" placeholder="Name" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <asp:TextBox ID="txtDescription" CssClass="form-control" TextMode="MultiLine" Rows="1" placeholder="Description" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Brand</label>
                                            <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Serial No</label>
                                            <asp:TextBox ID="txtSerialNo" CssClass="form-control" placeholder="Serial No" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Model</label>
                                            <asp:TextBox ID="txtModel" CssClass="form-control" placeholder="Email" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Supplier</label>
                                            <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Warranty</label>
                                            <asp:DropDownList ID="ddlWarranty" runat="server" CssClass="form-control">
                                                <asp:ListItem>1 Month</asp:ListItem>
                                                <asp:ListItem>3 Month</asp:ListItem>
                                                <asp:ListItem>6 Month</asp:ListItem>
                                                <asp:ListItem>1 Year</asp:ListItem>
                                                <asp:ListItem>1.5 Year</asp:ListItem>
                                                <asp:ListItem>2 Year</asp:ListItem>
                                                <asp:ListItem>3 Year</asp:ListItem>
                                                <asp:ListItem>4 Year</asp:ListItem>
                                                <asp:ListItem>5 Year</asp:ListItem>
                                                <asp:ListItem>6 Year</asp:ListItem>
                                                <asp:ListItem>8 Year</asp:ListItem>
                                                <asp:ListItem>10 Year</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Purchase Date</label>
                                            <asp:TextBox ID="txtPurchaseDate" CssClass="form-control datepicker" placeholder="mm/dd/yyyy" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat" OnClick="btnSave_OnClick" />
                                    <asp:Button ID="btnUpdate" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat" OnClick="btnUpdate_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="backend/lucky-form-val/js/bootstrap.min.js"></script>
    <script src="backend/lucky-form-val/js/framework/bootstrap.js"></script>
    <script src="backend/lucky-form-val/js/jquery.mask.js"></script>
    <script src="backend/plugins/select2/dist/js/select2.min.js"></script>
    <script src="backend/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!----------------------------------------------------------->
    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>

    <script type="text/javascript">
        var dateToday = new Date();
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true,
            orientation: 'bottom auto'
        });
    </script>
</body>
</html>
