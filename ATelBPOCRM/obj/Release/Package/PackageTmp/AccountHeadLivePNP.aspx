﻿<%@ Page Title="Maverick Leads" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="AccountHeadLivePNP.aspx.cs" Inherits="ATelBPOCRM.AccountHeadLivePNP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <%--<h1>Live PNP Stat</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Reports</a></li>
            <li class="active">Agency New Leads</li>
        </ol>--%>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Select Account Manager</label>
                                <asp:DropDownList ID="ddlAccountManager" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlAccountManager_OnSelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnStats_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // col-md-12 -->
        <div class="row">
            <div class="col-md-12">
                <div style="padding: 10px; width: 100%; background: #111; margin-bottom: 5px; color: #fff; font-weight: bold; text-align: center;">Live Transfer</div>
            </div>
        </div>
        <!-- // row -->
        <!-- col-md-12 -->
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pnlAgencyWise" runat="server" Visible="True">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agency Wise</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:GridView ID="gvLiveAgencyStat" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvLiveAgencyStat_OnRowDataBound" ShowFooter="True" FooterStyle="">
                                <Columns>
                                    <asp:TemplateField HeaderText="Lead Id">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <a href="javascript:;" data-find="producerRejected" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                            </a>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Manager">
                                        <ItemTemplate>
                                            <a href="javascript:;" data-find="producerRejected" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                <asp:Literal ID="ltManager" runat="server" Text='<%# Eval("Manager") %>'></asp:Literal>
                                            </a>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblHead1" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblState" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rejected">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltRejected" runat="server" Text='<%# Eval("Rejected") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblRejected" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accepted">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAccepted" runat="server" Text='<%# Eval("Accepted") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblAccepted" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#00c0ef" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- / Total -->
                </asp:Panel>
                <!-- // col-md-12 -->
            </div>
            <!-- // col-md-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div style="padding: 10px; width: 100%; background: #111; margin-bottom: 5px; color: #fff; font-weight: bold; text-align: center;">Ping N' Post</div>
            </div>
        </div>
        <!-- // row -->
        <!-- // col-md-12 -->
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" Visible="True">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agency Wise</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:GridView ID="gvPNPAgencyStat" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvPNPAgencyStat_OnRowDataBound" ShowFooter="True" FooterStyle="">
                                <Columns>
                                    <asp:TemplateField HeaderText="Lead Id">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <a href="javascript:;" data-find="producerRejected" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                            </a>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Manager">
                                        <ItemTemplate>
                                            <a href="javascript:;" data-find="producerRejected" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                <asp:Literal ID="ltManager" runat="server" Text='<%# Eval("Manager") %>'></asp:Literal>
                                            </a>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblHead1" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblState" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rejected">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltRejected" runat="server" Text='<%# Eval("Rejected") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblRejected" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accepted">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAccepted" runat="server" Text='<%# Eval("Accepted") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblAccepted" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#00c0ef" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- / Total -->
                </asp:Panel>
                <!-- // col-md-12 -->
            </div>
            <!-- // col-md-12 -->
        </div>
        <!-- col-md-12 -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <Lucky:DataTableControl ID="DataTable" runat="server" />
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.liveDataTable').prepend($("<thead></thead>").append($('.liveDataTable').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                'paging': true,
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering": false,
                "scrollX": true,
                'info': true,
                buttons: [
                    'csv', 'excel', 'pageLength'
                ]
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.pnpDataTable').prepend($("<thead></thead>").append($('.pnpDataTable').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                'paging': true,
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering": false,
                "scrollX": true,
                'info': true,
                buttons: [
                    'csv', 'excel', 'pageLength'
                ]
            });
        });
    </script>
</asp:Content>
