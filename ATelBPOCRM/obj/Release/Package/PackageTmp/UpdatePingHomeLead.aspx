﻿<%@ Page Title="Update Lead" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="UpdatePingHomeLead.aspx.cs" Inherits="ATelBPOCRM.UpdatePingHomeLead" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <asp:Panel ID="pnlGetLead" runat="server">
                <div class="col-md-12">
                    <div class="pull-left">
                        <div class="col-md-1">
                            Id:<asp:TextBox ID="txtLeadId" ReadOnly="True" TextMode="Number" CssClass="form-control" Width="70" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            VId:<asp:TextBox ID="txtViciLeadId" ReadOnly="True" TextMode="Number" CssClass="form-control" Width="70" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            Phone :<asp:TextBox ID="txtPhone" TextMode="Number" CssClass="form-control" MaxLength="10" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="Panel1" runat="server">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lead Information</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Producer</label>
                                        <asp:DropDownList ID="ddlAgencyProducer" CssClass="form-control select2" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <asp:Panel ID="pnlDisposition" runat="server">
                                        <div class="form-group">
                                            <label>Disposition</label>
                                            <asp:DropDownList ID="ddlDisposition" CssClass="form-control select2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDisposition_OnSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <asp:TextBox ID="txtFName" CssClass="form-control" placeholder="First Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <asp:TextBox ID="txtLName" CssClass="form-control" placeholder="Last Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                                            <asp:ListItem>Male</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Date-of-Birth</label>
                                        <asp:TextBox ID="txtDOB" CssClass="form-control datepicker" placeholder="Date-of-Birth" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <asp:TextBox ID="txtPhoneNo" CssClass="form-control" placeholder="Phone Number" ReadOnly="true" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <asp:TextBox ID="txtEmail" CssClass="form-control" placeholder="Email" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <asp:TextBox ID="txtAddress" CssClass="form-control" placeholder="Address" TextMode="MultiLine" Rows="1" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                        <div class="form-group">
                                            <label>No Flood</label>
                                            <asp:DropDownList ID="ddlIsFlooded" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>No Open Claims</label>
                                            <asp:DropDownList ID="ddlIsOpenClaim" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <asp:Panel ID="pnlFollow" runat="server" Visible="False">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Follow-up Date</label>
                                            <asp:TextBox ID="txtFollowDate" CssClass="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Note</label>
                                        <asp:TextBox ID="txtComments" Rows="2" CssClass="form-control" TextMode="MultiLine" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat btn-sm" OnClick="btnSave_OnClick" />
                                <% if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                                   { %>
                                 | <a href="LivePNP.aspx" class="btn btn-sm btn-info btn-flat"><< Back</a>
                                <% } %>
                                <% if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                                   { %>
                                    | <a href="ProducerLivePingPulseHome.aspx" class="btn btn-sm btn-info btn-flat"><< Back</a>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </section>
    <!-- // section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>
</asp:Content>
