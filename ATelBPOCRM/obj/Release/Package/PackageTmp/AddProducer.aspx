﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProducer.aspx.cs" Inherits="ATelBPOCRM.AddProducer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Agency Producer</title>
    <link rel="icon" href="backend/favicon.ico" type="image/ico" sizes="16x16" />
    <link href="backend/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="backend/icomoon/styles.css" rel="stylesheet" />
    <link href="backend/dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="backend/BS-4/css/bootstrap.min.css" rel="stylesheet" />
    <link href="backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="backend/lucky-form-val/css/formValidation.css" rel="stylesheet" />
    <link href="backend/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="backend/toaster/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />
    <style type="text/css">
        .label {
            -ms-border-radius: 0px !important;
            border-radius: 0px !important;
        }
    </style>
    <script src="backend/lucky-form-val/js/jquery/jquery.min.js"></script>
    <script src="backend/toaster/toastr.min.js"></script>
    <script src="backend/toaster/toasterCustom.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <form id="form1" runat="server">
        <div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Producer Form</h3>
                            <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="defaultForm" class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" Text="*" ControlToValidate="txtName" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtName" CssClass="form-control" placeholder="Name" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <asp:DropDownList ID="ddlGender" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                                <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Password &nbsp;<span><asp:CheckBox ID="chkAuto" runat="server" CssClass="" />&nbsp;Auo Generate</span></label>
                                            <asp:TextBox ID="txtPassword" CssClass="form-control password" placeholder="Password" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Date-of-Join</label>
                                            <asp:TextBox ID="txtDOJ" CssClass="form-control datepicker" placeholder="mm/dd/yyyy" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Username</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="Red" Text="*" ControlToValidate="txtUName" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUName" CssClass="form-control" placeholder="Username" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <asp:TextBox ID="txtEmail" CssClass="form-control" placeholder="Email" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Contact No</label>
                                            <asp:TextBox ID="txtContactNo" CssClass="form-control" placeholder="Contact No" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <asp:TextBox ID="txtAddress" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="Address" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Description</label>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat" OnClick="btnSave_OnClick" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="backend/lucky-form-val/js/bootstrap.min.js"></script>
    <script src="backend/lucky-form-val/js/formValidation.js"></script>
    <script src="backend/lucky-form-val/js/framework/bootstrap.js"></script>
    <script src="backend/lucky-form-val/js/jquery.mask.js"></script>
    <script src="backend/plugins/select2/dist/js/select2.min.js"></script>
    <script src="backend/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!----------------------------------------------------------->
    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>

    <script type="text/javascript">
        document.getElementById('chkAuto').onclick = function () {
            if (this.checked) {
                document.querySelector('.password').disabled = "disabled";
            } else {
                document.querySelector('.password').disabled = "";
            }
        };
    </script>
    <script type="text/javascript">
        $(function () {
            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                fields: {
                    '<%= txtUName.ClientID %>': {
                        message: 'The username is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The username is required'
                            },
                            stringLength: {
                                min: 3,
                                max: 10,
                                message: 'The username must be more than 3 and less than 10 characters long'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: 'The username can only consist of alphabetical, number, dot and underscore'
                            }
                        }
                    },
                    '<%= txtEmail.ClientID %>': {
                        validators: {
                            notEmpty: {
                                message: 'The email address is required'
                            },
                            emailAddress: {
                                message: 'The input is not a valid email address'
                            }
                        }
                    },
                    '<%= txtName.ClientID %>': {
                        validators: {
                            notEmpty: {
                                message: 'The text is not valid'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z ]+$/,
                            }
                        }
                    },
                    '<%= txtDOJ.ClientID %>': {
                        validators: {
                            date: {
                                format: 'DD/MM/YYYY',
                                message: 'The Date is not valid'
                            }
                        }
                    },
                    '<%= txtContactNo.ClientID %>': {
                        message: 'The phone number is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The phone number is required'
                            },
                            regexp: {
                                regexp: /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
                            }
                        }
                    },
                    validators: {
                        notEmpty: {
                            message: 'The text is not valid'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                        }
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        var dateToday = new Date();
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true,
            orientation: 'bottom auto'
        });
    </script>
    <script type="text/javascript">
        //$('.datepicker').mask('00/00/0000');
    </script>
</body>
</html>

