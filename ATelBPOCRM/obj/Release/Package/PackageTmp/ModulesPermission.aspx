﻿<%@ Page Title="Module Permission" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ModulesPermission.aspx.cs" Inherits="ATelBPOCRM.ModulesPermission" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Modules</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>System Settings</a></li>
            <li class="active">Modules</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Assign Module</h3>
                        <%-- <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlRole" CssClass="form-control select2" Style="width: 100%;" runat="server"></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:ListBox ID="ddlModule" runat="server" SelectionMode="Multiple"></asp:ListBox>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary btn-flat" OnClick="btnSave_OnClick" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Roles Module</h3>
                        <%-- <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
                        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
                        <%--<ContentTemplate>--%>
                        <div class="form-group">
                            <label>Select Role</label>
                            <asp:DropDownList ID="ddlRoles" CssClass="form-control select2" Style="width: 100%;" AutoPostBack="True" OnSelectedIndexChanged="ddlRoles_OnSelectedIndexChanged" runat="server"></asp:DropDownList>
                        </div>
                        <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateField HeaderText="Serial #">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Module Name">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltModule" runat="server" Text='<%# Eval("Module") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Role Name">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltRole" runat="server" Text='<%# Eval("Role") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkActive" Enabled="False" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("Active"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <%--  </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script>
        $(function () {
            $('.select2').select2();
        });
    </script>
    <link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
        rel="stylesheet" type="text/css" />
    <script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"
        type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=ddlModule]').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>
</asp:Content>
