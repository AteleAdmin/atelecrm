﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApiDetail.aspx.cs" Inherits="ATelBPOCRM.ApiDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="icon" href="backend/favicon.ico" type="image/ico" sizes="16x16" />
    <link href="backend/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="backend/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </div>
        </div>
    </form>
</body>
</html>
