﻿<%@ Page Title="Outsource Agent Report" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="OutsourceAgentReport.aspx.cs" Inherits="ATelBPOCRM.OutsourceAgentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Agent Report</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Reports</a></li>
            <li class="active">Agent Report</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:Panel ID="pnlOutsource" runat="server" Visible="False">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Outsource Owner</label>
                                    <asp:DropDownList ID="ddlOutsourceOwner" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlOutsourceOwner_OnSelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnStats_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-md-12 -->
            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leads Counts</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-info card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Total</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltTotalLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="totalLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Total -->
                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-success card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Accepted</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltAcceptedLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="acceptedLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Accepted -->

                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-danger card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Rejected</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltRejectedLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="rejectedLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Rejected -->
                    </div>
                </div>
            </div>
            <!-- // col-md-12 -->

            <div class="row">
                <div class="col-md-12">
                    <!-- col-md-4 -->
                    <div class="col-md-4">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">Rejected Leads</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <asp:GridView ID="gvRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lead Id">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agent Name">
                                            <ItemTemplate>
                                                <a href="javascript:;">
                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-4 -->
                    <!-- col-md-4 -->
                    <div class="col-md-4">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Accepted Leads</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <asp:GridView ID="gvAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lead Id">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agent Name">
                                            <ItemTemplate>
                                                <a href="javascript:;">
                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-4 -->
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Total Leads</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <asp:GridView ID="gvTotal" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lead Id">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agent Name">
                                            <ItemTemplate>
                                                <a href="javascript:;">
                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-4 -->
                </div>
            </div>
        </div>
        <!-- // col-md-12 -->

        <div class="row">
            <div class="col-md-12">
                <div style="padding: 10px; width: 100%; background: #111; margin-bottom: 5px; color: #fff; font-weight: bold; text-align: center;">Ping N' Post</div>
            </div>
        </div>

        <div class="row">
            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leads Counts</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-info card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Total</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltPNPTotalLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="totalLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Total -->
                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-success card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Accepted</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltPNPAcceptedLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="acceptedLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Accepted -->

                        <div class="col-md-4">
                            <div class="card-mat card-stats">
                                <div class="card-header card-header-danger card-header-icon">
                                    <div class="card-icon">
                                        <i class="icon-stats-bars2"></i>
                                    </div>
                                    <p class="card-category">Rejected</p>
                                    <h3 class="card-title">
                                        <asp:Literal ID="ltPNPRejectedLead" runat="server" Text="0"></asp:Literal>
                                        <small>Leads</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="icon-eye"></i>
                                        <a class="leadCount" data-request="rejectedLead" href="javascript:;">View Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Rejected -->
                    </div>
                </div>
            </div>
            <!-- // col-md-12 -->

            <div class="row">
                <div class="col-md-12">
                    <!-- col-md-4 -->
                    <div class="col-md-4">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">Rejected Leads</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <asp:GridView ID="gvPNPRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lead Id">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agent Name">
                                            <ItemTemplate>
                                                <a href="javascript:;">
                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-4 -->
                    <!-- col-md-4 -->
                    <div class="col-md-4">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Accepted Leads</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <asp:GridView ID="gvPNPAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lead Id">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agent Name">
                                            <ItemTemplate>
                                                <a href="javascript:;">
                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-4 -->
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Total Leads</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <asp:GridView ID="gvPNPTotal" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lead Id">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agent Name">
                                            <ItemTemplate>
                                                <a href="javascript:;">
                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-4 -->
                </div>
            </div>
        </div>
        <!-- // col-md-12 -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <Lucky:DataTableControl ID="DataTable" runat="server" />
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.display').prepend($("<thead></thead>").append($('.display').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                "paging": false,
                "scrollX": true,
                buttons: [
                    'csv', 'excel', 'colvis'
                ]
            });
        });
    </script>

    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 100%;
            margin: 0;
        }

        .dt-button-collection {
            margin-top: 3px !important;
        }
    </style>
</asp:Content>

