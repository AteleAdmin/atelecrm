﻿<%@ Page Title="Update Re Open Lead" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="UpdateReOpenLead.aspx.cs" Inherits="ATelBPOCRM.UpdateReOpenLead" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Re Open Lead Form</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Leads</a></li>
            <li class="active">Re Open Lead</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <asp:Panel ID="pnlGetLead" runat="server">
                <div class="col-md-12">
                    <div class="pull-left">
                        <div class="col-md-1">
                            Id:<asp:TextBox ID="txtLeadId" ReadOnly="True" TextMode="Number" CssClass="form-control" Width="70" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            VId:<asp:TextBox ID="txtViciLeadId" ReadOnly="True" TextMode="Number" CssClass="form-control" Width="70" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            Phone :<asp:TextBox ID="txtPhone" TextMode="Number" CssClass="form-control" MaxLength="10" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-4" style="margin-top: 19px; margin-bottom: 2px;">
                            <asp:LinkButton ID="btnGetInsuranceLead" runat="server" CssClass="btn btn-flat btn-primary btn-xs" OnClick="btnGetInsuranceLead_OnClick"><i class="fa fa-eye"></i>&nbsp; Get Vici Lead</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="Panel1" runat="server">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lead Information</h3>
                            <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <asp:TextBox ID="txtFName" CssClass="form-control" placeholder="First Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <asp:TextBox ID="txtLName" CssClass="form-control" placeholder="Last Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                                            <asp:ListItem>Male</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>City</label>
                                        <asp:TextBox ID="txtCity" CssClass="form-control" placeholder="City" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>State</label>
                                        <asp:TextBox ID="txtState" CssClass="form-control" placeholder="State" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Zip Code</label>
                                        <asp:TextBox ID="txtZip" CssClass="form-control" placeholder="Zip Code" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Phone No</label>
                                        <asp:TextBox ID="txtPhoneNo" CssClass="form-control" placeholder="Phone Number" ReadOnly="true" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Insurance Company</label>
                                        <asp:TextBox ID="txtCompany" CssClass="form-control" placeholder="Insurance Company" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Make</label>
                                        <asp:TextBox ID="txtMake" CssClass="form-control" placeholder="Make" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <asp:TextBox ID="txtYear" TextMode="Number" CssClass="form-control" placeholder="Year" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Model</label>
                                        <asp:TextBox ID="txtModel" CssClass="form-control" placeholder="Model" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Date-of-Birth</label>
                                        <asp:TextBox ID="txtDOB" CssClass="form-control datepicker" placeholder="Date-of-Birth" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <asp:TextBox ID="txtEmail" CssClass="form-control" placeholder="Email" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Home Ownership</label>
                                        <asp:DropDownList ID="ddlHOwnerShip" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="True">Yes</asp:ListItem>
                                            <asp:ListItem Value="False">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlAgency" runat="server">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Agency Owner</label>
                                            <asp:DropDownList ID="ddlAgencyOwner" CssClass="form-control select2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAgencyOwner_OnSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Agency Producer</label>
                                            <asp:DropDownList ID="ddlAgencyProducer" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <asp:TextBox ID="txtAddress" CssClass="form-control" placeholder="Address" TextMode="MultiLine" Rows="2" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlDisposition" runat="server">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Disposition</label>
                                            <asp:DropDownList ID="ddlDisposition" CssClass="form-control select2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDisposition_OnSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlFollow" runat="server" Visible="False">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Follow-up Date</label>
                                            <asp:TextBox ID="txtFollowDate" CssClass="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Comments</label>
                                        <asp:TextBox ID="txtComments" Rows="3" CssClass="form-control" TextMode="MultiLine" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat btn-sm" OnClick="btnSave_OnClick" />
                                | <a href="Leads.aspx" class="btn btn-sm btn-info btn-flat"><< Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </section>
    <!-- // section -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <%--<script src="backend/plugins/ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('<%= txtComments.ClientID %>');
        })
    </script>--%>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>

  <%--  <script type="text/javascript">
        var uri = window.location.toString();
        if (uri.indexOf("?") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("?"));
            window.history.replaceState({}, document.title, clean_uri);
        }
    </script>--%>
</asp:Content>
