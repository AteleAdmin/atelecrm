﻿<%@ Page Title="Maverick Leads" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="AgencyLivePingPulseHome.aspx.cs" Inherits="ATelBPOCRM.AgencyLivePingPulseHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .pnp-body {
            background: gray;
        }

        .ping-body {
            background: #1ab1f0;
        }

        .pnp-card {
            background-color: #444 !important;
        }

        .ping-card {
            background-color: #607d8b !important;
        }

        .pnp-category {
            color: #fff !important;
        }

        .pnp-title {
            color: #fff !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li <% if (MultiView1.ActiveViewIndex == 0) Response.Write("class=\"active\""); %> />
                        <asp:LinkButton ID="btnAnalytices" runat="server" OnClick="btnAnalytices_OnClick"><i class="fa fa-bar-chart"></i>&nbsp;Analytics</asp:LinkButton>
                        <li <% if (MultiView1.ActiveViewIndex == 1) Response.Write("class=\"active\""); %> />
                        <asp:LinkButton ID="btnPing" runat="server" OnClick="btnPing_OnClick"><img src="backend/img/Ping-1.png" style="height: 30px;" />&nbsp;Ping</asp:LinkButton>
                        <li <% if (MultiView1.ActiveViewIndex == 2) Response.Write("class=\"active\""); %> />
                        <asp:LinkButton ID="btnPNP" runat="server" OnClick="btnPNP_OnClick"><img src="backend/img/Pulse-1.png" style="height: 30px;" />&nbsp;Pulse</asp:LinkButton>
                        <li <% if (MultiView1.ActiveViewIndex == 3) Response.Write("class=\"active\""); %> />
                        <asp:LinkButton ID="btnLive" runat="server" OnClick="btnLive_OnClick"><img src="backend/img/Live-1.png" style="height: 30px;" />&nbsp;Live</asp:LinkButton>
                    </ul>
                    <div class="tab-content">
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Date Range</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>From :</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtAnalyticsLiveFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>To :</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtAnalyticsLiveTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" style="margin-top: 23px;">
                                                        <asp:Button ID="btnAnalyticsSubmit" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnAnalyticsSubmit_OnClick" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- // row date-range -->

                                <!-- LIVE -->
                                <!-- col-md-12 -->
                                <asp:Panel ID="pnlTotalCounter" runat="server" Visible="False">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Lead Summary </h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <div class="col-md-4">
                                                        <div class="card-mat card-stats">
                                                            <div class="card-header card-header-info card-header-icon">
                                                                <div class="card-icon">
                                                                    <i class="icon-stats-bars2"></i>
                                                                </div>
                                                                <p class="card-category">Total</p>
                                                                <h3 class="card-title">
                                                                    <asp:Literal ID="ltTotalLead" runat="server" Text="0"></asp:Literal>
                                                                    <small>Leads</small>
                                                                </h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="icon-eye"></i>
                                                                    <a class="leadCount" data-request="totalLead" href="javascript:;">View Detail</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- // Total -->
                                                    <div class="col-md-4">
                                                        <div class="card-mat card-stats">
                                                            <div class="card-header card-header-success card-header-icon">
                                                                <div class="card-icon">
                                                                    <i class="icon-stats-bars2"></i>
                                                                </div>
                                                                <p class="card-category">Accepted</p>
                                                                <h3 class="card-title">
                                                                    <asp:Literal ID="ltAcceptedLead" runat="server" Text="0"></asp:Literal>
                                                                    <small>Leads</small>
                                                                </h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="icon-eye"></i>
                                                                    <a class="leadCount" data-request="acceptedLead" href="javascript:;">View Detail</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- // Accepted -->

                                                    <div class="col-md-4">
                                                        <div class="card-mat card-stats">
                                                            <div class="card-header card-header-danger card-header-icon">
                                                                <div class="card-icon">
                                                                    <i class="icon-stats-bars2"></i>
                                                                </div>
                                                                <p class="card-category">Rejected</p>
                                                                <h3 class="card-title">
                                                                    <asp:Literal ID="ltRejectedLead" runat="server" Text="0"></asp:Literal>
                                                                    <small>Leads</small>
                                                                </h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="icon-eye"></i>
                                                                    <a class="leadCount" data-request="rejectedLead" href="javascript:;">View Detail</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- // Rejected -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <!-- // col-md-12 Count-->

                                <!-- col-md-12 -->
                                <asp:Panel ID="pnlAcceptedRejected" runat="server" Visible="False">
                                    <div class="row">
                                        <!-- col-md-6 -->
                                        <div class="col-md-6">
                                            <div class="box box-danger">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Rejected Leads</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <asp:GridView ID="gvRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowCreated="gvRejected_OnRowCreated">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Lead Id">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Disposition">
                                                                <ItemTemplate>
                                                                    <a href="javascript:;">
                                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- // col-md-6 -->
                                        <!-- col-md-6 -->
                                        <div class="col-md-6">
                                            <div class="box box-success">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Accepted Leads</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <asp:GridView ID="gvAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowCreated="gvAccepted_OnRowCreated">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Lead Id">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Disposition">
                                                                <ItemTemplate>
                                                                    <a href="javascript:;">
                                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- // col-md-6 -->
                                    </div>
                                </asp:Panel>
                                <!-- // col-md-12 Summary-->

                                <!-- col-md-12 -->
                                <asp:Panel ID="pnlAgencyProducer" runat="server" Visible="False">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="box box-danger">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Agency Producer Rejected</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <asp:GridView ID="gvAgencyProducerRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Lead Id">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <a href="javascript:;" data-find="producerRejected" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / Rejected -->
                                        <div class="col-md-4">
                                            <div class="box box-success">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Agency Producer Accepted</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <asp:GridView ID="gvAgencyProducerAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Lead Id">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <a href="javascript:;" data-find="producerAccepted" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / Accepted -->
                                        <div class="col-md-4">
                                            <div class="box box-info">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Agency Producer Total</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                    <!-- /.box-tools -->
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <asp:GridView ID="gvAgencyProducerTotal" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Lead Id">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <a href="javascript:;" data-find="producerTotal" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                        <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / Total -->
                                    </div>
                                </asp:Panel>
                                <!-- // col-md-12 Producer Count-->
                                <!-- // LIVE -->
                                <!-- PULSE -->
                                <div class="tab-content">
                                    <div style="padding: 10px; width: 100%; background: #111; margin-bottom: 5px; color: #fff; font-weight: bold; text-align: center;">Pulse</div>
                                    <!-- row -->
                                    <asp:Panel ID="pnlPNPTotalCounter" runat="server" Visible="False">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="box box-primary">
                                                    <div class="box-header with-border">
                                                        <%--<h3 class="box-title">Ping N' Post Summary</h3>--%>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body pnp-body">
                                                        <div class="col-md-4">
                                                            <div class="card-mat card-stats pnp-card">
                                                                <div class="card-header card-header-info card-header-icon">
                                                                    <div class="card-icon">
                                                                        <i class="icon-stats-bars2"></i>
                                                                    </div>
                                                                    <p class="card-category pnp-category">Total</p>
                                                                    <h3 class="card-title pnp-title">
                                                                        <asp:Literal ID="ltPNPTotalLead" runat="server" Text="0"></asp:Literal>
                                                                        <small class="pnp-title">Leads</small>
                                                                    </h3>
                                                                </div>
                                                                <div class="card-footer">
                                                                    <div class="stats">
                                                                        <%--<i class="icon-eye"></i>
                                                                        <a class="leadCount" data-request="totalLead" href="javascript:;">View Detail</a>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Total -->
                                                        <div class="col-md-4">
                                                            <div class="card-mat card-stats pnp-card">
                                                                <div class="card-header card-header-success card-header-icon">
                                                                    <div class="card-icon">
                                                                        <i class="icon-stats-bars2"></i>
                                                                    </div>
                                                                    <p class="card-category pnp-category">Accepted</p>
                                                                    <h3 class="card-title pnp-title">
                                                                        <asp:Literal ID="ltPNPAcceptedLead" runat="server" Text="0"></asp:Literal>
                                                                        <small class="pnp-title">Leads</small>
                                                                    </h3>
                                                                </div>
                                                                <div class="card-footer">
                                                                    <div class="stats">
                                                                        <%-- <i class="icon-eye"></i>
                                                                        <a class="leadCount" data-request="acceptedLead" href="javascript:;">View Detail</a>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Accepted -->

                                                        <div class="col-md-4">
                                                            <div class="card-mat card-stats pnp-card">
                                                                <div class="card-header card-header-danger card-header-icon">
                                                                    <div class="card-icon">
                                                                        <i class="icon-stats-bars2"></i>
                                                                    </div>
                                                                    <p class="card-category pnp-category">Rejected</p>
                                                                    <h3 class="card-title pnp-title">
                                                                        <asp:Literal ID="ltPNPRejectedLead" runat="server" Text="0"></asp:Literal>
                                                                        <small class="pnp-title">Leads</small>
                                                                    </h3>
                                                                </div>
                                                                <div class="card-footer">
                                                                    <div class="stats">
                                                                        <%-- <i class="icon-eye"></i>
                                                                        <a class="leadCount" data-request="rejectedLead" href="javascript:;">View Detail</a>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Rejected -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <!-- // row -->

                                    <!-- row -->
                                    <asp:Panel ID="pnlPNPAcceptedRejected" runat="server" Visible="False">
                                        <div class="row">
                                            <!-- col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="box box-danger">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Rejected Leads</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPNPRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowCreated="gvPNPRejected_OnRowCreated">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Disposition">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;">
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // col-md-6 -->
                                            <!-- col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="box box-success">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Accepted Leads</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPNPAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowCreated="gvPNPAccepted_OnRowCreated">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Disposition">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;">
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // col-md-6 -->
                                        </div>
                                    </asp:Panel>
                                    <!-- // row -->

                                    <!-- row -->
                                    <div class="row">
                                        <asp:Panel ID="pnlPNPAgencyProducer" runat="server" Visible="False">
                                            <div class="col-md-4">
                                                <div class="box box-danger">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Agency Producer Rejected</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPNPAgencyProducerRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;" data-find="producerRejected" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / Rejected -->
                                            <div class="col-md-4">
                                                <div class="box box-success">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Agency Producer Accepted</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPNPAgencyProducerAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;" data-find="producerAccepted" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / Accepted -->
                                            <div class="col-md-4">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Agency Producer Total</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPNPAgencyProducerTotal" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;" data-find="producerTotal" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / Total -->
                                        </asp:Panel>
                                    </div>
                                    <!-- // row -->
                                </div>
                                <!-- // PULSE -->
                                <!-- PING -->
                                <div class="tab-content">
                                    <div style="padding: 10px; width: 100%; background: #1AB1F0; margin-bottom: 5px; color: #fff; font-weight: bold; text-align: center;">Ping</div>
                                    <!-- row -->
                                    <asp:Panel ID="pnlPingTotalCounter" runat="server" Visible="False">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="box box-primary">
                                                    <div class="box-header with-border">
                                                        <%--<h3 class="box-title">Ping N' Post Summary</h3>--%>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body ping-body">
                                                        <div class="col-md-4">
                                                            <div class="card-mat card-stats ping-card">
                                                                <div class="card-header card-header-info card-header-icon">
                                                                    <div class="card-icon">
                                                                        <i class="icon-stats-bars2"></i>
                                                                    </div>
                                                                    <p class="card-category pnp-category">Total</p>
                                                                    <h3 class="card-title pnp-title">
                                                                        <asp:Literal ID="ltPingTotalLead" runat="server" Text="0"></asp:Literal>
                                                                        <small class="pnp-title">Leads</small>
                                                                    </h3>
                                                                </div>
                                                                <div class="card-footer">
                                                                    <div class="stats">
                                                                        <%--<i class="icon-eye"></i>
                                                                        <a class="leadCount" data-request="totalLead" href="javascript:;">View Detail</a>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Total -->
                                                        <div class="col-md-4">
                                                            <div class="card-mat card-stats ping-card">
                                                                <div class="card-header card-header-success card-header-icon">
                                                                    <div class="card-icon">
                                                                        <i class="icon-stats-bars2"></i>
                                                                    </div>
                                                                    <p class="card-category pnp-category">Accepted</p>
                                                                    <h3 class="card-title pnp-title">
                                                                        <asp:Literal ID="ltPingAcceptedLead" runat="server" Text="0"></asp:Literal>
                                                                        <small class="pnp-title">Leads</small>
                                                                    </h3>
                                                                </div>
                                                                <div class="card-footer">
                                                                    <div class="stats">
                                                                        <%-- <i class="icon-eye"></i>
                                                                        <a class="leadCount" data-request="acceptedLead" href="javascript:;">View Detail</a>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Accepted -->

                                                        <div class="col-md-4">
                                                            <div class="card-mat card-stats ping-card">
                                                                <div class="card-header card-header-danger card-header-icon">
                                                                    <div class="card-icon">
                                                                        <i class="icon-stats-bars2"></i>
                                                                    </div>
                                                                    <p class="card-category pnp-category">Rejected</p>
                                                                    <h3 class="card-title pnp-title">
                                                                        <asp:Literal ID="ltPingRejectedLead" runat="server" Text="0"></asp:Literal>
                                                                        <small class="pnp-title">Leads</small>
                                                                    </h3>
                                                                </div>
                                                                <div class="card-footer">
                                                                    <div class="stats">
                                                                        <%-- <i class="icon-eye"></i>
                                                                        <a class="leadCount" data-request="rejectedLead" href="javascript:;">View Detail</a>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- // Rejected -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <!-- // row -->

                                    <!-- row -->
                                    <asp:Panel ID="pnlPingAcceptedRejected" runat="server" Visible="False">
                                        <div class="row">
                                            <!-- col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="box box-danger">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Rejected Leads</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPingRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowCreated="gvPingRejected_OnRowCreated">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Disposition">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;">
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // col-md-6 -->
                                            <!-- col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="box box-success">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Accepted Leads</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPingAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server" OnRowCreated="gvPingAccepted_OnRowCreated">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Disposition">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;">
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // col-md-6 -->
                                        </div>
                                    </asp:Panel>
                                    <!-- // row -->

                                    <!-- row -->
                                    <div class="row">
                                        <asp:Panel ID="pnlPingAgencyProducer" runat="server" Visible="False">
                                            <div class="col-md-4">
                                                <div class="box box-danger">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Agency Producer Rejected</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPingAgencyProducerRejected" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;" data-find="producerRejected" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / Rejected -->
                                            <div class="col-md-4">
                                                <div class="box box-success">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Agency Producer Accepted</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPingAgencyProducerAccepted" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;" data-find="producerAccepted" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / Accepted -->
                                            <div class="col-md-4">
                                                <div class="box box-info">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Agency Producer Total</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <asp:GridView ID="gvPingAgencyProducerTotal" CssClass="table table-bordered table-striped dataTable" AutoGenerateColumns="False" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Lead Id">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:;" data-find="producerTotal" class="producerCountDetail" data-producer='<%# Eval("Name") %>'>
                                                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="ltTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Literal>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / Total -->
                                        </asp:Panel>
                                    </div>
                                    <!-- // row -->
                                </div>
                                <!--// PING -->
                            </asp:View>
                            <asp:View runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Date Range</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>From :</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtPingFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>To :</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtPingTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" style="margin-top: 23px;">
                                                        <asp:Button ID="btnGetPingLead" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnGetPingLead_OnClick" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- //col-md-12 -->
                                </div>
                                <!-- // row -->

                                <!-- row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Export Leads</h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <asp:GridView ID="gvPingAllLead" CssClass="table table-bordered table-striped pingDataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowCreated="gvPingAllLead_OnRowCreated" OnRowDataBound="gvPingAllLead_OnRowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Lead Id">
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex+1 %>
                                                                <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <a href="UpdatePNPLead.aspx?LeadId=<%# Eval("Id") %>">
                                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                </a>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltN" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="City" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="State" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zip" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Phone">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Email">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Company" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Year" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Model" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Agency" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Producer">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="DOB">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Homeowner" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Disposition">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Active" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Agent">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltAgent" runat="server" Text='<%# Eval("Agent") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Product">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltProduct" runat="server" Text='<%# Eval("IsDemandPNP") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Time">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltTime" runat="server" Text='<%# Eval("TimeStamp") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Address">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No Flood">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltNoFlood" runat="server" Text='<%# Eval("IsFlooded") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No Open Claim">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltNoOpenClaim" runat="server" Text='<%# Eval("IsOpenClaim") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                                <!-- // row -->
                            </asp:View>
                            <asp:View runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Date Range</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>From :</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtPNPFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>To :</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtPNPTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" style="margin-top: 23px;">
                                                        <asp:Button ID="btnGetLead" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnGetLead_OnClick" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- //col-md-12 -->
                                </div>
                                <!-- // row -->

                                <!-- row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Export Leads</h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <asp:GridView ID="gvPNPAllLeads" CssClass="table table-bordered table-striped pnpDataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowCreated="gvPNPAllLeads_OnRowCreated" OnRowDataBound="gvPNPAllLeads_OnRowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Lead Id">
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex+1 %>
                                                                <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <a href="UpdatePNPLead.aspx?LeadId=<%# Eval("Id") %>">
                                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                </a>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltN" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="City" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="State" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zip" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Phone">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Email">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Company" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Year" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Model" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Agency">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Producer">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="DOB">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Homeowner" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Disposition">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Active" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Agent">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltAgent" runat="server" Text='<%# Eval("Agent") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Product">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltProduct" runat="server" Text='<%# Eval("IsDemandPNP") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Time">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltTime" runat="server" Text='<%# Eval("TimeStamp") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Address">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No Flood">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltNoFlood" runat="server" Text='<%# Eval("IsFlooded") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No Open Claim">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltNoOpenClaim" runat="server" Text='<%# Eval("IsOpenClaim") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                                <!-- // row -->
                            </asp:View>
                            <asp:View runat="server">
                                <asp:Panel ID="pnlDateRange" runat="server" Visible="True">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Date Range</h3>
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>From :</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>To :</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group" style="margin-top: 23px;">
                                                            <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnStats_OnClick" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <!--// col-md-12 -->

                                <!-- col-md-12 -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Export Leads</h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.box-tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <asp:GridView ID="gvAllLeads" CssClass="table table-bordered table-striped liveDataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowCreated="gvAllLeads_OnRowCreated" OnRowDataBound="gvAllLeads_OnRowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Lead Id">
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex+1 %>
                                                                <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <a href="UpdateLead.aspx?LeadId=<%# Eval("Id") %>&ping=false">
                                                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                                </a>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltN" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="City" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="State" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zip" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Phone">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Email">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Company" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Year" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Model" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Agency">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Producer">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="DOB">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Homeowner" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Disposition">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Address">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltAddress" CssClass="label label-default" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No Flood">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltNoFlood" runat="server" Text='<%# Eval("IsFlooded") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No Open Claim">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ltNoOpenClaim" runat="server" Text='<%# Eval("IsOpenClaim") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <!-- // col-md-12 All Leads -->
                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <Lucky:DataTableControl ID="DataTable" runat="server" />
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.liveDataTable').prepend($("<thead></thead>").append($('.liveDataTable').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                'paging': true,
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering": false,
                "scrollX": true,
                'info': true,
                buttons: [
                    'csv', 'excel', 'pageLength'
                ],
                "language": {
                    "emptyTable": "No Record Found!"
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.pnpDataTable').prepend($("<thead></thead>").append($('.pnpDataTable').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                'paging': true,
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering": false,
                "scrollX": true,
                'info': true,
                buttons: [
                    'csv', 'excel', 'pageLength'
                ],
                "language": {
                    "emptyTable": "No Record Found!"
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.pingDataTable').prepend($("<thead></thead>").append($('.pingDataTable').find("tr:first"))).DataTable({
                dom: 'Bfrtip',
                'paging': true,
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "ordering": false,
                "scrollX": true,
                'info': true,
                buttons: [
                    'csv', 'excel', 'pageLength'
                ],
                "language": {
                    "emptyTable": "No Record Found!"
                }
            });
        });
    </script>
</asp:Content>

