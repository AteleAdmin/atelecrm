﻿<%@ Page Title="Add New Api" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AddApi.aspx.cs" Inherits="ATelBPOCRM.AddApi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>New Api</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Api Information</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" style="margin-bottom: 0px !important">
                                    <asp:TextBox ID="txtApiId" CssClass="form-control" TextMode="Number" Visible="False" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Agency</label>
                                    <asp:DropDownList ID="ddlAgencyOwner" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Client Name</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" Text="*" ControlToValidate="txtName" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtName" CssClass="form-control" placeholder="Client Name" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Auth Type</label>
                                    <asp:DropDownList ID="ddlAuthType" CssClass="form-control select2" runat="server">
                                        <asp:ListItem>No Auth</asp:ListItem>
                                        <asp:ListItem>Api Key</asp:ListItem>
                                        <asp:ListItem>Bearer Token</asp:ListItem>
                                        <asp:ListItem>Basic Auth</asp:ListItem>
                                        <asp:ListItem>Digest Auth</asp:ListItem>
                                        <asp:ListItem>OAuth 1.0</asp:ListItem>
                                        <asp:ListItem>OAuth 2.0</asp:ListItem>
                                        <asp:ListItem>Hawk Authentication</asp:ListItem>
                                        <asp:ListItem>AWS Signature</asp:ListItem>
                                        <asp:ListItem>NTLM Authentication [Beta]</asp:ListItem>
                                        <asp:ListItem>Akamai EdgeGrid</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Username</label>
                                    <asp:TextBox ID="txtUName" CssClass="form-control" placeholder="Username" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Password</label>
                                    <asp:TextBox ID="txtPassword" TextMode="Password" CssClass="form-control" placeholder="Password" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Request</label>
                                    <asp:DropDownList ID="ddlRequest" CssClass="form-control select2" runat="server">
                                        <asp:ListItem>XML</asp:ListItem>
                                        <asp:ListItem>JSON</asp:ListItem>
                                        <asp:ListItem>SOAP</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Response</label>
                                    <asp:DropDownList ID="ddlResponse" CssClass="form-control select2" runat="server">
                                        <asp:ListItem>XML</asp:ListItem>
                                        <asp:ListItem>JSON</asp:ListItem>
                                        <asp:ListItem>SOAP</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Resource</label>
                                    <asp:TextBox ID="txtResource" CssClass="form-control" placeholder="Resource" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Url</label>
                                    <asp:TextBox ID="txtUrl" CssClass="form-control" placeholder="Url" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label>Product</label>
                                    <asp:DropDownList ID="ddlProduct" CssClass="form-control select2" runat="server">
                                        <asp:ListItem Value="Live" Text="Live"></asp:ListItem>
                                        <asp:ListItem Value="Ping" Text="Ping"></asp:ListItem>
                                        <asp:ListItem Value="Pulse" Text="Pulse"></asp:ListItem>
                                        <asp:ListItem Value="Live Home" Text="Live Home"></asp:ListItem>
                                        <asp:ListItem Value="Ping Home" Text="Ping Home"></asp:ListItem>
                                        <asp:ListItem Value="Pulse Home" Text="Pulse Home"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Verbose Type</label>
                                    <asp:DropDownList ID="ddlVerbose" CssClass="form-control select2" runat="server">
                                        <asp:ListItem>GET</asp:ListItem>
                                        <asp:ListItem>POST</asp:ListItem>
                                        <asp:ListItem>PUT</asp:ListItem>
                                        <asp:ListItem>DELETE</asp:ListItem>
                                        <asp:ListItem>PATCH</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>End Point</label>
                                    <asp:TextBox ID="txtEndPoint" CssClass="form-control" placeholder="EndPoint" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Bearer Token</label>
                                    <asp:TextBox ID="txtBearerToken" CssClass="form-control" Rows="2" TextMode="MultiLine" placeholder="Bearer Token" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <asp:TextBox ID="txtDescription" CssClass="form-control" TextMode="MultiLine" placeholder="Description" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat" OnClick="btnSave_OnClick" />
                                    <asp:Button ID="btnUpdate" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat" OnClick="btnUpdate_OnClick" Visible="False" />
                                    <a href="ClientLeadApiList.aspx" class="btn btn-info btn-flat"><< Go Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>

    <script src="backend/plugins/ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('<%= txtDescription.ClientID %>');
        })
    </script>
</asp:Content>
