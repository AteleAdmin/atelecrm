﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeadCount.aspx.cs" Inherits="ATelBPOCRM.LeadCount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="icon" href="backend/favicon.ico" type="image/ico" sizes="16x16" />
    <link href="backend/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="backend/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="backend/dtPagination.css" rel="stylesheet" />
    <link href="backend/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <style type="text/css">
        .label {
            -ms-border-radius: 0px !important;
            border-radius: 0px !important;
        }
    </style>
    <script src="backend/plugins/jquery/dist/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <asp:Literal ID="ltHeader" runat="server"></asp:Literal>
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped dataTable display" runat="server" AutoGenerateColumns="False" OnRowCreated="GridView1_OnRowCreated" OnRowDataBound="GridView1_OnRowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Lead Id">
                            <ItemTemplate>
                                <%# Container.DataItemIndex+1 %>
                                <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <a href="AddLead.aspx?LeadId=<%# Eval("Id") %>">
                                    <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="City">
                            <ItemTemplate>
                                <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State">
                            <ItemTemplate>
                                <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Zip">
                            <ItemTemplate>
                                <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone">
                            <ItemTemplate>
                                <asp:Literal ID="ltPhone" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Literal ID="ltEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Company">
                            <ItemTemplate>
                                <asp:Literal ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Year">
                            <ItemTemplate>
                                <asp:Literal ID="ltYear" runat="server" Text='<%# Eval("Year") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Model">
                            <ItemTemplate>
                                <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Agency">
                            <ItemTemplate>
                                <asp:Literal ID="lAgency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Transfer To">
                            <ItemTemplate>
                                <asp:Literal ID="ltTransferTo" runat="server" Text='<%# Eval("TransferTo") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DOB">
                            <ItemTemplate>
                                <asp:Literal ID="ltDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="H.Ownership">
                            <ItemTemplate>
                                <asp:Label ID="ltHOwnerShip" CssClass="label label-info" runat="server" Text='<%# Eval("HOwnerShip") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Disposition">
                            <ItemTemplate>
                                <asp:Label ID="ltDisposition" CssClass="label label-default" runat="server" Text='<%# Eval("Disposition") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <ItemTemplate>
                                <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </form>
    <!----------------------------------------------------------->
    <!-- datTable -->
    <script src="backend/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.display').DataTable({
                dom: 'Bfrtip',
                "paging": false,
                "scrollX": true,
                buttons: [
                    'csv', 'excel'
                ]
            });
        });
    </script>
</body>
</html>
