﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ATelBPOCRM.Login" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="">
<head runat="server" lang="">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Log in | Mav Leads</title>
    <link rel="icon" href="backend/fav-icon.png" type="image/ico" sizes="16x16" />
    <link href="backend/login/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="backend/login/css/core.min.css" rel="stylesheet" type="text/css" />
    <link href="backend/login/css/components.min.css" rel="stylesheet" type="text/css" />
    <link href="backend/login/css/colors.min.css" rel="stylesheet" type="text/css" />
    <link href="backend/toaster/toastr.min.css" rel="stylesheet" />
    <!-- /global stylesheets -->
    <link href="minify/google-font.min.css" rel="stylesheet" />
    <link href="backend/login/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <!-- Global stylesheets -->
    <%--<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />--%>
    <!-- Core JS files -->
    <script src="backend/login/js/core/libraries/jquery.min.js" type="text/javascript"></script>
    <script src="backend/toaster/toastr.min.js" type="text/javascript"></script>
    <script src="backend/toaster/toasterCustom.js" type="text/javascript"></script>
    <style type="text/css">
        .icon-object {
            /*border-radius: 0px !important;
            border: none !important;
            margin-left: -30px;*/
        }

        .login-covers {
            background-color: #37474f;
            border-color: #37474f;
            color: #fff;
        }

        hr {
            margin-top: 10px !important;
            margin-bottom: 10px !important;
        }

        .pb-20 {
            padding-top: 110px !important;
        }

        .bg-pink-400 {
            background: #26a69a !important;
            border-color: #00bcd4 !important;
        }
    </style>
    <!-- /theme JS files -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
         
        <![endif]-->
</head>
<body class="login-container login-covers">
    <form id="form1" runat="server">
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main content -->
                <div class="content-wrapper">
                    <!-- Content area -->
                    <div class="content pb-20">
                        <!-- Form with validation -->
                        <div class="form-validate formnovalidate" id="formnovalidate">
                            <div class="panel panel-body login-form">
                                <div class="text-center">
                                    <img src="backend/images/login-logo.png" style="height: 60px;" alt="A Tel Logo" />
                                    <hr />
                                    <%--<div class="icon-object border-slate-300 text-slate-300">
                                        <i class="icon-reading"></i>
                                    </div>--%>
                                    <h5 class="content-group">Login to your account <small class="display-block"></small></h5>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Username / Email" required></asp:TextBox>
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <asp:TextBox ID="txtPassword" runat="server" class="form-control" TextMode="Password" placeholder="Password" required></asp:TextBox>
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group login-options">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="remember_me" value="1" />
                                                Remember me
                                            </label>
                                        </div>

                                        <%--<div class="col-sm-6 text-right">
                                            <a href="javascript:;">Forgot password?</a>
                                        </div>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Button ID="btnLogin" runat="server" Text="Sign In" class="btn bg-pink-400 btn-block" OnClick="btnLogin_OnClick" />
                                </div>
                                <div class="text-center">
                                    <img src="backend/img/WashHands.png" style="height: 140px;" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="backend/login/js/plugins/loaders/pace.min.js" type="text/javascript" async=""></script>
    <script src="backend/login/js/core/libraries/bootstrap.min.js" type="text/javascript" async=""></script>
    <script src="backend/login/js/plugins/loaders/blockui.min.js" type="text/javascript" async=""></script>
    <!-- /core JS files -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8" async=""></script>

    <!-- Include a polyfill for ES6 Promises (optional) for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js" type="text/javascript" async=""></script>
    <!-- Theme JS files -->
    <script src="backend/login/js/app.js" type="text/javascript" async=""></script>
    <script src="backend/login/js/plugins/ui/ripple.min.js" type="text/javascript" async=""></script>
</body>
</html>
