﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="ATelBPOCRM.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section>
        <div class="page-header1 page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="fa fa-arrow-circle-o-left position-left" aria-hidden="true"></i><span class="text-semibold">Home</span> - Dashboard</h4>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
            </div>

            <div class="breadcrumb-line">
                <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="Dashboard.aspx"><i class="fa fa-home position-left" aria-hidden="true"></i>Home</a></li>
                    <li class="active">Dashboard</li>
                </ul>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <asp:Panel ID="pnlAdmin" runat="server" Visible="False">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Date Range</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>From :</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>To :</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-top: 23px;">
                                    <asp:Button ID="btnStats" runat="server" Text="Get Stats" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnStats_OnClick" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- // col-md-12-->
                </div>
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Leads Counts</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-4">
                                <div class="card-mat card-stats">
                                    <div class="card-header card-header-info card-header-icon">
                                        <div class="card-icon">
                                            <i class="icon-stats-bars2"></i>
                                        </div>
                                        <p class="card-category">Total</p>
                                        <h3 class="card-title">
                                            <asp:Literal ID="ltTotalLead" runat="server" Text="0"></asp:Literal>
                                            <small>Leads</small>
                                        </h3>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="icon-eye"></i>
                                            <a class="leadCount" data-request="totalLead" href="javascript:;">View Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // Total -->
                            <div class="col-md-4">
                                <div class="card-mat card-stats">
                                    <div class="card-header card-header-success card-header-icon">
                                        <div class="card-icon">
                                            <i class="icon-stats-bars2"></i>
                                        </div>
                                        <p class="card-category">Accepted</p>
                                        <h3 class="card-title">
                                            <asp:Literal ID="ltAcceptedLead" runat="server" Text="0"></asp:Literal>
                                            <small>Leads</small>
                                        </h3>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="icon-eye"></i>
                                            <a class="leadCount" data-request="acceptedLead" href="javascript:;">View Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // Accepted -->

                            <div class="col-md-4">
                                <div class="card-mat card-stats">
                                    <div class="card-header card-header-danger card-header-icon">
                                        <div class="card-icon">
                                            <i class="icon-stats-bars2"></i>
                                        </div>
                                        <p class="card-category">Rejected</p>
                                        <h3 class="card-title">
                                            <asp:Literal ID="ltRejectedLead" runat="server" Text="0"></asp:Literal>
                                            <small>Leads</small>
                                        </h3>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="icon-eye"></i>
                                            <a class="leadCount" data-request="rejectedLead" href="javascript:;">View Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // Rejected -->
                        </div>
                    </div>
                </div>
                <!-- // Live Leads -->
                <div class="col-md-12">
                    <div style="padding: 10px; width: 100%; background: #111; margin-bottom: 5px; color: #fff; font-weight: bold; text-align: center;">Ping N' Post</div>
                </div>
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Leads Counts</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-4">
                                <div class="card-mat card-stats">
                                    <div class="card-header card-header-info card-header-icon">
                                        <div class="card-icon">
                                            <i class="icon-stats-bars2"></i>
                                        </div>
                                        <p class="card-category">Total</p>
                                        <h3 class="card-title">
                                            <asp:Literal ID="ltPNPTotal" runat="server" Text="0"></asp:Literal>
                                            <small>Leads</small>
                                        </h3>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="icon-eye"></i>
                                            <a class="leadCount" data-request="totalLead" href="javascript:;">View Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // Total -->
                            <div class="col-md-4">
                                <div class="card-mat card-stats">
                                    <div class="card-header card-header-success card-header-icon">
                                        <div class="card-icon">
                                            <i class="icon-stats-bars2"></i>
                                        </div>
                                        <p class="card-category">Accepted</p>
                                        <h3 class="card-title">
                                            <asp:Literal ID="ltPNPAccepted" runat="server" Text="0"></asp:Literal>
                                            <small>Leads</small>
                                        </h3>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="icon-eye"></i>
                                            <a class="leadCount" data-request="acceptedLead" href="javascript:;">View Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // Accepted -->

                            <div class="col-md-4">
                                <div class="card-mat card-stats">
                                    <div class="card-header card-header-danger card-header-icon">
                                        <div class="card-icon">
                                            <i class="icon-stats-bars2"></i>
                                        </div>
                                        <p class="card-category">Rejected</p>
                                        <h3 class="card-title">
                                            <asp:Literal ID="ltPNPRejected" runat="server" Text="0"></asp:Literal>
                                            <small>Leads</small>
                                        </h3>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="icon-eye"></i>
                                            <a class="leadCount" data-request="rejectedLead" href="javascript:;">View Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // Rejected -->
                        </div>
                    </div>
                </div>
                <!-- // PNP Leads -->
            </asp:Panel>
        </div>

        <asp:Panel ID="pnlAnalytic" runat="server" Visible="False">
            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Day Wise Week Live Transfer</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="dayWiseWeekLive" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Month Wise Live Transfer</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="monthWiseLive" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row-1 -->
           <%-- <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Day Wise Week Ping N' Post</h3>
                        </div>
                        <div class="box-body">
                            <div id="dayWiseWeekPNP" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Month Wise Ping N' Post</h3>
                        </div>
                        <div class="box-body">
                            <div id="monthWisePNP" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>--%>
            <!-- Row-2 -->
            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Day Wise Week Live Ping N' Post</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="dayWiseWeekLivePNP" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Month Wise Live Ping N' Post</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="monthWiseLivePNP" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row-3 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Day Wise Live LPD</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="dayWiseLiveLPD" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row-4 -->
            <%--<div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Day Wise Live LPD</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="dayWisePNPLPD" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>--%>
            <!-- Row-5 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Day Wise Live PNP LPD</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="dayWiseLivePNPLPD" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row-6 -->
        </asp:Panel>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>

    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/DayWiseWeekLive") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadDayWiseWeekLiveChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadDayWiseWeekLiveChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("dayWiseWeekLive",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "Live Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>
    <!-- Day Wise Week Live -->

   <%-- <script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/DayWiseWeekPNP") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadDayWiseWeekPNPChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadDayWiseWeekPNPChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("dayWiseWeekPNP",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "PNP Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>--%>
    <!-- Day Wise Week PNP -->

    <script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/DayWiseWeekLivePNP") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadDayWiseWeekLivePNPChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadDayWiseWeekLivePNPChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("dayWiseWeekLivePNP",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "Live PNP Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>
    <!-- Day Wise Week Live PNP -->

    <script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/MonthWiseLive") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadDayWiseMonthLiveChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadDayWiseMonthLiveChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("monthWiseLive",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "Live Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                    //dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>
    <!-- Month Wise Live -->

    <script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/MonthWiseLivePNP") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadMonthWiseLivePNPChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadMonthWiseLivePNPChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("monthWiseLivePNP",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "Live PNP Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                    //dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>
    <!-- Month Wise Live PNP -->

    <%--<script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/MonthWisePNP") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadMonthWisePNPChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadMonthWisePNPChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("monthWisePNP",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "PNP Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                    //dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>--%>
    <!-- Month Wise PNP -->

    <script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/DayWiseLiveLPD") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadDayWiseLiveLPDChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadDayWiseLiveLPDChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("dayWiseLiveLPD",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "Live LPD Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                    //dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>
    <!-- Day Wise Week Live LPD -->

    <%--<script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/DayWisePNPLPD") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadDayWisePNPLPDChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadDayWisePNPLPDChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("dayWisePNPLPD",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "PNP LPD Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                    //dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>--%>
    <!-- Day Wise Week PNP LPD -->

    <script type="text/javascript">
        $(function () {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("Dashboard.asmx/DayWiseLivePNPLPD") %>',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    loadDayWiseLivePNPLPDChart(r.d);
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
        function loadDayWiseLivePNPLPDChart(d) {
            var dtt = eval(d);
            var chart = new CanvasJS.Chart("dayWiseLivePNPLPD",
                {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: ""
                    },
                    axisX: {
                        title: "Live PNP LPD Stat",
                        labelAngle: 45,
                        labelFontSize: 13,
                        fontFamily: "Robot, sans-serif"
                    },
                    axisY: {
                        title: ""
                    },
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    dataPointWidth: 20,
                    data: dtt
                    //dtt
                });
            try {
                chart.render();
            }
            catch (e) {
            }
            return false;
        }
    </script>
    <!-- Day Wise Week PNP LPD -->
</asp:Content>
