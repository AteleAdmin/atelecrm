﻿<%@ Page Title="Lead Report" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="LeadReport.aspx.cs" Inherits="ATelBPOCRM.LeadReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Lead Report</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:;"><i class="fa fa-gears"></i>Leads</a></li>
			<li class="active">Lead Report</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Report Filter</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="col-md-2">
							<label>Lead Type</label>
							<asp:DropDownList ID="ddType" CssClass="form-control" runat="server">
								<asp:ListItem Value="All">All</asp:ListItem>
								<asp:ListItem Value="Live">Live Transfers</asp:ListItem>
								<asp:ListItem Value="PNP">Ping N' Post</asp:ListItem>
							</asp:DropDownList>
						</div>
						<div class="col-md-2">
							<label>Team</label>
							<asp:DropDownList ID="ddlTeam" CssClass="form-control" runat="server">
							</asp:DropDownList>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label>From :</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
								</div>
								<!-- /.input group -->
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label>To :</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
								</div>
								<!-- /.input group -->
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group" style="margin-top: 23px;">
								<asp:Button ID="btnGenerate" runat="server" Text="Generate" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnGenerate_OnClick" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- // col-md-12 -->
			<div class="col-md-12">
				<table class="table table-condensed">
					<thead>
						<tr>
							<th></th>
							<th>Header</th>
							<th>Header</th>
							<th>Header</th>
							<th>Header</th>
						</tr>
					</thead>
					<tbody>
						<tr id="tr-detail" class="tr-detail hidden">
							<td></td>
							<td colspan="4">
								<div class="detail-content">
									<ul>
										<li>
											<div class="detail"></div>

											<div class="detail detail-main">
												<fieldset>
													<legend><span class="label label-primary">Nested Data</span></legend>
													<div>
														<table class="table table-condensed">
															<thead>
																<tr>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																</tr>
															</thead>
															<tr>
																<td>123</td>
																<td>123</td>
																<td>123</td>
																<td>123</td>
																<td>123</td>
															</tr>
															<tr>
																<td>123</td>
																<td>123</td>
																<td>123</td>
																<td>123</td>
																<td>123</td>
															</tr>
														</table>
													</div>
												</fieldset>
												<!-- 								<div class="panel panel-default">
								<div class="panel-heading">Nested Data</div>
									<div class="panel-body">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
												</tr>
											</thead>
											<tr>
												<td>123</td>
												<td>123</td>
												<td>123</td>
												<td>123</td>
												<td>123</td>
											</tr>
										</table>
									</div>
								</div> -->

											</div>
										</li>
										<li>
											<div class="detail"></div>

											<div class="detail detail-main">
												<fieldset>
													<legend><span class="label label-primary">Nested Data</span></legend>
													<div>
														<table class="table table-condensed">
															<thead>
																<tr>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																</tr>
															</thead>
															<tr>
																<td>123</td>
																<td>123</td>
																<td>123</td>
																<td>123</td>
																<td>123</td>
															</tr>
														</table>
													</div>
												</fieldset>
												<!-- 								<div class="panel panel-default">
								<div class="panel-heading">Nested Data</div>
									<div class="panel-body">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
												</tr>
											</thead>
											<tr>
												<td>123</td>
												<td>123</td>
												<td>123</td>
												<td>123</td>
												<td>123</td>
											</tr>
										</table>
									</div>
								</div> -->

											</div>
										</li>
										<li>
											<div class="detail"></div>

											<div class="detail detail-main">
												<fieldset>
													<legend><span class="label label-primary">Nested Data</span></legend>
													<div>
														<table class="table table-condensed">
															<thead>
																<tr>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																	<th>Header</th>
																</tr>
															</thead>
															<tr>
																<td>123</td>
																<td>123</td>
																<td>123</td>
																<td>123</td>
																<td>123</td>
															</tr>
														</table>
													</div>
												</fieldset>
												<!-- 								<div class="panel panel-default">
								<div class="panel-heading">Nested Data</div>
									<div class="panel-body">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
													<th>Header</th>
												</tr>
											</thead>
											<tr>
												<td>123</td>
												<td>123</td>
												<td>123</td>
												<td>123</td>
												<td>123</td>
											</tr>
										</table>
									</div>
								</div> -->

											</div>
										</li>
									</ul>
								</div>
							</td>
						</tr>
						<tr>
							<td><span class="fa fa-chevron-right fa-fw"></span></td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
						</tr>
						<tr>
							<td><span class="fa fa-chevron-right fa-fw"></span></span>
							</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
						</tr>
						<tr>
							<td><span class="fa fa-chevron-right fa-fw"></span></span>
							</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
						</tr>
						<tr>
							<td><span class="fa fa-chevron-right fa-fw"></span></span>
							</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
						</tr>
						<tr>
							<td><span class="fa fa-chevron-right fa-fw"></span></span>
							</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
							<td>Data</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
	<script type="text/javascript">
		$('.datepicker').datepicker({
			format: 'mm/dd/yyyy',
			todayHighlight: true,
			todayBtn: true,
			todayBtn: 'linked',
			clearBtn: true
		});
	</script>
</asp:Content>
