﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class OutsourceAgentReport : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                {
                    if (Session["Role"] != null && Session["Role"].ToString() == "Outsource")
                    {
                        PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulateLeadsCount(Convert.ToInt32(Session["UId"].ToString()), "", "");

                        PopulatePNPLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulatePNPLeadsCount(Convert.ToInt32(Session["UId"].ToString()), "", "");
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                    {
                        PopulateLeadsSummary(0, "", "");
                        PopulateLeadsCount(0, "", "");

                        PopulatePNPLeadsSummary(0, "", "");
                        PopulatePNPLeadsCount(0, "", "");

                        pnlOutsource.Visible = true;

                        PopulateOutsourceOwner();
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        public void PopulateOutsourceOwner()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.OutsourceOwner WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlOutsourceOwner.DataSource = dt;
                    ddlOutsourceOwner.DataTextField = "Name";
                    ddlOutsourceOwner.DataValueField = "Id";
                    ddlOutsourceOwner.DataBind();

                    ddlOutsourceOwner.Items.Insert(0, "Select Owner");
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnStats_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Outsource")
                {
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulateLeadsCount(Convert.ToInt32(Session["UId"].ToString()), from, to);

                    PopulatePNPLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulatePNPLeadsCount(Convert.ToInt32(Session["UId"].ToString()), from, to);
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    if (ddlOutsourceOwner.SelectedIndex > 0)
                    {
                        PopulateLeadsSummary(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), from, to);
                        PopulateLeadsCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), from, to);

                        PopulatePNPLeadsSummary(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), from, to);
                        PopulatePNPLeadsCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), from, to);
                    }
                    else
                    {
                        PopulateLeadsSummary(0, from, to);
                        PopulateLeadsCount(0, from, to);

                        PopulatePNPLeadsSummary(0, from, to);
                        PopulatePNPLeadsCount(0, from, to);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        #region Live
        public void PopulateLeadsCount(int? outsourceId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }

                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }

                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy = u.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateLeadsSummary(int? outsourceId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvRejected.DataSource = dt;
                    gvRejected.DataBind();
                }
                else
                {
                    gvRejected.EmptyDataText = "No Record Found!";
                    gvRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAccepted.DataSource = dt;
                    gvAccepted.DataBind();
                }
                else
                {
                    gvAccepted.EmptyDataText = "No Record Found!";
                    gvAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Total

            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvTotal.DataSource = dt;
                    gvTotal.DataBind();
                }
                else
                {
                    gvTotal.EmptyDataText = "No Record Found!";
                    gvTotal.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        } 
        #endregion

        #region PNP

        public void PopulatePNPLeadsCount(int? outsourceId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }

                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }

                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulatePNPLeadsSummary(int? outsourceId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPRejected.DataSource = dt;
                    gvPNPRejected.DataBind();
                }
                else
                {
                    gvPNPRejected.EmptyDataText = "No Record Found!";
                    gvPNPRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAccepted.DataSource = dt;
                    gvPNPAccepted.DataBind();
                }
                else
                {
                    gvPNPAccepted.EmptyDataText = "No Record Found!";
                    gvPNPAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Total

            try
            {
                var query = "";
                if (outsourceId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                if (outsourceId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.OutsourceOwnerId =" + outsourceId + " AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }
                if (outsourceId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT op.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.OutsourceProducer op ON i.CreatedBy=op.Id JOIN dbo.OutsourceOwner oo ON i.OutsourceOwnerId=oo.Id WHERE i.IsActive =1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=1 GROUP BY op.Name ORDER BY COUNT(d.Name) DESC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPTotal.DataSource = dt;
                    gvPNPTotal.DataBind();
                }
                else
                {
                    gvPNPTotal.EmptyDataText = "No Record Found!";
                    gvPNPTotal.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        } 

        #endregion

        protected void ddlOutsourceOwner_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOutsourceOwner.SelectedIndex > 0)
            {
                if (!string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtTo.Text))
                {
                    PopulateLeadsSummary(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), "", "");
                    PopulateLeadsCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), "", "");

                    PopulatePNPLeadsSummary(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), "", "");
                    PopulatePNPLeadsCount(Convert.ToInt32(ddlOutsourceOwner.SelectedValue), "", "");
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }
        }
    }
}