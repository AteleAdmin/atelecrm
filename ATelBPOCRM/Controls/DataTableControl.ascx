﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataTableControl.ascx.cs" Inherits="ATelBPOCRM.Controls.DataTableControl" %>

<!-- datTable -->
<script src="backend/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="backend/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="backend/cdn/dataTables.buttons.min.js"></script>
<script src="backend/cdn/jszip.min.js"></script>
<script src="backend/cdn/pdfmake.min.js"></script>
<script src="backend/cdn/vfs_fonts.js"></script>
<script src="backend/cdn/buttons.html5.min.js"></script>
<script src="backend/cdn/buttons.colVis.min.js"></script>
<!-- Slimscroll -->
