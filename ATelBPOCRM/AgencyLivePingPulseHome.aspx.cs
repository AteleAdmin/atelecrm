﻿using Lucky.General;
using System;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Exception = System.Exception;

namespace ATelBPOCRM
{
    public partial class AgencyLivePingPulseHome : System.Web.UI.Page
    {
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
        //        {

        //        }
        //        else
        //        {
        //            if (Session["RoleId"] != null)
        //            {
        //                var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
        //                if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
        //                {
        //                    //
        //                }
        //                else
        //                    Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadPNP();
                LoadLive();
                LoadPing();
            }
            else
            {
                //
            }
        }

        #region Analytics
        protected void btnAnalytices_OnClick(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnAnalyticsSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    //i.CreatedDate,101)
                    var fdate = DateTime.Parse(txtAnalyticsLiveFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtAnalyticsLiveTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                    PopulateLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                    ///PNP
                    PopulatePNPLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                    PopulatePNPLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);

                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        PopulatePNPLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), from, to);
                    }
                    else
                    {
                        gvPNPAllLeads.EmptyDataText = "No Record Found!";
                        gvPNPAllLeads.DataBind();
                    }
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    var fdate = DateTime.Parse(txtAnalyticsLiveFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtAnalyticsLiveTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulateLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), from, to);

                    PopulateProducerCount(Convert.ToInt32(Session["UId"].ToString()), from, to);

                    pnlAgencyProducer.Visible = true;
                    pnlAcceptedRejected.Visible = true;
                    pnlTotalCounter.Visible = true;

                    //PNP
                    PopulatePNPLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulatePNPLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulatePNPLead(0, Convert.ToInt32(Session["UId"].ToString()), from, to);

                    PopulatePNPProducerCount(Convert.ToInt32(Session["UId"].ToString()), from, to);

                    pnlPNPAgencyProducer.Visible = true;
                    pnlPNPAcceptedRejected.Visible = true;
                    pnlPNPTotalCounter.Visible = true;

                    // Ping
                    PopulatePingLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulatePingLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), from, to);

                    PopulatePingProducerCount(Convert.ToInt32(Session["UId"].ToString()), from, to);

                    pnlPNPAgencyProducer.Visible = true;
                    pnlPNPAcceptedRejected.Visible = true;
                    pnlPNPTotalCounter.Visible = true;

                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    var fdate = DateTime.Parse(txtAnalyticsLiveFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtAnalyticsLiveTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(0, 0, from, to);
                    PopulateLeadsCount(0, 0, from, to);

                    pnlAcceptedRejected.Visible = true;
                    pnlTotalCounter.Visible = true;

                    ///PNP
                    PopulatePNPLeadsSummary(0, 0, from, to);
                    PopulatePNPLeadsCount(0, 0, from, to);
                    PopulatePNPLead(0, 0, from, to);

                    pnlPNPAcceptedRejected.Visible = true;
                    pnlPNPTotalCounter.Visible = true;
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        // Live
        private void LoadLive()
        {
            if (Session["Role"] != null && (Session["Role"].ToString() == "Super Admin" || Session["Role"].ToString() == "Agency Owner"))
            {
                if (Session["UId"] != null)
                {
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                        PopulateLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        PopulateLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulateLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulateProducerCount(Convert.ToInt32(Session["UId"].ToString()), "", "");

                        pnlAgencyProducer.Visible = true;
                        pnlAcceptedRejected.Visible = true;
                        pnlTotalCounter.Visible = true;

                        pnlPingAgencyProducer.Visible = true;
                        pnlPingAcceptedRejected.Visible = true;
                        pnlPingTotalCounter.Visible = true;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" &&
                        Session["Role"].ToString() != "Agency Owner")
                    {
                        PopulateLeadsSummary(0, 0, "", "");
                        PopulateLeadsCount(0, 0, "", "");

                        pnlDateRange.Visible = true;
                        pnlAcceptedRejected.Visible = true;
                        pnlTotalCounter.Visible = true;

                        pnlPingAgencyProducer.Visible = true;
                        pnlPingAcceptedRejected.Visible = true;
                        pnlPingTotalCounter.Visible = true;
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
                Response.Redirect("Dashboard.aspx");
        }

        public void PopulateProducerCount(int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyProducerTotal.DataSource = dt;
                    gvAgencyProducerTotal.DataBind();
                }
                else
                {
                    gvAgencyProducerTotal.EmptyDataText = "No Record Found!";
                    gvAgencyProducerTotal.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyProducerRejected.DataSource = dt;
                    gvAgencyProducerRejected.DataBind();
                }
                else
                {
                    gvAgencyProducerRejected.EmptyDataText = "No Record Found!";
                    gvAgencyProducerRejected.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                if (agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyProducerAccepted.DataSource = dt;
                    gvAgencyProducerAccepted.DataBind();
                }
                else
                {
                    gvAgencyProducerAccepted.EmptyDataText = "No Record Found!";
                    gvAgencyProducerAccepted.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

        }

        public void PopulateLeadsCount(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateLeadsSummary(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvRejected.DataSource = dt;
                    gvRejected.DataBind();
                }
                else
                {
                    gvRejected.EmptyDataText = "No Record Found!";
                    gvRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAccepted.DataSource = dt;
                    gvAccepted.DataBind();
                }
                else
                {
                    gvAccepted.EmptyDataText = "No Record Found!";
                    gvAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void gvRejected_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvAccepted_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        // Pulse
        private void LoadPNP()
        {
            if (Session["UId"] != null)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    PopulatePNPLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                    PopulatePNPLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    PopulatePNPLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                    PopulatePNPLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), "", "");

                    PopulatePNPProducerCount(Convert.ToInt32(Session["UId"].ToString()), "", "");
                    pnlPNPAgencyProducer.Visible = true;
                    pnlPNPAcceptedRejected.Visible = true;
                    pnlPNPTotalCounter.Visible = true;
                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    PopulatePNPLeadsSummary(0, 0, "", "");
                    PopulatePNPLeadsCount(0, 0, "", "");
                    pnlPNPAcceptedRejected.Visible = true;
                    pnlPNPTotalCounter.Visible = true;
                }
                else
                {
                    //
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
            }
        }

        public void PopulatePNPLeadsCount(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulatePNPLeadsSummary(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPRejected.DataSource = dt;
                    gvPNPRejected.DataBind();
                }
                else
                {
                    gvPNPRejected.EmptyDataText = "No Record Found!";
                    gvPNPRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAccepted.DataSource = dt;
                    gvPNPAccepted.DataBind();
                }
                else
                {
                    gvPNPAccepted.EmptyDataText = "No Record Found!";
                    gvPNPAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulatePNPProducerCount(int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAgencyProducerTotal.DataSource = dt;
                    gvPNPAgencyProducerTotal.DataBind();
                }
                else
                {
                    gvPNPAgencyProducerTotal.EmptyDataText = "No Record Found!";
                    gvPNPAgencyProducerTotal.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAgencyProducerRejected.DataSource = dt;
                    gvPNPAgencyProducerRejected.DataBind();
                }
                else
                {
                    gvPNPAgencyProducerRejected.EmptyDataText = "No Record Found!";
                    gvPNPAgencyProducerRejected.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13,23) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13,23) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAgencyProducerAccepted.DataSource = dt;
                    gvPNPAgencyProducerAccepted.DataBind();
                }
                else
                {
                    gvPNPAgencyProducerAccepted.EmptyDataText = "No Record Found!";
                    gvPNPAgencyProducerAccepted.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

        }

        protected void gvPNPRejected_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvPNPAccepted_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        // Ping
        private void LoadPing()
        {
            if (Session["UId"] != null)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    PopulatePingLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                    PopulatePingLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    PopulatePingLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                    PopulatePingLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), "", "");

                    PopulatePingProducerCount(Convert.ToInt32(Session["UId"].ToString()), "", "");

                    pnlPNPAgencyProducer.Visible = true;
                    pnlPNPAcceptedRejected.Visible = true;
                    pnlPNPTotalCounter.Visible = true;
                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    PopulatePingLeadsSummary(0, 0, "", "");
                    PopulatePingLeadsCount(0, 0, "", "");

                    pnlPNPAcceptedRejected.Visible = true;
                    pnlPNPTotalCounter.Visible = true;
                }
                else
                {
                    //
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
            }
        }

        public void PopulatePingLeadsCount(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPingTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPingRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "'";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPingAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulatePingLeadsSummary(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPingRejected.DataSource = dt;
                    gvPingRejected.DataBind();
                }
                else
                {
                    gvPingRejected.EmptyDataText = "No Record Found!";
                    gvPingRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPingAccepted.DataSource = dt;
                    gvPingAccepted.DataBind();
                }
                else
                {
                    gvPingAccepted.EmptyDataText = "No Record Found!";
                    gvPingAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulatePingProducerCount(int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPingAgencyProducerTotal.DataSource = dt;
                    gvPingAgencyProducerTotal.DataBind();
                }
                else
                {
                    gvPingAgencyProducerTotal.EmptyDataText = "No Record Found!";
                    gvPingAgencyProducerTotal.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPingAgencyProducerRejected.DataSource = dt;
                    gvPingAgencyProducerRejected.DataBind();
                }
                else
                {
                    gvPingAgencyProducerRejected.EmptyDataText = "No Record Found!";
                    gvPingAgencyProducerRejected.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13,23) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13,23) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPingAgencyProducerAccepted.DataSource = dt;
                    gvPingAgencyProducerAccepted.DataBind();
                }
                else
                {
                    gvPingAgencyProducerAccepted.EmptyDataText = "No Record Found!";
                    gvPingAgencyProducerAccepted.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

        }

        protected void gvPingRejected_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvPingAccepted_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }
        #endregion

        #region Live Transfers
        protected void btnLive_OnClick(object sender, EventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                PopulateLead(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                PopulateLead(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
            }
            if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
            {
                PopulateLead(0, 0, "", "");
            }
            else
            {
                //
            }
            MultiView1.ActiveViewIndex = 3;
        }

        public void PopulateLead(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAllLeads.DataSource = dt;
                    gvAllLeads.DataBind();

                    gvAllLeads.UseAccessibleHeader = true;
                    gvAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnStats_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    //i.CreatedDate,101)
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLead(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLead(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLead(0, 0, from, to);
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void gvAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                var flag = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                e.Row.Cells[12].Visible = false;

                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                //var flag = false;
                //e.Row.Cells[7].Visible = false;
                //e.Row.Cells[8].Visible = false;
                //e.Row.Cells[9].Visible = false;
                //e.Row.Cells[10].Visible = false;
                ////e.Row.Cells[12].Visible = false;

                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
            }
            else
            {
                //
            }
        }

        protected void gvAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var flooded = e.Row.FindControl("ltNoFlood") as Label;
                var openClaim = e.Row.FindControl("ltNoOpenClaim") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (flooded != null && flooded.Text == "True")
                    flooded.CssClass = "label label-success";
                else if (flooded != null) flooded.CssClass = "label label-danger";

                if (openClaim != null && openClaim.Text == "True")
                    openClaim.CssClass = "label label-success";
                else if (openClaim != null) openClaim.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
            }
            else
            {
                //
            }
        }
        #endregion

        #region Pulse Leads
        protected void btnPNP_OnClick(object sender, EventArgs e)
        {
            PNPLoad();
            MultiView1.ActiveViewIndex = 2;
        }
        protected void btnGetLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var fdate = DateTime.Parse(txtPNPFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtPNPTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        PopulatePNPLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), from, to);
                    }
                    else
                    {
                        gvPNPAllLeads.EmptyDataText = "No Record Found!";
                        gvPNPAllLeads.DataBind();
                    }
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    var fdate = DateTime.Parse(txtPNPFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtPNPTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulatePNPLead(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    var fdate = DateTime.Parse(txtPNPFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtPNPTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulatePNPLead(0, 0, from, to);
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void PNPLoad()
        {
            if (Session["UId"] != null)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    PopulatePNPLead(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                }
                else
                {
                    //
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
            }
        }

        public void PopulatePNPLead(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPNPAllLeads.DataSource = dt;
                    gvPNPAllLeads.DataBind();

                    gvPNPAllLeads.UseAccessibleHeader = true;
                    gvPNPAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvPNPAllLeads.EmptyDataText = "No Record Found!";
                    gvPNPAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void gvPNPAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;

                #region DisableEditPreviousLeads
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;
                #region DisableEditPreviousLead
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
            {
                //e.Row.Cells[20].Visible = true;
            }
        }

        protected void gvPNPAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var flooded = e.Row.FindControl("ltNoFlood") as Label;
                var openClaim = e.Row.FindControl("ltNoOpenClaim") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (flooded != null && flooded.Text == "True")
                    flooded.CssClass = "label label-success";
                else if (flooded != null) flooded.CssClass = "label label-danger";

                if (openClaim != null && openClaim.Text == "True")
                    openClaim.CssClass = "label label-success";
                else if (openClaim != null) openClaim.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
                if (disposition != null && (disposition.Text.Contains("Hot Lead")))
                {
                    disposition.CssClass = "label label-info";
                }
            }
            else
            {
                //
            }
        }
        #endregion

        #region Ping Leads

        private void PingLoad()
        {
            try
            {
                if (Session["UId"] != null)
                {
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        PopulatePingLead(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnPing_OnClick(object sender, EventArgs e)
        {
            PingLoad();
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnGetPingLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    var fdate = DateTime.Parse(txtPingFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtPingTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulatePingLead(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulatePingLead(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvPingAllLead.DataSource = dt;
                    gvPingAllLead.DataBind();

                    gvPingAllLead.UseAccessibleHeader = true;
                    gvPingAllLead.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvPingAllLead.EmptyDataText = "No Record Found!";
                    gvPingAllLead.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void gvPingAllLead_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;

                #region DisableEditPreviousLeads
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;
                #region DisableEditPreviousLead
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
            {
                //e.Row.Cells[20].Visible = true;
            }
        }

        protected void gvPingAllLead_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var flooded = e.Row.FindControl("ltNoFlood") as Label;
                var openClaim = e.Row.FindControl("ltNoOpenClaim") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (flooded != null && flooded.Text == "True")
                    flooded.CssClass = "label label-success";
                else if (flooded != null) flooded.CssClass = "label label-danger";

                if (openClaim != null && openClaim.Text == "True")
                    openClaim.CssClass = "label label-success";
                else if (openClaim != null) openClaim.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
                if (disposition != null && (disposition.Text.Contains("Hot Lead")))
                {
                    disposition.CssClass = "label label-info";
                }
            }
            else
            {
                //
            }
        }
        #endregion
    }
}