﻿using Lucky.General;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ATelBPOCRM
{
    public partial class LeadInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["state"] != null && Request.QueryString["phone"] != null)
                    BindLeadInfo(Request.QueryString["state"].ToString(), Request.QueryString["phone"].ToString());
            }
        }

        private void BindLeadInfo(string state, string phone)
        {
            try
            {
                using (var con = new SqlConnection(@"Data Source=192.168.18.200;Initial Catalog=StatesDataPortal;Persist Security Info=True;User ID=SqlAdmin;Password=@Dm!n!strator123;Max Pool Size=2000;"))
                {
                    con.Open();

                    using (var cmd = new SqlCommand("SELECT * FROM sys.tables", con))
                    {
                        var da = new SqlDataAdapter(cmd);
                        var dt = new DataTable();
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            for (var i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["name"].ToString().StartsWith(state))
                                {
                                    var table = dt.Rows[i]["name"].ToString();

                                    var cmdd = new SqlCommand("SELECT * FROM " + table + " WHERE Phone='" + phone + "'", con);
                                    var daa = new SqlDataAdapter(cmdd);
                                    var dtt = new DataTable();
                                    daa.Fill(dtt);
                                    if (dtt.Rows.Count > 0)
                                    {
                                        var sb = new StringBuilder();
                                        sb.Append("");
                                        sb.Append("<table class='table table-bordered table-striped'>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>Title</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["NAME_PRE"].ToString() + "</td>");
                                        sb.Append("<th>First Name</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["FN"].ToString() + "</td>");
                                        sb.Append("<th>Middle Name</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["MI"].ToString() + "</td>");
                                        sb.Append("<th>Last Name</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["LN"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>Address</th>");
                                        sb.Append("<td colspan='5'>" + dtt.Rows[0]["ADDR"].ToString() + "</td>");
                                        sb.Append("<th>Address Type</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["ADDR_TYP"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>City</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["CITY"].ToString() + "</td>");
                                        sb.Append("<th>Satte</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["ST"].ToString() + "</td>");
                                        sb.Append("<th>Zip</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["ZIP"].ToString() + "</td>");
                                        sb.Append("<th>Zip4</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["Z4"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>APT</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["APT"].ToString() + "</td>");
                                        sb.Append("<th>DPC</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["DPC"].ToString() + "</td>");
                                        sb.Append("<th>FIPS City</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["FIPS_CTY"].ToString() + "</td>");
                                        sb.Append("<th>CBSA</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["CBSA"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>Address Line</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["ADDR_LINE"].ToString() + "</td>");
                                        sb.Append("<th>DMA</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["DMA_SUPPR"].ToString() + "</td>");
                                        sb.Append("<th>CRA</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["CRA"].ToString() + "</td>");
                                        sb.Append("<th>Z4 Type</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["Z4_TYP"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>DFS</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["DSF_IND"].ToString() + "</td>");
                                        sb.Append("<th>DPD</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["DPD_IND"].ToString() + "</td>");
                                        sb.Append("<th>Phone Flag</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["PHONE_FLAG"].ToString() + "</td>");
                                        sb.Append("<th>Phone</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["PHONE"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>Time Zone</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["TIME_ZN"].ToString() + "</td>");
                                        sb.Append("<th>Gender</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["GENDER"].ToString() + "</td>");
                                        sb.Append("<th>Home Rank</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["INF_HH_RANK"].ToString() + "</td>");
                                        sb.Append("<th>Home O Source</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["HOME_OWNR_SRC"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>DOB Year</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["DOB_YR"].ToString() + "</td>");
                                        sb.Append("<th>DOB Month</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["DOB_MON"].ToString() + "</td>");
                                        sb.Append("<th>DOB Day</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["DOB_DAY"].ToString() + "</td>");
                                        sb.Append("<th>Exact Age</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["EXACT_AGE"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>Age</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["AGE"].ToString() + "</td>");
                                        sb.Append("<th>Income</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["HH_INCOME"].ToString() + "</td>");
                                        sb.Append("<th>Net Worth</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["NET_WORTH"].ToString() + "</td>");
                                        sb.Append("<th>Credit Line</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["CREDIT_LINES"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>Credit Range</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["CREDIT_RANGE_NEW"].ToString() + "</td>");
                                        sb.Append("<th>Education</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["EDUC"].ToString() + "</td>");
                                        sb.Append("<th>Occupation</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["OCC_OCCUP"].ToString() + "</td>");
                                        sb.Append("<th>Occupation Description</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["OCC_OCCUP_DET"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>Occ:Business Owner</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["OCC_BUSN_OWNR"].ToString() + "</td>");
                                        sb.Append("<th>No Of Kids</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["NUM_KIDS"].ToString() + "</td>");
                                        sb.Append("<th>Present Kids</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["PRES_KIDS"].ToString() + "</td>");
                                        sb.Append("<th>Marital Satus</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["HH_MARITAL_STAT"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>Home Owner</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["HOME_OWNR"].ToString() + "</td>");
                                        sb.Append("<th>LOR</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["LOR"].ToString() + "</td>");
                                        sb.Append("<th>Type</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["DWELL_TYP"].ToString() + "</td>");
                                        sb.Append("<th>No of Adults</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["NUM_ADULTS"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        sb.Append("<tr>");
                                        sb.Append("<th>H Size</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["HH_SIZE"].ToString() + "</td>");
                                        sb.Append("<th>Gens</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["GENS"].ToString() + "</td>");
                                        sb.Append("<th>User</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["CC_USER"].ToString() + "</td>");
                                        //sb.Append("<th>Invest Action</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INVEST_ACT"].ToString() + "</td>");
                                        sb.Append("<th>Credit Rating</th>");
                                        sb.Append("<td>" + dtt.Rows[0]["CREDIT_RATING"].ToString() + "</td>");
                                        sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Invest Pers</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INVEST_PERS"].ToString() + "</td>");
                                        //sb.Append("<th>Invest RL</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INVEST_RL_EST"].ToString() + "</td>");
                                        //sb.Append("<th>Invest Stock</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INVEST_STOCKS"].ToString() + "</td>");
                                        //sb.Append("<th>Invest News</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INVEST_READ_FIN_NEWS"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Money Seekr</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INVEST_MONEY_SEEKR"].ToString() + "</td>");
                                        //sb.Append("<th>GRP Invest</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INT_GRP_INVEST"].ToString() + "</td>");
                                        //sb.Append("<th>Invest Foreign</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INVEST_FOREIGN"].ToString() + "</td>");
                                        //sb.Append("<th>Invest Est. Drop</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INVEST_EST_PROP_OWN"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Donor GRP</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INT_GRP_DONOR"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Mail</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_MAIL_ORD"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Charitable</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_CHARITABLE"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Animal</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_ANIMAL"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Donor Arts</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_ARTS"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Kids</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_KIDS"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Wildlife</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_WILDLIFE"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Environ</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_ENVIRON"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Health</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_HEALTH"].ToString() + "</td>");
                                        //sb.Append("<th>Donor AID</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_INTL_AID"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Pol</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_POL"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Pol Cons</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_POL_CONS"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Donor Pol Lib</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_POL_LIB"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Religion</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_RELIG"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Vets</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_VETS"].ToString() + "</td>");
                                        //sb.Append("<th>Donor Oth</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_OTH"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Donor Comm Char</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DONR_COMM_CHAR"].ToString() + "</td>");
                                        //sb.Append("<th>Vet HH</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["VET_IN_HH"].ToString() + "</td>");
                                        //sb.Append("<th>Buy Gardening</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["BUY_GARDENING"].ToString() + "</td>");
                                        //sb.Append("<th>Buy Home Gardening/th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["BUY_HOME_GARD"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Casino</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INT_TRAV_CASINO"].ToString() + "</td>");
                                        //sb.Append("<th>Sweeps</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INT_HOB_SWEEPS"].ToString() + "</td>");
                                        //sb.Append("<th>Genl</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["INT_TRAV_GENL"].ToString() + "</td>");
                                        //sb.Append("<th>Life Home</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["LIFE_HOME"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Life Diy</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["LIFE_DIY"].ToString() + "</td>");
                                        //sb.Append("<th>Buy Auto Parts</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["BUY_AUTO_PARTS"].ToString() + "</td>");
                                        //sb.Append("<th>Ethnic Code</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["ETHNIC_CODE"].ToString() + "</td>");
                                        //sb.Append("<th>Ethnic Cof</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["ETHNIC_CONF"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Ethnic GRP</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["ETHNIC_GRP"].ToString() + "</td>");
                                        //sb.Append("<th>Ethnic Language</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["ETHNIC_LANG"].ToString() + "</td>");
                                        //sb.Append("<th>Ethnic Religion</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["ETHNIC_RELIG"].ToString() + "</td>");
                                        //sb.Append("<th>Ethnic Country</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["ETHNIC_HISP_CNTRY"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Assim</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["ETHNIC_ASSIM"].ToString() + "</td>");
                                        //sb.Append("<th>Credit Rating</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["CREDIT_RATING"].ToString() + "</td>");
                                        //sb.Append("<th>DNC Flag</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["DNC_FLAG"].ToString() + "</td>");
                                        //sb.Append("<th>Porp: Build Year</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["PROP_BLD_YR"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Prop: Ac</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["PROP_AC"].ToString() + "</td>");
                                        //sb.Append("<th>Prop: Pool</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["PROP_POOL"].ToString() + "</td>");
                                        //sb.Append("<th>Prop: Fuel</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["PROP_FUEL"].ToString() + "</td>");
                                        //sb.Append("<th>Prop: Sewer</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["PROP_SEWER"].ToString() + "</td>");
                                        //sb.Append("</tr>");

                                        //sb.Append("<tr>");
                                        //sb.Append("<th>Prop: Water</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["PROP_WATER"].ToString() + "</td>");
                                        //sb.Append("<th>Latitude</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["LATITUDE"].ToString() + "</td>");
                                        //sb.Append("<th>Lognitude</th>");
                                        //sb.Append("<td>" + dtt.Rows[0]["LONGITUDE"].ToString() + "</td>");
                                        //sb.Append("<th></th>");
                                        //sb.Append("<td></th>");
                                        //sb.Append("</tr>");

                                        sb.Append("</table>");

                                        ltLead.Text = sb.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}