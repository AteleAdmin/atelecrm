﻿using Lucky.General;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ATelBPOCRM
{
    public partial class ClientLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            var isValid = false;
            if (isValid == false)
            {
                var cmd = new SqlCommand(
                    "SELECT ao.Id,ao.Name,ao.Username,ao.Email,r.Id AS RoleId,r.Name AS Role,ao.Token FROM dbo.AgencyOwner ao JOIN dbo.Roles r ON ao.RoleId = r.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id WHERE am.Username='Saqib' AND ao.IsActive=1 AND (ao.Username=@Username OR ao.Email=@Email) AND ao.Password=@Password")
                {
                    Connection = Connection.Db()
                };
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Username", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                var da = new SqlDataAdapter(cmd);
                var dataTable = new DataTable();
                da.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    Session.Clear();

                    Session["UId"] = dataTable.Rows[0]["Id"].ToString();
                    Session["Name"] = dataTable.Rows[0]["Name"].ToString();
                    Session["UN"] = dataTable.Rows[0]["Username"].ToString();
                    Session["Email"] = dataTable.Rows[0]["Email"].ToString();
                    Session["RoleId"] = dataTable.Rows[0]["RoleId"].ToString();
                    Session["Role"] = dataTable.Rows[0]["Role"].ToString();
                    Session["Token"] = dataTable.Rows[0]["Token"].ToString();
                    isValid = true;

                    if (dataTable.Rows[0]["Role"].ToString() == "Agency Owner")
                        Response.Redirect("ClientAgencyLeads.aspx");

                    //var returnUrl = Convert.ToString(Request.QueryString["url"]);
                    //if (!string.IsNullOrEmpty(returnUrl))
                    //{
                    //    Response.Redirect(returnUrl);
                    //}
                    //else
                    //Response.Redirect("LivePNP.aspx");
                    //Response.Redirect("Dashboard.aspx");
                }
            }

            if (isValid == false)
            {
                var cmd = new SqlCommand(
                    "SELECT ap.Id,ap.Name,ap.Username,ap.Email,r.Id AS RoleId,r.Name AS Role,ao.Name AS Agency,ap.Token FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId=ao.Id JOIN dbo.Roles r ON ap.RoleId = r.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id WHERE am.Username='Saqib' AND ap.IsActive=1 AND (ap.Username=@Username OR ap.Email=@Email) AND ap.Password=@Password AND ao.IsActive=1")
                {
                    Connection = Connection.Db()
                };
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Username", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                var da = new SqlDataAdapter(cmd);
                var dataTable = new DataTable();
                da.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    Session.Clear();

                    Session["UId"] = dataTable.Rows[0]["Id"].ToString();
                    Session["Name"] = dataTable.Rows[0]["Name"].ToString();
                    Session["UN"] = dataTable.Rows[0]["Username"].ToString();
                    Session["Email"] = dataTable.Rows[0]["Email"].ToString();
                    Session["Role"] = dataTable.Rows[0]["Role"].ToString();
                    Session["RoleId"] = dataTable.Rows[0]["RoleId"].ToString();
                    Session["Agency"] = dataTable.Rows[0]["Agency"].ToString();
                    Session["Token"] = dataTable.Rows[0]["Token"].ToString();
                    isValid = true;

                    //var returnUrl = Convert.ToString(Request.QueryString["url"]);
                    //if (!string.IsNullOrEmpty(returnUrl))
                    //{
                    //    Response.Redirect(returnUrl);
                    //}
                    //else
                    if (dataTable.Rows[0]["Role"].ToString() == "Agency Producer")
                        Response.Redirect("ClientProducerLeads.aspx");
                    // Response.Redirect("Dashboard.aspx");
                }
            }
            if (isValid == false)
                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Invalid Username / Email or Pass');", true);
        }
    }
}