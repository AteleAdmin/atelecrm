﻿using System.Configuration;
namespace ATelBPOCRM
{
    public class AtelStrings
    {
        public static string BaseUrl { get; set; } = ConfigurationManager.AppSettings["BaseUrl"].ToString();
        public static string OpenUName { get; set; } = ConfigurationManager.AppSettings["OpenUName"].ToString();
        public static string OpenPassword { get; set; } = ConfigurationManager.AppSettings["OpenPassword"].ToString();
        public AtelStrings()
        {
            //baseUrl = ConfigurationManager.AppSettings[""].ToString();
        }
    }
}