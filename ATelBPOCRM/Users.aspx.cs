﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace ATelBPOCRM
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null)
                {
                    PopulateUsers(0);
                    PopulateOwner();
                    PopulateRoles();
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }
        }

        public void PopulateOwner()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name+' - '+Username AS Name FROM dbo.Users WHERE IsActive = 1");
                if (dt.Rows.Count > 0)
                {
                    ddlOwner.DataSource = dt;
                    ddlOwner.DataTextField = "Name";
                    ddlOwner.DataValueField = "Id";
                    ddlOwner.DataBind();

                    ddlOwner.Items.Insert(0, "Select User");
                }
                else
                {
                    //
                }

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void PopulateUsers(int? id)
        {
            try
            {
                var query = "";
                if (id == 0)
                    query =
                        "SELECT r.Name AS Role,uc.Id,uc.Name,uc.SudoName,uc.Username,uc.Email,uc.Password,uc.CNIC,uc.ContactNo,uc.IsActive,(CASE WHEN uc.ReportedTo = 0 THEN 'Muhammad Aamir' ELSE up.Name+' - '+up.Username END) AS ReportingTo FROM dbo.Users uc LEFT JOIN dbo.Users up ON uc.ReportedTo =up.Id JOIN dbo.Roles r ON uc.RoleId=r.Id";
                if (id > 0)
                    query = "SELECT r.Name AS Role,uc.Id,uc.Name,uc.SudoName,uc.Username,uc.Email,uc.Password,uc.CNIC,uc.ContactNo,uc.IsActive,(CASE WHEN uc.ReportedTo = 0 THEN 'Muhammad Aamir' ELSE up.Name+' - '+up.Username END) AS ReportingTo FROM dbo.Users uc LEFT JOIN dbo.Users up ON uc.ReportedTo =up.Id JOIN dbo.Roles r ON uc.RoleId=r.Id WHERE uc.ReportedTo=" + id;
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = dt;
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var res = new StringBuilder();
            var rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            var btn = sender as LinkButton;
            if (btn != null)
            {
                var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                if (hfId != null)

                    Response.Redirect("AddUser.aspx?UserId=" + Convert.ToInt32(hfId.Value) + "&accountType=User");
            }
            else
            {
                //
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var active = GridView1.Rows[rowIndex].FindControl("ltActive") as Label;
                    if (hfId != null)
                    {
                        var query = "";
                        var message = "";
                        if (active != null && active.Text == "True")
                        {
                            query = "UPDATE dbo.Users SET IsActive=0, LastModifiedBy=@LastModifiedBy, UpdatedDate=@UpdatedDate WHERE Id=@Id";
                            message = "Blocked";
                        }
                        else
                        {
                            query = "UPDATE dbo.Users SET IsActive=1, LastModifiedBy=@LastModifiedBy, UpdatedDate=@UpdatedDate WHERE Id=@Id";
                            message = "Active";
                        }
                        var cmd = new SqlCommand(query, Connection.Db());
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(hfId.Value));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            PopulateUsers(0);
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>User " + message + " Successfully');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Cannot be Blocked');", true);
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void ddlOwner_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOwner.SelectedIndex > 0)
                PopulateUsers(Convert.ToInt32(ddlOwner.SelectedValue));
            if (ddlOwner.SelectedIndex == 0)
                PopulateUsers(0);
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void GridView1_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != GridView1.EditIndex)
                {
                    var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT rp.Id,rp.RoleId,rp.CanAdd,rp.CanDelete,rp.CanEdit,rp.CanView FROM dbo.RolesPermissions rp JOIN dbo.SubModule sm ON rp.SubModuleId=sm.Id WHERE rp.IsActive=1 AND REPLACE(sm.Name,' ','')='" + pageName + "' AND rp.RoleId=" + Convert.ToInt32(Session["RoleId"]));
                    if (dt.Rows.Count > 0)
                    {
                        var editButton = e.Row.FindControl("btnEdit") as LinkButton;
                        var deleteButton = e.Row.FindControl("btnDelete") as LinkButton;
                        if (dt.Rows[0]["CanEdit"].ToString() == "False" && dt.Rows[0]["CanDelete"].ToString() == "False")
                        {
                            if (editButton != null) editButton.Visible = false;
                        }
                        if (dt.Rows[0]["CanEdit"].ToString() == "False")
                        {
                            if (editButton != null) editButton.Visible = false;
                        }
                        if (dt.Rows[0]["CanDelete"].ToString() == "False")
                        {
                            if (deleteButton != null) deleteButton.Visible = false;
                        }
                        if (dt.Rows[0]["CanAdd"].ToString() == "False")
                        {
                            pnlAddUser.Visible = false;
                        }
                        else
                        {
                            //
                        }

                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void PopulateRoles()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Roles WHERE IsActive=1 AND IsSuperAdmin=0 AND Id NOT IN (12,13)");
                if (dt.Rows.Count > 0)
                {
                    ddlRole.DataSource = dt;
                    ddlRole.DataTextField = "Name";
                    ddlRole.DataValueField = "Id";
                    ddlRole.DataBind();

                    ddlRole.Items.Insert(0, "Select Role");
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void ddlRole_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlRole.SelectedIndex > 0)
                {
                    PopulateRolesUsers(Convert.ToInt32(ddlRole.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void PopulateRolesUsers(int? roleId)
        {
            var ds = new PopulateDataSource();
            var dt = ds.DataTableSqlString("SELECT r.Name AS Role,uc.Id,uc.Name,uc.SudoName,uc.Username,uc.Email,uc.Password,uc.CNIC,uc.ContactNo,uc.IsActive,(CASE WHEN uc.ReportedTo = 0 THEN 'Muhammad Aamir' ELSE up.Name+' - '+up.Username END) AS ReportingTo FROM dbo.Users uc LEFT JOIN dbo.Users up ON uc.ReportedTo =up.Id JOIN dbo.Roles r ON uc.RoleId=r.Id WHERE uc.RoleId=" + roleId);
            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = "";
                GridView1.EmptyDataText = "No Record Found!";
                GridView1.DataBind();
            }
        }
    }
}