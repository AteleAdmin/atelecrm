﻿using Lucky.General;
using System;
using System.Data.SqlClient;
using System.Web.UI;

namespace ATelBPOCRM
{
    public partial class UpdateLiveHomeLead : System.Web.UI.Page
    {
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
        //        {

        //        }
        //        else
        //        {
        //            if (Session["RoleId"] != null)
        //            {
        //                var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
        //                if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
        //                {
        //                    //
        //                }
        //                else
        //                    Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                {
                    pnlDisposition.Visible = false;
                    Panel1.Visible = false;
                    PopulateDdlDisposition();
                    if (Request.QueryString["msg"] != null && Request.QueryString["phone"] != null)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Saved Successfully');", true);
                        txtPhone.Text = Request.QueryString["phone"].ToString();
                        txtPhone.ReadOnly = true;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }
                    if (Request.QueryString["LeadId"] != null)
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,i.Id,i.FirstName,i.LastName,i.ViciLeadId,i.Gender,i.City,UPPER(i.State) AS State,i.Zip,i.PhoneNo,i.Address,i.Comments,i.DOB,i.Company,(CASE WHEN i.HOwnerShip='True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip,i.Make,i.Year,i.Model,i.Make2,i.Year2,i.Model2,i.Email,i.ContinousCoverage,(CASE WHEN i.MaritalStatus='True' THEN 'Yes' ELSE 'No' END) AS MaritalStatus,ao.Name AS AgencyOwner,ap.Name AS AgencyProducer,d.Name AS Disposition FROM dbo.Insurance i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.Id=" + Convert.ToInt32(Request.QueryString["LeadId"]));
                        if (dt.Rows.Count > 0)
                        {

                            txtPhone.Text = dt.Rows[0]["PhoneNo"].ToString();

                            txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                            txtLName.Text = dt.Rows[0]["LastName"].ToString();
                            txtPhoneNo.Text = dt.Rows[0]["PhoneNo"].ToString();
                            txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                            txtEmail.Text = dt.Rows[0]["Email"].ToString();
                            txtAddress.Text = dt.Rows[0]["Address"].ToString();
                            txtComments.Text = dt.Rows[0]["Comments"].ToString();
                            txtProducer.Text = dt.Rows[0]["AgencyProducer"].ToString();
                            txtProducer.ReadOnly = true;

                            txtPhoneNo.ReadOnly = true;

                            txtViciLeadId.Text = "";
                            txtViciLeadId.Text = dt.Rows[0]["ViciLeadId"].ToString();
                            txtViciLeadId.ReadOnly = true;

                            ddlIsFlooded.ClearSelection();
                            ddlIsFlooded.SelectedIndex = ddlIsFlooded.Items.IndexOf(ddlIsFlooded.Items.FindByText(dt.Rows[0]["IsFlooded"].ToString()));
                            ddlIsFlooded.DataBind();

                            ddlIsOpenClaim.ClearSelection();
                            ddlIsOpenClaim.SelectedIndex = ddlIsOpenClaim.Items.IndexOf(ddlIsOpenClaim.Items.FindByText(dt.Rows[0]["IsOpenClaim"].ToString()));
                            ddlIsOpenClaim.DataBind();

                            ddlGender.ClearSelection();
                            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                            ddlGender.DataBind();

                            ddlDisposition.ClearSelection();
                            ddlDisposition.SelectedIndex = ddlDisposition.Items.IndexOf(ddlDisposition.Items.FindByText(dt.Rows[0]["Disposition"].ToString()));
                            ddlDisposition.DataBind();

                            txtLeadId.Text = "";
                            txtLeadId.Text = Request.QueryString["LeadId"];
                            txtLeadId.ReadOnly = true;

                            Panel1.Visible = true;
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        private void PopulateDdlDisposition()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id IN(5,11,12,14,15,22,25) ORDER BY Name"); // SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id !=23
                if (dt.Rows.Count > 0)
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();

                    //ddlAgencyOwner.Items.Insert(0, "Select Agency Owner");
                }
                else
                {
                    ddlDisposition.DataSource = "";
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtLeadId.Text) && !string.IsNullOrEmpty(txtViciLeadId.Text))
            {
                var cmd = new SqlCommand("Update Insurance SET FirstName=@FirstName, LastName=@LastName, Gender=@Gender, PhoneNo=@PhoneNo, DOB=@DOB, Email=@Email, Address=@Address, Comments=@Comments, IsFlooded=@IsFlooded, IsOpenClaim=@IsOpenClaim, FollowUpXDate=@FollowUpXDate, DispositionId=@DispositionId, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id AND ViciLeadId=@ViciLeadId");
                cmd.Parameters.Clear();
                cmd.Connection = Connection.Db();
                cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                cmd.Parameters.AddWithValue("@HOwnerShip", ddlIsFlooded.SelectedValue);
                cmd.Parameters.AddWithValue("@ContinousCoverage", ddlIsOpenClaim.SelectedValue);
                cmd.Parameters.AddWithValue("@FollowUpXDate", !string.IsNullOrWhiteSpace(txtFollowDate.Text) ? txtFollowDate.Text : "");
                cmd.Parameters.AddWithValue("@DispositionId", Convert.ToInt32(ddlDisposition.SelectedValue));
                cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text));
                if (Session["UId"] != null)
                {
                    cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtLeadId.Text));
                    try
                    {
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Updated Successfully.');", true);
                            Clear();
                            txtLeadId.Text = "";
                            txtViciLeadId.Text = "";
                            txtPhone.Text = "";
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Update.');", true);
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogging.SendExcepToDb(ex);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                }
            }
        }

        public void Clear()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtPhoneNo.Text = "";
            txtDOB.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtComments.Text = "";

            ddlDisposition.ClearSelection();
            ddlIsFlooded.ClearSelection();
            ddlIsOpenClaim.ClearSelection();

            txtLeadId.Text = "";
            txtViciLeadId.Text = "";
        }

        protected void btnGetInsuranceLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDuplicate("SELECT * FROM dbo.Insurance WHERE IsActive = 1 AND PhoneNo='" + txtPhone.Text.Trim() + "' AND DispositionId !=0") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "warningMsg('Lead Already Submitted.');", true);
                }
                else
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT * FROM dbo.ViciLead WHERE IsActive= 1 AND Phone='" + txtPhone.Text.Trim() + "'");
                    if (dt.Rows.Count > 0)
                    {
                        txtViciLeadId.Text = "";
                        txtViciLeadId.Text = dt.Rows[0]["Id"].ToString();
                        txtViciLeadId.ReadOnly = true;

                        txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                        txtLName.Text = dt.Rows[0]["LastName"].ToString();
                        txtPhoneNo.Text = dt.Rows[0]["Phone"].ToString();
                        txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                        txtEmail.Text = dt.Rows[0]["Email"].ToString();
                        txtAddress.Text = dt.Rows[0]["Address"].ToString();

                        //ddlGender.ClearSelection();
                        //ddlGender.SelectedIndex = ddlHOwnerShip.Items.IndexOf(ddlHOwnerShip.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                        //ddlGender.DataBind();

                        Panel1.Visible = true;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void ddlDisposition_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDisposition.SelectedItem.Text == "Follow-Up/X-Date")
                pnlFollow.Visible = true;
        }
    }
}