﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;


namespace ATelBPOCRM
{
    public partial class UserManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                    PopulateProducers(Convert.ToInt32(Session["UId"]));
            }
        }
        public void PopulateProducers(int? id)
        {
            try
            {
                var query = "";
                if (id == 0)
                    query = "SELECT ap.Id, ap.Name, ap.Username, ap.Email, ap.Password, ap.Gender, ap.ContactNo, ap.Address, ap.Description, CONVERT(NVARCHAR(12),ap.DOJ, 101) AS DOJ,(CASE WHEN ao.State IS NULL THEN ao.Name ELSE ao.Name+' - '+ao.State END) AS AgencyOwner,(CASE WHEN ap.IsActive =1 THEN 'Yes' ELSE 'No' END) AS IsActive FROM dbo.AgencyProducer ap JOIN AgencyOwner ao ON ap.AgencyOwnerId=ao.Id ORDER BY ap.Id ASC";
                if (id > 0)
                    query = "SELECT ap.Id, ap.Name, ap.Username, ap.Email, ap.Password, ap.Gender, ap.ContactNo, ap.Address, ap.Description, CONVERT(NVARCHAR(12),ap.DOJ, 101) AS DOJ,(CASE WHEN ao.State IS NULL THEN ao.Name ELSE ao.Name+' - '+ao.State END) AS AgencyOwner,(CASE WHEN ap.IsActive =1 THEN 'Yes' ELSE 'No' END) AS IsActive FROM dbo.AgencyProducer ap JOIN AgencyOwner ao ON ap.AgencyOwnerId=ao.Id WHERE ap.AgencyOwnerId=" + id + " ORDER BY ap.Id ASC";
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    if (hfId != null)
                    {
                        var cmd = new SqlCommand();
                        cmd.Parameters.Clear();
                        cmd.Connection = Connection.Db();
                        cmd.CommandText = "UPDATE dbo.AgencyProducer SET IsActive=0 WHERE Id=" + Convert.ToInt32(hfId.Value);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Producer Block Successfully.');", true);
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Producer Cannot be Blocked.');", true);
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "Yes")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
        }

        protected void btnActive_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    if (hfId != null)
                    {
                        var cmd = new SqlCommand();
                        cmd.Parameters.Clear();
                        cmd.Connection = Connection.Db();
                        cmd.CommandText = "UPDATE dbo.AgencyProducer SET IsActive=1 WHERE Id=" + Convert.ToInt32(hfId.Value);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('Producer Active Successfully.');", true);
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Producer Cannot be Active.');", true);
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}