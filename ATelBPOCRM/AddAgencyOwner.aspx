﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAgencyOwner.aspx.cs" Inherits="ATelBPOCRM.AddAgencyOwner" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Agency Owner</title>
    <link rel="icon" href="backend/favicon.ico" type="image/ico" sizes="16x16" />
    <link href="backend/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="backend/icomoon/styles.css" rel="stylesheet" />
    <link href="backend/dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="backend/BS-4/css/bootstrap.min.css" rel="stylesheet" />
    <link href="backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="backend/lucky-form-val/css/formValidation.css" rel="stylesheet" />
    <link href="backend/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="backend/toaster/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />
    <style type="text/css">
        .label {
            -ms-border-radius: 0px !important;
            border-radius: 0px !important;
        }

        .select2-selection__choice {
            border: none;
            background: #333 !important;
        }

        .select2-container {
            width: 584px !important;
        }
    </style>
    <script src="backend/lucky-form-val/js/jquery/jquery.min.js"></script>
    <script src="backend/toaster/toastr.min.js"></script>
    <script src="backend/toaster/toasterCustom.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <form id="form1" runat="server">
        <div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Owner Form</h3>
                            <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="defaultForm" class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Role</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" InitialValue="0" runat="server" ForeColor="Red" Text="*" ControlToValidate="ddlRole" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlRole" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRole_OnSelectedIndexChanged">
                                                <asp:ListItem Value="0">Select Role</asp:ListItem>
                                                <asp:ListItem Value="12">Agency Owner</asp:ListItem>
                                                <asp:ListItem Value="16">Account Head</asp:ListItem>
                                                <asp:ListItem Value="17">Account Manager</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Panel ID="pnlReportedTo" runat="server" Visible="False">
                                            <div class="form-group">
                                                <label>Reported To</label>
                                                <asp:DropDownList ID="ddlReportedTo" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" Text="*" ControlToValidate="txtName" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtName" CssClass="form-control" placeholder="Name" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Licensed State</label>
                                            <asp:ListBox ID="ddlState" CssClass="form-control select2" runat="server" SelectionMode="Multiple" required="">
                                                <asp:ListItem Text="AB" Value="AB"></asp:ListItem>
                                                <asp:ListItem Text="AK" Value="AK"></asp:ListItem>
                                                <asp:ListItem Text="AL" Value="AL"></asp:ListItem>
                                                <asp:ListItem Text="AR" Value="AR"></asp:ListItem>
                                                <asp:ListItem Text="AZ" Value="AZ"></asp:ListItem>
                                                <asp:ListItem Text="BC" Value="BC"></asp:ListItem>
                                                <asp:ListItem Text="CA" Value="CA"></asp:ListItem>
                                                <asp:ListItem Text="CO" Value="CO"></asp:ListItem>
                                                <asp:ListItem Text="CT" Value="CT"></asp:ListItem>
                                                <asp:ListItem Text="DC" Value="DC"></asp:ListItem>
                                                <asp:ListItem Text="DE" Value="DE"></asp:ListItem>
                                                <asp:ListItem Text="FL" Value="FL"></asp:ListItem>
                                                <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                                                <asp:ListItem Text="GU" Value="GU"></asp:ListItem>
                                                <asp:ListItem Text="HI" Value="HI"></asp:ListItem>
                                                <asp:ListItem Text="IA" Value="IA"></asp:ListItem>
                                                <asp:ListItem Text="ID" Value="ID"></asp:ListItem>
                                                <asp:ListItem Text="IL" Value="IL"></asp:ListItem>
                                                <asp:ListItem Text="IN" Value="IN"></asp:ListItem>
                                                <asp:ListItem Text="KS" Value="KS"></asp:ListItem>
                                                <asp:ListItem Text="KY" Value="KY"></asp:ListItem>
                                                <asp:ListItem Text="LA" Value="LA"></asp:ListItem>
                                                <asp:ListItem Text="MA" Value="MA"></asp:ListItem>
                                                <asp:ListItem Text="MB" Value="MB"></asp:ListItem>
                                                <asp:ListItem Text="MD" Value="MD"></asp:ListItem>
                                                <asp:ListItem Text="ME" Value="ME"></asp:ListItem>
                                                <asp:ListItem Text="MI" Value="MI"></asp:ListItem>
                                                <asp:ListItem Text="MN" Value="MN"></asp:ListItem>
                                                <asp:ListItem Text="MO" Value="MO"></asp:ListItem>
                                                <asp:ListItem Text="MP" Value="MP"></asp:ListItem>
                                                <asp:ListItem Text="MS" Value="MS"></asp:ListItem>
                                                <asp:ListItem Text="MT" Value="MT"></asp:ListItem>
                                                <asp:ListItem Text="NB" Value="NB"></asp:ListItem>
                                                <asp:ListItem Text="NC" Value="NC"></asp:ListItem>
                                                <asp:ListItem Text="ND" Value="ND"></asp:ListItem>
                                                <asp:ListItem Text="NE" Value="NE"></asp:ListItem>
                                                <asp:ListItem Text="NH" Value="NH"></asp:ListItem>
                                                <asp:ListItem Text="NJ" Value="NJ"></asp:ListItem>
                                                <asp:ListItem Text="NL" Value="NL"></asp:ListItem>
                                                <asp:ListItem Text="NM" Value="NM"></asp:ListItem>
                                                <asp:ListItem Text="NS" Value="NS"></asp:ListItem>
                                                <asp:ListItem Text="NV" Value="NV"></asp:ListItem>
                                                <asp:ListItem Text="NY" Value="NY"></asp:ListItem>
                                                <asp:ListItem Text="OH" Value="OH"></asp:ListItem>
                                                <asp:ListItem Text="OK" Value="OK"></asp:ListItem>
                                                <asp:ListItem Text="ON" Value="ON"></asp:ListItem>
                                                <asp:ListItem Text="OR" Value="OR"></asp:ListItem>
                                                <asp:ListItem Text="PA" Value="PA"></asp:ListItem>
                                                <asp:ListItem Text="PR" Value="PR"></asp:ListItem>
                                                <asp:ListItem Text="QC" Value="QC"></asp:ListItem>
                                                <asp:ListItem Text="RI" Value="RI"></asp:ListItem>
                                                <asp:ListItem Text="SC" Value="SC"></asp:ListItem>
                                                <asp:ListItem Text="SD" Value="SD"></asp:ListItem>
                                                <asp:ListItem Text="SK" Value="SK"></asp:ListItem>
                                                <asp:ListItem Text="TN" Value="TN"></asp:ListItem>
                                                <asp:ListItem Text="TX" Value="TX"></asp:ListItem>
                                                <asp:ListItem Text="UT" Value="UT"></asp:ListItem>
                                                <asp:ListItem Text="VA" Value="VA"></asp:ListItem>
                                                <asp:ListItem Text="VI" Value="VI"></asp:ListItem>
                                                <asp:ListItem Text="VT" Value="VT"></asp:ListItem>
                                                <asp:ListItem Text="WA" Value="WA"></asp:ListItem>
                                                <asp:ListItem Text="WI" Value="WI"></asp:ListItem>
                                                <asp:ListItem Text="WV" Value="WV"></asp:ListItem>
                                                <asp:ListItem Text="WY" Value="WY"></asp:ListItem>
                                                <asp:ListItem Text="YT" Value="YT"></asp:ListItem>
                                            </asp:ListBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <asp:DropDownList ID="ddlGender" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                                <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Password &nbsp;<span><asp:CheckBox ID="chkAuto" runat="server" CssClass="" />&nbsp;Auo Generate</span></label>
                                            <asp:TextBox ID="txtPassword" CssClass="form-control password" placeholder="Password" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Date-of-Join</label>
                                            <asp:TextBox ID="txtDOJ" CssClass="form-control datepicker" placeholder="mm/dd/yyyy" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Username</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="Red" Text="*" ControlToValidate="txtUName" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUName" CssClass="form-control" placeholder="Username" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <asp:TextBox ID="txtEmail" CssClass="form-control" placeholder="Email" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Contact No</label>
                                            <asp:TextBox ID="txtContactNo" CssClass="form-control" placeholder="Contact No" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Company Agency</label>
                                            <%--<asp:TextBox ID="txtCompany" CssClass="form-control" placeholder="Company Agency" runat="server"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlInsuranceCompany" CssClass="form-control select2" runat="server">
                                                <asp:ListItem Value=""></asp:ListItem>
                                                <asp:ListItem Value="ALFA">ALFA</asp:ListItem>
                                                <asp:ListItem Value="Allstate">Allstate</asp:ListItem>
                                                <asp:ListItem Value="American Family">American Family</asp:ListItem>
                                                <asp:ListItem Value="Amica">Amica</asp:ListItem>
                                                <asp:ListItem Value="Auto Owners">Auto Owners</asp:ListItem>
                                                <asp:ListItem Value="Erie">Erie</asp:ListItem>
                                                <asp:ListItem Value="Farmers">Farmers</asp:ListItem>
                                                <asp:ListItem Value="Farm Bureau">Farm Bureau</asp:ListItem>
                                                <asp:ListItem Value="Geico">Geico</asp:ListItem>
                                                <asp:ListItem Value="Liberty Mutual">Liberty Mutual</asp:ListItem>
                                                <asp:ListItem Value="Nationwide">Nationwide</asp:ListItem>
                                                <asp:ListItem Value="Progressive">Progressive</asp:ListItem>
                                                <asp:ListItem Value="Safeco">Safeco</asp:ListItem>
                                                <asp:ListItem Value="State Farm">State Farm</asp:ListItem>
                                                <asp:ListItem Value="The Hartford">The Hartford</asp:ListItem>
                                                <asp:ListItem Value="Travelers">Travelers</asp:ListItem>
                                                <asp:ListItem Value="USAA">USAA</asp:ListItem>
                                                <asp:ListItem Value="Other">Other</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <asp:TextBox ID="txtAddress" TextMode="MultiLine" Rows="1" CssClass="form-control" placeholder="Address" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Description</label>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" Rows="1" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat" OnClick="btnSave_OnClick" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="backend/lucky-form-val/js/bootstrap.min.js"></script>
    <script src="backend/lucky-form-val/js/formValidation.js"></script>
    <script src="backend/lucky-form-val/js/framework/bootstrap.js"></script>
    <script src="backend/lucky-form-val/js/jquery.mask.js"></script>

    <script src="backend/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!----------------------------------------------------------->
    <script type="text/javascript">
        document.getElementById('chkAuto').onclick = function () {
            if (this.checked) {
                document.querySelector('.password').disabled = "disabled";
            } else {
                document.querySelector('.password').disabled = "";
            }
        };
    </script>
    <script type="text/javascript">
        $(function () {
            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                fields: {
                    '<%= txtUName.ClientID %>': {
                        message: 'The username is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The username is required'
                            },
                            stringLength: {
                                min: 3,
                                max: 10,
                                message: 'The username must be more than 3 and less than 10 characters long'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: 'The username can only consist of alphabetical, number, dot and underscore'
                            }
                        }
                    },
                    '<%= txtEmail.ClientID %>': {
                        validators: {
                            notEmpty: {
                                message: 'The email address is required'
                            },
                            emailAddress: {
                                message: 'The input is not a valid email address'
                            }
                        }
                    },
                    '<%= txtName.ClientID %>': {
                        validators: {
                            notEmpty: {
                                message: 'The text is not valid'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z ]+$/,
                            }
                        }
                    },
                    '<%= txtDOJ.ClientID %>': {
                        validators: {
                            date: {
                                format: 'DD/MM/YYYY',
                                message: 'The Date is not valid'
                            }
                        }
                    },
                    '<%= txtContactNo.ClientID %>': {
                        message: 'The phone number is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The phone number is required'
                            },
                            regexp: {
                                regexp: /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
                            }
                        }
                    },
                    validators: {
                        notEmpty: {
                            message: 'The text is not valid'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                        }
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        var dateToday = new Date();
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true,
            orientation: 'bottom auto'
        });
    </script>
    <script src="backend/plugins/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.select2').select2();

            //$('[id*=ddlState]').multiselect({
            //    includeSelectAllOption: true
            //});
        });
    </script>
</body>
</html>
