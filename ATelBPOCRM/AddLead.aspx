﻿<%@ Page Title="Add Lead" EnableEventValidation="false" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AddLead.aspx.cs" Inherits="ATelBPOCRM.AddInsurance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <%--<section class="content-header">
        <h1>Lead Form</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Leads</a></li>
            <li class="active">Add New Lead</li>
        </ol>
    </section>--%>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <asp:Panel ID="pnlGetLead" runat="server">
                <div class="col-md-12">
                    <div class="pull-left">
                        <div class="col-md-1">
                            Id:<asp:TextBox ID="txtLeadId" ReadOnly="True" TextMode="Number" CssClass="form-control" Width="70" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            VId:<asp:TextBox ID="txtViciLeadId" ReadOnly="True" TextMode="Number" CssClass="form-control" Width="70" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            Phone :<asp:TextBox ID="txtPhone" TextMode="Number" CssClass="form-control" MaxLength="10" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-4" style="margin-top: 19px; margin-bottom: 2px;">
                            <asp:LinkButton ID="btnGetInsuranceLead" runat="server" CssClass="btn btn-flat btn-primary btn-xs" OnClick="btnGetInsuranceLead_OnClick"><i class="fa fa-eye"></i>&nbsp; Get Vici Lead</asp:LinkButton>
                        </div>
                        <div class="col-md-2" style="margin-top: 26px; margin-bottom: 2px;">
                            <a class="leadInfo btn label label-success" href="javascript:;">
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="Panel1" runat="server">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lead Information</h3>
                            <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <asp:TextBox ID="txtFName" CssClass="form-control" placeholder="First Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <asp:TextBox ID="txtLName" CssClass="form-control" placeholder="Last Name" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="Male">Male</asp:ListItem>
                                            <asp:ListItem Value="Female">Female</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>City</label>
                                        <asp:TextBox ID="txtCity" CssClass="form-control" placeholder="City" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>State</label>
                                        <asp:TextBox ID="txtState" CssClass="form-control state" AutoPostBack="True" OnTextChanged="txtState_OnTextChanged" placeholder="State" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Zip Code</label>&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator2" Text="*" ControlToValidate="txtZip" ForeColor="Red" ValidationExpression="\d{5}(-\d{4})?" runat="server" ErrorMessage="RegularExpressionValidator"></asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txtZip" CssClass="form-control" placeholder="Zip Code" MaxLength="5" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Phone No</label>
                                        <asp:TextBox ID="txtPhoneNo" CssClass="form-control phoneNo" placeholder="Phone Number" ReadOnly="true" MaxLength="10" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Insurance Company</label>
                                        <asp:DropDownList ID="ddlInsuranceCompany" CssClass="form-control select2" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="ALFA">ALFA</asp:ListItem>
                                            <asp:ListItem Value="Allstate">Allstate</asp:ListItem>
                                            <asp:ListItem Value="American Family">American Family</asp:ListItem>
                                            <asp:ListItem Value="Amica">Amica</asp:ListItem>
                                            <asp:ListItem Value="Auto Owners">Auto Owners</asp:ListItem>
                                            <asp:ListItem Value="Erie">Erie</asp:ListItem>
                                            <asp:ListItem Value="Farmers">Farmers</asp:ListItem>
                                            <asp:ListItem Value="Farm Bureau">Farm Bureau</asp:ListItem>
                                            <asp:ListItem Value="Geico">Geico</asp:ListItem>
                                            <asp:ListItem Value="Liberty Mutual">Liberty Mutual</asp:ListItem>
                                            <asp:ListItem Value="Nationwide">Nationwide</asp:ListItem>
                                            <asp:ListItem Value="Progressive">Progressive</asp:ListItem>
                                            <asp:ListItem Value="Safeco">Safeco</asp:ListItem>
                                            <asp:ListItem Value="State Farm">State Farm</asp:ListItem>
                                            <asp:ListItem Value="The Hartford">The Hartford</asp:ListItem>
                                            <asp:ListItem Value="Travelers">Travelers</asp:ListItem>
                                            <asp:ListItem Value="USAA">USAA</asp:ListItem>
                                            <asp:ListItem Value="Other">Other</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Date-of-Birth</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtDOB" Text="*" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtDOB" CssClass="form-control datepicker" placeholder="Date-of-Birth" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Marital Status</label>
                                        <asp:DropDownList ID="ddlMaritalStatus" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="False">Single</asp:ListItem>
                                            <asp:ListItem Value="True">Married</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Email</label>&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor="Red" ControlToValidate="txtEmail" Text="*" ErrorMessage="RegularExpressionValidator" ValidationExpression="^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"></asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txtEmail" CssClass="form-control" placeholder="Email" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <%-- <div class="box box-solid card">
                                        <div class="card-header" style="font-size: 20px; font-weight: bold;">Vehicle 1:</div>
                                    </div>--%>
                                    <div class="card-body">
                                        <div class="col-md-3">
                                            <div class="card-header" style="font-size: 20px; font-weight: bold;">Vehicle 1:</div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Year</label>
                                                <asp:TextBox ID="txtYear" TextMode="Number" CssClass="form-control" placeholder="Year" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Make</label>
                                                <%--<asp:TextBox ID="txtMake" CssClass="form-control" placeholder="Make" runat="server" autocomplete="off"></asp:TextBox>--%>
                                                <asp:DropDownList ID="ddlMake" runat="server" CssClass="form-control select2">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="Acura">Acura</asp:ListItem>
                                                    <asp:ListItem Value="Alfa Romeo">Alfa Romeo</asp:ListItem>
                                                    <asp:ListItem Value="Audi">Audi</asp:ListItem>
                                                    <asp:ListItem Value="BMW">BMW</asp:ListItem>
                                                    <asp:ListItem Value="Buick">Buick</asp:ListItem>
                                                    <asp:ListItem Value="Cadillac">Cadillac</asp:ListItem>
                                                    <asp:ListItem Value="Chevrolet">Chevrolet</asp:ListItem>
                                                    <asp:ListItem Value="Chrysler">Chrysler</asp:ListItem>
                                                    <asp:ListItem Value="Dodge">Dodge</asp:ListItem>
                                                    <asp:ListItem Value="Fiat">Fiat</asp:ListItem>
                                                    <asp:ListItem Value="Ford">Ford</asp:ListItem>
                                                    <asp:ListItem Value="GMC">GMC</asp:ListItem>
                                                    <asp:ListItem Value="Honda">Honda</asp:ListItem>
                                                    <asp:ListItem Value="Hyundai">Hyundai</asp:ListItem>
                                                    <asp:ListItem Value="Infiniti">Infiniti</asp:ListItem>
                                                    <asp:ListItem Value="Jaguar">Jaguar</asp:ListItem>
                                                    <asp:ListItem Value="Jeep">Jeep</asp:ListItem>
                                                    <asp:ListItem Value="Kia">Kia</asp:ListItem>
                                                    <asp:ListItem Value="Land Rover">Land Rover</asp:ListItem>
                                                    <asp:ListItem Value="Lexus">Lexus</asp:ListItem>
                                                    <asp:ListItem Value="Lincoln">Lincoln</asp:ListItem>
                                                    <asp:ListItem Value="Mazda">Mazda</asp:ListItem>
                                                    <asp:ListItem Value="Mercedes-Benz">Mercedes-Benz</asp:ListItem>
                                                    <asp:ListItem Value="Mini">Mini</asp:ListItem>
                                                    <asp:ListItem Value="Mitsubishi">Mitsubishi</asp:ListItem>
                                                    <asp:ListItem Value="Nissan">Nissan</asp:ListItem>
                                                    <asp:ListItem Value="Porsche">Porsche</asp:ListItem>
                                                    <asp:ListItem Value="Ram">Ram</asp:ListItem>
                                                    <asp:ListItem Value="Smart">Smart</asp:ListItem>
                                                    <asp:ListItem Value="Subaru">Subaru</asp:ListItem>
                                                    <asp:ListItem Value="Toyota">Toyota</asp:ListItem>
                                                    <asp:ListItem Value="Volkswagen">Volkswagen</asp:ListItem>
                                                    <asp:ListItem Value="Volvo">Volvo</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Model</label>
                                                <asp:TextBox ID="txtModel" CssClass="form-control" placeholder="Model" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card-body">
                                        <div class="col-md-3">
                                            <div class="card-header" style="font-size: 20px; font-weight: bold;">Vehicle 2:</div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Year</label>
                                                <asp:TextBox ID="txtYearV2" TextMode="Number" CssClass="form-control" placeholder="Year" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Make</label>
                                                <%--<asp:TextBox ID="txtMakeV2" CssClass="form-control" placeholder="Make" runat="server" autocomplete="off"></asp:TextBox>--%>
                                                <asp:DropDownList ID="ddlMake2" runat="server" CssClass="form-control select2">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="Acura">Acura</asp:ListItem>
                                                    <asp:ListItem Value="Alfa Romeo">Alfa Romeo</asp:ListItem>
                                                    <asp:ListItem Value="Audi">Audi</asp:ListItem>
                                                    <asp:ListItem Value="BMW">BMW</asp:ListItem>
                                                    <asp:ListItem Value="Buick">Buick</asp:ListItem>
                                                    <asp:ListItem Value="Cadillac">Cadillac</asp:ListItem>
                                                    <asp:ListItem Value="Chevrolet">Chevrolet</asp:ListItem>
                                                    <asp:ListItem Value="Chrysler">Chrysler</asp:ListItem>
                                                    <asp:ListItem Value="Dodge">Dodge</asp:ListItem>
                                                    <asp:ListItem Value="Fiat">Fiat</asp:ListItem>
                                                    <asp:ListItem Value="Ford">Ford</asp:ListItem>
                                                    <asp:ListItem Value="GMC">GMC</asp:ListItem>
                                                    <asp:ListItem Value="Honda">Honda</asp:ListItem>
                                                    <asp:ListItem Value="Hyundai">Hyundai</asp:ListItem>
                                                    <asp:ListItem Value="Infiniti">Infiniti</asp:ListItem>
                                                    <asp:ListItem Value="Jaguar">Jaguar</asp:ListItem>
                                                    <asp:ListItem Value="Jeep">Jeep</asp:ListItem>
                                                    <asp:ListItem Value="Kia">Kia</asp:ListItem>
                                                    <asp:ListItem Value="Land Rover">Land Rover</asp:ListItem>
                                                    <asp:ListItem Value="Lexus">Lexus</asp:ListItem>
                                                    <asp:ListItem Value="Lincoln">Lincoln</asp:ListItem>
                                                    <asp:ListItem Value="Mazda">Mazda</asp:ListItem>
                                                    <asp:ListItem Value="Mercedes-Benz">Mercedes-Benz</asp:ListItem>
                                                    <asp:ListItem Value="Mini">Mini</asp:ListItem>
                                                    <asp:ListItem Value="Mitsubishi">Mitsubishi</asp:ListItem>
                                                    <asp:ListItem Value="Nissan">Nissan</asp:ListItem>
                                                    <asp:ListItem Value="Porsche">Porsche</asp:ListItem>
                                                    <asp:ListItem Value="Ram">Ram</asp:ListItem>
                                                    <asp:ListItem Value="Smart">Smart</asp:ListItem>
                                                    <asp:ListItem Value="Subaru">Subaru</asp:ListItem>
                                                    <asp:ListItem Value="Toyota">Toyota</asp:ListItem>
                                                    <asp:ListItem Value="Volkswagen">Volkswagen</asp:ListItem>
                                                    <asp:ListItem Value="Volvo">Volvo</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Model</label>
                                                <asp:TextBox ID="txtModelV2" CssClass="form-control" placeholder="Model" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Homeowner</label>
                                        <asp:DropDownList ID="ddlHOwnerShip" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="False">No</asp:ListItem>
                                            <asp:ListItem Value="True">Yes</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Length of Continuous Coverage</label>
                                        <asp:DropDownList ID="ddlContinousCoverage" CssClass="form-control" runat="server">
                                            <%--<asp:ListItem Value="Less than 1 Year">Less than 1 Year</asp:ListItem>--%>
                                            <asp:ListItem Value="1-2 Years">1-2 Years</asp:ListItem>
                                            <asp:ListItem Value="3-5 Years">3-5 Years</asp:ListItem>
                                            <asp:ListItem Value="5+ Years">5+ Years</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Agency Owner</label>
                                        <asp:DropDownList ID="ddlAgencyOwner" CssClass="form-control select2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAgencyOwner_OnSelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlProducer" runat="server">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Agency Producer</label>
                                            <asp:DropDownList ID="ddlAgencyProducer" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <asp:TextBox ID="txtAddress" CssClass="form-control" placeholder="Address" TextMode="MultiLine" Rows="1" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlDisposition" runat="server">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Disposition</label>
                                            <asp:DropDownList ID="ddlDisposition" CssClass="form-control select2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDisposition_OnSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlFollow" runat="server" Visible="False">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Follow-up Date</label>
                                            <asp:TextBox ID="txtFollowDate" CssClass="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Home Lead</label>
                                        <br />
                                        <asp:CheckBox ID="chkHomeLead" runat="server" CssClass="chkHomeLead" />
                                    </div>
                                </div>
                                <div id="homeLead" class="" style="border-radius: 0px !important; display: none">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>No Flood</label>
                                            <asp:DropDownList ID="ddlIsFlooded" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>No Open Claims</label>
                                            <asp:DropDownList ID="ddlIsOpenClaim" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Note</label>
                                        <asp:TextBox ID="txtComments" Rows="2" CssClass="form-control" TextMode="MultiLine" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat btn-sm" OnClick="btnSave_OnClick" />
                                |
                                <asp:Button ID="btnPingNPost" runat="server" Text="Ping" CssClass="btn btn-warning btn-flat btn-sm" OnClick="btnPingNPost_OnClick" />
                                |
                                <asp:Button ID="btnPendingSave" runat="server" Text="Pulse" CssClass="btn btn-info btn-flat btn-sm" OnClick="btnPendingSave_OnClick" />
                                |
                                <asp:Button ID="btnRicochetSave" runat="server" Text="Ricochet Data" CssClass="btn btn-success btn-flat btn-sm" OnClick="btnRicochetSave_OnClick" Visible="False" />
                                |
                                <asp:Button ID="btnRicochetLive" runat="server" Text="Ricochet Live" CssClass="btn btn-primary btn-flat btn-sm" OnClick="btnRicochetLive_OnClick" Visible="False" />
                                <div class="pull-right">
                                    <asp:Label ID="lblAgencyPhone" CssClass="label label-info" runat="server" Font-Size="16" Text=""></asp:Label>
                                    <asp:Panel ID="pnlCopy" runat="server" Visible="False" Style="display: inline !important">
                                        <button class="btn btn-sm simptip-position-top" data-tooltip="Copy" style="margin-bottom: 8px;" onclick="copyToClipboard('#<%= lblAgencyPhone.ClientID %>'); return false;"><i class="fa fa-copy"></i></button>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </section>
    <!-- // section -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>
    <script type="text/javascript">
        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }
    </script>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>

    <script type="text/javascript">
        $(function () {
            $('#<%= chkHomeLead.ClientID %>').click(function () {
                debugger;
                if ($(this).prop("checked") == true) {
                    $('#homeLead').css('display', 'inline');
                } else {
                    $('#homeLead').css('display', 'none');
                }
            });
        })
    </script>

    <script type="text/javascript">
        $(function () {
            $('.leadInfo').on('click', function () {
                debugger;
                var state = $('.state').val();
                var phone = $('.phoneNo').val();
                window.open('LeadInfo.aspx?state=' + state + '&phone=' + phone, '_blank');
                //$.fancybox.open({
                //    src: 'LeadInfo.aspx?state=' + state + '&phone=' + phone,
                //    type: 'iframe'
                //});
            });
        })
    </script>
</asp:Content>
