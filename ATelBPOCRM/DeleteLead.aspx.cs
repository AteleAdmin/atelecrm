﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class DeleteLead : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var isValid = false;
                var cmd = new SqlCommand("GetLeadForDelete", Connection.Db());
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
                var da = new SqlDataAdapter(cmd);
                var ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    for (var i = 0; i < ds.Tables.Count; i++)
                    {
                        if (ds.Tables[i].Rows.Count > 0)
                        {
                            GridView1.DataSource = ds.Tables[i];
                            GridView1.DataBind();

                            isValid = true;
                        }
                    }
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();

                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('No Lead Found!.');", true);
                }

                if (isValid == false)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('No Lead Found!.');", true);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var type = GridView1.Rows[rowIndex].FindControl("ltType") as Label;
                    if (type.Text == "Live")
                    {
                        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                        {
                            var cmd = new SqlCommand("DELETE FROM dbo.Insurance WHERE PhoneNo=@PhoneNo", Connection.Db());
                            cmd.Parameters.Clear();
                            if (Session["UId"] != null)
                            {
                                cmd.Parameters.AddWithValue("@PhoneNo", txtPhone.Text.Trim());
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Deleted Successfully.');", true);

                                    GridView1.EmptyDataText = "No Record Found!";
                                    GridView1.DataBind();
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot Deleted.');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Time Out.');", true);
                            }
                        }
                    }
                    if (type.Text == "Ping")
                    {
                        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                        {
                            var cmd = new SqlCommand("DELETE FROM dbo.PingNPost WHERE (IsDemandPNP = 0 OR IsDemandPNP IS NULL) AND PhoneNo=@PhoneNo", Connection.Db());
                            cmd.Parameters.Clear();
                            if (Session["UId"] != null)
                            {
                                cmd.Parameters.AddWithValue("@PhoneNo", txtPhone.Text.Trim());
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Deleted Successfully.');", true);

                                    GridView1.EmptyDataText = "No Record Found!";
                                    GridView1.DataBind();
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot Deleted.');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Time Out.');", true);
                            }
                        }
                    }
                    if (type.Text == "Pulse")
                    {
                        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                        {
                            var cmd = new SqlCommand("DELETE FROM dbo.PingNPost WHERE (IsDemandPNP = 1 OR IsDemandPNP IS NULL) AND PhoneNo=@PhoneNo", Connection.Db());
                            cmd.Parameters.Clear();
                            if (Session["UId"] != null)
                            {
                                cmd.Parameters.AddWithValue("@PhoneNo", txtPhone.Text.Trim());
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Deleted Successfully.');", true);

                                    GridView1.EmptyDataText = "No Record Found!";
                                    GridView1.DataBind();
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot Deleted.');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Time Out.');", true);
                            }
                        }
                    }
                    if (type.Text == "Pending")
                    {
                        if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                        {
                            var cmd = new SqlCommand("DELETE FROM dbo.PendingPNP WHERE PhoneNo=@PhoneNo", Connection.Db());
                            cmd.Parameters.Clear();
                            if (Session["UId"] != null)
                            {
                                cmd.Parameters.AddWithValue("@PhoneNo", txtPhone.Text.Trim());
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Deleted Successfully.');", true);

                                    GridView1.EmptyDataText = "No Record Found!";
                                    GridView1.DataBind();
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot Deleted.');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Time Out.');", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}