﻿using DbManager;
using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class PendingPNPLeads : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateLead(0, 0, "", "");
                PopulateAgencyCount(0, "", "");
            }
            else
            {
                //
            }
        }

        public void PopulateLead(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ao.Email AS AgencyMail,ao.Token,ao.AToken,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource=1 THEN 'Outsource' ELSE 'ATel' END) AS LeadOwner FROM dbo.PendingPNP i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " AND i.IsDemandPNP=1 ORDER BY i.Id ASC"; // AND i.IsDemandPNP=1
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ao.Email AS AgencyMail,ao.Token,ao.AToken,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource=1 THEN 'Outsource' ELSE 'ATel' END) AS LeadOwner FROM dbo.PendingPNP i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " AND i.IsDemandPNP=1 ORDER BY i.Id ASC"; ;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ao.Email AS AgencyMail,ao.Token,ao.AToken,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource=1 THEN 'Outsource' ELSE 'ATel' END) AS LeadOwner FROM dbo.PendingPNP i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " AND i.IsDemandPNP=1 ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ao.Email AS AgencyMail,ao.Token,ao.AToken,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource=1 THEN 'Outsource' ELSE 'ATel' END) AS LeadOwner FROM dbo.PendingPNP i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " AND i.IsDemandPNP=1 ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ao.Email AS AgencyMail,ao.Token,ao.AToken,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource=1 THEN 'Outsource' ELSE 'ATel' END) AS LeadOwner FROM dbo.PendingPNP i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsDemandPNP=1 ORDER BY i.Id ASC"; //CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,ao.Email AS AgencyMail,ao.Token,ao.AToken,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource=1 THEN 'Outsource' ELSE 'ATel' END) AS LeadOwner FROM dbo.PendingPNP i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND i.DispositionId NOT IN (1,3) ORDER BY i.Id ASC"; //CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAllLeads.DataSource = dt;
                    gvAllLeads.DataBind();

                    gvAllLeads.UseAccessibleHeader = true;
                    gvAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void gvAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var pnl = e.Row.FindControl("pnlAction") as Panel;
                if (Session["Role"] != null && Session["Role"].ToString() == "Team Lead")
                {
                    pnl.Visible = false;
                }
            }

            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;

                #region DisableEditPreviousLeads
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;
                #region DisableEditPreviousLead
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
            {
                //e.Row.Cells[20].Visible = true;
            }
        }

        protected void gvAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
                if (disposition != null && (disposition.Text.Contains("Hot Lead")))
                {
                    disposition.CssClass = "label label-info";
                }
            }
            else
            {
                //
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn == null) return;
                var index = Convert.ToInt32(btn.Attributes["RowIndex"]);
                var hfId = (HiddenField)gvAllLeads.Rows[index].FindControl("hfId");
                if (hfId != null)
                {
                    var cmd = new SqlCommand("DELETE FROM dbo.PendingPNP WHERE Id=" + Convert.ToInt32(hfId.Value), Connection.Db());
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Deleted Successfully.');", true);
                        PopulateLead(0, 0, "", "");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Deleted.');", true);
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnMove_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn == null) return;
                var index = Convert.ToInt32(btn.Attributes["RowIndex"]);
                var hfId = (HiddenField)gvAllLeads.Rows[index].FindControl("hfId");
                var phoneNo = (HiddenField)gvAllLeads.Rows[index].FindControl("hfPhoneNo");
                var agencyMail = (HiddenField)gvAllLeads.Rows[index].FindControl("hfAgEmail");
                var token = (HiddenField)gvAllLeads.Rows[index].FindControl("hfAgToken");
                if (hfId != null)
                {
                    var cmd = new SqlCommand("INSERT INTO dbo.PingNPost (FirstName, LastName, Address, City, State, Zip, PhoneNo, Company, Make, Year, Model, Make2, Year2, Model2, DOB, Email, Gender, Comments, HOwnerShip, ContinousCoverage, MaritalStatus, FollowUpXDate, AgencyOwnerId, AgencyProducerId, ViciLeadId, DispositionId, IsActive, OutsourceOwnerId, IsOutsource, IsDemandPNP, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) SELECT pp.FirstName,pp.LastName,pp.Address,pp.City,pp.State,pp.Zip,pp.PhoneNo,pp.Company,pp.Make,pp.Year,pp.Model,pp.Make2,pp.Year2,pp.Model2,pp.DOB,pp.Email,pp.Gender,pp.Comments,pp.HOwnerShip,pp.ContinousCoverage,pp.MaritalStatus,pp.FollowUpXDate,pp.AgencyOwnerId,pp.AgencyProducerId,pp.ViciLeadId,pp.DispositionId,pp.IsActive,pp.OutsourceOwnerId,pp.IsOutsource,pp.IsDemandPNP,pp.CreatedBy,'" + DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")) + "',pp.LastModifiedBy,'" + DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")) + "' FROM dbo.PendingPNP pp WHERE pp.PhoneNo='" + phoneNo.Value + "'", Connection.Db());
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Move Successfully.');", true);

                        if (!string.IsNullOrEmpty(token.Value.ToString()))
                        {
                            var filePath = HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.p12");

                            var notify = new PushNotify();
                            notify.SendPushNotification(token.Value.ToString(), "Maverick Leads - New lead available", filePath);
                        }

                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT pp.FirstName,pp.LastName,pp.Gender,pp.DOB,pp.City,pp.State,pp.Zip,pp.Address,pp.PhoneNo,pp.Company,pp.Email,(CASE WHEN pp.MaritalStatus=1 THEN 'Married' ELSE 'Single' END) AS MaritalStatus,(CASE WHEN pp.HOwnerShip=1 THEN 'Yes' ELSE 'No' END) AS HOwnerShip,pp.ContinousCoverage FROM dbo.PendingPNP pp WHERE pp.Id=" + Convert.ToInt32(hfId.Value));
                        if (dt.Rows.Count > 0)
                        {
                            var thread = new Thread(delegate ()
                            {
                                var sb = new StringBuilder();
                                sb.Append("");
                                sb.Append("<div style=\"padding: 10px; background: #eff1f1; font-weight: bold;\">New Lead</div>");
                                sb.Append("<table style=\"margin-top: 30px;\">");

                                sb.Append("<tr>");
                                sb.Append("<th>First Name</th>");
                                sb.Append("<td>" + dt.Rows[0]["FirstName"].ToString() + "</td>");
                                sb.Append("<th>Last Name</th>");
                                sb.Append("<td>" + dt.Rows[0]["LastName"].ToString() + "</td>");
                                sb.Append("<th>Gender</th>");
                                sb.Append("<td>" + dt.Rows[0]["Gender"].ToString() + "</td>");
                                sb.Append("<th>Date-of-Birth</th>");
                                sb.Append("<td>" + dt.Rows[0]["DOB"].ToString() + "</td>");
                                sb.Append("</tr>");

                                sb.Append("<tr>");
                                sb.Append("<th>City</th>");
                                sb.Append("<td>" + dt.Rows[0]["City"].ToString() + "</td>");
                                sb.Append("<th>State</th>");
                                sb.Append("<td>" + dt.Rows[0]["State"].ToString() + "</td>");
                                sb.Append("<th>Zip</th>");
                                sb.Append("<td>" + dt.Rows[0]["Zip"].ToString() + "</td>");
                                sb.Append("<th>Address</th>");
                                sb.Append("<td>" + dt.Rows[0]["Address"].ToString() + "</td>");
                                sb.Append("</tr>");

                                sb.Append("<tr>");
                                sb.Append("<th>Phone</th>");
                                sb.Append("<td>" + dt.Rows[0]["PhoneNo"].ToString() + "</td>");
                                sb.Append("<th>Company</th>");
                                sb.Append("<td>" + dt.Rows[0]["Company"].ToString() + "</td>");
                                sb.Append("<th>Disposition</th>");
                                sb.Append("<td>Lead</td>");
                                sb.Append("</tr>");

                                sb.Append("<tr>");
                                sb.Append("<th>Email</th>");
                                sb.Append("<td>" + dt.Rows[0]["Email"].ToString() + "</td>");
                                sb.Append("<th>Marital Status</th>");
                                sb.Append("<td>" + dt.Rows[0]["MaritalStatus"].ToString() + "</td>");
                                sb.Append("<th>Home Owner</th>");
                                sb.Append("<td>" + dt.Rows[0]["HOwnerShip"].ToString() + "</td>");
                                sb.Append("<th>Continuous Coverage</th>");
                                sb.Append("<td>" + dt.Rows[0]["ContinousCoverage"].ToString() + "</td>");
                                sb.Append("</tr>");

                                sb.Append("<tr>");
                                sb.Append("<th><a href='http://mavleads.online/LivePNP.aspx'>View Lead</a></td>");
                                sb.Append("</tr>");

                                sb.Append("</table>");

                                MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads", agencyMail.Value.ToString(), "", "", "Maverick Leads - New Lead in Portal", sb.ToString(), true);
                                MailHelper.SendEmail("notifications@mavleads.com", "Maverick Leads", "jason.smith@rapidratesonline.com", "", "", "Maverick Leads - New Lead in Portal", sb.ToString(), true);
                            });

                            thread.IsBackground = true;
                            thread.Start();
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot Moved.');", true);
                    }

                    var command = new SqlCommand("DELETE FROM dbo.PendingPNP WHERE Id=" + Convert.ToInt32(hfId.Value), Connection.Db());
                    command.ExecuteNonQuery();
                    PopulateLead(0, 0, "", "");
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulateAgencyCount(int? teamLeadId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (teamLeadId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PendingPNP i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 GROUP BY ao.Name, ao.State";
                }
                if (teamLeadId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PendingPNP i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id JOIN dbo.Users u ON i.CreatedBy = u.Id LEFT JOIN dbo.Users tl ON u.ReportedTo = tl.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND tl.Id=" + teamLeadId + " AND i.IsOutsource=0 GROUP BY ao.Name, ao.State";
                }
                if (teamLeadId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PendingPNP i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' GROUP BY ao.Name, ao.State";
                }
                if (teamLeadId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ao.Name,ao.State,COUNT(CASE WHEN i.DispositionId IN (5,8,10,11,12,13) THEN 1 END) AS Rejected,COUNT(CASE WHEN i.DispositionId NOT IN (1,5,3,8,10,11,12,13) THEN 1 END) AS Accepted,COUNT(CASE WHEN i.DispositionId NOT IN(1,3) THEN 1 END) AS Total FROM dbo.PendingPNP i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId = ao.Id WHERE i.IsHomeLead=0 AND i.IsActive=1 AND i.IsDemandPNP=1 GROUP BY ao.Name, ao.State"; //AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101)
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyStat.DataSource = dt;
                    gvAgencyStat.DataBind();
                }
                else
                {
                    gvAgencyStat.EmptyDataText = "No Record Found!";
                    gvAgencyStat.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        private int _rejected = 0;
        private int _accepted = 0;
        private int _total = 0;
        protected void gvAgencyStat_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    _rejected += Convert.ToInt32(((Literal)e.Row.FindControl("ltRejected")).Text);
                    _accepted += Convert.ToInt32(((Literal)e.Row.FindControl("ltAccepted")).Text);
                    _total += Convert.ToInt32(((Literal)e.Row.FindControl("ltTotal")).Text);
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    // (e.Row.FindControl("lblAmount") as Label).Text = amount.ToString();
                    //e.Row.Cells[7].Text = String.Format("{0:0}", "<b>" + Total + "</b>");
                    //gvAgencyStat.FooterRow.Cells[0].Text = "Total";

                    var head = (Label)e.Row.FindControl("lblHead");
                    var saleRejected = (Label)e.Row.FindControl("lblRejected");
                    var saleAccepted = (Label)e.Row.FindControl("lblAccepted");
                    var saleTotal = (Label)e.Row.FindControl("lblTotal");

                    head.Text = "Total";
                    saleRejected.Text = _rejected.ToString();
                    saleAccepted.Text = _accepted.ToString();
                    saleTotal.Text = _total.ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnStats_OnClick(object sender, EventArgs e)
        {
            try
            {
                var fdate = DateTime.Parse(txtFrom.Text.Trim());
                var from = fdate.ToString("yyyyMMdd");

                var tdate = DateTime.Parse(txtTo.Text.Trim());
                var to = tdate.ToString("yyyyMMdd");

                PopulateLead(0, 0, from, to);
                PopulateAgencyCount(0, from, to);
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}