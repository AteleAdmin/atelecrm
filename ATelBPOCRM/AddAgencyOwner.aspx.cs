﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class AddAgencyOwner : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["agencyOwnerId"] != null)
                {
                    if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "accountHead")
                    {
                        pnlReportedTo.Visible = false;
                    }

                    if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "accountManager")
                    {
                        pnlReportedTo.Visible = true;

                        var source = new PopulateDataSource();
                        var table = source.DataTableSqlString("SELECT ao.Id,ao.Name,ao.Name AS ReportedTo FROM dbo.AgencyOwner ao WHERE ao.RoleId=16 AND ReportedTo=" + Convert.ToInt32(Request.QueryString["agencyOwnerId"]));
                        if (table.Rows.Count > 0)
                        {
                            ddlReportedTo.DataSource = table;
                            ddlReportedTo.DataTextField = "Name";
                            ddlReportedTo.DataValueField = "Id";
                            ddlReportedTo.DataBind();

                            ddlReportedTo.ClearSelection();
                            ddlReportedTo.SelectedIndex = ddlReportedTo.Items.IndexOf(ddlReportedTo.Items.FindByText(table.Rows[0]["ReportedTo"].ToString()));
                            ddlReportedTo.DataBind();

                        }
                        else
                        {
                            ddlReportedTo.Items.Insert(0, "");
                        }
                    }

                    if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "agency")
                    {
                        pnlReportedTo.Visible = true;

                        var source = new PopulateDataSource();
                        var table = source.DataTableSqlString("SELECT ao.Id,ao.Name,ao.Name AS ReportedTo FROM dbo.AgencyOwner ao WHERE ao.RoleId=17 AND ReportedTo=" + Convert.ToInt32(Request.QueryString["agencyOwnerId"]));
                        if (table.Rows.Count > 0)
                        {
                            ddlReportedTo.DataSource = table;
                            ddlReportedTo.DataTextField = "Name";
                            ddlReportedTo.DataValueField = "Id";
                            ddlReportedTo.DataBind();

                            ddlReportedTo.ClearSelection();
                            ddlReportedTo.SelectedIndex = ddlReportedTo.Items.IndexOf(ddlReportedTo.Items.FindByText(table.Rows[0]["ReportedTo"].ToString()));
                            ddlReportedTo.DataBind();
                        }
                        else
                        {
                            ddlReportedTo.Items.Insert(0, "");
                        }
                    }

                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ao.Id, ao.Name, ao.Username, ao.Email, ao.Password, ao.Gender, ao.ContactNo, ao.Address, ao.State, r.Name AS Role, ao.Company, ao.Description, CONVERT(NVARCHAR(12),ao.DOJ, 101) AS DOJ FROM dbo.AgencyOwner ao JOIN dbo.Roles r ON ao.RoleId = r.Id WHERE ao.Id=" + Convert.ToInt32(Request.QueryString["agencyOwnerId"]));
                    if (dt.Rows.Count > 0)
                    {
                        txtName.Text = dt.Rows[0]["Name"].ToString();
                        txtUName.Text = dt.Rows[0]["Username"].ToString();
                        txtUName.ReadOnly = true;
                        txtEmail.Text = dt.Rows[0]["Email"].ToString();
                        txtPassword.Text = dt.Rows[0]["Password"].ToString();
                        chkAuto.Enabled = false;
                        txtPassword.ReadOnly = true;
                        txtContactNo.Text = dt.Rows[0]["ContactNo"].ToString();
                        //txtCompany.Text = dt.Rows[0]["Company"].ToString();
                        txtAddress.Text = dt.Rows[0]["Address"].ToString();
                        txtDescription.Text = dt.Rows[0]["Description"].ToString();
                        txtDOJ.Text = dt.Rows[0]["DOJ"].ToString();

                        ddlGender.ClearSelection();
                        ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                        ddlGender.DataBind();

                        ddlInsuranceCompany.ClearSelection();
                        ddlInsuranceCompany.SelectedIndex = ddlInsuranceCompany.Items.IndexOf(ddlInsuranceCompany.Items.FindByText(dt.Rows[0]["Company"].ToString()));
                        ddlInsuranceCompany.DataBind();

                        ddlRole.ClearSelection();
                        ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByText(dt.Rows[0]["Role"].ToString()));
                        ddlRole.DataBind();

                        ddlState.Items.Insert(0, dt.Rows[0]["State"].ToString());
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //Response.Redirect("Login.aspx");
                }
            }
            else
            {
                //
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["agencyOwnerId"] != null)
                {
                    var cmd = new SqlCommand("Update dbo.AgencyOwner SET Name=@Name, Email=@Email, RoleId=@RoleId, ReportedTo=@ReportedTo, Gender=@Gender, ContactNo=@ContactNo, Company=@Company, Address=@Address, State=@State, Description=@Description, DOJ=@DOJ, LastModifiedBy=@LastModifiedBy, UpdatedDate=@UpdatedDate WHERE Id=@Id");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.ToLower().Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Company", ddlInsuranceCompany.SelectedValue);
                    cmd.Parameters.AddWithValue("@RoleId", Convert.ToInt32(ddlRole.SelectedValue));

                    if (!string.IsNullOrEmpty(ddlReportedTo.SelectedItem.Text.ToString()))
                    {
                        if (ddlRole.SelectedItem.Text == "Account Head")
                            cmd.Parameters.AddWithValue("@ReportedTo", 0);
                        if (ddlRole.SelectedItem.Text == "Account Manager")
                            cmd.Parameters.AddWithValue("@ReportedTo", Convert.ToInt32(ddlReportedTo.SelectedValue));
                        if (ddlRole.SelectedItem.Text == "Agency Owner")
                            cmd.Parameters.AddWithValue("@ReportedTo", Convert.ToInt32(ddlReportedTo.SelectedValue));
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ReportedTo", 0);
                    }

                    var states = "";
                    foreach (ListItem item in ddlState.Items)
                    {
                        if (item.Selected)
                        {
                            states += item.Text.Trim() + ",".Trim();
                        }
                        else
                        {
                            //
                        }
                    }

                    states = states.TrimEnd(',');
                    if (!string.IsNullOrEmpty(states))
                    {
                        cmd.Parameters.AddWithValue("@State", states);
                    }
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim());
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Request.QueryString["agencyOwnerId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Agency Updated Successfully.');", true);
                            chkAuto.Enabled = true;
                            txtUName.ReadOnly = false;
                            txtPassword.ReadOnly = false;
                            Clear();
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Agency Cannot be Update.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Please login and try again.');", true);
                    }
                }
                else
                {
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Users WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyProducer WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Admin WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.OutsourceOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                        return;
                    }
                    else
                    {
                        var cmd = new SqlCommand("INSERT INTO dbo.AgencyOwner(Name, Username, Email, ReportedTo, Password, Gender, ContactNo, Company, Address, State, Description, DOJ, RoleId, IsActive, CreatedBy, CreatedDate, LastModifiedBy, UpdatedDate)VALUES(@Name, @Username, @Email, @ReportedTo, @Password, @Gender, @ContactNo, @Company, @Address, @State, @Description, @DOJ, @RoleId, @IsActive, @CreatedBy, @CreatedDate, @LastModifiedBy, @UpdatedDate)");
                        cmd.Parameters.Clear();
                        cmd.Connection = Connection.Db();
                        cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                        cmd.Parameters.AddWithValue("@Username", txtUName.Text.Trim());
                        cmd.Parameters.AddWithValue("@Email", txtEmail.Text.ToLower().Trim());
                        if (!chkAuto.Checked && string.IsNullOrEmpty(txtPassword.Text.Trim()))
                            cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                        if (chkAuto.Checked)
                            cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                        if (!chkAuto.Checked && !string.IsNullOrEmpty(txtPassword.Text.Trim()))
                            cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                        cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                        cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.Trim());
                        cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                        cmd.Parameters.AddWithValue("@Company", ddlInsuranceCompany.SelectedValue);

                        if (!string.IsNullOrEmpty(ddlReportedTo.SelectedItem.Text.ToString()))
                        {
                            if (ddlRole.SelectedItem.Text == "Account Head")
                                cmd.Parameters.AddWithValue("@ReportedTo", 0);
                            if (ddlRole.SelectedItem.Text == "Account Manager")
                                cmd.Parameters.AddWithValue("@ReportedTo", Convert.ToInt32(ddlReportedTo.SelectedValue));
                            if (ddlRole.SelectedItem.Text == "Agency Owner")
                                cmd.Parameters.AddWithValue("@ReportedTo", Convert.ToInt32(ddlReportedTo.SelectedValue));
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@ReportedTo", 0);
                        }

                        var states = "";
                        foreach (ListItem item in ddlState.Items)
                        {
                            if (item.Selected)
                            {
                                states += item.Text.Trim() + ",".Trim();
                            }
                            else
                            {
                                //
                            }
                        }

                        states = states.TrimEnd(',');
                        if (!string.IsNullOrEmpty(states))
                        {
                            cmd.Parameters.AddWithValue("@State", states);
                        }
                        cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                        cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim());
                        cmd.Parameters.AddWithValue("@RoleId", Convert.ToInt32(ddlRole.SelectedValue));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        if (Session["UId"] != null)
                        {
                            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"].ToString()));
                            cmd.Parameters.AddWithValue("@CreatedDate",
                                DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                            cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Agency Save Successfully.');", true);
                                Clear();
                                ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Agency Cannot be Saved.');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Please login and try again.');", true);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void Clear()
        {
            txtName.Text = "";
            txtUName.Text = "";
            txtEmail.Text = "";
            txtPassword.Text = "";
            txtContactNo.Text = "";
            txtAddress.Text = "";
            txtDescription.Text = "";
            txtDOJ.Text = "";

            ddlGender.SelectedIndex = 0;
            ddlState.SelectedIndex = 0;
        }

        private static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var res = new StringBuilder();
            var rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        protected void ddlRole_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRole.SelectedItem.Text == "Account Head")
            {
                pnlReportedTo.Visible = false;
            }

            if (ddlRole.SelectedItem.Text == "Account Manager")
            {
                pnlReportedTo.Visible = true;

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT ao.Id,ao.Name FROM dbo.AgencyOwner ao WHERE ao.RoleId=16");
                if (dt.Rows.Count > 0)
                {
                    ddlReportedTo.DataSource = dt;
                    ddlReportedTo.DataTextField = "Name";
                    ddlReportedTo.DataValueField = "Id";
                    ddlReportedTo.DataBind();
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }

            if (ddlRole.SelectedItem.Text == "Agency Owner")
            {
                pnlReportedTo.Visible = true;

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT ao.Id,ao.Name FROM dbo.AgencyOwner ao WHERE ao.RoleId=17");
                if (dt.Rows.Count > 0)
                {
                    ddlReportedTo.DataSource = dt;
                    ddlReportedTo.DataTextField = "Name";
                    ddlReportedTo.DataValueField = "Id";
                    ddlReportedTo.DataBind();
                }
                else
                {
                    //
                }
            }
        }
    }
}