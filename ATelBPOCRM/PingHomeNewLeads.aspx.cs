﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class PingHomeNewLeads : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                {
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                        PopulateLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");

                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                        if (dt.Rows.Count > 0)
                        {
                            PopulateLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), "", "");
                        }
                        else
                        {
                            gvAllLeads.DataSource = "";
                            gvAllLeads.EmptyDataText = "No Record Found!";
                            gvAllLeads.DataBind();
                        }
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        PopulateLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulateLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulateLead(0, Convert.ToInt32(Session["UId"].ToString()), "", "");

                        PopulateProducerCount(Convert.ToInt32(Session["UId"].ToString()), "", "");
                        pnlAgencyProducer.Visible = true;
                        pnlAcceptedRejected.Visible = true;
                        pnlTotalCounter.Visible = true;
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                    {
                        PopulateLeadsSummary(0, 0, "", "");
                        PopulateLeadsCount(0, 0, "", "");
                        PopulateLead(0, 0, "", "");
                        pnlAcceptedRejected.Visible = true;
                        pnlTotalCounter.Visible = true;
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        protected void btnGetLead_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                    PopulateLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);

                    //PopulateLeadsSummary(Convert.ToInt32(Session["UId"].ToString()), 0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, txtFrom.Text.Trim(), txtTo.Text.Trim());

                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        PopulateLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), from, to);
                        //PopulateLead(0, Convert.ToInt32(dt.Rows[0]["Id"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    }
                    else
                    {
                        gvAllLeads.DataSource = "";
                        gvAllLeads.EmptyDataText = "No Record Found!";
                        gvAllLeads.DataBind();
                    }
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulateLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulateLead(0, Convert.ToInt32(Session["UId"].ToString()), from, to);

                    //PopulateLeadsSummary(0, Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLead(0, Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());

                    PopulateProducerCount(Convert.ToInt32(Session["UId"].ToString()), from, to);
                    //PopulateProducerCount(Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
                    pnlAgencyProducer.Visible = true;
                    pnlAcceptedRejected.Visible = true;
                    pnlTotalCounter.Visible = true;
                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLeadsSummary(0, 0, from, to);
                    PopulateLeadsCount(0, 0, from, to);
                    PopulateLead(0, 0, from, to);

                    //PopulateLeadsSummary(0, 0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLeadsCount(0, 0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                    //PopulateLead(0, 0, txtFrom.Text.Trim(), txtTo.Text.Trim());

                    pnlAcceptedRejected.Visible = true;
                    pnlTotalCounter.Visible = true;
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulateLead(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " AND i.IsDemandPNP=0 ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " AND i.IsDemandPNP=0 ORDER BY i.Id ASC"; ;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " AND i.IsDemandPNP=0 ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " AND i.IsDemandPNP=0 ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsDemandPNP=0 ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsDemandPNP=0 ORDER BY i.Id ASC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAllLeads.DataSource = dt;
                    gvAllLeads.DataBind();

                    gvAllLeads.UseAccessibleHeader = true;
                    gvAllLeads.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateLeadsCount(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive=1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulateLeadsSummary(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Rejected

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvRejected.DataSource = dt;
                    gvRejected.DataBind();
                }
                else
                {
                    gvRejected.EmptyDataText = "No Record Found!";
                    gvRejected.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }

            // Accepted

            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY d.Name";
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0 GROUP BY d.Name";
                }

                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0 GROUP BY d.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAccepted.DataSource = dt;
                    gvAccepted.DataBind();
                }
                else
                {
                    gvAccepted.EmptyDataText = "No Record Found!";
                    gvAccepted.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulateProducerCount(int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.IsActive =1 AND i.IsDemandPNP=0 i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyProducerTotal.DataSource = dt;
                    gvAgencyProducerTotal.DataBind();
                }
                else
                {
                    gvAgencyProducerTotal.EmptyDataText = "No Record Found!";
                    gvAgencyProducerTotal.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyProducerRejected.DataSource = dt;
                    gvAgencyProducerRejected.DataBind();
                }
                else
                {
                    gvAgencyProducerRejected.EmptyDataText = "No Record Found!";
                    gvAgencyProducerRejected.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13,23) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                if (agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT ap.Name,COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id WHERE i.IsHomeLead=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13,23) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId + " GROUP BY ap.Name";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    gvAgencyProducerAccepted.DataSource = dt;
                    gvAgencyProducerAccepted.DataBind();
                }
                else
                {
                    gvAgencyProducerAccepted.EmptyDataText = "No Record Found!";
                    gvAgencyProducerAccepted.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }

        }

        protected void gvAllLeads_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;

                #region DisableEditPreviousLeads
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                var flag = false;
                //e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                //e.Row.Cells[11].Visible = false;
                //e.Row.Cells[12].Visible = false;
                e.Row.Cells[17].Visible = false;
                #region DisableEditPreviousLead
                //if (flag == false)
                //{
                //    if (!string.IsNullOrEmpty(txtFrom.Text.Trim()))
                //    {
                //        var to = Convert.ToDateTime(DateTime.UtcNow).ToString("d");
                //        var from = Convert.ToDateTime(txtFrom.Text).ToString("d");

                //        var d = DateTime.Parse(to) - DateTime.Parse(from);
                //        var days = d.TotalDays;
                //        if (days >= 5)
                //        {
                //            e.Row.Cells[1].Visible = false;
                //            e.Row.Cells[2].Visible = true;
                //        }
                //        else
                //        {
                //            e.Row.Cells[1].Visible = true;
                //            e.Row.Cells[2].Visible = false;
                //        }

                //        flag = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[2].Visible = false;
                //    }
                //}
                #endregion
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
            {
                //e.Row.Cells[20].Visible = true;
            }
        }

        protected void gvAllLeads_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var disposition = e.Row.FindControl("ltDisposition") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (disposition != null && (disposition.Text.Contains("REJECTED") || disposition.Text.Contains("Hung Up") || disposition.Text.Contains("Ineligible : 2+ At - Fault/Major/No Insurance") || disposition.Text.Contains("Disconnected") || disposition.Text.Contains("Rejected") || disposition.Text.Contains("DNC")))
                {
                    disposition.CssClass = "label label-danger";
                }
                if (disposition != null && (disposition.Text.Contains("Hot Lead")))
                {
                    disposition.CssClass = "label label-info";
                }
            }
            else
            {
                //
            }
        }

        protected void gvRejected_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void gvAccepted_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                var flooded = e.Row.FindControl("ltNoFlood") as Label;
                var openClaim = e.Row.FindControl("ltNoOpenClaim") as Label;

                if (active != null && active.Text == "True")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                if (flooded != null && flooded.Text == "True")
                    flooded.CssClass = "label label-success";
                else if (flooded != null) flooded.CssClass = "label label-danger";

                if (openClaim != null && openClaim.Text == "True")
                    openClaim.CssClass = "label label-success";
                else if (openClaim != null) openClaim.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }
    }
}