﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace ATelBPOCRM
{
    public partial class QAPingLeads : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    //PopulateLeads(Convert.ToInt32(Session["UId"].ToString()), 0);
                    pnlAdd.Visible = false;
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    // PopulateLeads(0, Convert.ToInt32(Session["UId"].ToString()));
                    pnlAdd.Visible = false;
                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    PopulateLeads(0, 0, "", "");
                }
            }
            else
            {
                //
            }
        }

        public void PopulateLeads(int? producerId, int? agencyOwnerId, string from, string to)
        {
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource = 1 THEN 'Outsource' ELSE 'ATel' END) AS Owner FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId + "ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource = 1 THEN 'Outsource' ELSE 'ATel' END) AS Owner FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId + "ORDER BY i.Id ASC"; ;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource = 1 THEN 'Outsource' ELSE 'ATel' END) AS Owner FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.Address,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP,(CASE WHEN i.IsOutsource = 1 THEN 'Outsource' ELSE 'ATel' END) AS Owner FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                    GridView1.UseAccessibleHeader = true;
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "Yes")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";

                var owner = e.Row.FindControl("ltOwner") as Label;
                if (owner != null && owner.Text == "ATel")
                    owner.CssClass = "label label-primary";
                else if (owner != null) owner.CssClass = "label label-default";
            }
            else
            {
                //
            }
        }

        protected void GridView1_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["Role"] != null)
            {
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != GridView1.EditIndex)
                {
                    var ds = new PopulateDataSource();
                    var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                    if (pageName != null) pageName = pageName.Replace(".aspx", "").Trim();
                    var q = "SELECT sm.Name AS SubModule,sm.PageUrl,rp.CanAdd,rp.CanEdit,rp.CanDelete FROM dbo.SubModule sm JOIN dbo.RolesPermissions rp ON sm.Id=rp.SubModuleId WHERE rp.CanView=1 AND REPLACE(sm.PageUrl,'.aspx','')='" + pageName + "' AND rp.RoleId=" + Convert.ToInt32(Session["RoleId"]);
                    var dt = ds.DataTableSqlString(q);

                    if (dt.Rows.Count > 0)
                    {
                        var editButton = e.Row.FindControl("btnEdit") as LinkButton;
                        var deleteButton = e.Row.FindControl("btnDelete") as LinkButton;
                        if (dt.Rows[0]["CanEdit"].ToString() == "False")
                        {
                            if (editButton != null) editButton.Visible = false;
                        }

                        if (dt.Rows[0]["CanDelete"].ToString() == "False")
                        {
                            if (deleteButton != null) deleteButton.Visible = false;
                        }

                        if (dt.Rows[0]["CanAdd"].ToString() == "False")
                        {
                            pnlAdd.Visible = false;
                        }
                    }
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    e.Row.Cells[7].Visible = false;
                    e.Row.Cells[8].Visible = false;
                    e.Row.Cells[9].Visible = false;
                    e.Row.Cells[10].Visible = false;
                    //e.Row.Cells[11].Visible = false;
                    e.Row.Cells[12].Visible = false;
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    e.Row.Cells[7].Visible = false;
                    e.Row.Cells[8].Visible = false;
                    e.Row.Cells[9].Visible = false;
                    e.Row.Cells[10].Visible = false;
                    //e.Row.Cells[11].Visible = false;
                    e.Row.Cells[12].Visible = false;
                }
                else
                {
                    //
                }
            }
        }

        protected void btnLead_OnClick(object sender, EventArgs e)
        {
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
            {
                DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                var from = fdate.ToString("yyyyMMdd");

                DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                var to = tdate.ToString("yyyyMMdd");

                PopulateLeads(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                //PopulateLeads(Convert.ToInt32(Session["UId"].ToString()), 0, txtFrom.Text.Trim(), txtTo.Text.Trim());
                pnlAdd.Visible = false;
            }
            if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
            {
                DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                var from = fdate.ToString("yyyyMMdd");

                DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                var to = tdate.ToString("yyyyMMdd");

                PopulateLeads(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                //PopulateLeads(0, Convert.ToInt32(Session["UId"].ToString()), txtFrom.Text.Trim(), txtTo.Text.Trim());
                pnlAdd.Visible = false;
            }
            if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
            {
                DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                var from = fdate.ToString("yyyyMMdd");

                DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                var to = tdate.ToString("yyyyMMdd");

                PopulateLeads(0, 0, from, to);
                //PopulateLeads(0, 0, txtFrom.Text.Trim(), txtTo.Text.Trim());
            }
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var query = "";
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyOwnerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) AND i.AgencyProducerId=" + Convert.ToInt32(Session["UId"]) + " ORDER BY i.Id ASC";
                    }
                }

                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Yesterday")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-1, GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 7 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-7, GETDATE()) AND i.CreatedDate <= GETDATE() ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days + Today")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) AND i.CreatedDate <= GETDATE() ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last 30 Days")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND i.CreatedDate >= DATEADD(day,-30, GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND MONTH(i.CreatedDate) = MONTH(GETDATE()) AND YEAR(i.CreatedDate) = YEAR(GETDATE()) ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "Last Month")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND DATEPART(m, i.CreatedDate) = DATEPART(m, DATEADD(m, -1, GETDATE()))AND DATEPART(yyyy, i.CreatedDate) = DATEPART(yyyy, DATEADD(m, -1, GETDATE())) ORDER BY i.Id ASC";
                    }
                    if (ddlFilter.SelectedItem.Text.ToString().Trim() == "This Year")
                    {
                        query = "SELECT d.Name AS Disposition,ao.Name AS Agency,ap.Name AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,(CASE WHEN i.IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address,(CASE WHEN i.IsDemandPNP=1 THEN 'Pulse' ELSE 'Ping' END) AS IsDemandPNP FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id LEFT JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE (i.IsDemandPNP = 0 OR i.IsDemandPNP IS NULL) AND i.IsActive=1 AND YEAR(i.CreatedDate) = YEAR(GETDATE()) ORDER BY i.Id ASC";
                    }
                }

                var cmd = new SqlCommand(query, Connection.Db());
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();

                    GridView1.UseAccessibleHeader = true;
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    GridView1.DataSource = "";
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnDetail_OnClick(object sender, EventArgs e)
        {

        }
    }
}