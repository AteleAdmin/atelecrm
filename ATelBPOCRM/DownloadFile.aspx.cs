﻿using ClosedXML.Excel;
using Lucky.General;
using Lucky.Security;
using System;
using System.Data;
using System.IO;
using System.Web.UI;


namespace ATelBPOCRM
{
    public partial class DownloadFile : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var fdate = DateTime.Parse(txtFrom.Text.Trim());
                var from = fdate.ToString("yyyyMMdd");

                var tdate = DateTime.Parse(txtTo.Text.Trim());
                var to = tdate.ToString("yyyyMMdd");

                Download(from, to);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void Download(string from, string to)
        {
            try
            {
                var ds = new PopulateDataSource();
                //var dt = ds.DataSetStoreProcedure("AllLeadReport");
                var dt = ds.DataTabletStoreProcedure("AllLeadReport");
                //if (dt.Tables[0].Rows.Count > 0)
                if (dt.Rows.Count > 0)
                {
                    //ExportDataSetToExcel(dt);
                    gvAllLeads.DataSource = dt;
                    gvAllLeads.DataBind();
                }
                else
                {
                    gvAllLeads.EmptyDataText = "No Record Found!";
                    gvAllLeads.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        private void ExportDataSetToExcel(DataSet ds)
        {
            try
            {
                string file = Server.MapPath("ExcelFiles/DataFile.xlsx");
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(ds.Tables[0]);
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;
                    wb.SaveAs(file);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}