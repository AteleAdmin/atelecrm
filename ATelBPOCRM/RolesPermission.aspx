﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="RolesPermission.aspx.cs" Inherits="ATelBPOCRM.RolesPermission" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Roles
            <small>Manage Roles</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>System Settings</a></li>
            <li class="active">Roles</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Roles Permission <label class="label label-info">
                            <asp:Literal ID="ltRole" runat="server"></asp:Literal></label>
                        </h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-condensed table-hover" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateField HeaderText="Module">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:Literal ID="ltModule" runat="server" Text='<%# Eval("Module") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sub Module">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltSubNodule" runat="server" Text='<%# Eval("SubModule") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkView" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("CanView"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Add">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkAdd" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("CanAdd"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEdit" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("CanEdit"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkDelete" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("CanDelete"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="form-group pull-right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary btn-flat" OnClick="btnSave_OnClick" />
                            |
                            &nbsp;<a href="Roles.aspx" class="btn btn-info btn-flat"><< Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>

