﻿using Lucky.General;
using System;
using System.Text;

namespace ATelBPOCRM
{
    public partial class EmployeeDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["empId"] != null)
                {
                    BindEmployeeDetail(Convert.ToInt32(Request.QueryString["empId"]));
                }
            }
            else
            {
                //
            }
        }

        private void BindEmployeeDetail(int? empId)
        {
            try
            {
                var sb = new StringBuilder();
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT ur.Name AS ReportedTo,r.Name AS Role,uc.Id,uc.Name,uc.ReferenceOfferLetter,uc.Username,uc.Email,uc.Password,uc.CNIC,uc.MobileNo,uc.Project,uc.IsActive,uc.AttendanceId,uc.Guardian,uc.RelationWithGuardian,Convert(varchar, uc.DOB, 106) AS DOB,Convert(varchar, uc.DOJ, 106) AS DOJ,Convert(varchar, uc.DOE, 106) AS DOE,uc.Reason,uc.AltPhoneNo,uc.CurrentAddress,uc.PermanentAddress,uc.SalaryOffered,uc.Gender,uc.MeritalStatus,uc.ContactPerson,uc.RelationWithContactPerson,uc.JoinCount,uc.Profile,uc.Status,uc.IsAssigned,Convert(varchar, uc.CreatedDate, 106) AS CreatedDate FROM dbo.Employees uc JOIN dbo.Roles r ON uc.RoleId=r.Id LEFT JOIN Users ur ON uc.ReportedTo=ur.Id WHERE uc.Id=" + empId);
                if (dt.Rows.Count > 0)
                {
                    sb.Append("");
                    sb.Append("<table class='table table-bordered table-striped'>");

                    sb.Append("<tr>");
                    sb.Append("<th>Name</th>");
                    sb.Append("<td>" + dt.Rows[0]["Name"].ToString() + "</td>");
                    sb.Append("<th>Guardian</th>");
                    sb.Append("<td>" + dt.Rows[0]["Guardian"].ToString() + "</td>");
                    sb.Append("<th>Relation</th>");
                    sb.Append("<td>" + dt.Rows[0]["RelationWithGuardian"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Ref. Offer Letter</th>");
                    sb.Append("<td>" + dt.Rows[0]["ReferenceOfferLetter"].ToString() + "</td>");
                    sb.Append("<th>Gender</th>");
                    sb.Append("<td>" + dt.Rows[0]["Gender"].ToString() + "</td>");
                    sb.Append("<th>CNIC</th>");
                    sb.Append("<td>" + dt.Rows[0]["CNIC"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>DOB</th>");
                    sb.Append("<td>" + dt.Rows[0]["DOB"].ToString() + "</td>");
                    sb.Append("<th>DOJ</th>");
                    sb.Append("<td>" + dt.Rows[0]["DOJ"].ToString() + "</td>");
                    sb.Append("<th>DOE</th>");
                    sb.Append("<td>" + dt.Rows[0]["DOE"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Reason</th>");
                    sb.Append("<td>" + dt.Rows[0]["Reason"].ToString() + "</td>");
                    sb.Append("<th>MobileNo</th>");
                    sb.Append("<td>" + dt.Rows[0]["MobileNo"].ToString() + "</td>");
                    sb.Append("<th>Alt.Phone No</th>");
                    sb.Append("<td>" + dt.Rows[0]["AltPhoneNo"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>CurrentAddress</th>");
                    sb.Append("<td colspan='2'>" + dt.Rows[0]["CurrentAddress"].ToString() + "</td>");
                    sb.Append("<th>PermanentAddress</th>");
                    sb.Append("<td colspan='2'>" + dt.Rows[0]["PermanentAddress"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Merital Status</th>");
                    sb.Append("<td>" + dt.Rows[0]["MeritalStatus"].ToString() + "</td>");
                    sb.Append("<th>Contact Person</th>");
                    sb.Append("<td>" + dt.Rows[0]["ContactPerson"].ToString() + "</td>");
                    sb.Append("<th>Relation</th>");
                    sb.Append("<td>" + dt.Rows[0]["RelationWithContactPerson"].ToString() + "</td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>Attendance Id</th>");
                    sb.Append("<td><span class='label label-info' style='padding: 3px 20px;border: 4px;'>" + dt.Rows[0]["AttendanceId"].ToString() + "</span></td>");
                    sb.Append("<th>Project</th>");
                    sb.Append("<td><span class='label label-default' style='padding: 3px 20px;border: 4px;'>" + dt.Rows[0]["Project"].ToString() + "</span></td>");
                    sb.Append("<th>Status</th>");
                    sb.Append("<td><span class='label label-default' style='padding: 3px 20px;border: 4px;'>" + dt.Rows[0]["Status"].ToString() + "</span></td>");
                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<th>IsAssigned</th>");
                    sb.Append("<td><span class='label label-info' style='padding: 3px 20px;border: 4px;'>" + dt.Rows[0]["IsAssigned"].ToString() + "</span></td>");
                    sb.Append("<th>Reported To</th>");
                    sb.Append("<td><span class='label label-default' style='padding: 3px 20px;border: 4px;'>" + dt.Rows[0]["ReportedTo"].ToString() + "</span></td>");
                    sb.Append("<th>Role</th>");
                    sb.Append("<td><span class='label label-default' style='padding: 3px 20px;border: 4px;'>" + dt.Rows[0]["Role"].ToString() + "</span></td>");
                    sb.Append("</tr>");

                    sb.Append("</table>");

                    ltLead.Text = sb.ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}