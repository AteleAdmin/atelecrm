﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class AddUser : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateRoles();
                PopulateReportedTo();

                if (Request.QueryString["UserId"] != null && Request.QueryString["accountType"] != null)
                {
                    if (Request.QueryString["accountType"].ToString() == "User")
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT (ur.Name+' - '+ur.SudoName) AS ReportedTo,r.Name AS Role,u.Id,u.Name,u.SudoName,u.Description,u.FatherName,u.CNIC,u.Gender,u.ContactNo,u.EmergencyNo,u.Username,u.Email,u.Password,CONVERT(NVARCHAR(12),u.DOB,101) AS DOB,CONVERT(NVARCHAR(12),u.DOJ,101) AS DOJ,u.Department,u.Address,u.CNIC FROM dbo.Users u JOIN dbo.Roles r ON u.RoleId= r.Id JOIN dbo.Users ur ON u.ReportedTo = ur.Id WHERE u.Id=" + Convert.ToInt32(Request.QueryString["UserId"]));
                        if (dt.Rows.Count > 0)
                        {
                            txtName.Text = dt.Rows[0]["Name"].ToString();
                            txtSudoName.Text = dt.Rows[0]["SudoName"].ToString();
                            txtFName.Text = dt.Rows[0]["FatherName"].ToString();

                            txtUName.Text = dt.Rows[0]["Username"].ToString();
                            txtUName.ReadOnly = true;

                            txtEmail.Text = dt.Rows[0]["Email"].ToString();
                            txtCNIC.Text = dt.Rows[0]["CNIC"].ToString();
                            txtContactNo.Text = dt.Rows[0]["ContactNo"].ToString();
                            txtEmergencyContact.Text = dt.Rows[0]["EmergencyNo"].ToString();
                            txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                            txtDOJ.Text = dt.Rows[0]["DOJ"].ToString();
                            txtDepartment.Text = dt.Rows[0]["Department"].ToString();
                            txtAddress.Text = dt.Rows[0]["Address"].ToString();
                            txtDescription.Text = dt.Rows[0]["Description"].ToString();

                            txtPassword.Text = dt.Rows[0]["Password"].ToString();
                            chkAuto.Enabled = false;
                            txtPassword.ReadOnly = true;

                            ddlGender.ClearSelection();
                            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                            ddlGender.DataBind();

                            ddlRole.ClearSelection();
                            ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByText(dt.Rows[0]["Role"].ToString()));
                            ddlRole.DataBind();

                            ddlReportingTo.ClearSelection();
                            ddlReportingTo.SelectedIndex = ddlReportingTo.Items.IndexOf(ddlReportingTo.Items.FindByText(dt.Rows[0]["ReportedTo"].ToString()));
                            ddlReportingTo.DataBind();

                            txtUserId.Text = Request.QueryString["UserId"];
                            btnSave.Visible = false;
                            btnUpdate.Visible = true;
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }
        }

        public void PopulateReportedTo()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name+' - '+SudoName AS Name FROM dbo.Users WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlReportingTo.DataSource = dt;
                    ddlReportingTo.DataTextField = "Name";
                    ddlReportingTo.DataValueField = "Id";
                    ddlReportingTo.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void PopulateRoles()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Roles WHERE IsActive=1 AND IsSuperAdmin=0 AND Id NOT IN (12,13,15)");
                if (dt.Rows.Count > 0)
                {
                    ddlRole.DataSource = dt;
                    ddlRole.DataTextField = "Name";
                    ddlRole.DataValueField = "Id";
                    ddlRole.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Users WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyProducer WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Admin WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.OutsourceOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    return;
                }
                else
                {
                    var cmd = new SqlCommand("INSERT INTO Users(Name, SudoName, FatherName, Username, Email, Password, CNIC, Gender, ContactNo, Address, EmergencyNo, DOB, DOJ, Profile, Department, RoleId, ReportedTo, IsActive, CreatedBy, CreatedDate, LastModifiedBy, UpdatedDate)VALUES(@Name, @SudoName, @FatherName, @Username, @Email, @Password, @CNIC, @Gender, @ContactNo, @Address, @EmergencyNo, @DOB, @DOJ, @Profile, @Department, @RoleId, @ReportedTo, @IsActive, @CreatedBy, @CreatedDate, @LastModifiedBy, @UpdatedDate)", Connection.Db());
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@SudoName", txtSudoName.Text.Trim());
                    cmd.Parameters.AddWithValue("@FatherName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Username", txtUName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    if (!chkAuto.Checked && string.IsNullOrEmpty(txtPassword.Text.Trim()))
                        cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                    if (chkAuto.Checked)
                        cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                    if (!chkAuto.Checked && !string.IsNullOrEmpty(txtPassword.Text.Trim()))
                        cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                    cmd.Parameters.AddWithValue("@CNIC", txtCNIC.Text.Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@EmergencyNo", txtEmergencyContact.Text.Trim());
                    cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());//DateTime.ParseExact(txtDOB.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim());//DateTime.ParseExact(txtDOJ.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Department", txtDepartment.Text.Trim());
                    cmd.Parameters.AddWithValue("@Profile", "https://via.placeholder.com/150");
                    cmd.Parameters.AddWithValue("@RoleId", Convert.ToInt32(ddlRole.SelectedValue));
                    if (!string.IsNullOrEmpty(ddlReportingTo.SelectedValue))
                        cmd.Parameters.AddWithValue("@ReportedTo", Convert.ToInt32(ddlReportingTo.SelectedValue));
                    else
                        cmd.Parameters.AddWithValue("@ReportedTo", 0);
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>User Save Successfully.');", true);
                            Clear();

                            txtUserId.Text = "";
                        }
                        else
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Cannot be Save.');", true);
                    }
                    else
                    {
                        //
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void Clear()
        {
            txtName.Text = "";
            txtFName.Text = "";
            txtUName.Text = "";
            txtEmail.Text = "";
            ddlGender.SelectedIndex = 0;
            txtAddress.Text = "";
            txtCNIC.Text = "";
            txtContactNo.Text = "";
            txtEmergencyContact.Text = "";
            txtDepartment.Text = "";
            txtSudoName.Text = "";
            txtPassword.ReadOnly = false;
            txtPassword.Text = "";
            txtUName.ReadOnly = false;
            txtDOB.Text = "";
            txtDOJ.Text = "";
            txtDescription.Text = "";
            chkAuto.Enabled = true;

            ddlRole.SelectedIndex = 0;
            ddlReportingTo.SelectedIndex = 0;
        }

        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var res = new StringBuilder();
            var rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        protected void ddlRole_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRole.SelectedIndex > 0)
            {
                try
                {
                    var query = "";
                    if (ddlRole.SelectedItem.Text == "Agency Producer")
                    {
                        query = "SELECT Id,Name+' - '+State AS Name FROM dbo.AgencyOwner WHERE IsActive=1";
                        ddlReporting.Visible = true;
                    }

                    if (ddlRole.SelectedItem.Text == "Agency Owner")
                    {
                        ddlReporting.Visible = false;
                    }
                    if (ddlRole.SelectedItem.Text != "Agency Owner" && ddlRole.SelectedItem.Text != "Agency Producer")
                    {
                        query = "SELECT Id,Name FROM dbo.Users WHERE IsActive=1";

                        ddlReporting.Visible = true;
                    }

                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString(query);
                    if (dt.Rows.Count > 0)
                    {
                        ddlReportingTo.DataSource = dt;
                        ddlReportingTo.DataTextField = "Name";
                        ddlReportingTo.DataValueField = "Id";
                        ddlReportingTo.DataBind();

                        //ddlReportingTo.Items.Insert(0, "Select Role");
                    }
                    else
                    {
                        ddlReportingTo.DataSource = "";
                        ddlReportingTo.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogging.SendExcepToDb(ex);
                }
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtUserId.Text.Trim()))
                {
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Users WHERE (Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    }
                    else
                    {
                        var cmd = new SqlCommand("Update Users SET Name=@Name, SudoName=@SudoName, FatherName=@FatherName, Email=@Email, CNIC=@CNIC, Gender=@Gender, ContactNo=@ContactNo, Address=@Address, EmergencyNo=@EmergencyNo, DOB=@DOB, DOJ=@DOJ, Department=@Department, RoleId=@RoleId, ReportedTo=@ReportedTo, LastModifiedBy=@LastModifiedBy, UpdatedDate=@UpdatedDate WHERE Id=@Id",
                            Connection.Db());
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                        cmd.Parameters.AddWithValue("@SudoName", txtSudoName.Text.Trim());
                        cmd.Parameters.AddWithValue("@FatherName", txtFName.Text.Trim());
                        cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                        cmd.Parameters.AddWithValue("@CNIC", txtCNIC.Text.Trim());
                        cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                        cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.Trim());
                        cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                        cmd.Parameters.AddWithValue("@EmergencyNo", txtEmergencyContact.Text.Trim());
                        cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim()); //DateTime.ParseExact(txtDOB.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim()); //DateTime.ParseExact(txtDOJ.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Department", txtDepartment.Text.Trim());
                        cmd.Parameters.AddWithValue("@RoleId", Convert.ToInt32(ddlRole.SelectedValue));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtUserId.Text.Trim()));
                        if (!string.IsNullOrEmpty(ddlReportingTo.SelectedValue))
                            cmd.Parameters.AddWithValue("@ReportedTo", Convert.ToInt32(ddlReportingTo.SelectedValue));
                        else
                            cmd.Parameters.AddWithValue("@ReportedTo", 0);
                        if (Session["UId"] != null)
                        {
                            cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                            cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>User Save Successfully.');", true);
                                Clear();

                                txtUserId.Text = "";

                                btnSave.Visible = true;
                                btnUpdate.Visible = false;
                            }
                            else
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Cannot be Save.');", true);
                        }
                        else
                        {

                        }
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}