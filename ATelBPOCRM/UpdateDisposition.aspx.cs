﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class UpdateDisposition : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {
                    PopulateDdlDisposition();

                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();

                    GridView2.EmptyDataText = "No Record Found!";
                    GridView2.DataBind();
                }
            }
            else
            {
                //
            }
        }

        private void PopulateDdlDisposition()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id !=23");
                if (dt.Rows.Count > 0)
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();

                    ddlDisposition.Items.Insert(0, "Select Disposition");

                    ddlPDisposition.DataSource = dt;
                    ddlPDisposition.DataTextField = "Name";
                    ddlPDisposition.DataValueField = "Id";
                    ddlPDisposition.DataBind();

                    ddlPDisposition.Items.Insert(0, "Select Disposition");
                }
                else
                {
                    ddlDisposition.DataSource = "";
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();

                    ddlPDisposition.DataSource = "";
                    ddlPDisposition.DataTextField = "Name";
                    ddlPDisposition.DataValueField = "Id";
                    ddlPDisposition.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT 'Live' Type, u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,CONVERT(VARCHAR(20),i.CreatedDate,101) AS CreatedDate,i.Address FROM dbo.Insurance i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsActive=1 AND i.PhoneNo='" + txtPhone.Text.Trim() + "'");
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }

                txtPhone.ReadOnly = true;
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnReset_OnClick(object sender, EventArgs e)
        {
            GridView1.EmptyDataText = "No Record Found!";
            GridView1.DataBind();

            txtPhone.Text = "";
            txtPhone.ReadOnly = false;

            ddlDisposition.SelectedIndex = 0;
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                var cmd = new SqlCommand("UPDATE dbo.Insurance SET DispositionId=@DispositionId WHERE PhoneNo=@PhoneNo", Connection.Db());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@DispositionId", Convert.ToInt32(ddlDisposition.SelectedValue));
                cmd.Parameters.AddWithValue("@PhoneNo", txtPhone.Text);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Disposition Updated Successfully');", true);

                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();

                    txtPhone.Text = "";
                    txtPhone.ReadOnly = false;

                    ddlDisposition.SelectedIndex = 0;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Disposition Cannot be Updated');", true);
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnPSearch_OnClick(object sender, EventArgs e)
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT 'Ping N Post' Type, u.Name AS Agent,d.Name AS Disposition,ao.Name AS Agency,(CASE WHEN i.AgencyProducerId=0 THEN 'N/A' ELSE ap.Name END) AS TransferTo,i.Id,(i.FirstName+' '+i.LastName) AS Name,i.City,i.State,i.Zip,i.PhoneNo,i.Company,i.DOB,i.Email,i.IsActive,i.Make,i.[Year],i.Model,(CASE WHEN i.HOwnerShip = 'True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip, CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,CONVERT(VARCHAR(20),i.CreatedDate,101) AS CreatedDate,i.Address FROM dbo.PingNPost i JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId=ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id LEFT JOIN dbo.Users u ON i.CreatedBy=u.Id WHERE i.IsActive=1 AND i.PhoneNo='" + txtPPhone.Text.Trim() + "'");
                if (dt.Rows.Count > 0)
                {
                    GridView2.DataSource = dt;
                    GridView2.DataBind();
                }
                else
                {
                    GridView2.EmptyDataText = "No Record Found!";
                    GridView2.DataBind();
                }

                txtPPhone.ReadOnly = true;
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        protected void btnPReset_OnClick(object sender, EventArgs e)
        {
            GridView2.EmptyDataText = "No Record Found!";
            GridView2.DataBind();

            txtPPhone.Text = "";
            txtPPhone.ReadOnly = false;

            ddlPDisposition.SelectedIndex = 0;
        }

        protected void btnPUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                var cmd = new SqlCommand("UPDATE dbo.PingNPost SET DispositionId=@DispositionId WHERE PhoneNo=@PhoneNo", Connection.Db());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@DispositionId", Convert.ToInt32(ddlPDisposition.SelectedValue));
                cmd.Parameters.AddWithValue("@PhoneNo", txtPPhone.Text);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Disposition Updated Successfully');", true);

                    GridView2.EmptyDataText = "No Record Found!";
                    GridView2.DataBind();

                    txtPPhone.Text = "";
                    txtPPhone.ReadOnly = false;

                    ddlPDisposition.SelectedIndex = 0;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Disposition Cannot be Updated');", true);
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}