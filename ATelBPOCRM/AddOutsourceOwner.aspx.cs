﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;

namespace ATelBPOCRM
{
    public partial class AddOutsourceOwner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ownerId"] != null)
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT Id, Name, FatherName, Username, Email, Password, Gender, ContactNo, Address, Description, CONVERT(NVARCHAR(12),DOJ, 101) AS DOJ,CNIC FROM dbo.OutsourceOwner WHERE Id=" + Convert.ToInt32(Request.QueryString["ownerId"]));
                    if (dt.Rows.Count > 0)
                    {
                        txtName.Text = dt.Rows[0]["Name"].ToString();
                        txtFName.Text = dt.Rows[0]["FatherName"].ToString();
                        txtUName.Text = dt.Rows[0]["Username"].ToString();
                        txtUName.ReadOnly = true;
                        txtEmail.Text = dt.Rows[0]["Email"].ToString();
                        txtPassword.Text = dt.Rows[0]["Password"].ToString();
                        chkAuto.Enabled = false;
                        txtPassword.ReadOnly = true;
                        txtContactNo.Text = dt.Rows[0]["ContactNo"].ToString();
                        txtCNIC.Text = dt.Rows[0]["CNIC"].ToString();
                        txtAddress.Text = dt.Rows[0]["Address"].ToString();
                        txtDescription.Text = dt.Rows[0]["Description"].ToString();
                        txtDOJ.Text = dt.Rows[0]["DOJ"].ToString();

                        ddlGender.ClearSelection();
                        ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                        ddlGender.DataBind();
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //Response.Redirect("Login.aspx");
                }
            }
            else
            {
                //
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["ownerId"] != null)
                {
                    var cmd = new SqlCommand("Update dbo.OutsourceOwner SET Name=@Name, FatherName=@FatherName, Email=@Email, CNIC=@CNIC, Gender=@Gender, ContactNo=@ContactNo, Address=@Address, Description=@Description, DOJ=@DOJ, LastModifiedBy=@LastModifiedBy, UpdatedDate=@UpdatedDate WHERE Id=@Id");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@FatherName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.ToLower().Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@CNIC", txtCNIC.Text.Trim());
                    cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim());
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Request.QueryString["ownerId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Owner Updated Successfully.');", true);
                            chkAuto.Enabled = true;
                            txtUName.ReadOnly = false;
                            txtPassword.ReadOnly = false;
                            Clear();
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Owner Cannot be Update.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Please login and try again.');", true);
                    }
                }
                else
                {
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Users WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Owner Already Exists');", true);
                    }
                    else
                    {
                        if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Users WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                            return;
                        }
                        if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                            return;
                        }
                        if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.AgencyProducer WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                            return;
                        }
                        if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Admin WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                            return;
                        }
                        if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.OutsourceOwner WHERE Username='" + txtUName.Text.Trim() + "'") > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                            return;
                        }
                        else
                        {
                            var cmd = new SqlCommand("INSERT INTO OutsourceOwner(Name, FatherName, Username, Email, Password, CNIC, Gender, ContactNo, Address, Description, DOJ, RoleId, IsActive, CreatedBy, CreatedDate, LastModifiedBy, UpdatedDate)VALUES(@Name, @FatherName, @Username, @Email, @Password, @CNIC, @Gender, @ContactNo, @Address, @Description, @DOJ, @RoleId, @IsActive, @CreatedBy, @CreatedDate, @LastModifiedBy, @UpdatedDate)", Connection.Db());
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                            cmd.Parameters.AddWithValue("@FatherName", txtFName.Text.Trim());
                            cmd.Parameters.AddWithValue("@Username", txtUName.Text.Trim());
                            cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                            if (!chkAuto.Checked && string.IsNullOrEmpty(txtPassword.Text.Trim()))
                                cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                            if (chkAuto.Checked)
                                cmd.Parameters.AddWithValue("@Password", CreatePassword(8));
                            if (!chkAuto.Checked && !string.IsNullOrEmpty(txtPassword.Text.Trim()))
                                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                            cmd.Parameters.AddWithValue("@CNIC", txtCNIC.Text.Trim());
                            cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                            cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.Trim());
                            cmd.Parameters.AddWithValue("@DOJ", txtDOJ.Text.Trim());
                            cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                            cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                            cmd.Parameters.AddWithValue("@RoleId", 15);
                            if (Session["UId"] != null)
                            {
                                cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"].ToString()));
                                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                                cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                                cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                                cmd.Parameters.AddWithValue("@IsActive", 1);
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Owner Save Successfully.');", true);
                                    Clear();
                                    ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                                }
                                else
                                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Owner Cannot be Save.');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Please login and try again.');", true);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void Clear()
        {
            txtName.Text = "";
            txtFName.Text = "";
            txtUName.Text = "";
            txtEmail.Text = "";
            txtPassword.Text = "";
            txtContactNo.Text = "";
            txtAddress.Text = "";
            txtDescription.Text = "";
            txtDOJ.Text = "";

            ddlGender.SelectedIndex = 0;
        }

        private static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var res = new StringBuilder();
            var rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }
}