﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.Security;

namespace ATelBPOCRM
{
    public partial class RolesPermission : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Role"] != null)
                {
                    ltRole.Text = Request.QueryString["Role"].ToString();
                }
                if (Request.QueryString["RId"] != null)
                {
                    PopulateModule(Convert.ToInt32(Request.QueryString["RId"]));
                }
                else
                    Response.Redirect("Roles.aspx");
            }
            else
            {
                //
            }
        }


        public void PopulateModule(int roleId)
        {
            try
            {
                var ds = new Lucky.General.PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT rp.Id,m.Name AS Module,sm.Name AS SubModule,rp.CanView,rp.CanAdd,rp.CanEdit,rp.CanDelete FROM dbo.RolesPermissions rp JOIN dbo.SubModule sm ON rp.SubModuleId= sm.Id JOIN dbo.Module m ON sm.ModuleId = m.Id WHERE rp.RoleId=" + roleId + " ORDER BY m.DisplayOrder ASC");
                //var dt = ds.DataTableSqlString("SELECT rp.Id,m.Name AS Module,sm.Name AS SubModule,rp.CanAdd,rp.CanDelete,rp.CanEdit,rp.CanView FROM dbo.RolesPermissions rp JOIN dbo.Module m ON rp.ModuleId=m.Id LEFT JOIN dbo.SubModule sm ON rp.SubModuleId = sm.Id WHERE rp.IsActive=1 AND rp.RoleId=" + roleId);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = dt;
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            var flag = false;
            try
            {
                foreach (GridViewRow row in GridView1.Rows)
                {
                    var id = row.FindControl("hfId") as HiddenField;

                    var view = row.FindControl("chkView") as CheckBox;
                    var add = row.FindControl("chkAdd") as CheckBox;
                    var edit = row.FindControl("chkEdit") as CheckBox;
                    var delete = row.FindControl("chkDelete") as CheckBox;

                    var r = new Lucky.Security.RolesPermissions();
                    if (id != null) r.Id = Convert.ToInt32(id.Value);
                    r.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    if (Request.QueryString["RId"] != null)
                        r.RoleId = Convert.ToInt32(Request.QueryString["RId"]);
                    if (view != null && view.Checked == true)
                        r.CanView = true;
                    else
                        r.CanView = false;
                    if (add != null && add.Checked == true)
                        r.CanAdd = true;
                    else
                        r.CanAdd = false;
                    if (edit != null && edit.Checked == true)
                        r.CanEdit = true;
                    else
                        r.CanEdit = false;
                    if (delete != null && delete.Checked == true)
                        r.CanDelete = true;
                    else
                        r.CanDelete = false;
                    flag = r.UpdateRolePermission(r) > 0;
                }
            }
            catch (Exception ex)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(ex);
            }
            if (flag == true)
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Permissions Granted Successfully.');", true);
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Permissions Granted Failed.');", true);
        }
    }
}