﻿using Lucky.General;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ATelBPOCRM
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //var d= System.DateTime.Now.ToShortDateString();
            var m = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            if (!IsPostBack)
            {

            }
        }

        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            var isValid = false;
            {
                var cmd = new SqlCommand("SELECT Id,Name,Username,Email,Role,Profile,Position FROM dbo.Admin WHERE IsActive=1 AND (Username=@Username OR Email=@Email) AND Password=@Password")
                {
                    Connection = Connection.Db()
                };
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Username", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                var da = new SqlDataAdapter(cmd);
                var dataTable = new DataTable();
                da.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    Session.Clear();

                    Session["UId"] = dataTable.Rows[0]["Id"].ToString();
                    Session["Name"] = dataTable.Rows[0]["Name"].ToString();
                    Session["UN"] = dataTable.Rows[0]["Username"].ToString();
                    Session["Email"] = dataTable.Rows[0]["Email"].ToString();
                    Session["Profile"] = dataTable.Rows[0]["Profile"].ToString();
                    Session["Role"] = dataTable.Rows[0]["Role"].ToString();
                    Session["RoleId"] = 1;
                    Session["Position"] = dataTable.Rows[0]["Position"].ToString();
                    isValid = true;

                    var returnUrl = Convert.ToString(Request.QueryString["url"]);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        Response.Redirect(returnUrl);
                    }
                    else
                        Response.Redirect("Dashboard.aspx");
                }
            }
            if (isValid == false)
            {
                var u = new Lucky.Security.Users
                {
                    Username = txtEmail.Text.Trim(),
                    Email = txtEmail.Text.Trim(),
                    Password = txtPassword.Text.Trim()
                };
                var dt = u.GetLogin(u);
                if (dt.Rows.Count > 0)
                {
                    Session.Clear();

                    Session["UId"] = dt.Rows[0]["Id"].ToString();
                    Session["Name"] = dt.Rows[0]["Name"].ToString();
                    Session["UN"] = dt.Rows[0]["Username"].ToString();
                    Session["Email"] = dt.Rows[0]["Email"].ToString();
                    Session["ContactNo"] = dt.Rows[0]["ContactNo"].ToString();
                    Session["Profile"] = dt.Rows[0]["Profile"].ToString();
                    Session["RoleId"] = dt.Rows[0]["RoleId"].ToString();
                    Session["Role"] = dt.Rows[0]["Role"].ToString();
                    Session["CompanyId"] = dt.Rows[0]["CompanyId"].ToString();
                    isValid = true;

                    var returnUrl = Convert.ToString(Request.QueryString["url"]);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        Response.Redirect(returnUrl);
                    }
                    else
                        Response.Redirect("Dashboard.aspx");
                }
                else
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Invalid Username / Email or Pass.');", true);
                }
            }

            if (isValid == false)
            {
                var cmd = new SqlCommand(
                    "SELECT ao.Id,ao.Name,ao.Username,ao.Email,r.Id AS RoleId,r.Name AS Role,ao.Token FROM dbo.AgencyOwner ao JOIN dbo.Roles r ON ao.RoleId = r.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id WHERE am.Username <>'Saqib' AND ao.IsActive=1 AND (ao.Username=@Username OR ao.Email=@Email) AND ao.Password=@Password")
                {
                    Connection = Connection.Db()
                };
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Username", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                var da = new SqlDataAdapter(cmd);
                var dataTable = new DataTable();
                da.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    Session.Clear();

                    Session["UId"] = dataTable.Rows[0]["Id"].ToString();
                    Session["Name"] = dataTable.Rows[0]["Name"].ToString();
                    Session["UN"] = dataTable.Rows[0]["Username"].ToString();
                    Session["Email"] = dataTable.Rows[0]["Email"].ToString();
                    Session["RoleId"] = dataTable.Rows[0]["RoleId"].ToString();
                    Session["Role"] = dataTable.Rows[0]["Role"].ToString();
                    Session["Token"] = dataTable.Rows[0]["Token"].ToString();
                    isValid = true;

                    if (dataTable.Rows[0]["Role"].ToString() == "Account Head")
                        Response.Redirect("AccountHeadLivePNP.aspx");

                    if (dataTable.Rows[0]["Role"].ToString() == "Account Manager")
                        Response.Redirect("AccountManagerLivePNP.aspx");

                    if (dataTable.Rows[0]["Role"].ToString() == "Agency Owner")
                        Response.Redirect("LivePNP.aspx");

                    //var returnUrl = Convert.ToString(Request.QueryString["url"]);
                    //if (!string.IsNullOrEmpty(returnUrl))
                    //{
                    //    Response.Redirect(returnUrl);
                    //}
                    //else
                    //Response.Redirect("LivePNP.aspx");
                    //Response.Redirect("Dashboard.aspx");
                }
            }

            if (isValid == false)
            {
                var cmd = new SqlCommand(
                    "SELECT ap.Id,ap.Name,ap.Username,ap.Email,r.Id AS RoleId,r.Name AS Role,ao.Name AS Agency,ap.Token FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId=ao.Id JOIN dbo.Roles r ON ap.RoleId = r.Id JOIN dbo.AgencyOwner am ON ao.ReportedTo=am.Id WHERE am.Username <>'Saqib' AND ap.IsActive=1 AND (ap.Username=@Username OR ap.Email=@Email) AND ap.Password=@Password AND ao.IsActive=1")
                {
                    Connection = Connection.Db()
                };
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Username", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                var da = new SqlDataAdapter(cmd);
                var dataTable = new DataTable();
                da.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    Session.Clear();

                    Session["UId"] = dataTable.Rows[0]["Id"].ToString();
                    Session["Name"] = dataTable.Rows[0]["Name"].ToString();
                    Session["UN"] = dataTable.Rows[0]["Username"].ToString();
                    Session["Email"] = dataTable.Rows[0]["Email"].ToString();
                    Session["Role"] = dataTable.Rows[0]["Role"].ToString();
                    Session["RoleId"] = dataTable.Rows[0]["RoleId"].ToString();
                    Session["Agency"] = dataTable.Rows[0]["Agency"].ToString();
                    Session["Token"] = dataTable.Rows[0]["Token"].ToString();
                    isValid = true;

                    //var returnUrl = Convert.ToString(Request.QueryString["url"]);
                    //if (!string.IsNullOrEmpty(returnUrl))
                    //{
                    //    Response.Redirect(returnUrl);
                    //}
                    //else
                    Response.Redirect("ProducerLivePNP.aspx");
                    // Response.Redirect("Dashboard.aspx");
                }
            }
            if (isValid == false)
            {
                var cmd = new SqlCommand("SELECT oo.Id,oo.Name,oo.Username,oo.Email,r.Id AS RoleId, r.Name AS Role FROM dbo.OutsourceOwner oo JOIN dbo.Roles r ON oo.RoleId = r.Id WHERE oo.IsActive=1 AND r.IsActive=1 AND (oo.Username=@Username OR oo.Email=@Email) AND oo.Password=@Password")
                {
                    Connection = Connection.Db()
                };
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Username", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                var da = new SqlDataAdapter(cmd);
                var dataTable = new DataTable();
                da.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    Session.Clear();

                    Session["UId"] = dataTable.Rows[0]["Id"].ToString();
                    Session["Name"] = dataTable.Rows[0]["Name"].ToString();
                    Session["UN"] = dataTable.Rows[0]["Username"].ToString();
                    Session["Email"] = dataTable.Rows[0]["Email"].ToString();
                    Session["Role"] = dataTable.Rows[0]["Role"].ToString();
                    Session["RoleId"] = dataTable.Rows[0]["RoleId"].ToString();
                    Session["OutsourceOwnerId"] = dataTable.Rows[0]["Id"].ToString();
                    isValid = true;

                    var returnUrl = Convert.ToString(Request.QueryString["url"]);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        Response.Redirect(returnUrl);
                    }
                    else
                        Response.Redirect("Dashboard.aspx");
                }
            }
            if (isValid == false)
            {
                var cmd = new SqlCommand("SELECT op.Id,op.Name,op.Username,op.Email,r.Id AS RoleId,r.Name AS Role,oo.Id AS OwnerId FROM dbo.OutsourceProducer op JOIN dbo.OutsourceOwner oo ON op.OutsourceOwnerId=oo.Id JOIN dbo.Roles r ON op.RoleId = r.Id WHERE op.IsActive=1 AND r.IsActive=1 AND oo.IsActive=1 AND (op.Username=@Username OR op.Email=@Email) AND op.Password=@Password")
                {
                    Connection = Connection.Db()
                };
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Username", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                var da = new SqlDataAdapter(cmd);
                var dataTable = new DataTable();
                da.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    Session.Clear();

                    Session["UId"] = dataTable.Rows[0]["Id"].ToString();
                    Session["Name"] = dataTable.Rows[0]["Name"].ToString();
                    Session["UN"] = dataTable.Rows[0]["Username"].ToString();
                    Session["Email"] = dataTable.Rows[0]["Email"].ToString();
                    Session["Role"] = dataTable.Rows[0]["Role"].ToString();
                    Session["RoleId"] = dataTable.Rows[0]["RoleId"].ToString();
                    Session["OutsourceOwnerId"] = dataTable.Rows[0]["OwnerId"].ToString();
                    isValid = true;

                    var returnUrl = Convert.ToString(Request.QueryString["url"]);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        Response.Redirect(returnUrl);
                    }
                    else
                        Response.Redirect("Dashboard.aspx");
                }
            }
            if (isValid == false)
                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Invalid Username / Email or Pass');", true);
        }
    }
}