﻿<%@ Page Title="Exceptions" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="DbExceptions.aspx.cs" Inherits="ATelBPOCRM.DbExceptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Exception Log</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>System Settings</a></li>
            <li class="active">Exception Log</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date Range</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFrom" CssClass="form-control datepicker" placeholder="From Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtTo" CssClass="form-control datepicker" placeholder="To Date" runat="server" data-inputmask="'alias': 'mm/dd/yyyy'" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 23px;">
                                <asp:Button ID="btnException" runat="server" Text="Get Exceptions" CssClass="btn btn-flat btn-primary btn-sm btn-block" OnClick="btnException_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Exception List</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped dataTable display" AutoGenerateColumns="False" runat="server">
                            <Columns>
                                <asp:TemplateField HeaderText="Lead Id">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Exception Message">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltExceptionMsg" runat="server" Text='<%# Eval("ExceptionMsg") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Exception Type">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltExceptionType" runat="server" Text='<%# Eval("ExceptionType") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Exception URL">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltExceptionURL" runat="server" Text='<%# Eval("ExceptionURL") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Exception Time">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltExceptionTime" runat="server" Text='<%# Eval("ExceptionTime") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true
        });
    </script>
</asp:Content>
