﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;


namespace ATelBPOCRM
{
    public partial class AddApi : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateAgeny();

                if (Request.QueryString["ApiId"] != null)
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT ca.Product,ca.Id,ca.Client,ca.AuthType,ca.Resource,ca.Url,ca.EndPoint,ca.Username,ca.Password,ca.BearerToken,ca.Description,ca.RequestType,ca.ResponseType,ca.Verbose,ao.Name AS AgencyOwner, ao.Username AS AgencyUsername,ca.IsImplemented,ca.IsActive,CONVERT(VARCHAR(20), ao.CreatedDate,101) AS Date FROM ClientApi ca JOIN AgencyOwner ao ON ca.AgencyId=ao.Id WHERE ca.Id=" + Convert.ToInt32(Request.QueryString["ApiId"]));
                    if (dt.Rows.Count > 0)
                    {
                        txtName.Text = dt.Rows[0]["Client"].ToString();
                        txtResource.Text = dt.Rows[0]["Resource"].ToString();
                        txtUrl.Text = dt.Rows[0]["Url"].ToString();
                        txtEndPoint.Text = dt.Rows[0]["EndPoint"].ToString();
                        txtUName.Text = dt.Rows[0]["Username"].ToString();
                        txtPassword.Text = dt.Rows[0]["Password"].ToString();
                        txtBearerToken.Text = dt.Rows[0]["BearerToken"].ToString();
                        txtDescription.Text = dt.Rows[0]["Description"].ToString();

                        ddlAgencyOwner.ClearSelection();
                        ddlAgencyOwner.SelectedIndex = ddlAgencyOwner.Items.IndexOf(ddlAgencyOwner.Items.FindByText(dt.Rows[0]["AgencyOwner"].ToString()));
                        ddlAgencyOwner.DataBind();

                        ddlAuthType.ClearSelection();
                        ddlAuthType.SelectedIndex = ddlAuthType.Items.IndexOf(ddlAuthType.Items.FindByText(dt.Rows[0]["AuthType"].ToString()));
                        ddlAuthType.DataBind();

                        ddlRequest.ClearSelection();
                        ddlRequest.SelectedIndex = ddlRequest.Items.IndexOf(ddlRequest.Items.FindByText(dt.Rows[0]["RequestType"].ToString()));
                        ddlRequest.DataBind();

                        ddlResponse.ClearSelection();
                        ddlResponse.SelectedIndex = ddlResponse.Items.IndexOf(ddlResponse.Items.FindByText(dt.Rows[0]["ResponseType"].ToString()));
                        ddlResponse.DataBind();

                        ddlVerbose.ClearSelection();
                        ddlVerbose.SelectedIndex = ddlVerbose.Items.IndexOf(ddlVerbose.Items.FindByText(dt.Rows[0]["Verbose"].ToString()));
                        ddlVerbose.DataBind();

                        ddlProduct.ClearSelection();
                        ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByText(dt.Rows[0]["Product"].ToString()));
                        ddlProduct.DataBind();

                        txtApiId.Text = Request.QueryString["ApiId"];
                        btnSave.Visible = false;
                        btnUpdate.Visible = true;
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }
        }

        public void PopulateAgeny()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT ao.Id,ao.Name FROM dbo.AgencyOwner ao WHERE ao.IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlAgencyOwner.DataSource = dt;
                    ddlAgencyOwner.DataTextField = "Name";
                    ddlAgencyOwner.DataValueField = "Id";
                    ddlAgencyOwner.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.ClientApi WHERE Url='" + txtUrl.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Api Already Exists');", true);
                    return;
                }
                else
                {
                    var cmd = new SqlCommand("INSERT INTO ClientApi(Client, AuthType, Resource, Url, EndPoint, Username, Password, BearerToken, Product, Description, RequestType, ResponseType, Verbose, AgencyId, IsActive, CreatedBy, CreatedDate)VALUES(@Client, @AuthType, @Resource, @Url, @EndPoint, @Username, @Password, @BearerToken, @Product, @Description, @RequestType, @ResponseType, @Verbose, @AgencyId, @IsActive, @CreatedBy, @CreatedDate)", Connection.Db());
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@Client", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@AuthType", ddlAuthType.SelectedValue);
                    cmd.Parameters.AddWithValue("@Resource", txtResource.Text.Trim());
                    cmd.Parameters.AddWithValue("@Url", txtUrl.Text.Trim());
                    cmd.Parameters.AddWithValue("@EndPoint", txtEndPoint.Text.Trim());
                    cmd.Parameters.AddWithValue("@Username", txtUName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                    cmd.Parameters.AddWithValue("@BearerToken", txtBearerToken.Text.Trim());
                    cmd.Parameters.AddWithValue("@Product", ddlProduct.SelectedValue);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@RequestType", ddlRequest.SelectedValue);
                    cmd.Parameters.AddWithValue("@ResponseType", ddlResponse.SelectedValue);
                    cmd.Parameters.AddWithValue("@Verbose", ddlVerbose.SelectedValue);
                    cmd.Parameters.AddWithValue("@AgencyId", Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@IsActive", 1);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Api Save Successfully.');", true);
                            Clear();

                            txtApiId.Text = "";
                        }
                        else
                            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Api Cannot be Save.');", true);
                    }
                    else
                    {
                        //
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void Clear()
        {
            txtName.Text = "";
            txtResource.Text = "";
            txtUrl.Text = "";
            txtEndPoint.Text = "";
            txtUName.Text = "";
            txtPassword.Text = "";
            txtDescription.Text = "";
            txtBearerToken.Text = "";

            ddlAgencyOwner.SelectedIndex = 0;
            ddlAuthType.SelectedIndex = 0;
            ddlRequest.SelectedIndex = 0;
            ddlResponse.SelectedIndex = 0;
            ddlVerbose.SelectedIndex = 0;
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtApiId.Text.Trim()))
                {
                    if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.ClientApi WHERE (Url='" + txtUrl.Text.Trim() + "'") > 0)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>User Already Exists');", true);
                    }
                    else
                    {
                        var cmd = new SqlCommand("Update ClientApi SET Client=@Client, AuthType=@AuthType, Resource=@Resource, Url=@Url, EndPoint=@EndPoint, Username=@Username, Password=@Password, BearerToken=@BearerToken, Product=@Product, Description=@Description, RequestType=@RequestType, ResponseType=@ResponseType, Verbose=@Verbose, AgencyId=@AgencyId, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id",
                            Connection.Db());
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Client", txtName.Text.Trim());
                        cmd.Parameters.AddWithValue("@AuthType", ddlAuthType.SelectedValue);
                        cmd.Parameters.AddWithValue("@Resource", txtResource.Text.Trim());
                        cmd.Parameters.AddWithValue("@Url", txtUrl.Text.Trim());
                        cmd.Parameters.AddWithValue("@EndPoint", txtEndPoint.Text.Trim());
                        cmd.Parameters.AddWithValue("@Username", txtUName.Text.Trim());
                        cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                        cmd.Parameters.AddWithValue("@BearerToken", txtBearerToken.Text.Trim());
                        cmd.Parameters.AddWithValue("@Product", ddlProduct.SelectedValue);
                        cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                        cmd.Parameters.AddWithValue("@RequestType", ddlRequest.SelectedValue);
                        cmd.Parameters.AddWithValue("@ResponseType", ddlResponse.SelectedValue);
                        cmd.Parameters.AddWithValue("@Verbose", ddlVerbose.SelectedValue);
                        cmd.Parameters.AddWithValue("@AgencyId", Convert.ToInt32(ddlAgencyOwner.SelectedValue));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtApiId.Text));
                        if (Session["UId"] != null)
                        {
                            cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Api Save Successfully.');", true);
                                Clear();

                                txtApiId.Text = "";

                                btnSave.Visible = true;
                                btnUpdate.Visible = false;
                            }
                            else
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Api Cannot be Save.');", true);
                        }
                        else
                        {

                        }
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}