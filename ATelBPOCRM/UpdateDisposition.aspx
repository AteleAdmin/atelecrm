﻿<%@ Page Title="Update Lead Disposition" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="UpdateDisposition.aspx.cs" Inherits="ATelBPOCRM.UpdateDisposition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Update Lead Disposition</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Leads</a></li>
            <li class="active">Update Lead</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Live Transfer</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <asp:TextBox ID="txtPhone" TextMode="Number" MaxLength="10" CssClass="form-control" runat="server" placeholder="Phone Number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top: 22px;">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary btn-flat" OnClick="btnSearch_OnClick" />
                                |
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-warning btn-flat" OnClick="btnReset_OnClick" />
                            </div>
                        </div>

                        <div class="col-md-12">
                            <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped dataTable" runat="server" AutoGenerateColumns="False" EmptyDataText="No Record Found!">
                                <Columns>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltType" runat="server" Text='<%# Eval("Type") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Zip">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone No">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPhoneNo" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Disposition">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltDisposition" runat="server" Text='<%# Eval("Disposition") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agent">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAgent" runat="server" Text='<%# Eval("Agent") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agency">
                                        <ItemTemplate>
                                            <asp:Literal ID="Agency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCreatedDate" runat="server" Text='<%# Eval("CreatedDate") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select Disposition</label>
                                <asp:DropDownList ID="ddlDisposition" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" style="margin-top: 22px;">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-success btn-flat" OnClick="btnUpdate_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ping N' Post</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <asp:TextBox ID="txtPPhone" TextMode="Number" MaxLength="10" CssClass="form-control" runat="server" placeholder="Phone Number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" style="margin-top: 22px;">
                                <asp:Button ID="btnPSearch" runat="server" Text="Search" CssClass="btn btn-primary btn-flat" OnClick="btnPSearch_OnClick" />
                                |
                                <asp:Button ID="btnPReset" runat="server" Text="Reset" CssClass="btn btn-warning btn-flat" OnClick="btnPReset_OnClick" />
                            </div>
                        </div>

                        <div class="col-md-12">
                            <asp:GridView ID="GridView2" CssClass="table table-bordered table-striped dataTable" runat="server" AutoGenerateColumns="False" EmptyDataText="No Record Found!">
                                <Columns>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltType" runat="server" Text='<%# Eval("Type") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCity" runat="server" Text='<%# Eval("City") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltState" runat="server" Text='<%# Eval("State") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Zip">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltZip" runat="server" Text='<%# Eval("Zip") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone No">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPhoneNo" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Disposition">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltDisposition" runat="server" Text='<%# Eval("Disposition") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agent">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAgent" runat="server" Text='<%# Eval("Agent") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agency">
                                        <ItemTemplate>
                                            <asp:Literal ID="Agency" runat="server" Text='<%# Eval("Agency") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCreatedDate" runat="server" Text='<%# Eval("CreatedDate") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select Disposition</label>
                                <asp:DropDownList ID="ddlPDisposition" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" style="margin-top: 22px;">
                                <asp:Button ID="btnPUpdate" runat="server" Text="Update" CssClass="btn btn-success btn-flat" OnClick="btnPUpdate_OnClick" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
