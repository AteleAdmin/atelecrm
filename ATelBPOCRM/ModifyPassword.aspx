﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ModifyPassword.aspx.cs" Inherits="ATelBPOCRM.ModifyPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Change Password</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>Account Settings</a></li>
            <li class="active">Change Password</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Account</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label>Old Password</label> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtOldPassword" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtOldPassword" TextMode="Password" placeholder="Old Password" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>New Password</label> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtPassword" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtPassword" TextMode="Password" placeholder="New Password" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <label>Confirm Password</label> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtCPassword" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtCPassword" TextMode="Password" placeholder="Confirm Password" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <hr/>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCPassword" ControlToCompare="txtPassword" ForeColor="White" ErrorMessage="Password doesn't match."></asp:CompareValidator>
                        
                        <div class="form-group">
                            <asp:Button ID="btnSubmit" CssClass="btn btn-success" runat="server" Text="Change Password" OnClick="btnSubmit_OnClick" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>