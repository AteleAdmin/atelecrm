﻿<%@ Page Title="Client Lead Api List" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ClientLeadApiList.aspx.cs" Inherits="ATelBPOCRM.ClientLeadApiList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Api List</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a href="AddApi.aspx" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i>&nbsp;Add New Api</a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <%-- <div class="col-md-4">
                            <asp:DropDownList ID="ddlAgency" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>--%>
                        <div class="table-responsive">
                            <asp:GridView ID="gvAllLeads" CssClass="table table-bordered table-striped dataTable display" AutoGenerateColumns="False" EmptyDataText="No Record Found!" runat="server" OnRowDataBound="gvAllLeads_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Serial.#">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                            <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Client Name">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Client") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agency Name">
                                        <ItemTemplate>
                                            <a class="api-detail" data-request="<%# Eval("AgencyId") %>" href="javascript:;">
                                                <asp:Literal ID="ltAgency" runat="server" Text='<%# Eval("AgencyOwner") %>'></asp:Literal>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agency Username">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAgencyUsername" runat="server" Text='<%# Eval("AgencyUsername") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Username">
                                        <ItemTemplate>
                                            <asp:Literal ID="txtUsername" runat="server" Text='<%# Eval("Username") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Password">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPassword" runat="server" Text='<%# Eval("Password") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Auth Type">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltAuthType" runat="server" Text='<%# Eval("AuthType") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltProduct" runat="server" Text='<%# Eval("Product") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Implemented">
                                        <ItemTemplate>
                                            <asp:Label ID="ltIsImplemented" runat="server" Text='<%# Eval("IsImplemented") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active">
                                        <ItemTemplate>
                                            <asp:Label ID="ltActive" runat="server" Text='<%# Eval("IsActive") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEdit" runat="server" OnClick="btnEdit_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-pencil btn-sm label-success"></i></asp:LinkButton>
                                            |
                                            <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_OnClick" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-toggle-on btn-sm label-info"></i></asp:LinkButton>
                                            |
                                            <asp:LinkButton ID="btnImplement" runat="server" OnClick="btnImplement_Click" RowIndex='<%# Container.DisplayIndex %>'><i class="fa fa-check btn-sm label-success"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $('.api-detail').on('click', function () {
                var agency = $(this).data('request');
                if (agency !== '') {
                    $.fancybox.open({
                        src: 'ApiDetail.aspx?agencyId=' + agency,
                        type: 'iframe'
                    });
                }
            });
        })
    </script>

    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 100%;
            margin: 0;
        }
    </style>
</asp:Content>
