﻿using System;
using System.IO;
using System.Web.UI.WebControls;
using Lucky.General;
using Lucky.Security;
using System.Web.UI;
namespace ATelBPOCRM
{
    public partial class WarrantyLeads : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateLeads("", "");
            }
            else
            {
                //
            }
        }

        public void PopulateLeads(string from, string to)
        {
            try
            {
                var query = "";
                if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT Id,Title,FirstName+' '+LastName AS Name,Phone,Address1,Address2,Address3,PostalCode,State,Province,City,Comments,(CASE WHEN IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive FROM dbo.AutoWarranty WHERE IsActive =1 AND CONVERT(VARCHAR(12),CreatedDate,101) >= '" + from + "' AND CONVERT(VARCHAR(12),CreatedDate,101) <= '" + to + "' ORDER BY Id ASC";
                }
                if (string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT Id,Title,FirstName+' '+LastName AS Name,Phone,Address1,Address2,Address3,PostalCode,State,Province,City,Comments,(CASE WHEN IsActive = 'True' THEN 'Yes' ELSE 'No' END) AS IsActive FROM dbo.AutoWarranty WHERE IsActive =1 AND CONVERT(VARCHAR(12),CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY Id ASC";
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                    GridView1.UseAccessibleHeader = true;
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    GridView1.DataSource = dt;
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }

            }
            catch (Exception ex)
            {
                Lucky.General.ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var active = e.Row.FindControl("ltActive") as Label;
                if (active != null && active.Text == "Yes")
                    active.CssClass = "label label-success";
                else if (active != null) active.CssClass = "label label-danger";
            }
            else
            {
                //
            }
        }

        protected void btnLead_OnClick(object sender, EventArgs e)
        {
            PopulateLeads(txtFrom.Text.Trim(), txtTo.Text.Trim());
        }
    }
}