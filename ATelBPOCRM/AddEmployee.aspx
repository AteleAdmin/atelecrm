﻿<%@ Page Title="Add Employee" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AddEmployee.aspx.cs" Inherits="ATelBPOCRM.AddEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="backend/dropify/dist/css/dropify.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Employee Form</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:;"><i class="fa fa-gears"></i>System Settings</a></li>
            <li class="active">Add Employee</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Employee</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>--%>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group" style="margin-bottom: 0px !important">
                                    <asp:TextBox ID="txtEmployeeId" CssClass="form-control" TextMode="Number" Visible="False" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Select Role</label>
                                    <asp:DropDownList ID="ddlRole" CssClass="form-control select2 ddlRole" Style="width: 100%;" runat="server"></asp:DropDownList>
                                    <!-- AutoPostBack="True" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" -->
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Name</label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" Text="*" ControlToValidate="txtName" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtName" CssClass="form-control" placeholder="Name" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <asp:DropDownList ID="ddlGender" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                        <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>CNIC</label>
                                <div class="form-group">
                                    <asp:TextBox ID="txtCNIC" CssClass="form-control cnic" placeholder="CNIC" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Date-of-Birth</label>
                                    <asp:TextBox ID="txtDOB" CssClass="form-control datepicker" placeholder="DOB" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Date-of-Join</label>
                                    <asp:TextBox ID="txtDOJ" CssClass="form-control datepicker" placeholder="DOJ" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Guardian Name</label>
                                    <asp:TextBox ID="txtGuardianName" CssClass="form-control" placeholder="Guardian Name" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Relation With Guardian</label>
                                    <asp:TextBox ID="txtGRelation" CssClass="form-control" placeholder="Relation With Guardian" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Reference Offer Letter</label>
                                    <asp:TextBox ID="txtRefOfferLetter" CssClass="form-control" placeholder="Reference Offer Letter" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Username</label>&nbsp;<asp:RequiredFieldValidator ForeColor="Red" Text="*" ControlToValidate="txtUName" ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtUName" CssClass="form-control" placeholder="Username" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Email</label>
                                    <asp:TextBox ID="txtEmail" CssClass="form-control" placeholder="Email" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Password &nbsp;<span><asp:CheckBox ID="chkAuto" runat="server" CssClass="" />&nbsp;Auo Generate</span></label>
                                    <asp:TextBox ID="txtPassword" CssClass="form-control password" placeholder="Password" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <!-- // row -->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Mobile No</label>
                                    <asp:TextBox ID="txtMobileNo" CssClass="form-control mobile" placeholder="Mobile No" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Alt. Phone No</label>
                                <div class="form-group">
                                    <asp:TextBox ID="txtAltPhoneNo" CssClass="form-control" placeholder="Alt. Phone No" runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label>Contact Person No</label>
                                    <asp:TextBox ID="txtContactPNo" CssClass="form-control mobile" placeholder="Contact Person No" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Relation With Contact Person</label>
                                <div class="form-group">
                                    <asp:TextBox ID="txtRelationWContactPerson" CssClass="form-control" placeholder="Relation With Contact Person" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Current Address</label>
                                    <asp:TextBox ID="txtCurrentAddress" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="Current Address" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Permanent Address</label>
                                    <asp:TextBox ID="txtPermanentAddress" TextMode="MultiLine" Rows="2" CssClass="form-control" placeholder="Permanent Address" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Salary Offered</label>
                                    <asp:TextBox ID="txtSalaryOffered" CssClass="form-control" TextMode="Number" placeholder="Salary Offered" runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label>Marital Status</label>
                                     <asp:DropDownList ID="ddlMaritalSatus" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Single" Value="Single"></asp:ListItem>
                                        <asp:ListItem Text="Married" Value="Married"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Attendance Id</label>
                                    <asp:TextBox ID="txtAttendanceId" CssClass="form-control" TextMode="Number" placeholder="Attendance Id" runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label>Position</label>
                                    <asp:TextBox ID="txtPosition" CssClass="form-control" placeholder="Position" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label>Project</label>
                                    <asp:DropDownList ID="ddlProject" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Auto Insurance" Value="Auto Insurance"></asp:ListItem>
                                        <asp:ListItem Text="Transcription" Value="Transcription"></asp:ListItem>
                                        <asp:ListItem Text="Miscellaneous" Value="Miscellaneous"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Profile</label>
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="dropify" data-height="300" data-min-width="100" data-max-file-size="2M" data-show-loader="false" data-show-errors="true" data-errors-position="outside" data-allowed-file-extensions="png jpg jpeg gif" data-max-file-size-preview="2M" />
                            </div>
                             <asp:Panel ID="pnlThumbnail" runat="server" Visible="false">
                                <div class="col-md-4" style="margin-top: 21px;">
                                    <asp:Image ID="Image1" Height="200" runat="server" CssClass="img img-responsive img-thumbnail" />
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnSave" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat" OnClick="btnSave_OnClick" />
                            <asp:Button ID="btnUpdate" runat="server" Text="Save Changes" CssClass="btn btn-primary btn-flat" OnClick="btnUpdate_OnClick" Visible="False" />
                            <a href="Employees.aspx" class="btn btn-info btn-flat"><< Go Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- // section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="backend/dropify/dist/js/dropify.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Drag and drop a file here or click',
                'replace': 'Drag and drop or click to replace',
                'remove': 'Remove',
                'error': 'Ooops, something wrong happended.'
            }
        });
    </script>

    <script type="text/javascript">
        var dateToday = new Date();
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight: true,
            todayBtn: true,
            todayBtn: 'linked',
            clearBtn: true,
            orientation: 'bottom auto'
        });
    </script>

    <script src="backend/plugins/ckeditor/ckeditor.js"></script>
    <%-- <script>
        $(function () {
            CKEDITOR.replace('<%= txtDescription.ClientID %>');
        })
    </script>--%>

    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
        })
    </script>

    <script type="text/javascript">
        $(function () {
            $('.ddlRole').on('change', function () {
                debugger;
                var val = $(".ddlRole option:selected").text();
                if (val === 'CEO' || val === 'Agency Owner') {
                    $('.display').css('display', 'none');
                    $('.exp').addClass('col-md-4');
                } else {
                    $('.display').css('display', 'block');
                    $('.exp').addClass('col-md-2');
                }
                return false;
            });
        });
    </script>
    <!----------------------------------------------------------->
    <script type="text/javascript">
        $(function () {
            document.getElementById('<%= chkAuto.ClientID %>').onclick = function () {
                if (this.checked) {
                    document.querySelector('.password').disabled = "disabled";
                } else {
                    document.querySelector('.password').disabled = "";
                }
            };
        });
    </script>
    <script src="backend/inputMask/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.cnic').mask("99999-9999999-9", { placeholder: "_____-_______-_" });
            $('.mobile').mask("9999-9999999", { placeholder: "____-________" });
        })
    </script>
</asp:Content>
