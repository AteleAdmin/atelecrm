﻿using Lucky.General;
using System;
using System.Data.SqlClient;
using System.Web.UI;

namespace ATelBPOCRM
{
    public partial class UpdatePingHomeLead : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UId"] != null)
                {
                    PopulateDdlDisposition();
                    PopulateProducers();

                    if (Request.QueryString["msg"] != null && Request.QueryString["phone"] != null)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Saved Successfully');", true);
                        txtPhone.Text = Request.QueryString["phone"].ToString();
                        txtPhone.ReadOnly = true;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;

                        //ddlAgencyProducer.Enabled = false;
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        pnlDisposition.Visible = true;
                        pnlGetLead.Visible = false;
                    }

                    if (Request.QueryString["LeadId"] != null)
                    {
                        var ds = new PopulateDataSource();
                        var dt = ds.DataTableSqlString("SELECT (CASE WHEN i.IsFlooded=0 THEN 'No' ELSE 'Yes' END) AS IsFlooded,(CASE WHEN i.IsOpenClaim=0 THEN 'No' ELSE 'Yes' END) AS IsOpenClaim,i.Id,i.FirstName,i.LastName,i.ViciLeadId,i.Gender,i.City,UPPER(i.State) AS State,i.Zip,i.PhoneNo,i.Address,i.Comments,i.DOB,i.Company,(CASE WHEN i.HOwnerShip='True' THEN 'Yes' ELSE 'No' END) AS HOwnerShip,i.Make,i.Year,i.Model,i.Make2,i.Year2,i.Model2,i.Email,i.ContinousCoverage,(CASE WHEN i.MaritalStatus='True' THEN 'Yes' ELSE 'No' END) AS MaritalStatus,ao.Name AS AgencyOwner,ap.Name AS AgencyProducer,d.Name AS Disposition FROM dbo.PingNPost i LEFT JOIN dbo.AgencyOwner ao ON i.AgencyOwnerId=ao.Id LEFT JOIN dbo.AgencyProducer ap ON i.AgencyProducerId = ap.Id JOIN dbo.Disposition d ON i.DispositionId=d.Id WHERE i.Id=" + Convert.ToInt32(Request.QueryString["LeadId"]));
                        if (dt.Rows.Count > 0)
                        {

                            txtPhone.Text = dt.Rows[0]["PhoneNo"].ToString();

                            txtFName.Text = dt.Rows[0]["FirstName"].ToString();
                            txtLName.Text = dt.Rows[0]["LastName"].ToString();
                            txtPhoneNo.Text = dt.Rows[0]["PhoneNo"].ToString();
                            txtDOB.Text = dt.Rows[0]["DOB"].ToString();
                            txtEmail.Text = dt.Rows[0]["Email"].ToString();
                            txtAddress.Text = dt.Rows[0]["Address"].ToString();
                            txtComments.Text = dt.Rows[0]["Comments"].ToString();

                            txtPhoneNo.ReadOnly = true;

                            txtViciLeadId.Text = "";
                            txtViciLeadId.Text = dt.Rows[0]["ViciLeadId"].ToString();
                            txtViciLeadId.ReadOnly = true;

                            ddlIsFlooded.ClearSelection();
                            ddlIsFlooded.SelectedIndex = ddlIsFlooded.Items.IndexOf(ddlIsFlooded.Items.FindByText(dt.Rows[0]["IsFlooded"].ToString()));
                            ddlIsFlooded.DataBind();

                            ddlIsOpenClaim.ClearSelection();
                            ddlIsOpenClaim.SelectedIndex = ddlIsOpenClaim.Items.IndexOf(ddlIsOpenClaim.Items.FindByText(dt.Rows[0]["IsOpenClaim"].ToString()));
                            ddlIsOpenClaim.DataBind();

                            ddlGender.ClearSelection();
                            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
                            ddlGender.DataBind();

                            ddlDisposition.ClearSelection();
                            ddlDisposition.SelectedIndex = ddlDisposition.Items.IndexOf(ddlDisposition.Items.FindByText(dt.Rows[0]["Disposition"].ToString()));
                            ddlDisposition.DataBind();

                            ddlAgencyProducer.SelectedIndex = ddlAgencyProducer.Items.IndexOf(ddlAgencyProducer.Items.FindByText(dt.Rows[0]["AgencyProducer"].ToString()));
                            ddlAgencyProducer.DataBind();

                            txtLeadId.Text = "";
                            txtLeadId.Text = Request.QueryString["LeadId"];
                            txtLeadId.ReadOnly = true;

                            Panel1.Visible = true;
                        }
                        else
                        {
                            //
                        }
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }
            }
            else
            {
                //
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["LeadId"] != null)
                {
                    var cmd = new SqlCommand("Update PingNPost SET FirstName=@FirstName, LastName=@LastName, Gender=@Gender, PhoneNo=@PhoneNo, DOB=@DOB, Email=@Email, Address=@Address, Comments=@Comments, IsFlooded=@IsFlooded, IsOpenClaim=@IsOpenClaim, FollowUpXDate=@FollowUpXDate, DispositionId=@DispositionId, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate, AgencyProducerId=@AgencyProducerId WHERE Id=@Id AND ViciLeadId=@ViciLeadId");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@FirstName", txtFName.Text.Trim());
                    cmd.Parameters.AddWithValue("@LastName", txtLName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@PhoneNo", txtPhoneNo.Text.Trim());
                    cmd.Parameters.AddWithValue("@DOB", txtDOB.Text.Trim());
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                    cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                    cmd.Parameters.AddWithValue("@IsFlooded", ddlIsFlooded.SelectedValue);
                    cmd.Parameters.AddWithValue("@IsOpenClaim", ddlIsOpenClaim.SelectedValue);
                    cmd.Parameters.AddWithValue("@FollowUpXDate", !string.IsNullOrWhiteSpace(txtFollowDate.Text) ? txtFollowDate.Text : "");
                    cmd.Parameters.AddWithValue("@DispositionId", Convert.ToInt32(ddlDisposition.SelectedValue));
                    cmd.Parameters.AddWithValue("@ViciLeadId", Convert.ToInt32(txtViciLeadId.Text));
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@AgencyProducerId", Convert.ToInt32(ddlAgencyProducer.SelectedValue));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Request.QueryString["LeadId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Lead Updated Successfully.');", true);
                            Clear();
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Lead Cannot be Update.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void PopulateProducers()
        {
            try
            {
                var query = "";
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    var d = new PopulateDataSource();
                    var t = d.DataTableSqlString("SELECT ao.Id FROM dbo.AgencyProducer ap JOIN dbo.AgencyOwner ao ON ap.AgencyOwnerId = ao.Id WHERE ap.Id=" + Convert.ToInt32(Session["UId"].ToString()));
                    if (t.Rows.Count > 0)
                    {
                        query = "SELECT Id,Name FROM dbo.AgencyProducer WHERE IsActive =1 AND AgencyOwnerId=" + Convert.ToInt32(t.Rows[0]["Id"].ToString());
                    }
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    query = "SELECT Id,Name FROM dbo.AgencyProducer WHERE IsActive =1 AND AgencyOwnerId=" + Convert.ToInt32(Session["UId"]);
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    ddlAgencyProducer.DataSource = dt;
                    ddlAgencyProducer.DataTextField = "Name";
                    ddlAgencyProducer.DataValueField = "Id";
                    ddlAgencyProducer.DataBind();
                }
                else
                {

                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        private void PopulateDdlDisposition()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id IN(5,11,12,14,15,22,25) ORDER BY Name"); // SELECT Id,Name FROM dbo.Disposition WHERE IsActive=1 AND Id !=23
                if (dt.Rows.Count > 0)
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();
                }
                else
                {
                    ddlDisposition.DataSource = dt;
                    ddlDisposition.DataTextField = "Name";
                    ddlDisposition.DataValueField = "Id";
                    ddlDisposition.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        public void Clear()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtPhoneNo.Text = "";
            txtDOB.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtComments.Text = "";

            ddlDisposition.ClearSelection();

            txtLeadId.Text = "";
            txtViciLeadId.Text = "";

            ddlIsFlooded.ClearSelection();
            ddlIsOpenClaim.ClearSelection();
        }

        protected void ddlDisposition_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDisposition.SelectedItem.Text == "Follow-Up/X-Date")
                pnlFollow.Visible = true;
        }
    }
}