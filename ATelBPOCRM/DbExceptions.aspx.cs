﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lucky.General;

namespace ATelBPOCRM
{
    public partial class DbExceptions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null)
                {
                    PopulateExceptions("", "");
                }
                else
                {
                    //
                }
            }
            else
            {
                //
            }
        }

        private void PopulateExceptions(string from, string to)
        {
            try
            {
                var query = "";
                if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT * FROM dbo.ExceptionLog e WHERE CONVERT(VARCHAR(15),e.ExceptionTime,101) >= '" + from + "' AND CONVERT(VARCHAR(15),e.ExceptionTime,101) <= '" + to + "' ORDER BY e.Id DESC";
                }

                if (string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT * FROM dbo.ExceptionLog e WHERE CONVERT(VARCHAR(15),e.ExceptionTime,101) = CONVERT(VARCHAR(15), GETDATE(), 101) ORDER BY e.Id DESC";
                }

                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnException_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtFrom.Text.Trim()) && !string.IsNullOrEmpty(txtTo.Text.Trim()))
                {
                    PopulateExceptions(txtFrom.Text.Trim(), txtTo.Text.Trim());
                }

                if (string.IsNullOrEmpty(txtFrom.Text.Trim()) && string.IsNullOrEmpty(txtTo.Text.Trim()))
                {
                    PopulateExceptions("", "");
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}