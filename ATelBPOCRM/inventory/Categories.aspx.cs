﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM.inventory
{
    public partial class Categories : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateCategories();
            }
        }

        public void PopulateCategories()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT c.Id,c.Name,c.Description,c.IsActive FROM Categories c");
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {

            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Categories WHERE Name='" + txtName.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Category Already Exists');", true);
                }
                else
                {
                    var cmd = new SqlCommand();
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.CommandText = "INSERT INTO dbo.Categories(Name, IsActive, Description, CreatedBy, CreatedDate)VALUES(@Name, @IsActive, @Description, @CreatedBy, @CreatedDate)";
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                    cmd.Parameters.AddWithValue("@IsActive", 1);
                    cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Category Saved Successfully.');", true);
                        PopulateCategories();
                        txtName.Text = "";
                        txtCategoryId.Text = "";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Category Cannot be Saved.');", true);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    if (hfId != null) txtCategoryId.Text = hfId.Value;
                    var lblName = (Literal)GridView1.Rows[rowIndex].FindControl("ltName");
                    var lblDescription = (Literal)GridView1.Rows[rowIndex].FindControl("ltDescription");

                    txtName.Text = lblName.Text;
                    txtDescription.Text = lblDescription.Text;
                }

                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var active = GridView1.Rows[rowIndex].FindControl("chkActive") as CheckBox;
                    if (hfId != null)
                    {
                        var query = "";
                        var message = "";
                        if (active != null && active.Checked == true)
                        {
                            query = "UPDATE dbo.Categories SET LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate, IsActive=0 WHERE Id=@Id";
                            message = "Blocked";
                        }
                        else
                        {
                            query = "UPDATE dbo.Categories SET LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate, IsActive=1 WHERE Id=@Id";
                            message = "Active";
                        }

                        var cmd = new SqlCommand(query, Connection.Db());
                        cmd.Parameters.Clear();
                        if (Session["UId"] != null)
                        {
                            cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(hfId.Value));
                            cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>Category " + message + " Successfully');", true);
                                PopulateCategories();
                            }
                            else
                            {
                                //
                            }
                        }
                        else
                        {
                            //
                        }
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCategoryId.Text.Trim()))
                {
                    var cmd = new SqlCommand();
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.CommandText = "Update dbo.Categories SET Name=@Name, IsActive=@IsActive, Description=@Description, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                    cmd.Parameters.AddWithValue("@IsActive", 1);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtCategoryId.Text));

                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Category Updated Successfully.');", true);
                        PopulateCategories();
                        txtName.Text = "";
                        txtCategoryId.Text = "";

                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Category Cannot be Updated.');", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Update Error.');", true);
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}