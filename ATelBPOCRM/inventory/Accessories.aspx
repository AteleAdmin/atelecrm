﻿<%@ Page Title="Accessories | Inventory" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Accessories.aspx.cs" Inherits="ATelBPOCRM.inventory.Accessories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
    <%--<section class="content-header">
        <h1>Accessories</h1>
    </section>--%>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Accessories List</h3>
                       <div class="box-tools pull-right">
                            <a href="javascript:;" class="btn btn-primary addDevice"><i class="fa fa-plus-circle"></i>&nbsp;Add Accessories</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateField HeaderText="Serial #">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <a class="update-item" data-request="<%# Eval("Id") %>" href="javascript:;">
                                            <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Brand">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltBrand" runat="server" Text='<%# Eval("Brand") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Model">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Supplier">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltSupplier" runat="server" Text='<%# Eval("Supplier") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Warranty">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltWarranty" runat="server" Text='<%# Eval("Warranty") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Purchase Date">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltPurchaseDate" runat="server" Text='<%# Eval("PurchaseDate") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkActive" Enabled="False" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("IsActive"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $('.add-item').on('click',
                function () {
                    if (id !== '') {
                        $.fancybox.open({
                            src: 'AddAccessories.aspx',
                            type: 'iframe'
                        });
                    }
                });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('.update-item').on('click',
                function () {
                    var id = $(this).data('request');
                    if (id !== '') {
                        $.fancybox.open({
                            src: 'AddAccessories.aspx?itemId=' + id,
                            type: 'iframe'
                        });
                    }
                });
        });
    </script>

    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 100%;
            margin: 0;
        }
    </style>
</asp:Content>
