﻿<%@ Page Title="Devices | Inventory" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Devices.aspx.cs" Inherits="ATelBPOCRM.inventory.Devices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Content Header (Page header) -->
   <%-- <section class="content-header">
        <h1>Devices</h1>
    </section>--%>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Devices List</h3>
                        <div class="box-tools pull-right">
                            <a href="javascript:;" class="btn btn-primary add-device"><i class="fa fa-plus-circle"></i>&nbsp;Add Device</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Serial #">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                            <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <a class="update-device" data-request="<%# Eval("Id") %>" href="javascript:;">
                                                <asp:Literal ID="ltName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Brand">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltBrand" runat="server" Text='<%# Eval("Brand") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Model">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltModel" runat="server" Text='<%# Eval("Model") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Purchase Date">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPurchaseDate" runat="server" Text='<%# Eval("PurchaseDate") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Warranty">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltWarranty" runat="server" Text='<%# Eval("Warranty") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Category">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCategory" runat="server" Text='<%# Eval("Category") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Supplier">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltSupplier" runat="server" Text='<%# Eval("Supplier") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkActive" Enabled="False" runat="server" class="" Checked='<%# Convert.ToBoolean(Eval("IsActive"))%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript">
        $(function () {
            $('.add-device').on('click',
                function () {
                    if (id !== '') {
                        $.fancybox.open({
                            src: 'AddDevice.aspx',
                            type: 'iframe'
                        });
                    }
                });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('.update-device').on('click',
                function () {
                    var id = $(this).data('request');
                    if (id !== '') {
                        $.fancybox.open({
                            src: 'AddDevice.aspx?deviceId=' + id,
                            type: 'iframe'
                        });
                    }
                });
        });
    </script>

    <style type="text/css">
        .fancybox-slide--iframe .fancybox-content {
            max-height: 100%;
            margin: 0;
        }
    </style>
</asp:Content>
