﻿using Lucky.General;
using Lucky.Security;
using System;
using System.IO;
using System.Web.UI;


namespace ATelBPOCRM.inventory
{
    public partial class Devices : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateDevices();
            }
        }
        public void PopulateDevices()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT d.Id,d.Name,d.Description,d.SerialNo,d.Model,d.Warranty,CONVERT(VARCHAR(20), d.PurchaseDate,101) AS PurchaseDate,b.Name AS Brand,s.Name AS Supplier,c.Name AS Category,d.IsActive,CONVERT(VARCHAR(20),d.CreatedDate,101) AS CreatedDate FROM Devices d JOIN Brands b ON d.BrandId=b.Id JOIN Suppliers s ON d.SupplierId= s.Id JOIN Categories c ON d.CategoryId=c.Id");
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }
    }
}