﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM.inventory
{
    public partial class Suppliers : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateSuppliers();
            }
        }

        public void PopulateSuppliers()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT s.Id,s.Name,s.Address,s.Phone,s.Email,s.IsActive FROM Suppliers s");
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.EmptyDataText = "No Record Found!";
                    GridView1.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {

            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Suppliers WHERE Name='" + txtName.Text.Trim() + "'") > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Supplier Already Exists');", true);
                }
                else
                {
                    var cmd = new SqlCommand();
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.CommandText = "INSERT INTO dbo.Suppliers(Name, Address, Phone, Email, IsActive, CreatedBy, CreatedDate)VALUES(@Name, @Address, @Phone, @Email, @IsActive,, @CreatedBy, @CreatedDate)";
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    cmd.Parameters.AddWithValue("@Phone", txtPhone.Text);
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@IsActive", 1);
                    cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"]));
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Supplier Saved Successfully.');", true);
                        PopulateSuppliers();
                        txtName.Text = "";
                        txtSupplierId.Text = "";
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Supplier Cannot be Saved.');", true);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnEdit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    if (hfId != null) txtSupplierId.Text = hfId.Value;
                    var lblName = (Literal)GridView1.Rows[rowIndex].FindControl("ltName");
                    var lblAddress = (Literal)GridView1.Rows[rowIndex].FindControl("ltAddress");
                    var lblPhone = (Literal)GridView1.Rows[rowIndex].FindControl("ltPhone");
                    var lblEmail = (Literal)GridView1.Rows[rowIndex].FindControl("ltEmail");

                    txtName.Text = lblName.Text;
                    txtAddress.Text = lblAddress.Text;
                    txtEmail.Text = lblPhone.Text;
                    txtPhone.Text = lblEmail.Text;
                }

                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var btn = sender as LinkButton;
                if (btn != null)
                {
                    var rowIndex = Convert.ToInt32(btn.Attributes["RowIndex"]);
                    var hfId = GridView1.Rows[rowIndex].FindControl("hfId") as HiddenField;
                    var active = GridView1.Rows[rowIndex].FindControl("chkActive") as CheckBox;
                    if (hfId != null)
                    {
                        var query = "";
                        var message = "";
                        if (active != null && active.Checked == true)
                        {
                            query = "UPDATE dbo.Suppliers SET LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate, IsActive=0 WHERE Id=@Id";
                            message = "Blocked";
                        }
                        else
                        {
                            query = "UPDATE dbo.Suppliers SET LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate, IsActive=1 WHERE Id=@Id";
                            message = "Active";
                        }

                        var cmd = new SqlCommand(query, Connection.Db());
                        cmd.Parameters.Clear();
                        if (Session["UId"] != null)
                        {
                            cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(hfId.Value));
                            cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                            cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "infoMsg('<h4>Info</h4>Supplier " + message + " Successfully');", true);
                                PopulateSuppliers();
                            }
                            else
                            {
                                //
                            }
                        }
                        else
                        {
                            //
                        }
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtSupplierId.Text.Trim()))
                {
                    var cmd = new SqlCommand();
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.CommandText = "Update dbo.Suppliers SET Name=@Name, IsActive=@IsActive, Address=@Address, Email=@Email, Phone=@Phone, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id";
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@Phone", txtPhone.Text);
                    cmd.Parameters.AddWithValue("@IsActive", 1);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"]));
                    cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtSupplierId.Text));

                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Supplier Updated Successfully.');", true);
                        PopulateSuppliers();
                        txtName.Text = "";
                        txtSupplierId.Text = "";

                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Supplier Cannot be Updated.');", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Update Error.');", true);
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
    }
}