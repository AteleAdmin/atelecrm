﻿using Lucky.General;
using Lucky.Security;
using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM.inventory
{
    public partial class Computers : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateServers("Laptops", GvLaptops);
            }
        }
        public void PopulateServers(string assetType, GridView gv)
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT s.Id,s.Name,s.Description,s.AssetType,s.Model,s.Processor,s.RAMMemory,s.HDMemory,s.OS,s.SerialNo,CONVERT(VARCHAR(20),s.PurchaseDate,101) AS PurchaseDate,s.Warranty,s.IsActive,b.Name AS Brand,s.Name AS Supplier FROM Servers s JOIN Brands b ON s.BrandId=b.Id JOIN Suppliers s1 ON s.SupplierId=s1.Id WHERE s.AssetType='" + assetType + "'");
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();
                }
                else
                {
                    gv.EmptyDataText = "No Record Found!";
                    gv.DataBind();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnVLaptops_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
            PopulateServers("Laptops", GvLaptops);
        }

        protected void btnVPcs_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
            PopulateServers("PC", GvPCs);
        }

        protected void btnVServer_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
            PopulateServers("Server", GvServers);
        }
    }
}