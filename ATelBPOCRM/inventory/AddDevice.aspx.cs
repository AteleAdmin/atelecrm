﻿using Lucky.General;
using System;
using System.Data.SqlClient;
using System.Web.UI;

namespace ATelBPOCRM.inventory
{
    public partial class AddDevice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCategory();
                BindBrand();
                BindSupplier();

                if (Request.QueryString["deviceId"] != null)
                {
                    var ds = new PopulateDataSource();
                    var dt = ds.DataTableSqlString("SELECT d.Id,d.Name,d.Description,d.SerialNo,d.Model,d.Warranty,CONVERT(NVARCHAR(18),d.PurchaseDate, 101) AS PurchaseDate,d.IsActive,c.Name AS Category,b.Name AS Brand,s.Name AS Supplier FROM Devices d JOIN Categories c ON d.CategoryId=c.Id JOIN Brands b ON d.BrandId=b.Id JOIN Suppliers s ON d.SupplierId=s.Id WHERE d.Id=" + Convert.ToInt32(Request.QueryString["deviceId"]));
                    if (dt.Rows.Count > 0)
                    {
                        txtName.Text = dt.Rows[0]["Name"].ToString();
                        txtDescription.Text = dt.Rows[0]["Description"].ToString();
                        txtModel.Text = dt.Rows[0]["Model"].ToString();
                        txtSerialNo.Text = dt.Rows[0]["SerialNo"].ToString();
                        txtPurchaseDate.Text = dt.Rows[0]["PurchaseDate"].ToString();

                        ddlCategory.ClearSelection();
                        ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByText(dt.Rows[0]["Category"].ToString()));
                        ddlCategory.DataBind();

                        ddlBrand.ClearSelection();
                        ddlBrand.SelectedIndex = ddlBrand.Items.IndexOf(ddlBrand.Items.FindByText(dt.Rows[0]["Brand"].ToString()));
                        ddlBrand.DataBind();

                        ddlSupplier.ClearSelection();
                        ddlSupplier.SelectedIndex = ddlSupplier.Items.IndexOf(ddlSupplier.Items.FindByText(dt.Rows[0]["Supplier"].ToString()));
                        ddlSupplier.DataBind();

                        ddlWarranty.ClearSelection();
                        ddlWarranty.SelectedIndex = ddlWarranty.Items.IndexOf(ddlWarranty.Items.FindByText(dt.Rows[0]["Warranty"].ToString()));
                        ddlWarranty.DataBind();

                        txtDeviceId.Text = Request.QueryString["deviceId"].ToString();

                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    //Response.Redirect("Login.aspx");
                }
            }
            else
            {
                //
            }
        }
        private void BindCategory()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id, Name FROM Categories WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlCategory.DataSource = dt;
                    ddlCategory.DataTextField = "Name";
                    ddlCategory.DataValueField = "Id";
                    ddlCategory.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
        private void BindBrand()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id, Name FROM Brands WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlBrand.DataSource = dt;
                    ddlBrand.DataTextField = "Name";
                    ddlBrand.DataValueField = "Id";
                    ddlBrand.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
        private void BindSupplier()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT Id, Name FROM Suppliers WHERE IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlSupplier.DataSource = dt;
                    ddlSupplier.DataTextField = "Name";
                    ddlSupplier.DataValueField = "Id";
                    ddlSupplier.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Utility.ChkDataTableDuplicate("SELECT Id FROM dbo.Devices WHERE Name='" + txtName.Text.Trim() + "'") > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Device Already Exists');", true);
                    return;
                }
                else
                {
                    var cmd = new SqlCommand("INSERT INTO dbo.Devices(Name, Description, SerialNo, Model, Warranty, PurchaseDate, BrandId, SupplierId, CategoryId, IsActive, CreatedBy, CreatedDate)VALUES(@Name, @Description, @SerialNo, @Model, @Warranty, @PurchaseDate, @BrandId, @SupplierId, @CategoryId, @IsActive, @CreatedBy, @CreatedDate)");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.ToLower().Trim());
                    cmd.Parameters.AddWithValue("@SerialNo", txtSerialNo.Text);
                    cmd.Parameters.AddWithValue("@Model", txtModel.Text);
                    cmd.Parameters.AddWithValue("@Warranty", ddlWarranty.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@PurchaseDate", txtPurchaseDate);
                    cmd.Parameters.AddWithValue("@BrandId", Convert.ToInt32(ddlBrand.SelectedValue));
                    cmd.Parameters.AddWithValue("@SupplierId", Convert.ToInt32(ddlSupplier.SelectedValue));
                    cmd.Parameters.AddWithValue("@CategoryId", Convert.ToInt32(ddlCategory.SelectedValue));
                    cmd.Parameters.AddWithValue("@IsActive", 1);
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Device Save Successfully.');", true);
                            Clear();
                            txtDeviceId.Text = "";
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Device Cannot be Saved.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Please login and try again.');", true);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }
        private void Clear()
        {
            txtName.Text = "";
            txtDescription.Text = "";
            txtModel.Text = "";
            txtSerialNo.Text = "";

            ddlBrand.SelectedIndex = 0;
            ddlCategory.SelectedIndex = 0;
            ddlSupplier.SelectedIndex = 0;
            ddlWarranty.SelectedIndex = 0;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["deviceId"] != null)
                {
                    var cmd = new SqlCommand("Update dbo.Devices SET Name=@Name, Description=@Description, SerialNo=@SerialNo, Model=@Model, Warranty=@Warranty, PurchaseDate=@PurchaseDate, BrandId=@BrandId, SupplierId=@SupplierId, CategoryId=@CategoryId, LastModifiedBy=@LastModifiedBy, LastModifiedDate=@LastModifiedDate WHERE Id=@Id");
                    cmd.Parameters.Clear();
                    cmd.Connection = Connection.Db();
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.ToLower().Trim());
                    cmd.Parameters.AddWithValue("@SerialNo", txtSerialNo.Text);
                    cmd.Parameters.AddWithValue("@Model", txtModel.Text);
                    cmd.Parameters.AddWithValue("@Warranty", ddlWarranty.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@PurchaseDate", txtPurchaseDate);
                    cmd.Parameters.AddWithValue("@BrandId", Convert.ToInt32(ddlBrand.SelectedValue));
                    cmd.Parameters.AddWithValue("@SupplierId", Convert.ToInt32(ddlSupplier.SelectedValue));
                    cmd.Parameters.AddWithValue("@CategoryId", Convert.ToInt32(ddlCategory.SelectedValue));
                    if (Session["UId"] != null)
                    {
                        cmd.Parameters.AddWithValue("@LastModifiedBy", Convert.ToInt32(Session["UId"].ToString()));
                        cmd.Parameters.AddWithValue("@LastModifiedDate", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(Request.QueryString["deviceId"]));
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('Device Updated Successfully.');", true);
                            Clear();
                            txtDeviceId.Text = "";
                            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Device Cannot be Update.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('Please login and try again.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}