﻿using System;

namespace ATelBPOCRM
{
    public partial class ClientPanel : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && (Session["Role"].ToString() == "Agency Owner" || Session["Role"].ToString() == "Agency Producer"))
                {
                    if (Session["Role"] != null && Session["Name"] != null)
                    {
                        ltUName.Text = Session["Name"].ToString();
                    }
                }
                //if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                //{
                //    if (Session["Role"] != null && Session["Name"] != null)
                //    {
                //        ltUName.Text = Session["Name"].ToString();
                //    }
                //}
                else
                {
                    Response.Redirect("ClientLogin.aspx", false);
                }
            }
        }
    }
}