﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Lucky.General;

namespace ATelBPOCRM
{
    /// <summary>
    /// Summary description for TodayAgentLeads
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TodayAgentLeads : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public void GetTodayAgentLeads()
        {
            var listEmployees = new List<AgentLeads>();

            var cmd = new SqlCommand("SELECT u.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(10), GETDATE(), 111) GROUP BY u.Name", Connection.Db());
            var rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                var agentLeads = new AgentLeads
                {
                    Name = rdr["Name"].ToString(),
                    Total = Convert.ToInt32(rdr["Total"])
                };
                listEmployees.Add(agentLeads);
            }

            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(listEmployees));
        }
    }
}
