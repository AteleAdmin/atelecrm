﻿using Lucky.General;
using Lucky.Security;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATelBPOCRM
{
    public partial class MoveLeads : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                {

                }
                else
                {
                    if (Session["RoleId"] != null)
                    {
                        var pageName = Path.GetFileNameWithoutExtension(Page.AppRelativeVirtualPath);
                        if (IsAuthorized.IsValid(Convert.ToInt32(Session["RoleId"]), pageName))
                        {
                            //
                        }
                        else
                            Response.Redirect("Dashboard.aspx?IsAuthorized=false&page=" + pageName);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Session Timeout');", true);
                    }
                }
            }
            else
            {
                //
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    PopulateLead(0, "", "", "");
                }
            }
            else
            {
                //
            }
        }

        protected void btnGetLeads_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    if (ddlFilter.SelectedValue == "Agency Owner")
                    {
                        PopulateLead(Convert.ToInt32(ddlFromAgency.SelectedValue), ddlFilter.SelectedValue, from, to);
                    }
                    else
                        PopulateLead(0, ddlFilter.SelectedValue, from, to);

                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulateLead(int? agencyOwner, string filter, string from, string to)
        {
            try
            {
                var query = "";
                if (filter == "Agency Owner" && agencyOwner > 0)
                {
                    if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                    {
                        query = "SELECT i.Id,(i.FirstName+' '+i.LastName) + ' | ' +i.PhoneNo +' | '+ao.Name AS Name,CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address FROM dbo.PingNPost i LEFT JOIN AgencyOwner ao ON i.AgencyOwnerId=ao.Id  WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwner + " ORDER BY i.Id ASC";
                    }
                    if (string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                    {
                        query = "SELECT i.Id,(i.FirstName+' '+i.LastName) + ' | ' +i.PhoneNo +' | '+ao.Name AS Name,CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address FROM dbo.PingNPost i LEFT JOIN AgencyOwner ao ON i.AgencyOwnerId=ao.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwner + " ORDER BY i.Id ASC";
                    }
                }

                if (filter == "Phone No")
                {
                    var phone = txtPhoneNo.Text.Trim();
                    if (phone.EndsWith(","))
                    {
                        phone = phone.TrimEnd(',');
                    }

                    var phoneNo = "";
                    var d = new PopulateDataSource();
                    var t = d.DataTableSqlString("SELECT Item FROM dbo.SplitString('" + phone + "', ',')");
                    if (t.Rows.Count > 0)
                    {
                        for (var i = 0; i < t.Rows.Count; i++)
                        {
                            phoneNo += "'" + t.Rows[i]["Item"].ToString() + "',";
                        }

                        phoneNo = phoneNo.TrimEnd(',');
                    }

                    if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                    {
                        query = "SELECT i.Id,(i.FirstName+' '+i.LastName) + ' | ' +i.PhoneNo +' | '+ao.Name AS Name,CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address FROM dbo.PingNPost i LEFT JOIN AgencyOwner ao ON i.AgencyOwnerId=ao.Id  WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.PhoneNo IN (" + phoneNo + ") ORDER BY i.Id ASC";
                    }
                    if (string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                    {
                        query = "SELECT i.Id,(i.FirstName+' '+i.LastName) + ' | ' +i.PhoneNo +' | '+ao.Name AS Name,CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address FROM dbo.PingNPost i LEFT JOIN AgencyOwner ao ON i.AgencyOwnerId=ao.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.PhoneNo IN (" + phoneNo + ") ORDER BY i.Id ASC";
                    }
                }
                if (filter == "0")
                {
                    if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                    {
                        query = "SELECT i.Id,(i.FirstName+' '+i.LastName) + ' | ' +i.PhoneNo +' | '+ao.Name AS Name,CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address FROM dbo.PingNPost i LEFT JOIN AgencyOwner ao ON i.AgencyOwnerId=ao.Id  WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' ORDER BY i.Id ASC";
                    }
                    if (string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                    {
                        query = "SELECT i.Id,(i.FirstName+' '+i.LastName) + ' | ' +i.PhoneNo +' | '+ao.Name AS Name,CONVERT(VARCHAR(20),DateAdd(hour,-5,i.CreatedDate),22) AS TimeStamp,i.Address FROM dbo.PingNPost i LEFT JOIN AgencyOwner ao ON i.AgencyOwnerId=ao.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,3) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) ORDER BY i.Id ASC";
                    }
                }
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString(query);
                if (dt.Rows.Count > 0)
                {
                    ddlField.DataSource = dt;
                    ddlField.DataTextField = "Name";
                    ddlField.DataValueField = "Id";
                    ddlField.DataBind();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var message = false;
                foreach (ListItem item in ddlField.Items)
                {
                    if (item.Selected)
                    {
                        if (ddlFilter.SelectedValue == "Agency Owner")
                        {
                            if (ddlToAgency.SelectedIndex > 0)
                            {
                                DateTime date = DateTime.Parse(txtMoveDate.Text.Trim());
                                var dte = date.ToString("yyyy-MM-dd");
                                var cmd = new SqlCommand("UPDATE dbo.PingNPost SET CreatedDate = STUFF(CONVERT(VARCHAR(50),CreatedDate,126) ,1, 10, '" + dte + "'), AgencyOwnerId=" + Convert.ToInt32(ddlToAgency.SelectedValue) + " WHERE Id=@Id", Connection.Db());
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(item.Value));
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    message = true;
                                }
                                else
                                {
                                    message = false;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>To Agency Owner is Not Selected.');", true);
                            }
                        }
                        else
                        {
                            DateTime date = DateTime.Parse(txtMoveDate.Text.Trim());
                            var dte = date.ToString("yyyy-MM-dd");
                            var cmd = new SqlCommand("UPDATE dbo.PingNPost SET CreatedDate = STUFF(CONVERT(VARCHAR(50),CreatedDate,126) ,1, 10, '" + dte + "') WHERE Id=@Id", Connection.Db());
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(item.Value));
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                message = true;
                            }
                            else
                            {
                                message = false;
                            }
                        }
                    }
                }
                if (message == true)
                {
                    ddlField.ClearSelection();
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Leads Moved Successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Leads Cannot be Moved.');", true);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }

        protected void ddlFilter_OnTextChanged(object sender, EventArgs e)
        {
            if (ddlFilter.SelectedValue == "Agency Owner")
            {
                PopulateAgencyOwner();

                pnlAgency.Visible = true;
                pnlPhone.Visible = false;
            }

            if (ddlFilter.SelectedValue == "Phone No")
            {
                pnlPhone.Visible = true;
                pnlAgency.Visible = false;
            }

        }

        private void PopulateAgencyOwner()
        {
            try
            {
                var ds = new PopulateDataSource();
                var dt = ds.DataTableSqlString("SELECT * FROM dbo.AgencyOwner ao WHERE ao.IsActive=1");
                if (dt.Rows.Count > 0)
                {
                    ddlFromAgency.DataSource = dt;
                    ddlFromAgency.DataTextField = "Name";
                    ddlFromAgency.DataValueField = "Id";
                    ddlFromAgency.DataBind();
                    ddlFromAgency.Items.Insert(0, "Select Agnecy");

                    ddlToAgency.DataSource = dt;
                    ddlToAgency.DataTextField = "Name";
                    ddlToAgency.DataValueField = "Id";
                    ddlToAgency.DataBind();
                    ddlToAgency.Items.Insert(0, "Select Agnecy");
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        protected void btnPNPMove_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtMFrom.Text) && !string.IsNullOrEmpty(txtMTo.Text))
                {
                    var IsValid = false;
                    var fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    var tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    var cmd = new SqlCommand("UPDATE dbo.PingNPost SET DispositionId=25 WHERE CONVERT(VARCHAR,CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR,CreatedDate,112) <='" + to + "' AND DispositionId=12", Connection.Db());
                    cmd.Parameters.Clear();
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        IsValid = true;
                    }

                    if (IsValid == false)
                    {
                        var command = new SqlCommand("UPDATE dbo.PingNPost SET DispositionId=25 WHERE CONVERT(VARCHAR,CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR,CreatedDate,112) <='" + to + "' AND DispositionId=11 AND Comments=''", Connection.Db());
                        command.Parameters.Clear();
                        if (command.ExecuteNonQuery() > 0)
                        {
                            IsValid = true;
                        }
                    }

                    if (IsValid == true)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "successMsg('<h4>Success</h4>Leads Moved Successfully.');", true);
                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Leads Cannot be Moved.');", true);
                    }
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDb(ex);
            }
        }
    }
}