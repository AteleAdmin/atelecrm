﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Web.Services;
using Lucky.General;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace ATelBPOCRM
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["IsAuthorized"] != null && Request.QueryString["IsAuthorized"].ToString() == "false" && Request.QueryString["page"] != null)
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>You are not authorized to <b>" + Request.QueryString["page"].ToString() + "</b>');", true);

                ///

                if (Session["UId"] != null)
                {
                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                    {
                        PopulateLiveLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                        PopulatePNPLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, "", "");
                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                    {
                        PopulateLiveLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                        PopulatePNPLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), "", "");
                    }
                    if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                    {
                        PopulateLiveLeadsCount(0, 0, "", "");
                        PopulatePNPLeadsCount(0, 0, "", "");

                    }

                    if (Session["Role"] != null && Session["Role"].ToString() == "Super Admin")
                    {
                        pnlAnalytic.Visible = true;
                        pnlAdmin.Visible = true;
                    }
                    else
                    {
                        //
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "hwa", "errorMsg('<h4>Error</h4>Please Login and try again later');", true);
                }

            }
            else
            {
                //
            }
        }

        [WebMethod]
        public void TodayAgentLeads()
        {
            var listEmployees = new List<AgentLeads>();

            var cmd = new SqlCommand("SELECT u.Name,COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id JOIN dbo.Users u ON i.CreatedBy = u.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(10), GETDATE(), 111) GROUP BY u.Name", Connection.Db());
            var rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                var agentLeads = new AgentLeads
                {
                    Name = rdr["Name"].ToString(),
                    Total = Convert.ToInt32(rdr["Total"])
                };
                listEmployees.Add(agentLeads);
            }

            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(listEmployees));
        }

        protected void btnStats_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Producer")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLiveLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                    PopulatePNPLeadsCount(Convert.ToInt32(Session["UId"].ToString()), 0, from, to);
                }
                if (Session["Role"] != null && Session["Role"].ToString() == "Agency Owner")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLiveLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                    PopulatePNPLeadsCount(0, Convert.ToInt32(Session["UId"].ToString()), from, to);
                }
                if (Session["Role"] != null && Session["Role"].ToString() != "Agency Producer" && Session["Role"].ToString() != "Agency Owner")
                {
                    DateTime fdate = DateTime.Parse(txtFrom.Text.Trim());
                    var from = fdate.ToString("yyyyMMdd");

                    DateTime tdate = DateTime.Parse(txtTo.Text.Trim());
                    var to = tdate.ToString("yyyyMMdd");

                    PopulateLiveLeadsCount(0, 0, from, to);
                    PopulatePNPLeadsCount(0, 0, from, to);

                    pnlAdmin.Visible = true;
                }
                else
                {
                    //
                }
            }
            catch (Exception exception)
            {
                ExceptionLogging.SendExcepToDb(exception);
            }
        }

        public void PopulateLiveLeadsCount(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltTotalLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltRejectedLead.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.Insurance i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltAcceptedLead.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }

        public void PopulatePNPLeadsCount(int? producerId, int? agencyOwnerId, string from, string to)
        {
            //Total
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPTotal.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Rejected
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId IN (5,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPRejected.Text = dr["Total"].ToString();
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
            //Accepted
            try
            {
                var query = "";
                if (producerId > 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId > 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyProducerId=" + producerId;
                }
                if (producerId == 0 && agencyOwnerId > 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.AgencyOwnerId=" + agencyOwnerId;
                }
                if (producerId == 0 && agencyOwnerId == 0 && !string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,112) >= '" + from + "' AND CONVERT(VARCHAR(12),i.CreatedDate,112) <= '" + to + "' AND i.IsOutsource=0";
                }
                if (producerId == 0 && agencyOwnerId == 0 && string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
                {
                    query = "SELECT COUNT(d.Name) AS Total FROM dbo.PingNPost i JOIN dbo.Disposition d ON i.DispositionId = d.Id WHERE i.IsActive=1 AND i.DispositionId NOT IN (1,5,3,8,10,11,12,13) AND CONVERT(VARCHAR(12),i.CreatedDate,101) = CONVERT(VARCHAR(12), GETDATE(), 101) AND i.IsOutsource=0";
                }
                var cmd = new SqlCommand { Connection = Connection.Db(), CommandText = query };
                var dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                        ltPNPAccepted.Text = dr["Total"].ToString();
                }
                else
                {
                    //
                }
            }
            catch (Exception e)
            {
                ExceptionLogging.SendExcepToDb(e);
            }
        }
    }
}