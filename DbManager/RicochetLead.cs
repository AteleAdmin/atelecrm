﻿

namespace DbManager
{
    public class RicochetLead
    {
        //public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNo { get; set; }
        public string Phone_Number { get; set; }
        public string Company { get; set; }
        public string Make { get; set; }
        public string Year { get; set; }
        public string Model { get; set; }
        public string Make2 { get; set; }
        public string Year2 { get; set; }
        public string Model2 { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Comments { get; set; }
        public bool HOwnerShip { get; set; }
        public string ContinousCoverage { get; set; }
        public bool MaritalStatus { get; set; }
        public string FollowUpXDate { get; set; }
        //public int AgencyOwnerId { get; set; }
        //public int AgencyProducerId { get; set; }
        //public int ViciLeadId { get; set; }
        public int DispositionId { get; set; }
        public bool IsActive { get; set; }
        //public int OutsourceOwnerId { get; set; }
        public bool IsOutsource { get; set; }
        public bool IsDemandPNP { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public int LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
    }
}
