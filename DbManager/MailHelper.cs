﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace DbManager
{
    public class MailHelper
    {
        public static bool SendEmail(string fromEmail, string fromName, string to, string cc, string bcc, string subject, string body, bool isBodyHtml)
        {
            if (string.IsNullOrEmpty(to))
                throw new Exception("Must provide to email address to send email.");
            var message = new MailMessage();
            var smtpClient = new SmtpClient();
            smtpClient.Host = ConfigurationManager.AppSettings["Host"].ToString();
            smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"].ToString());
            smtpClient.UseDefaultCredentials = false;
            smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSL"].ToString());
            smtpClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["FromEmail"].ToString(), ConfigurationManager.AppSettings["FromPassword"].ToString());
            if (!string.IsNullOrEmpty(fromEmail))
                message.From = new MailAddress(fromEmail, fromName);
            if (to.Contains(","))
            {
                var str = to;
                var chArray = new char[1] { ',' };
                foreach (var address in str.Split(chArray))
                {
                    if (!string.IsNullOrEmpty(address))
                        message.To.Add(new MailAddress(address));
                }
            }
            else if (!string.IsNullOrEmpty(to))
                message.To.Add(new MailAddress(to));
            if (cc.Contains(","))
            {
                var str = cc;
                var chArray = new char[1] { ',' };
                foreach (var address in str.Split(chArray))
                {
                    if (!string.IsNullOrEmpty(address))
                        message.CC.Add(new MailAddress(address));
                }
            }
            else if (!string.IsNullOrEmpty(cc))
                message.CC.Add(new MailAddress(cc));
            if (bcc.Contains(","))
            {
                var str = bcc;
                var chArray = new char[1] { ',' };
                foreach (var address in str.Split(chArray))
                {
                    if (!string.IsNullOrEmpty(address))
                        message.Bcc.Add(new MailAddress(address));
                }
            }
            else if (!string.IsNullOrEmpty(bcc))
                message.Bcc.Add(new MailAddress(bcc));
            if (!string.IsNullOrEmpty(subject))
                message.Subject = subject;
            if (!string.IsNullOrEmpty(body))
                message.Body = body;
            message.IsBodyHtml = isBodyHtml;
            try
            {
                smtpClient.Send(message);
            }
            catch (Exception)
            {

            }
            finally
            {
                smtpClient.Dispose();
            }
            return true;
        }
    }
}
