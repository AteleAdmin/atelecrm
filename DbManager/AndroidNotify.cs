﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace DbManager
{
    public class AndroidNotify
    {
        public static string PushNotification(string deviceToken, string title, string message)
        {
            var errorMessage = "";
            try
            {
                var applicationId = "AAAAyi2y-WY:APA91bEsow46bRgciC75OciLsu4rFntMy0p03yQ2VUnUz5EMd7s8kuu7mxC597VJuQ-HdRdcPIM4i2_8ZO2wMXU60BFdxsXLGywRJjpm0nZ00abn8yk1-WE66jdahg2HPnyzRLA9QnSR";
                var senderId = "868350097766";
                var token = deviceToken;

                var tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                //tRequest.UseDefaultCredentials = true;
                //tRequest.PreAuthenticate = true;
                //tRequest.Credentials = CredentialCache.DefaultCredentials;

                //String username = "aamirzafar07@gmail.com";
                //String password = "tehreena786";
                //String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                //tRequest.Headers.Add("Authorization", "Basic " + encoded);

                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = token,
                    notification = new
                    {
                        body = message,
                        title = title,
                        icon = ""
                    }
                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationId));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;

                using (var dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (var tResponse = tRequest.GetResponse())
                    {
                        using (var dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (var tReader = new StreamReader(dataStreamResponse))
                            {
                                var sResponseFromServer = tReader.ReadToEnd();
                                errorMessage = sResponseFromServer;
                                //Console.WriteLine(str);
                                //Console.ReadKey();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return errorMessage;
        }
    }
}
