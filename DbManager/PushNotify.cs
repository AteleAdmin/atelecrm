﻿using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System;

namespace DbManager
{
    public class PushNotify
    {
        public string SendPushNotification(string deviceToken, string message, string filePath)
        {
            var errorMessage = "";
            try
            {
                //Get Certificate
                var appleCert = System.IO.File.ReadAllBytes(filePath);
                //var appleCert = System.IO.File.ReadAllBytes(HttpContext.Current.Server.MapPath("~/Certificate/IOS/production_com.ml.leads.pem"));

                // Configuration (NOTE: .pfx can also be used here)
                var config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, appleCert, "");
                config.InternalBatchSize = 2000;
                // Create a new broker
                var apnsBroker = new ApnsServiceBroker(config);

                // Wire up events
                apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                {

                    aggregateEx.Handle(ex =>
                    {

                        // See what kind of exception it was to further diagnose
                        if (ex is ApnsNotificationException)
                        {
                            var notificationException = (ApnsNotificationException)ex;

                            // Deal with the failed notification
                            var apnsNotification = notificationException.Notification;
                            var statusCode = notificationException.ErrorStatusCode;
                            string desc = "Apple Notification Failed: ID=" + apnsNotification.Identifier + ", Code=" + statusCode;
                            Console.WriteLine(desc);
                            errorMessage = desc;
                        }
                        else
                        {
                            string desc = "Apple Notification Failed for some unknown reason : " + ex.InnerException;
                            // Inner exception might hold more useful information like an ApnsConnectionException
                            Console.WriteLine(desc);
                            errorMessage = desc;
                        }

                        // Mark it as handled
                        return true;
                    });
                };

                apnsBroker.OnNotificationSucceeded += (notification) =>
                {
                    errorMessage = "Apple Notification Sent successfully!";
                };

                var fbs = new FeedbackService(config);
                fbs.FeedbackReceived += (string devicToken, DateTime timestamp) =>
                {
                    // Remove the deviceToken from your database
                    // timestamp is the time the token was reported as expired
                };

                // Start Proccess
                apnsBroker.Start();

                if (deviceToken != "")
                {
                    apnsBroker.QueueNotification(new ApnsNotification
                    {
                        DeviceToken = deviceToken,
                        Payload = JObject.Parse(("{\"aps\":{\"badge\":1,\"sound\":\"oven.caf\",\"alert\":\"" + (message + "\"}}")))
                    });
                }

                apnsBroker.Stop();

            }
            catch (Exception)
            {

                throw;
            }

            return errorMessage;
        }
    }
}
